<?PHP

function get_request ( $key , $default = "" ) {
	global $prefilled_requests ;
	if ( isset ( $prefilled_requests[$key] ) ) return $prefilled_requests[$key] ;
	if ( isset ( $_REQUEST[$key] ) ) return str_replace ( "\'" , "'" , $_REQUEST[$key] ) ;
	return $default ;
}

function redir ( $path ) {
	$base = 'https://mix-n-match.toolforge.org/#' ;

	if ( 0 ) {
		$x = headers_sent() ;
		header('Content-type: text/plain; charset=UTF-8');
		print "$base$path" ;
		print ( $x ? " YES" : " NO" ) ;
	} else {
		if ( !headers_sent() ) {
			header('Location: '.$base.$path,true,301);
			exit() ;
		}
		header('Content-type: text/html; charset=UTF-8');
		print "<html><head><meta http-equiv='Location' content='$base$path'></head><script>window.location.replace('$base$path');</script></html>" ;
	}
	exit() ;
}

$catalog = get_request ( 'catalog' , '' ) ;
$mode = get_request ( 'mode' , '' ) ;
if ( $mode == '' ) redir ( '/' ) ;

if ( $mode == 'catalog_details' ) redir ( "/catalog/$catalog" ) ;
if ( $mode == 'sitestats' ) redir ( "/site_stats/$catalog" ) ;
if ( $mode == 'creation_candidates' ) redir ( "/creation_candidates" ) ;
if ( $mode == 'catalog' ) {
	if ( get_request('show_userq')==1 ) redir ( "/list/$catalog/manual" ) ;
	if ( get_request('show_autoq')==1 ) redir ( "/list/$catalog/auto" ) ;
	if ( get_request('show_noq')==1 ) redir ( "/list/$catalog/unmatched" ) ;
	if ( get_request('show_nowd')==1 ) redir ( "/list/$catalog/nowd" ) ;
	if ( get_request('show_na')==1 ) redir ( "/list/$catalog/na" ) ;
}

?>