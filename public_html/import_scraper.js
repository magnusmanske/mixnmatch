var Scraper = Vue.extend ( {
	props : [ "opt" ] ,
	data : function () { return { types:[] , type2count:{} , levels:[] , options:{simple_space:false,utf8_encode:false,skip_failed:true} , is_example:false , test_results:{status:''} , scraper:{url:'',rx_block:'',rx_entry:'',resolve:{
		id:{'use':'',rx:[]},
		name:{'use':'',rx:[]},
		desc:{'use':'',rx:[]},
		url:{'use':''},
		type:{'use':''},
	}} , meta : { catalog_id:'' , name:'' , desc:'' , property:'' , lang:'en' , type:'unknown' , url:'' } , note:{
		id:'',
		name:'For people (Q5), try to get "first_name last_name", maybe with a RegEx (below): ^(.+?), (.+)$ => $2 $1',
		desc:'For people (Q5), try to get birth/death dates into the description, it can help with the auto-matching',
		url:'E.g. https://www.thesource.com/entry/$1 if $1 is the entry ID',
		type:'Use a Q number to set a default type; optional (e.g. Q5=human; Q16521=taxon)',
	} , block_matches:0 , internal_results:[] , can_save_message:'' , catalog_from_save:0 } } ,
	created : function () {
		var me = this ;
		me.init('') ;
		$.each ( me.meta , function ( k , v ) {
			if ( typeof me.$route.query[k] == 'undefined' ) return ;
			me.meta[k] = me.$route.query[k] ;
		} ) ;
		if (me.meta['property']!='') me.onPropertyChanged();
	} ,
	updated : function () { tt.updateInterface(this.$el) ; } ,
	mounted : function () { tt.updateInterface(this.$el) ; } ,
	methods : {
		init : function ( o ) {
			this.updateTypes() ;
//			this.loadExample() ; // TESTING
		} ,
		ucFirst : function ( s ) {
			return s.charAt(0).toUpperCase() + s.slice(1).replace(/_/g,' ') ;
		} ,
		updateTypes : function () {
			var me = this ;
			me.types = [] ;
			me.type2count = {} ;
			$.each ( get_all_catalogs() , function ( k , v ) { if(typeof me.type2count[v.type]=='undefined')me.type2count[v.type]=1;else me.type2count[v.type]++; } ) ;
			$.each ( me.type2count , function ( k , v ) { me.types.push ( k ) } ) ;
			me.types.sort() ;
		} ,
		loadExample : function () {
			var me = this ;
			me.levels = [ { mode:'keys' , keys:"abcdefghijklmnopqrstuvwxyzõäöü".split('') } ] ;
			me.test_results = {status:''} ;
			me.scraper = {url:'https://digikogu.ekm.ee/ekm/authors?letter=$1',rx_block:'',rx_entry:'<a href=\"/ekm/authors/author_id-(\\d+)\">(.+?)</a>',resolve:{
				id:{'use':'$1',rx:[]},
				name:{'use':'$2',rx:[['^(.+?), (.+)$','$2 $1']]},
				desc:{'use':'',rx:[]},
				url:{'use':'https://digikogu.ekm.ee/ekm/authors/author_id-$1'},
				type:{'use':'Q5'},
			} } ;
			me.meta = { catalog_id:'' , name:'Art Museum of Estonia artist' , desc:'Artist identifier for the Art Museum of Estonia' , property:'P4563' , lang:'et' , url:'https://digikogu.ekm.ee' , type:'biography' } ;
			me.options = {simple_space:true,utf8_encode:false,skip_failed:false} ;
			me.is_example = true ;
			me.canSaveScraper();
		} ,
		saveScraper : function () {
			var me = this ;
			if ( !me.canSaveScraper() ) return ; // Paranoia
			var params = {
				query:'save_scraper',
				scraper:me.scraper,
				options:{},
				levels:me.levels,
				tusc_user:widar.getUserName(),
				meta:me.meta
			} ;
			$.each ( me.options , function ( k , v ) {
				if ( v ) params.options[k] = 1 ;
			} ) ;
			params.scraper = JSON.stringify ( params.scraper ) ;
			params.meta = JSON.stringify ( params.meta ) ;
			params.levels = JSON.stringify ( params.levels ) ;
			params.options = JSON.stringify ( params.options ) ;
			$('#save_scraper_button').hide() ;
			$.post ( api , params , function ( d ) {
				if ( d.status != 'OK' ) alert ( d.status ) ;
				else {
					me.catalog_from_save = d.data.catalog ;
				}
			} , 'json' )
			.fail ( function () {
				alert ( "Save failed" ) ;
			} ) . always ( function () {
				$('#save_scraper_button').show() ;
			} ) ;
		} ,
		canSaveScraper : function () {
			var me = this ;
			me.can_save_message = '' ;
			if ( me.is_example ) { me.can_save_message='This is an example, you cannot save it' ; return false ; }
			if ( me.test_results.status == '' ) { me.can_save_message='Run a test that returns results to save this scraper/catalog' ; return false ; }
			if ( me.test_results.status == '' ) return false ;
			if ( me.test_results.status == 'RUNNING' ) return false ;
			if ( me.test_results.status != 'OK' ) { me.can_save_message='ERROR: '+me.test_results.status ; return false ; }
			if ( me.test_results.results.length == 0 ) { me.can_save_message='Empty result set' ; return false ; }
			if ( me.meta.catalog_id == '' ) {
				if ( me.meta.name == '' || me.meta.desc == '' || me.meta.lang == '' ) { me.can_save_message='Catalog information missing' ; return false ; } // desc and property are optional
			}
			var username = widar.getUserName() ;
			if ( typeof username == 'undefined' ) { me.can_save_message='You are not logged in' ; return false ; } // Not logged in
			me.can_save_message = 'OK' ;
			return true ;
		} ,
		getURLestimate : function () {
			var me = this ;
			if ( me.levels.length == 0 ) return '' ;
			var product = 1 ;
			var is_uncertain = false ;
			$.each ( me.levels , function ( dummy , l ) {
				if ( l.mode == 'keys' ) product *= l.keys.length ;
				if ( l.mode == 'range' ) product *= Math.floor ( (l.end-l.start+1)/l.step ) ;
				if ( l.mode == 'follow' ) is_uncertain = true ;
			} ) ;
			var ret = "This will scrape " + product + " URLs." ;
			if ( is_uncertain ) ret += " Most likely a lot more, as there is no way to estimate the \"follow\" level type." ;
			return ret ;
		} ,
		doesCatalogWithPropertyExist : function () {
			var me = this ;
			var ret = false ;
			var prop = me.meta.property.replace(/\D/g,'') ;
			if ( prop == '' ) return ;
			$.each ( get_all_catalogs() , function ( k , v ) {
				if ( v.wd_prop == prop ) ret = true ;
			} ) ;
			return ret ;
		} ,
		onPropertyChanged : function () {
			var me = this ;
			var prop = me.meta.property.replace(/\D/g,'') ;
			if ( prop == '' ) return ;
			prop = "P" + prop ;
			$.getJSON ( 'https://www.wikidata.org/w/api.php?callback=?' , {
				action:'wbgetentities',
				ids:prop,
				format:'json'
			} , function ( d ) {
				var e = d.entities[prop] ;
				if ( typeof e == 'undefined' ) return ;
				if ( me.meta.name == '' && typeof e.labels.en != 'undefined' ) me.meta.name = e.labels.en.value.replace(/ ID$/,'') ;
				if ( me.meta.desc == '' && typeof e.descriptions.en != 'undefined' ) me.meta.desc = e.descriptions.en.value ;
				if ( typeof e.claims == 'undefined' ) return ;
				if ( me.meta.url == '' && typeof e.claims['P1896'] != 'undefined' ) me.meta.url = e.claims['P1896'][0].mainsnak.datavalue.value ;
				if ( me.scraper.resolve.url.use == '' && typeof e.claims['P1630'] != 'undefined' ) me.scraper.resolve.url.use = e.claims['P1630'][0].mainsnak.datavalue.value ;
			} ) ;
		} ,
		calculateMatches : function () {
			var me = this ;
			me.canSaveScraper();
			if ( typeof me.test_results == 'undefined' ) return ;
			if ( me.test_results.status != 'OK' ) return ;
			if ( typeof me.test_results.html == 'undefined' ) return ;

			me.block_matches = 0 ;
			me.internal_results = [] ;
			if ( typeof me.test_results.html == 'undefined' ) return ;
			
			var h = me.test_results.html ;
			if ( me.options.simple_space ) h = h.replace ( /\s+/gm , ' ' ) ;
			
			var res_block = [] ;
			if ( me.scraper.rx_block != '' ) {
				var r ;
				try {
					r = new RegExp ( me.scraper.rx_block , 'g' ) ;
				} catch(e) {
				}
				if ( typeof r != 'undefined' ) {
					while ((tmp = r.exec(h)) !== null) {
						if ( typeof tmp == 'undefined' ) continue ;
						if ( typeof tmp[1] == 'undefined' ) continue ;
						res_block.push ( tmp[1] ) ;
					}
					me.block_matches = res_block.length ;
				}
			} else res_block = [ h ] ;

			if ( me.scraper.rx_entry != '' ) {
				var r ;
				try {
					r = new RegExp ( me.scraper.rx_entry , 'g' ) ;
				} catch(e) {
				}
				if ( typeof r != 'undefined' ) {
					$.each ( res_block , function ( dummy , rh ) {
						while ((tmp = r.exec(rh)) !== null) {
							var o = [] ;
							for ( var a = 1 ; a <= 9 ; a++ ) {
								if ( typeof tmp[a] != 'undefined' ) o[a] = tmp[a] ;
							}
							me.internal_results.push ( o ) ;
						}
					} ) ;
				}
			}
			
			// TODO could do some display with me.internal_results

			me.canSaveScraper();
		} ,
		testScraper : function () {
			var me = this ;
			me.test_results = {status:'RUNNING'} ;
			me.canSaveScraper();
			$.post ( api , {
				query:'autoscrape_test',
				json:me.generateJSON(),
				rand:Math.random() // Bypass cache
			} , function ( d ) {
				me.test_results = d.data ;
				me.test_results.status = d.status ;
				me.calculateMatches() ;
			} , 'json' )
			.fail ( function () {
				me.test_results.status = 'Test has failed, for reasons unknown. Could be the server to be scraped is too slow?' ;
			} ) ;
		} ,
		addLevel : function ( mode ) {
			var me = this ;
			var o = { mode:mode } ;
			if ( mode == 'keys' ) {
				o.keys = [] ;
			} else if ( mode == 'range' ) {
				o.start = 0 ;
				o.end = 10 ;
				o.step = 1 ;
			} else if ( mode == 'follow' ) {
				o.rx = '' ;
				o.url = '' ;
			}
			me.levels.push ( o ) ;
		} ,
		deleteLevel : function ( level_id ) {
			var me = this ;
			me.levels.splice ( level_id , 1 ) ;
		} ,
		moveLevelUp : function ( level_id ) {
			this.swapLevels ( level_id , level_id-1 ) ;
		} ,
		moveLevelDown : function ( level_id ) {
			this.swapLevels ( level_id , level_id+1 ) ;
		} ,
		swapLevels : function ( l1 , l2 ) {
			var me = this ;
			var dummy = me.levels[l1] ;
			Vue.set ( me.levels , l1 , me.levels[l2] ) ;
			Vue.set ( me.levels , l2 , dummy ) ;
		} ,
		addRegex : function ( event ) {
			var me = this ;
			var t = $(event.target) ;
			var rid = t.attr('rid') ;
			me.scraper.resolve[rid].rx.push ( ['',''] ) ;
		} ,
		removeRegex : function ( event ) {
			var me = this ;
			var t = $(event.target) ;
			var rid = t.attr('rid') ;
			var rxid = t.attr('rxid') ;
			me.scraper.resolve[rid].rx.splice ( rxid , 1 ) ;
		} ,
		keysChanged : function ( event ) {
			var me = this ;
			var t = $(event.target) ;
			var level_id = t.attr('level') ;
			var text = t.val() ;
			me.levels[level_id].keys = $.trim(text).split("\n") ;
		} ,
		setUcAZ : function ( event ) {
			var me = this ;
			var t = $($(event.target).parents('div.col').find('textarea')) ;
			var level_id = t.attr('level') ;
			me.levels[level_id].keys = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split('') ;
		} ,
		setLcAZ : function ( event ) {
			var me = this ;
			var t = $($(event.target).parents('div.col').find('textarea')) ;
			var level_id = t.attr('level') ;
			me.levels[level_id].keys = "abcdefghijklmnopqrstuvwxyz".split('') ;
		} ,
		generateJSON : function () {
			var me = this ;
			var j = {
				levels:me.levels,
				options:me.options ,
				scraper:me.scraper
			} ;
			return JSON.stringify ( j , null , 2 ) ;
		}
	} ,
	watch: {
		'$route' (to, from) { this.init ( to.params.opt ) } ,
		scraper: { deep:true , handler:function () { this.calculateMatches() } } ,
		options : { deep:true , handler:function () { this.calculateMatches() } } ,
	} ,
	template: '#scraper-template'
} ) ;
