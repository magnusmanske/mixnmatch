<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
include_once ( "../php/common.php" ) ;

srand();
$j = json_decode ( file_get_contents ( $wdq_internal_url.'?q='.urlencode('claim[31:(tree[570600][][279])] and noclaim[1216]') ) ) ;
$j = $j->items ;
$total = count ( $j ) ;
shuffle ( $j ) ;
$j = array_slice ( $j , 0 , 200 ) ;

print get_common_header ( '' , 'Wiki Loves Monuments - UK' ) ;

$db = openDB ( 'wikidata' , 'wikidata' ) ;

$q2title = array() ;
$title2q = array () ;
$sql = "select * from wb_items_per_site where ips_site_id='enwiki' and ips_item_id IN (" . implode(',',$j)  . ") AND ips_site_page NOT LIKE '%list%'" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$q2title['Q'.$o->ips_item_id] = get_db_safe ( $o->ips_site_page ) ;
	$title2q[ucfirst($o->ips_site_page)] = 'Q'.$o->ips_item_id ;
}

//print "<pre>" ; print_r ( $q2title ) ; print "</pre>" ;

$title2url = array() ;
$db2 = openDB ( 'en' , 'wikipedia' ) ;
$sql = "select * from page,externallinks WHERE page_id=el_from AND page_namespace=0 AND page_title IN ('".join("','",$q2title)."') AND el_index LIKE 'http://uk.org.english-heritage.list./resultsingle.aspx?uid=_%'" ;
if(!$result = $db2->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$title2url[str_replace('_',' ',$o->page_title)][] = $o->el_to ;
}

print "<p>
This tool can help you add English hetitage ID numbers to Wikidata items for buildings.
It suggests ID numbers based on URLs in the Wikipedia article.
Always check if it's the correct ID by clicking on the ID number (usually, it's the first one, but...) before adding it to Wikidata!
Here are a random " . count($title2url) . " out of $total Wikidata items that are a graded UK building but do not have a heritage ID number (the total list will update with ~15min lag).<br/>
<i>Note:</i> To use the \"add\" function, you first need to <a href='http://tools.wmflabs.org/widar/index.php?action=authorize' target='_blank'>authorise WiDaR</a> to make edits on your behalf on Wikidata.
</p><hr/>" ;

print "<table class='table-condensed table-striped'>" ;
print "<thead><tr><th>Item</th><th>Article</th><th>Possible IDs</th></tr></thead><tbody>" ;
foreach ( $title2url AS $title => $urls ) {
	$q = $title2q[$title] ;
	print "<tr>" ;
	print "<td style='vertical-align:top'><a href='//www.wikidata.org/wiki/$q' target='_blank'>$q</th></td>" ;
	print "<td style='vertical-align:top'><a href='//en.wikipedia.org/wiki/".urlencode(str_replace(' ','_',$title))."' target='_blank'>$title</th></td>" ;
	print "<td style='vertical-align:top'>" ;
	foreach ( $urls AS $url ) {
		$m = array() ;
		if ( false === preg_match ( '/uid=(\d+)/' , $url , $m ) ) continue ;
		$id = $m[1] ;
		print "<div><a href='http://list.english-heritage.org.uk/resultsingle.aspx?uid=$id' target='_blank'>$id</a>" ;
		print "&nbsp;|&nbsp;<span><a href='#' onclick='addID(this,\"$q\",\"$id\");return false' title='Add this ID to the Wikidata item'>add</a></span></div>" ;
	}
	print "</td>" ;
	print "</tr>" ;
}
print "</tbody></table>" ;

print "<hr/><p><a href='?'>Load new random set</a> | <a href='http://www.english-heritage.org.uk/professional/protection/process/national-heritage-list-for-england/' target='_blank'>England National Heritage search</a></p>" ;

//print "<pre>" ; print_r ( $title2url ) ; print "</pre>" ;



print '<script>

function addID ( o , q , s ) {
	q = (q+"").replace(/\D/g,"") ;
	$.getJSON ( "/widar/index.php?callback=?" , {
		action:"set_string",
		id:"Q"+q,
		prop:"P1216",
		text:s,
		botmode:1
	} , function ( d ) {
		if ( d.error == "OK" ) {
			$($(o).parents("span").get(0)).html("<b>Added!</b>") ;
		} else alert ( d.error ) ;
	} ) .fail(function( jqxhr, textStatus, error ) {
		alert ( error ) ;
	} ) ;
}
</script>' ;


print get_common_footer() ;

?>