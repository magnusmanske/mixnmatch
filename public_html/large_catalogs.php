<?PHP

ini_set('memory_limit','2500M');
set_time_limit ( 60 * 2 ) ; // Seconds
error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

require_once ( '../manual_lists/large_catalogs/shared.php' ) ;

$types_label = [
	'MISMATCH' => 'Value mismatch' ,
	'EXT_MISMATCH' => 'External mismatch' ,
	'REMOVED_BEFORE' => 'Property was removed before' ,
	'DATE_MISMATCH' => 'Date mismatch' ,
	'OTHER_I' => 'Other item already has data'
] ;

function getTypeLabel ( $type ) {
	global $types_label ;
	if ( isset($types_label[$type]) ) return $types_label[$type] ;
	return $type ; // Fallback
}

function showReport () {
	global $lc ;
	$cat = $lc->getCat() ;
	print "<h2>Reports on <a href='?action=report_list&catalog={$cat->id}'>{$cat->name}</a> dataset</h2>" ;
	
	$matrix = [] ;
	$section = [] ;
	$row = [] ;
	$col = [] ;
	$sql = "SELECT prop,status,`type`,count(*) AS cnt FROM report WHERE catalog_id={$cat->id} GROUP BY prop,status,`type`" ;
	if(!$result = $lc->db->query($sql)) die('There was an error running the query [' . $lc->db->error . ']'."\n$sql\n");
	while($o = $result->fetch_object()) {
		$p = 'P'.$o->prop ;
		$matrix[$p][$o->type][$o->status] = $o->cnt ;
		$matrix[$p][$o->type]['total'] += $o->cnt ;
		$section[$o->prop] = $p ;
		$col[$o->status] = $o->status ;
		$row[$o->type] = $o->type ;
	}

	
	
	$lc->wil->loadItems ( $section ) ;
	
//	print "<pre>" ; print_r ( $open ) ; print "</pre>" ;
	
	foreach ( $section AS $s ) {
		$i = $lc->wil->getItem ( $s ) ;
		print "<h3><a class='external' href='https://www.wikidata.org/wiki/Property:$s' target='_blank'>{$i->getLabel()}</a> <small>[$s]</small></h3>" ;
		print "<table class='table table-condensed table-striped' style='width:auto'>" ;
		print "<thead><tr><th>Type</th>" ;
		foreach ( $col AS $c ) print "<th>" . ucfirst(strtolower($c)) . "</th>" ;
		print "<th>Total</th></tr></thead><tbody>" ;
		foreach ( $row AS $r ) {
			if ( !isset($matrix[$s][$r]['total']) ) continue ;
			print "<tr><th>" . getTypeLabel($r) . "</th>" ;
			foreach ( $col AS $c ) {
				print "<td>" ;
				if ( isset($matrix[$s][$r][$c]) ) {
					print "<a href='?catalog={$cat->id}&action=report_list&status={$c}&type={$r}&prop={$s}'>" . $matrix[$s][$r][$c] . "</a>" ;
				}
				print "</td>" ;
			}
			print "<td>" ;
			print "<a href='?catalog={$cat->id}&action=report_list&type={$r}&prop={$s}'>" . $matrix[$s][$r]['total'] . "</a>" ;
			print "</td>" ;
			print "</tr>" ;
		}
		print "</tbody></table>" ;
	}
	
}

	

function prettyTime ( $ts ) {
	return substr($ts,0,4).'-'.substr($ts,4,2).'-'.substr($ts,6,2).'<br/>'.substr($ts,8,2).':'.substr($ts,10,2).':'.substr($ts,12,2) ;
}

function renderEntryTable ( $rows , $params ) {
	global $lc ;
	foreach ( ['prop','status','type','user'] AS $p ) {
		if ( !isset($params[$p]) ) $params[$p] = '' ;
	}
//print "<pre>" ; print_r ( $params ) ; print "</pre>" ;
	print "<table class='table table-condensed table-striped'>" ;
	print "<thead><tr><th>#</th><th>Item</th>" ;
	if ( $params['prop'] == '' ) print "<th>Property</th>" ;
	print "<th>Message</th>" ;
	if ( $params['status'] == '' ) print "<th>Status</th>" ;
	if ( $params['type'] == '' ) print "<th>Type</th>" ;
	if ( $params['user'] == '' ) print "<th>User</th>" ;
	print "<th>Time</th><th>Action</th></tr></thead><tbody>" ;
	foreach ( $rows AS $cnt => $o ) {
		print "<tr>" ;
		print "<td>" . ($params['offset']+$cnt+1) . "</td>" ;
		print "<td><a class='external' href='https://www.wikidata.org/wiki/Q{$o->q}' target='_blank'>Q" . $o->q . "</a></th>" ;
		if ( $params['prop'] == '' ) print "<td><a class='external' href='https://www.wikidata.org/wiki/Property:P{$o->prop}' target='_blank'>P" . $o->prop . "</a></th>" ;
		
		print "<td><small>" ;
		if ( $o->html != '' ) {
			print $o->html ;
		} else {
			$html = $lc->wiki2html ( $o->message , $o->q ) ;
			print $html ;
		}
		print "</small></td>" ;
		
		if ( $params['status'] == '' ) {
			print "<td>" ;
			if ( $o->status == 'REOPENED' ) print "<span style='color:red'>$o->status</span>" ;
			else print $o->status ;
			print "</td>" ;
		}
		if ( $params['type'] == '' ) print "<td>" . getTypeLabel($o->type) . "</td>" ;
		if ( $params['user'] == '' ) {
			print "<td nowrap>" ;
			if ( $o->user != '' ) print "<small><a class='external' href='https://www.wikidata.org/wiki/User:" . myurlencode($o->user) . "' target='_blank'>{$o->user}</a></small>" ;
			print "</td>" ;
		}
		print "<td><small>" . prettyTime($o->timestamp) . "</small></td>" ;
		print "<td>" ;
		if ( $o->status != 'DONE' ) print "<button class='btn btn-dark done_button' entry_id='{$o->id}'>Done</button>" ;
		else print "<button class='btn btn-danger open_button' entry_id='{$o->id}'>Re-open</button>" ;
		print "</td>" ;
		print "</tr>" ;
	}

	print "</tbody></table>" ;
}

function showRC() {
	global $lc ;
	$limit = get_request ( 'limit' , '50' ) * 1 ;
	$offset = get_request ( 'offset' , '0' ) * 1 ;
	$users = get_request ( 'users' , 0 ) ;
	$sql = "SELECT * FROM report " ;
	if ( $users ) $sql .= "WHERE user!='' " ;
	$sql .= "ORDER BY `timestamp` DESC LIMIT $limit OFFSET $offset" ;
	if(!$result = $lc->db->query($sql)) die('There was an error running the query [' . $lc->db->error . ']'."\n$sql\n");

	$rows = [] ;
	while($o = $result->fetch_object()) $rows[] = $o ;
#	$params = [ 'prop'=>$prop , 'status'=>$status , 'type'=>$type , 'user'=>$user , 'offset'=>$offset ] ;
	$params = [ 'offset'=>$offset ] ;
	renderEntryTable ( $rows , $params ) ;

}

function showReportList () {
	global $lc ;
	$cat = $lc->getCat() ;
	$status = get_request ( 'status' , '' ) ;
	$type = get_request ( 'type' , '' ) ;
	$user = get_request ( 'user' , '' ) ;
	$prop = preg_replace ( '/\D/' , '' , get_request ( 'prop' , '' ) ) ;
	$limit = get_request ( 'limit' , '20' ) * 1 ;
	$offset = get_request ( 'offset' , '0' ) * 1 ;
	
	print "<h2>Report list on <a href='?action=report&catalog={$cat->id}'>{$cat->name}</a> dataset</h2>" ;
	
	if ( $prop != '' ) {
		$lc->wil->loadItem ( "P{$prop}" ) ;
		$i = $lc->wil->getItem("P{$prop}") ;
		print "<h4>Property: <a class='external' href='https://www.wikidata.org/wiki/Property:P{$prop}' target='_blank'>" . $i->getLabel() . "</a> <small>[P{$prop}]</small></h4>" ;
	}
	if ( $status != '' ) print "<h4>Status: {$status}</h4>" ;
	if ( $type != '' ) print "<h4>Type: " . getTypeLabel($type) . "</h4>" ;
	if ( $user != '' ) print "<h4>User: {$user}</h4>" ;
	
	
	$sql = "SELECT * FROM report WHERE catalog_id={$cat->id}" ;
	if ( $status != '' ) $sql .= " AND `status`='" . $lc->db->real_escape_string($status) . "'" ;
	if ( $type != '' ) $sql .= " AND `type`='" . $lc->db->real_escape_string($type) . "'" ;
	if ( $user != '' ) $sql .= " AND `user`='" . $lc->db->real_escape_string($user) . "'" ;
	if ( $prop != '' ) $sql .= " AND `prop`=" . $lc->db->real_escape_string($prop) ;
	$sql .= " LIMIT $limit OFFSET $offset" ;
//	print "<pre>$sql</pre>" ;
	if(!$result = $lc->db->query($sql)) die('There was an error running the query [' . $lc->db->error . ']'."\n$sql\n");

	$rows = [] ;
	while($o = $result->fetch_object()) $rows[] = $o ;
	$params = [ 'prop'=>$prop , 'status'=>$status , 'type'=>$type , 'user'=>$user , 'offset'=>$offset ] ;
	renderEntryTable ( $rows , $params ) ;

	print "<p>" ;
	if ( $offset > 0 ) {
		print "<a href='?action=report_list&catalog={$cat->id}&status=$status&type=$type&prop=$prop&limit=$limit&offset=".($offset-$limit)."&user=$user'>Prev $limit</a>" ;
	}
	if ( count($rows) == 0 ) {
		print "</p><p>No results here!</p>" ;
	} else if ( count($rows) == $limit ) {
		if ( $offset > 0 ) print " | " ;
		print "<a href='?action=report_list&catalog={$cat->id}&status=$status&type=$type&prop=$prop&limit=$limit&offset=".($offset+$limit)."&user=$user'>Next $limit</a></p>" ;
	}
	
}

function api_set_status () {
	global $lc , $out ;
	$status = trim ( get_request ( 'status' , '' ) ) ;
	if ( $status == '' ) { $out['status']='ERROR'; $out['msg']='Empty status' ; return ; }
	$id = get_request ( 'id' , '0' ) * 1 ;
	if ( $id == 0 ) { $out['status']='ERROR'; $out['msg']='Empty ID' ; return ; }
	$user = trim ( get_request ( 'user' , '' ) ) ;
	if ( $user == '' ) { $out['status']='ERROR'; $out['msg']='Not logged in' ; return ; }
	
	$ts = $lc->getTimestamp() ;
	$sql = "UPDATE report SET status='" . $lc->db->real_escape_string($status) . "',`timestamp`='$ts',user='" . $lc->db->real_escape_string($user) . "' WHERE id={$id}" ;
	$out['sql'] = $sql ;
	if(!$result = $lc->db->query($sql)) { $out['status']='ERROR'; $out['msg']='There was an error running the query [' . $lc->db->error . ']'."\n$sql\n"; }
}


$action = get_request ( 'action' , '' ) ;
$catalog_id = get_request ( 'catalog' , 0 ) ;
$lc = new largeCatalog ( $catalog_id ) ;

if ( $action == 'api' ) {
	header('Content-type: application/json; charset=UTF-8');
	$out = [ 'status' => 'OK' ] ;
	$api_action = get_request ( 'api_action' , '' ) ;
	if ( $api_action == 'set_status' ) api_set_status () ;
	else {
		$out['status'] = 'ERROR' ;
		$out['msg'] = "Unknown API action: $api_action" ;
	}
	print json_encode ( $out ) ;
	exit ( 0 ) ;
}


print get_common_header ( '' , 'Large catalogs' ) ;
?>

<style>
a.external {
	color:rgb(40, 167, 69);
}
</style>

<script src="//tools-static.wmflabs.org/magnustools/resources/js/widar.js"></script>
<script>
var widar;
$(document).ready(function(){
	$('a[rel="mw:WikiLink"]').addClass('external') ;
	widar = new WiDaR ( function () {
		if ( !widar.isLoggedIn() ) { // No action buttons for non-widar
			$('button.done_button').parent().html('<a href="/widar/" target="_blank">Log in</a>') ;
			return ;
		}
		$('button.done_button').click ( function () {
			if ( !widar.isLoggedIn() ) return ; // Paranoia
			var o = $(this) ;
			var entry_id = o.attr('entry_id') ;
			$.get ( './large_catalogs.php' , {
				action:'api',
				api_action:'set_status',
				status:'DONE',
				id:entry_id,
				user:widar.getUserName()
			} , function ( d ) {
				console.log ( d ) ;
				o.parent().html ( '<span>DONE</span>' ) ;
			} , 'json' ) ;
		} ) ;
		$('button.open_button').click ( function () {
			if ( !widar.isLoggedIn() ) return ; // Paranoia
			var o = $(this) ;
			var entry_id = o.attr('entry_id') ;
			$.get ( './large_catalogs.php' , {
				action:'api',
				api_action:'set_status',
				status:'REOPENED',
				id:entry_id,
				user:widar.getUserName()
			} , function ( d ) {
				console.log ( d ) ;
				o.parent().html ( '<span>RE-OPENED</span>' ) ;
			} , 'json' ) ;
		} ) ;
	} ) ;
});
</script>

<?PHP

if ( $action == 'rc' ) {
	showRC() ;
	exit ( 0 ) ;
}

if ( $catalog_id == 0 ) {
	$open = [] ;
	$sql = "SELECT catalog_id,count(*) AS cnt FROM report WHERE status!='DONE' GROUP BY catalog_id" ;	
	if(!$result = $lc->db->query($sql)) die('There was an error running the query [' . $lc->db->error . ']'."\n$sql\n");
	while($o = $result->fetch_object()) $open[$o->catalog_id] = $o->cnt ;

	print "<p>Recent Changes: <a href='?action=rc'>all</a>, <a href='?action=rc&users=1'>users only</a></p>" ;
	print "<h2>Available catalogs</h2>" ;
	print "<ul>" ;
	$cats = $lc->getCatalogs() ;
	foreach ( $cats AS $c ) {
#		print "<li><a href='?catalog={$c->id}'>{$c->name}</a> (<a href='?catalog={$c->id}&action=report'>reports</a>)</li>" ;
		print "<li><b>{$c->name}</b> (<a href='?catalog={$c->id}&action=report'>reports</a>)" ;
		if ( isset($open[$c->id]) ) print ", " . number_format($open[$c->id],0) . " unresolved issues" ;
		print "</li>" ;
	}
	print "</ul>" ;
	print get_common_footer() ;
	exit ( 0 ) ;
}

if ( $action == 'report' ) showReport() ;
else if ( $action == 'report_list' ) showReportList() ;
else {
	print "Unknown action '{$action}'" ;
}

print get_common_footer() ;

?>