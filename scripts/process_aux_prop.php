#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
#ini_set('display_errors', 'On');

require_once ( "/data/project/mix-n-match/manual_lists/large_catalogs/shared.php" ) ;
#require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;
require_once dirname(__DIR__) . '/vendor/autoload.php';

class AuxPropThing {
	public $mnm ;
	public $prop ;
	public $lc;

	function __construct ( $prop , $mnm = '' ) {
		$this->prop = $prop ;
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch\MixNMatch ;
		$this->mnm->tfc->getQS('mixnmatch:ProcessAuxProps','/data/project/mix-n-match/bot.ini',true) ;
		$this->lc = new largeCatalog (2) ; # VIAF
		$this->lc->prop2field[2]['P213'] = 'ISNI' ; // Activating for this specific purpose
		$this->lc->prop2field[2]['P1006'] = 'NTA' ; // Activating for this specific purpose
	}

	function refresh_table() {
		$table = "tmp_p{$this->prop}";
		$this->mnm->getSQL ( "TRUNCATE {$table}" ) ;

		$cond = '' ;
		if ( $this->prop==214 ) $cond = " AND aux_name RLIKE '^\\\\d{4,}$'" ;

		$sql = "INSERT IGNORE INTO {$table} (entry_id,prop_value,q,catalog) 
			SELECT entry_id,aux_name,IF(q IS NULL OR q<=0 OR user IS NULL,null,q),catalog
			FROM auxiliary,entry
			WHERE entry_id=entry.id AND aux_p={$this->prop} {$cond}";
		$this->mnm->getSQL($sql);

		$sql = "INSERT IGNORE INTO {$table} (entry_id,prop_value,q,catalog) 
			SELECT entry.id,ext_id,IF(q IS NULL OR q<=0 OR user IS NULL,null,q),catalog
			FROM entry 
			WHERE catalog IN (SELECT id FROM catalog WHERE wd_prop={$this->prop} AND active=1) {$cond}";
		$this->mnm->getSQL($sql);

		$this->mnm->getSQL("DELETE FROM {$table} WHERE catalog IN (SELECT id FROM catalog WHERE active=0)");
	}

	function match_and_remove($entry_ids,$q) {
		$table = "tmp_p{$this->prop}";
		foreach ($entry_ids as $entry_id) {
			$this->mnm->setMatchForEntryID ( $entry_id , $q , 4 , true , false ) ;
		}
		$entry_ids_str = implode(",",$entry_ids);
		$this->mnm->getSQL ( "DELETE FROM {$table} WHERE entry_id IN ({$entry_ids_str})" ) ;
	}

	# Use only entries from active catalogs
	# NOT USED
	function filter_entry_ids(&$entry_ids) {
		$entry_ids_str = implode(",",$entry_ids);
		$sql = "SELECT entry.id AS entry_id FROM entry,catalog WHERE entry.id IN ({$entry_ids_str}) AND entry.catalog=catalog.id AND catalog.active=1" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		$entry_ids = [] ;
		while ( $o = $result->fetch_object() ) $entry_ids[] = $o->entry_id;
	}

	function create_new_items($min) {
		$table = "tmp_p{$this->prop}";
		$sql = "SELECT prop_value,group_concat(entry_id) AS entry_ids,group_concat(DISTINCT IFNULL(q,'NULL')) AS qs,count(DISTINCT entry_id) AS cnt FROM {$table} GROUP BY prop_value HAVING cnt>={$min} AND qs='NULL'";
		$result = $this->mnm->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			$entry_ids = explode(',',$o->entry_ids);
			if ( count($entry_ids)<3 ) { # Hard cutoff
				print "Not enough entries left from {$o->entry_ids}\n" ;
				continue;
			}
			
			$items = $this->mnm->getSearchResults("haswbstatement:\"P{$this->prop}={$o->prop_value}\"");
			if ( count($items)>0 ) {
				if ( count($items)>1 ) continue ; // TODO Already multiple WD items with this prop
				$q = $items[0]->title ;
				print "Matching {$o->entry_ids} to item https://www.wikidata.org/wiki/{$q}\n";
				$this->match_and_remove($entry_ids,$q);
				continue;
			}

			$commands = $this->mnm->getCreateItemCommandsForEntries($entry_ids,$this->lc);
			if ( !isset($commands) ) {
				print "Failed to generate commands for {$o->entry_ids}\n" ;
				continue;
			}
			$q = $this->mnm->tfc->runCommandsQS ( $commands ) ;
			if ( isset($q) ) {
				print "Created new item https://www.wikidata.org/wiki/{$q}\n" ;
				$this->match_and_remove($entry_ids,$q);
			} else {
				print "Failed to create new item for {$o->entry_ids}\n" ;
			}
		}
	}

	function matchg_to_existing() {
		$table = "tmp_p{$this->prop}";
		$sql = "SELECT prop_value,group_concat(entry_id) AS entry_ids,group_concat(DISTINCT IFNULL(q,'NULL')) AS qs,count(distinct IFNULL(q,'NULL')) AS cnt FROM {$table} GROUP BY prop_value HAVING cnt=2 AND qs LIKE '%NULL%' AND qs rlike '\\\\d'";
		$result = $this->mnm->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			$entry_ids = explode(',',$o->entry_ids);
			$qs = explode(",",$o->qs);
			if ( count($qs)!=2 ) continue; # Paranoia
			if ( $qs[0]!="NULL" ) $q = $qs[0]*1;
			else if ( $qs[1]!="NULL" ) $q = $qs[1]*1;
			else continue; # Paranoia
			print "{$o->entry_ids} => Q{$q}\n" ;
			$this->match_and_remove($entry_ids,$q);
		}

	}

}

$aps = new AuxPropThing(214);
#$aps->refresh_table();
$aps->create_new_items(4);
#$aps->matchg_to_existing();

/*
toolforge-jobs run --image tf-php74 --mem 512Mi --command '/data/project/mix-n-match/scripts/process_aux_prop.php' process-aux-prop
*/

?>