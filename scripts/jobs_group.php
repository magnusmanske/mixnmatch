#!/usr/bin/php
<?PHP

ini_set('memory_limit','250M');

require_once dirname(__DIR__) . '/vendor/autoload.php';

# Setting this up to fail ;-)
function fail_current_job () {
	global $jobs ;
	if ( $jobs->get_running_job_id() == '' ) return ;
	$jobs->mnm->dbmConnect(true);
	$job_id_running = $jobs->get_running_job_id() ;
	$jobs->mnm->set_job_status($job_id_running,'FAILED');
	$job_id_running = '' ;
}

register_shutdown_function('fail_current_job');
pcntl_signal(SIGTERM, "fail_current_job");
pcntl_signal(SIGHUP,  "fail_current_job");
pcntl_signal(SIGUSR1, "fail_current_job");

# Initialize
$jobs = new MixNMatch\Jobs ;
$job_type_groups = array_keys($jobs->job_types) ;
if ( !isset($argv[1]) or !in_array($argv[1], $job_type_groups) ) die("Usage: {$argv[0]} [".implode('|',$job_type_groups)."] [JOB_ID(optional)]") ;
$jobs->valid_job_types = $jobs->job_types[$argv[1]] ;

# Run specific job ID
if ( isset($argv[2]) ) {
	$job = $jobs->get_job_by_id($argv[2]);
	$jobs->mnm->set_job_status($job,'RUNNING');
	$jobs->run_job_inline($job);
	$jobs->mnm->set_job_status($job,'DONE');
	$jobs->mnm->show_proc_info();
	exit(0);
}

# Backgound job
print "Starting job group {$argv[1]}\n" ;
$jobs->reset_running_jobs() ; # Clear old jobs left running
while ( true ) $jobs->run_next_inline_job();

?>