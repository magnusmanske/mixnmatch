#!/usr/bin/php
<?PHP

# THIS SCRIPT CREATES "FULL" NEW MATCHES TO WIKIDATA ITEMS, BASED ON (FUZZY) NAME, BIRTH&DEATH YEAR MATCH

#USAGE:
# match_person_entries_by_name_and_dates.php // Random 50K
# match_person_entries_by_name_and_dates.php 1[,2,3,...] // Specific catalog(s)

require_once dirname(__DIR__) . '/vendor/autoload.php';
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);


if ( isset($argv[1]) ) {
	$catalog = $argv[1]*1 ;
	if ( $catalog <= 0 ) die ( "Bad catalog: {$argv[1]}\n" ) ;
}

$verbose = preg_match ( '/\bverbose\b/' , $argv[2]??'') ;

$pm = new MixNMatch\PersonMatcher ( false , true , $verbose ) ;
foreach ( $pm->candidateEntriesGenerator($catalog) AS $o ) $pm->process_entry ( $o ) ;
if ( isset($catalog) ) $pm->mnm->queue_job($catalog,'microsync');

?>