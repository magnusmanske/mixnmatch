#!/usr/bin/php
<?PHP

ini_set('default_socket_timeout', 900); // 900 Seconds = 15 Minutes
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR); // 
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

class CerseiSync {
	public $mnm ;

	public function __construct ( /*int*/ $mnm = '' ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch\MixNMatch ;
	}

	protected function get_cersei_scrapers() {
		return json_decode(file_get_contents('https://cersei.toolforge.org/api/scrapers'))->scrapers;
	}

	protected function get_current_scrapers() {
		$ret = [];
		$sql = "SELECT * FROM `cersei`";
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$o->cersei_scraper_id *= 1;
			$o->catalog_id *= 1;
			$ret[$o->cersei_scraper_id] = $o ;
		}
		return $ret;
	}

	protected function update_empty_ext_urls($s,$catalog_id) {
		if ( !isset($s->url_pattern) or $s->url_pattern=='' ) return;
		$url_safe = $this->mnm->escape($s->url_pattern);
		$sql = "UPDATE `entry` SET `ext_url`=replace('{$url_safe}','$1',ext_id) WHERE `catalog`={$catalog_id} AND `ext_url`=''" ;
		$this->mnm->getSQL ( $sql ) ;
	}

	public function create_new_catalogs() {
		$scrapers_current = $this->get_current_scrapers();
		$scrapers_cersei = $this->get_cersei_scrapers();
		foreach ( $scrapers_cersei AS $s ) {
			if ( $s->status!='active' ) continue;
			if ( isset($scrapers_current[$s->id]) ) { # We have that scraper
				$this->update_empty_ext_urls($s,$scrapers_current[$s->id]->catalog_id);
				continue;
			}
			$meta = (object) [
				'name' => $s->name,
				'url' => $s->url,
				'property' => ($s->property ?? ''),
				'lang' => $s->language,
				'note' => "CERSEI scraper #{$s->id}"
			];

			# Check if catalog with that name exists
			$sql = "SELECT * FROM `catalog` WHERE `name`='".$this->mnm->escape($meta->name)."'";
			$result = $this->mnm->getSQL ( $sql ) ;
			if($o = $result->fetch_object()) $meta->name .= " [CERSEI]";

			# Create new catalog
			$nc = new MixNMatch\Catalog(0,$this->mnm);
			$catalog_id = $nc->createNew($meta,6);
			if ( !isset($catalog_id) ) throw new \Exception("Failed to create a new catalog for scraper {$s->id}");

			# Link cersei and catalog
			$sql = "INSERT IGNORE INTO `cersei` (`cersei_scraper_id`,`catalog_id`) VALUES ({$s->id},{$catalog_id})" ;
			$this->mnm->getSQL ( $sql ) ;
		}
	}

	protected function parse_time($time_precision) {
		# NOTE: this ignores all BCE dates
		if ( !isset($time_precision) ) return;
		$parts = explode('/',$time_precision);
		if ( $parts[1]=='9' ) return preg_replace('|^\+(\d+).*$|','$1',$parts[0]);
		else if ( $parts[1]=='10' ) return preg_replace('|^\+(\d+-\d{1,2}).*$|','$1',$parts[0]);
		else if ( $parts[1]=='11' ) return preg_replace('|^\+(\d+-\d{1,2}-\d{1,2}).*$|','$1',$parts[0]);
		else return ; # Fallback
	}

	protected function set_human_dates_flag($catalog_id) {
		$sql = "SELECT * FROM vw_dates WHERE catalog={$catalog_id} LIMIT 1";
		$result = $this->mnm->getSQL ( $sql ) ;
		if($o = $result->fetch_object()) {
			$sql = "UPDATE `catalog` SET `has_person_date`='yes' WHERE `id`={$catalog_id} AND `has_person_date`!='yes'" ;
			$this->mnm->getSQL ( $sql ) ;
			return true;
		}
		return false;
	}

	public function sync_scraper($scraper_id,$catalog_id,$last_sync) {
		$catalog = new MixNMatch\Catalog($catalog_id,$this->mnm);
		$existing_ext_ids = $catalog->get_all_external_ids();
		$start_time = $this->mnm->getFutureTimestamp(-60); # 1min before now, to catch race conditions
		$j = (object) [ "offset" =>0 , "scraper_id" => $scraper_id , "no_json" => 1, "wide" => 1 , "limit" => 500 ];
		if ( ($last_sync??'')!='' ) $j->revision_since = $last_sync;
		$added_new_entries = false;
		while ( 1 ) {
			$url = 'https://cersei.toolforge.org/api/get_entries/' . urlencode(json_encode($j));
			for ( $i = 0 ; $i <= 5 ; $i++ ) {
				$result = json_decode(file_get_contents($url));
				if ( !isset($result) or $result==null ) {
					sleep(5*($i+1));
					if ( str_contains($url, '?') ) $url .= "&";
					else $url .= "?";
					$url .= "blah".rand()."=".rand(); # Bypass caching
					$this->mnm->dbmConnect(false);
				} else break;
			}
			if ( !isset($result) or $result==null ) throw new \Exception ("CERSAI fail on {$url}");
			$this->mnm->dbmConnect(true);
			foreach ( $result->entries AS $ce ) {
				$ne = (object) [
						'catalog' => $catalog_id,
						'id' => $ce->source_id,
						'name' => $ce->label??'',
						'desc' => $ce->description,
						'url' => $ce->url??'',
						'q' => $ce->q,
						'born' => $this->parse_time($ce->p569),
						'died' => $this->parse_time($ce->p570),
				];
				if ( $ne->name=='' ) $ne->name = $ce->original_label;
				$ne->name = substr($ne->name,0,127);
				$ne->desc = substr($ne->desc,0,255);
				if ( $ne->name=='' or $ne->id=='' ) {
					// print "Bad new entry: ".json_encode($ne)."\n";
					continue;
				}
				if ( isset($ce->p31) and $ce->p31!='' ) $ne->type = "Q{$ce->p31}";


				if ( isset($existing_ext_ids[$ne->id]) ) {
					// print "Updating {$ne->id}: {$ne->name}\n" ;
					$entry_id = $existing_ext_ids[$ne->id];
					$name_safe = $this->mnm->escape($ne->name);
					$desc_safe = $this->mnm->escape($ne->desc??'');
					$url_safe = $this->mnm->escape($ne->url??'');
					$type_safe = $this->mnm->escape($ne->type??'');
					$sql = "UPDATE `entry` SET `ext_name`='{$name_safe}',`ext_desc`='$desc_safe',`type`='{$type_safe}',`ext_url`='{$url_safe}'" ;
					$sql .= " WHERE `id`={$entry_id}" ;
					$sql .= " AND (`ext_name`!='{$name_safe}' OR `ext_desc`!='$desc_safe' OR `type`!='{$type_safe}' OR `ext_url`!='{$url_safe}')" ;
					try {
						$this->mnm->getSQL ( $sql ) ;
					} catch (Exception $e) {
						print "ERROR UPDATE {$d->cersei_scraper_id}: ".$e->getMessage()."\n" ;
						continue;
					}

					$born = $ne->born ?? '' ;
					$died = $ne->died ?? '' ;
					if ( $born.$died != '' ) $this->mnm->setPersonDates ( $entry_id , $born , $died ) ;

				} else {
					// print "Creating ".json_encode($ne)."\n" ;
					try {
						$entry_id = $this->mnm->addNewEntry($ne);
					} catch (Exception $e) {
						print "ERROR UPDATE {$d->cersei_scraper_id}: ".$e->getMessage()."\n" ;
						continue;
					}
					$existing_ext_ids[$ne->id] = $entry_id;
					$added_new_entries = true;
				}
			}
			if ( count($result->entries) < $j->limit ) break ; # This is the end, my friend, the end
			$j->offset += $j->limit ;
		}

		$this->sync_internal_relations($scraper_id,$last_sync);
		
		$sql = "UPDATE `cersei` SET `last_sync`='{$start_time}' WHERE `cersei_scraper_id`={$scraper_id}";
		$this->mnm->getSQL ( $sql ) ;
		$this->mnm->updateCatalogs ( [$catalog_id] ) ;
		if ( $this->set_human_dates_flag($catalog_id) ) { # Has person dates
			$this->mnm->queue_job ( $catalog_id , 'match_person_dates' ) ;
			$this->mnm->queue_job ( $catalog_id , 'match_on_birthdate' ) ;
		}

		if ( $added_new_entries ) {
			# Start automatch jobs
			$this->mnm->queue_job ( $catalog_id , 'automatch_by_sitelink' ) ;
			$this->mnm->queue_job ( $catalog_id , 'automatch_from_other_catalogs' ) ;
			$this->mnm->queue_job ( $catalog_id , 'automatch_by_search' ) ;
		}
	}

	public function sync_all_scrapers() {
		$scrapers = $this->get_current_scrapers();
		foreach ( $scrapers AS $s ) {
			// print "BEGIN SCRAPER CATALOG {$s->catalog_id}: {$s->name}\n";
			try {
				$this->sync_scraper($s->cersei_scraper_id,$s->catalog_id,$s->last_sync);
			} catch (Exception $e) {
				print "ERROR SCRAPER {$d->cersei_scraper_id}: ".$e->getMessage()."\n" ;
			}
			// print "END SCRAPER CATALOG {$s->catalog_id}: {$s->name}\n";
		}
	}

	public function sync_internal_relations($scraper_id, $earliest='') {
		$batch_size = 5000;
		$scrapers = $this->get_current_scrapers();
		$scraper2catalog = [];
		foreach ( $scrapers AS $s ) $scraper2catalog[$s->cersei_scraper_id] = $s->catalog_id;
		$s = $scrapers[$scraper_id];
		if ( !isset($scraper2catalog[$scraper_id]) ) return;
		$catalog_id = $scraper2catalog[$scraper_id];
		$offset = 0;
		while ( true ) {
			# Gete CERSEI relations
			$url = "https://cersei.toolforge.org/api/relations/{$scraper_id}/{$offset}?limit={$batch_size}&earliest={$earliest}";
			$j = json_decode(file_get_contents($url));
			$candidates = [];
			foreach ( $j->rows AS $row ) {
				$ext_id = $row->e1_source_id;
				$scraper_id2 = $row->e2_scraper_id;
				if ( !isset($scraper2catalog[$scraper_id2]) ) continue;
				$catalog_id2 = $scraper2catalog[$scraper_id2];
				$ext_id2 = $row->e2_source_id;
				if ( !isset($candidates[$row->property]) ) $candidates[$row->property] = [];
				$candidates[$row->property][] = [ $ext_id,$catalog_id2*1,$ext_id2 ];
			}

			# Remove existing ones, and add new ones
			foreach ( $candidates AS $property => $list ) {
				$parts = [];
				foreach ( $list AS $l ) {
					$parts[] = "e1.ext_id='".$this->mnm->escape($l[0])."' AND e2.catalog={$l[1]} AND e2.ext_id='".$this->mnm->escape($l[2])."'";
				}
				$sql = "SELECT e1.ext_id AS ext_id1,e2.catalog,e2.ext_id AS ext_id2 FROM mnm_relation,entry e1,entry e2"
					." WHERE entry_id=e1.id AND target_entry_id=e2.id AND e1.catalog=6135 AND property={$property} AND (("
					. implode(") OR (",$parts) . "))";
				// print "{$sql}\n";
				$result = $this->mnm->getSQL ( $sql ) ;
				while($o = $result->fetch_object()) {
					$l = [ $o->ext_id1 , $o->catalog*1 , $o->ext_id2 ];
					if (($key = array_search($l, $list)) !== false) {
						unset($list[$key]);
					}
				}
				foreach ( $list AS $l ) {
					$ext_id_1 = $this->mnm->escape($l[0]);
					$ext_id_2 = $this->mnm->escape($l[2]);
					$sql = "INSERT IGNORE INTO mnm_relation (entry_id,property,target_entry_id) SELECT"
						." (select id from entry where catalog={$catalog_id} AND ext_id='{$ext_id_1}') AS entry_id,"
						."{$property} as property,"
						."(select id from entry where catalog={$l[1]} AND ext_id='{$ext_id_2}') AS target_entry_id"
						." HAVING entry_id IS NOT NULL AND target_entry_id IS NOT NULL";
					// print "{$sql}\n";
					$this->mnm->getSQL ( $sql ) ;
				}
			}


			if ( count($j->rows) < $batch_size ) return ;
			$offset += $batch_size;
		}
	}

}

$cs = new CerseiSync;

if ( !isset($argv[1]) ) {

	$cs->create_new_catalogs();
	$cs->sync_all_scrapers();

	$maintenance = new MixNMatch\Maintenance ( $cs->mnm ) ;
	$maintenance->importRelationsIntoAux();

} else if ( $argv[1]=='relations' ) {

	$cs->sync_internal_relations($argv[2]*1);

}else if ( $argv[1]=='update_aux' ) {

	$maintenance = new MixNMatch\Maintenance ( $cs->mnm ) ;
	$maintenance->importRelationsIntoAux();

}


?>