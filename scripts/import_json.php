#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';


if ( !isset($argv[1]) ) die ( "Pass JSON file as first parameter!\n" ) ;
$file = $argv[1] ;
$import = new MixNMatch\ImportJSON ;
$import->process_file ( $file ) ;

?>