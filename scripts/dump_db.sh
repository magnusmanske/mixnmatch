#!/bin/bash
day=$(date --date=${dateinfile#?_} "+%A")
mysqldump --defaults-file=~/replica.my.cnf --host=tools-readonly.db.svc.wikimedia.cloud --skip-lock-tables s51434__mixnmatch_p | gzip -c > ~/dumps/$day.sql.gz
