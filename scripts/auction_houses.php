#!/usr/bin/php
<?PHP
error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_ALL);

require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once dirname(__DIR__).'/public_html/php/wikidata.php';
require_once '/data/project/magnustools/public_html/php/WikimediaFileUpload.php';
require_once '/data/project/quickstatements/public_html/quickstatements.php' ;


$main_property = 'P4927';
$main_property_exclusive = true;
#$testing = true ;

// ________________________________________________________________________________________________________________


// This does not seem to have anything on PD creators?

class SourceChristies { # 
	public $tool_key = 'christies_creator';
	public $artist_url_pattern = 'https://artist.christies.com/_-$1.aspx';

	public function get_lots_from_html($html) {
		if ( !preg_match('|window.chrComponents.configurableSearch =(.+?\});|',$html,$m) ) throw new \Exception("Could not find artworks");
		$j = @json_decode($m[1]);
		if (!isset($j->data->lots) ) throw new \Exception("No data for artist");
		return $j->data->lots;
	}

	public function parse_lot($lot) {
		$ret = (object) ['is_valid'=>false];
		if ( preg_match('|/NoImage/|',$lot->image->image_src) ) return $ret;
		$ret->image_urls = [$lot->image->image_src];
		$ret->url = $lot->url;
		if ( preg_match('|https://onlineonly\.christies\.com/sso\?ObjectID=(.+?)&LotNumber=(.+)$|',$ret->url,$m) ) $ret->id = $m[1]."-".$m[2];
		else $ret->id = $ret->url;
		// $ret->desc = strip_tags($lot->description_txt);
		$ret->desc = trim($lot->title_secondary_txt . ' ' . $lot->title_tertiary_txt);

		$ret->title = $lot->title_primary_txt . ', ' . $lot->title_secondary_txt;

		$ret->is_valid = true ;
		return $ret;
	}
}

class SourceArtnet { # P3782
	public $tool_key = 'artnet_artist';
	public $artist_url_pattern = 'https://www.artnet.com/artists/$1/past-auction-results';

	public function get_lots_from_html($html) {
		if ( !preg_match('|<script type="application/ld\+json">(.+?)</script>|',$html,$m) ) throw new \Exception("Could not find artwork for artist {$artist_q} at {$artist_url}");
		$j = @json_decode($m[1])[0];
		if (!isset($j->makesOffer) ) {
			throw new \Exception("No data for artist");
		}
		return $j->makesOffer;
	}

	public function parse_lot($lot) {
		$ret = (object) ['is_valid'=>false];
		if ( !isset($lot->itemOffered) ) return $ret;
		$io = $lot->itemOffered;

		$image_url = 'https:'.$io->image->thumbnailUrl;
		$image_size = getFileSize($image_url);
		if ( $image_size===false ) return $ret; # Something's wrong with the file
		if ( $image_size==2139 ) return $ret; # Placeholder
		if ( $image_size<10*1024 ) return $ret; # Skip small files <10KB
		$ret->image_urls = [$image_url];

		$ret->desc = @$io->description;
		$ret->title = $io->name;
		$ret->title = preg_replace('| *\(.*$|','',$ret->title);

		$ret->url = $io->url;
		$ret->id = preg_replace('|^.+?-([^.-]+).*$|','$1',$ret->url);

		$ret->is_valid = true ;
		return $ret;
	}
}

class SourceInvaluable { # P4927
	public $tool_key = 'invaluable_com';
	public $artist_url_pattern = 'https://www.invaluable.com/artist/$1/';

	public function get_lots_from_html($html) {
		if ( !preg_match('|__APP_INITIAL_STATE__ *= *(.+?)\n|',$html,$m) ) throw new \Exception("Could not find artwork for artist");
		$j = @json_decode($m[1]);
		if (!isset($j->carouselLotsAlgoliaData->pastCarouselData->lotsToDisplay) ) {
			throw new \Exception("No carousel data for artist {$artist_q} at {$artist_url}");
		}
		return $j->carouselLotsAlgoliaData->pastCarouselData->lotsToDisplay;
	}

	public function parse_lot($lot) {
		$ret = (object) ['is_valid'=>false];

		if ( !isset($lot->photoPath) ) return $ret;
		$image_url2 = 'https://image.invaluable.com/housePhotos/'.$lot->photoPath; # Default
		$image_url1 = str_replace('.jpg','_original.jpg',$image_url2);
		$ret->image_urls = [$image_url1,$image_url2];

		$ret->desc = @$lot->_highlightResult->lotDescription->value;
		if ( !isset($ret->desc) ) return $ret;
		$ret->title = preg_replace('|^.+?\) *|','',$ret->desc);
		$ret->title = preg_replace('|[".].*$|','',$ret->title);

		$ret->id = strtolower($lot->lotRef);
		if ( preg_match('|href="(/auction-lot/[^">]+-'.$ret->id.')">|',$html,$m) ) {
			$ret->url = "https://www.invaluable.com".$m[1];
		}


		$ret->is_valid = true ;
		return $ret;
	}
}

$sources = [
	'P4200' => new SourceChristies(),
	'P3782' => new SourceArtnet(),
	'P4927' => new SourceInvaluable(),
];



// ________________________________________________________________________________________________________________

$testing = $testing===true ;
$source = $sources[$main_property];
$ignore_properties = array_keys($sources); # For SPARQL query
if (($key = array_search($main_property, $ignore_properties)) !== false) {
    unset($ignore_properties[$key]);
}


$bearer_token = file_get_contents('/data/project/mix-n-match/commons_upload_bearer.token');
$wmfu = new WikimediaFileUpload($bearer_token) ;
// $wmfu->verbose = true;

$mnm = new MixNMatch\MixNMatch ;
$mnm->tfc->getQS('mixnmatch:'.$source->tool_key,'/data/project/mix-n-match/reinheitsgebot.conf',true) ;

$uploaded_logfile = $source->tool_key.'.uploaded';
$uploaded = explode("\n",@file_get_contents($uploaded_logfile));


function followURLandReturnHTML ( $url ) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
//	curl_setopt($ch, CURLOPT_HEADER, TRUE);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	$a = curl_exec($ch);
	return $a ;
}

function getFileSize($url) {
    $headers = get_headers($url, 1);
    if (isset($headers['Content-Length'])) {
        return (int)$headers['Content-Length'];
    }
    return false; // Size not available
}

function commons_page_exists($page) {
	$url = "https://commons.wikimedia.org/wiki/".str_replace(' ','_',$page);
	$html = followURLandReturnHTML($url);
	return (1!==preg_match('/(No file by this name exists|This page does not currently exist)/',$html));
}

# Will add .jpg
function sanitize_filename($new_file) {
	$new_file = str_replace('/',' ',$new_file);
	$new_file = preg_replace('| *: *|',' ',$new_file);
	$new_file = preg_replace("|'{2,}|","'",$new_file);
	$new_file = preg_replace('|[ ,;.-]+$|','',$new_file);
	$new_file = trim($new_file).".jpg";
	return $new_file;
}

function get_new_filename($artist_name,$title,$lotref_lc){
	if ( trim($title)=='' ) $title = $lotref_lc;
	$new_file = sanitize_filename("{$artist_name} - {$title}");
	$cnt = 1;
	while ( commons_page_exists("File:{$new_file}") or strlen($new_file)>200 ) {
		$cnt++;
		$new_file = "{$artist_name} - {$title} ({$cnt})";
		if ( strlen($new_file)>200 ) {
			$new_file = "{$artist_name} - {$lotref_lc} ({$cnt})";
		}
		if ( strlen($new_file)>200 ) {
			$new_file = "Artwork {$lotref_lc} ({$cnt})";
		}
		$new_file = sanitize_filename($new_file);
		print "Changing filename => {$new_file}\n";
		if ( $cnt>10 ) {
			throw new \Exception("SKIPPING FILE {$new_file}");
		}
	}
	return $new_file;
}

function add_media_info($file,$artist_q) {
	global $wmfu;
	try {
		$url = "https://commons.wikimedia.org/w/api.php?action=query&prop=imageinfo&iilimit=1&format=json&titles=File:".str_replace(' ','_',$file);
		$j = @json_decode(file_get_contents($url));
		if ( !isset($j->query->pages) ) throw new \Exception("Could not find Commons file {$file}");
		foreach ( $j->query->pages AS $k => $v ) $media_id = "M{$k}";
		// print "Using Media ID {$media_id}";
		$artist_q_numeric = preg_replace('|\D|','',$artist_q);
		$wmfu->add_statement($media_id,'P50',["entity-type" => "item", "numeric-id" => $artist_q_numeric]);
	} catch (Exception $e) {
		print "add_media_info: ".$e->getMessage()."\n";
		return;
	}
}

function process_artist($artist_q) {
	global $wmfu,$mnm,$uploaded,$uploaded_logfile,$main_property,$source,$testing;
	print "\nPROCESSING https://www.wikidata.org/wiki/{$artist_q}\n";
	$wil = new WikidataItemList ;
	$wil->loadItems ( [$artist_q] ) ;
	$i = $wil->getItem($artist_q);
	if ( !isset($i) ) throw new \Exception("Could not find Wikidata item {$artist_q}");
	$artist_id = $i->getFirstString($main_property);
	$artist_name = $i->getLabel();
	
	// Use existing category, or create new one
	$category = $i->getFirstString("P373");
	$category_exists = true;
	if ( !isset($category)  or $category=='' ) {
		$category = $artist_name;
		if ( commons_page_exists("Category:{$category}") ) {
			$category = "{$artist_name} (artist)";
			if ( commons_page_exists("Category:{$category}") ) {
				$category = "{$artist_name} ({$artist_q})";
			}
		}
		$category_exists = false;
	}
	$category_url = "https://commons.wikimedia.org/wiki/Category:".urlencode(str_replace(' ','_',$category));
	print "CATEGORY {$category_url}\n";


	$artist_url = str_replace('$1',$artist_id,$source->artist_url_pattern);
	print "ARTIST {$artist_url}\n";
	$html = followURLandReturnHTML($artist_url);

	$lots = $source->get_lots_from_html($html);

	$had_title = [];
	$uploaded_count = 0;
	foreach ( $lots as $lot ) {
		try {
			$parts = $source->parse_lot($lot);
		} catch (Exception $e) {
			continue;
		}
		if ( $testing ) print_r($parts);

		# Sanity checks
		if ( !$parts->is_valid ) continue;
		if ( preg_match('|(withdrawn)|',$parts->title) ) continue; # No image file
		if ( isset($had_title[$parts->title]) ) continue ; # Sometimes the same work is tracked under different lotrefs
		$had_title[$parts->title] = $parts->title;
		if ( in_array($parts->id, $uploaded) ) continue;

		# Misc fixes
		if ( !isset($parts->url) or $parts->url=='' ) $parts->url = $artist_url; # Fallback
		$parts->desc = str_replace('|','<nowiki>|</nowiki>',$parts->desc??'');


		# Generate description template
		$desc = "=={{int:filedesc}}==
{{Artwork
 |artist             = {{Creator|wikidata={$artist_q}}}
 |author             =
 |title              = {$parts->title}
 |object type        =
 |description        = {$parts->desc}
 |depicted people    =
 |depicted place     =
 |date               =
 |medium             =
 |dimensions         =
 |institution        =
 |department         =
 |accession number   =
 |place of creation  =
 |place of discovery =
 |object history     =
 |exhibition history =
 |credit line        =
 |inscriptions       =
 |notes              =
 |references         =
 |source             = {$parts->url}
 |permission         =
 |other_versions     =
 |wikidata           =
}} 

=={{int:license-header}}==
{{PD-old-70}}

[[Category:{$category}]]
";

		try {
			$new_file = get_new_filename($artist_name,$title,$parts->id);
		} catch (Exception $e) {
			continue;
		}


		# Upload file
		print "Uploading {$new_file}\n";
		if ( $testing ) {
			print_r($parts->image_urls);
		} else {
			while ( count($parts->image_urls)>0 ) {
				$image_url = array_shift($parts->image_urls);
				try {
					$wmfu->uploadURL($image_url,$new_file,$desc);
					$uploaded_count++;
					$uploaded[] = $parts->id;
					file_put_contents($uploaded_logfile, implode("\n",$uploaded));
					add_media_info($new_file,$artist_q);
					break;
				} catch (Exception $e) {
					print $e->getMessage()."\n";
				}
			}
		}
	}

	print "Uploaded {$uploaded_count} files\n";
	if ($uploaded_count==0) return;

	if ( $testing ) return;

	# Create category
	if ( !$category_exists ) {
		$category_text = "{{Infobox Wikidata|\n\tqid = {$artist_q}\n}}";
		try {
			$wmfu->editPage("Category:{$category}",$category_text,"Creating category for artist {$artist_q}");
			print "Created Commons category\n";
		} catch (Exception $e) {
			print $e->getMessage()." [CATEGORY PAGE]\n";
		}
	}


	# Add category to wikidata item
	$c = $i->getFirstString("P373");
	if ( !isset($c) or $c=='' ) {
		$commands = ["{$artist_q}\tP373\t\"{$category}\""];
		try {
			$mnm->tfc->runCommandsQS ( $commands ) ;
			print "Added Commons category to Wikidata item\n";
		} catch (Exception $e) {
			print $e->getMessage()." [WIKIDATA EDIT]\n";
		}
	}
}

# Specific Wikidata item?
$artist_q = @$argv[1];
if ( isset($artist_q) ) {
	try {
		process_artist(strtoupper(trim($artist_q)));
	} catch (Exception $e) {
		print $e->getMessage()."\n";
	}
	exit(0);
}

print "Running SPARQL query to find people with an ID who died more than 70 years ago but don't have a Commons category...\n";
	$sparql = "SELECT ?q  {
  ?q wdt:P31 wd:Q5 ; wdt:{$main_property} [] . # Person with an ID
  ?q wdt:P570 ?died . FILTER ( year(?died)<YEAR(NOW())-70 ) . # who died over 70 years ago
  MINUS { ?q wdt:P373 [] }";
if ( $main_property_exclusive ) {
  foreach ( $ignore_properties as $p ) $sparql .= " . MINUS { ?q wdt:{$p} [] }";
}
$sparql .= '}';
#print "{$sparql}\n"; exit(0);

$items = $mnm->tfc->getSPARQLitems($sparql,'q');
shuffle($items);
print count($items)." items found\n\n";

foreach ( $items AS $artist_q ) {
	try {
		process_artist($artist_q);
	} catch (Exception $e) {
		print $e->getMessage()."\n";
	}
	sleep(5);
}

?>