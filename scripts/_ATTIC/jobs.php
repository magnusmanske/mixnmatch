#!/usr/bin/php
<?PHP

ini_set('memory_limit','3500M');

require_once dirname(__DIR__) . '/vendor/autoload.php';

$jobs = new MixNMatch\Jobs ;
$jobs->valid_job_types = $jobs->get_high_memory_actions() ; # No low-memory actions

$cmd = 'next' ; # Default
if ( isset($argv[1]) ) $cmd = $argv[1] ;


# Setting this up to fail ;-)
function fail_current_job () { # TODO move to Jobs.php?
	global $jobs ;
	if ( $jobs->get_running_job_id() == '' ) return ;
	$jobs->mnm->dbmConnect(true);
	$job_id_running = $jobs->get_running_job_id() ;
	$jobs->mnm->set_job_status($job_id_running,'FAILED');
	$job_id_running = '' ;
}

register_shutdown_function('fail_current_job');
pcntl_signal(SIGTERM, "fail_current_job");
pcntl_signal(SIGHUP,  "fail_current_job");
pcntl_signal(SIGUSR1, "fail_current_job");

if ( $cmd == 'next' ) {
	$job = $jobs->get_next_job();
	print_r ( $job ) ;
	if ( !isset($job) ) exit(0);
	$job->catalog *= 1 ; # Paranoia
	$jobs->run_job_command($job) ;
} else if ( $cmd == 'run' ) {
	$job = $mnm->get_job_by_id($argv[2]);
	$jobs->run_job_command($job) ;
} else die ( "Unknown command: '{$cmd}'\n") ;

?>