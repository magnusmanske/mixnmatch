#!/usr/bin/php
<?PHP

function verifyDates ( $q , $year1 , $year2 ) {
	$q = "Q$q" ;
//	print "TESTING $q , $year1 , $year2\n" ;
	
	$url = "http://www.wikidata.org/wiki/Special:EntityData/$q.json" ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	if ( !isset($j->entities->$q) ) return false ;
	if ( !isset($j->entities->$q->claims) ) return false ;
	$claims = $j->entities->$q->claims ;
	if ( !isset($claims->P569) || !isset($claims->P570) ) return false ;
	
//	print_r ( $claims->P569 ) ;
	
	$found = false ;
	foreach ( $claims->P569 AS $v ) {
		$time = $v->mainsnak->datavalue->value->time ;
		if ( 1 == count ( explode ( $year1.'-' , $time ) ) ) continue ;
		$found = true ;
	}
	if ( !$found ) return false ;

//	print_r ( $claims->P570 ) ;

	$found = false ;
	foreach ( $claims->P570 AS $v ) {
		$time = $v->mainsnak->datavalue->value->time ;
		if ( 1 == count ( explode ( $year2.'-' , $time ) ) ) continue ;
		$found = true ;
	}
	if ( !$found ) return false ;
	
	return true ;
}



if ( isset($argv[1]) ) $catalog = $argv[1] ; // Unset this to do all



require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;

$mnm = new MixNMatch ;

$randomize = true ;
if ( isset($catalog) and isset($argv[2]) and $argv[2] == 'NORAND' ) $randomize = false ;
if ( isset ( $catalog ) and $randomize ) {
	$randomize = false ;
	$sql = "SELECT count(*) AS cnt FROM entry WHERE (user=0 or user is null) AND catalog=$catalog AND type IN ('Q5','person')" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		if ( $o->cnt < 20000 ) $randomize = false ;
	}
}

$cnt = 0 ;
$sql = "select * from entry" ;
$sql .= " LEFT OUTER JOIN person_dates ON id=entry_id" ;
$sql .= " where (user=0 or user is null)" ;
$sql .= " AND ext_desc regexp binary '[0-9]{3,4}[^0-9].*[0-9]{3,4}'" ;
$sql .= " AND type IN ('Q5','person')" ;
$sql .= " AND NOT EXISTS (SELECT * FROM `log` WHERE log.entry_id=entry.id)" ; # Prevent re-linking for manually unlinked items
if ( isset ( $catalog ) ) $sql .= " AND catalog=$catalog" ;
//$sql .= " AND id=257148" ; // TESTING

$sql .= " order by rand()" ;
if ( $randomize ) $sql .= " limit 20000" ; // For large catalogs

$sqls = array() ;

#print "$sql\n" ; exit(0);
$max_year = 2020 ;
$all_candidates = array() ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){

	if ( isset($o->born) and isset($o->died) ) {
		if ( preg_match ( '/^(\d+)/' , $o->born , $y1 ) and  preg_match ( '/^(\d+)/' , $o->died , $y2 ) ) {
			$all_candidates[] = array ( $o , array ( 'dummy' , $y1[1]*1 , $y2[1]*1 ) ) ;
		}
	} else if ( 1 ) { // Take the two farthest-apart dates
	
	
		if ( !preg_match_all ( '/\b(\d\d\d\d)\b/' , $o->ext_desc , $m ) ) continue ;
		$m = $m[0] ;
		if ( count($m) < 2 ) continue ;
		sort($m, SORT_NUMERIC); 
		$y1 = $m[0]*1 ;
		$y2 = $m[count($m)-1]*1 ;

		if ( $y1 > $y2 ) continue ; // Nope
		if ( $y2-$y1 > 120 ) continue ; // Age filter
		if ( $y1 > $max_year or $y2 > $max_year ) continue ;
		$all_candidates[] = array ( $o , array ( 'dummy' , $y1 , $y2 ) ) ;
		
	} else {

		if ( !preg_match ( '/\b(\d{3,4})\D.*?(\d{3,4})\b/' , $o->ext_desc , $m ) ) continue ;
		if ( $m[1]*1>$max_year or $m[2]*1>$max_year ) continue ; // Paranoia
		$all_candidates[] = array ( $o , $m ) ;

	}
}

$dbwd = openDB ( 'wikidata' , 'wikidata' , true ) ;
foreach ( $all_candidates AS $cand ) {
	$o = $cand[0] ;
	$m = $cand[1] ;
	$y1 = $m[1] ;
	$y2 = $m[2] ;

	// Fix name
	$name = $o->ext_name ;
	$name = preg_replace ( '/\b(Mmle|pseud\.|diverses)\b/' , '' , $name ) ;
	$name = preg_replace ( '/ Bt$/' , ' Baronet' , $name ) ;
	$name = preg_replace ( '/\b[A-Z]\.{0,1}\b/' , ' ' , $name ) ; # Single letter
	$name = trim ( preg_replace ( '/\s+/' , ' ' , $name ) ) ;
	
	$names = array ( $dbwd->real_escape_string($name) ) ;
	if ( preg_match ( '/^Sir /' , $name ) ) {
		$n = preg_replace ( '/^Sir /' , '' , $name ) ;
		$n = preg_replace ( '/\s*\(.+\)/' , '' , $n ) ;
		$names[] = $dbwd->real_escape_string($n) ;
	} else if ( preg_match ( '/^(\w+)\s(\w+)\s(\w+)$/' , $name , $m ) ) {
		$names[] = $dbwd->real_escape_string ( $m[1] . ' ' . $m[3] ) ;
		$names[] = $dbwd->real_escape_string ( $m[2] . ' ' . $m[3] ) ;
	}
	if ( preg_match ( '/^(Baron|Baronesse{0,1}|Graf|Gräfin) (.+)$/' , $name , $m ) ) {
		$names[] = $dbwd->real_escape_string ( $m[1] ) ;
	}
	
	if ( preg_match ( '/^(.+) de (.+)$/' , $name , $m ) ) $names[] = $dbwd->real_escape_string ( $m[1].' '.$m[2] ) ;
	if ( preg_match ( '/^(.+) de la (.+)$/' , $name , $m ) ) $names[] = $dbwd->real_escape_string ( $m[1].' '.$m[2] ) ;
	if ( preg_match ( '/^(.+) of (.+)$/' , $name , $m ) ) $names[] = $dbwd->real_escape_string ( $m[1].' '.$m[2] ) ;
	if ( preg_match ( '/^(.+) von (.+)$/' , $name , $m ) ) $names[] = $dbwd->real_escape_string ( $m[1].' '.$m[2] ) ;
	
	$n2 = preg_replace ( '/^([A-Z]\.\s*|[A-Z][a-z]\.\s*)+$/' , '' , $name ) ;
	if ( $n2 != $name ) $names[] = $dbwd->real_escape_string ( $n2 ) ;
	
	foreach ( $names AS $a => $b ) $names[$a] = str_replace ( '"' , '' , $b ) ; // Quote paranoia

	foreach ( $names AS $a => $b ) { // Dash
		if ( !preg_match ( '/^(.+)-(.+)$/' , $b , $m ) ) continue ;
		$names[] = $m[1] . ' ' . $m[2] ;
	}

	foreach ( $names AS $a => $b ) { // Middle initial
		if ( !preg_match ( '/^(.+) [A-Z]\. (.+)$/' , $b , $m ) ) continue ;
		$names[] = $m[1] . ' ' . $m[2] ;
	}


	$namelist = '"' . strtolower ( implode ( '","' , $names ) ) . '"' ;

	$sparql = 'SELECT DISTINCT ?item WHERE {   ?item wdt:P569 ?time0 . FILTER ( ?time0 >= "'.$y1.'-00-00T00:00:00Z"^^xsd:dateTime && ?time0 <= "'.$y1.'-13-31T00:00:00Z"^^xsd:dateTime )' ;
	$sparql .= ' ?item wdt:P570 ?time1 . FILTER ( ?time1 >= "'.$y2.'-00-00T00:00:00Z"^^xsd:dateTime && ?time1 <= "'.$y2.'-13-31T00:00:00Z"^^xsd:dateTime )' ;
	$sparql .= ' OPTIONAL { ?item rdfs:label ?label } OPTIONAL { ?item skos:altLabel ?alias } ' ;
	$sparql .= "FILTER ( LCASE(str(?label)) IN ($namelist) || LCASE(str(?alias)) IN ($namelist) )" ;
	$sparql .= '}' ;

	$items = getSPARQLitems ( $sparql , 'item' ) ;
	if ( count($items) != 1 ) continue ;

	$q = $items[0] ;
	$mnm->setMatchForEntryID ( $o->id , $q , 3 , true , false ) ;
	
	print $o->id . " => Q$q\n" ;
	
	$cnt++ ;
}

print "$cnt assigned\n" ;

# Unnecessary, but just in case...
if ( $cnt > 0 ) file_get_contents ( 'https://tools.wmflabs.org/mix-n-match/api.php?query=update_overview' ) ; // Update stats

?>
