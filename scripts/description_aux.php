#!/usr/bin/php
<?PHP

# Uses the `description_aux` table to add auxiliary data from ext_desc

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;

$catalog_id = ($argv[1]??0)*1;
if ( $catalog_id<=0 ) {
	die("USAGE: {$argv[0]} CATALOG_ID [1(=testing)]\n") ;
}
$testing = ($argv[2]=='1');

# Load table description_aux
$description_aux = [];
$sql = "SELECT * FROM description_aux" ;
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) $description_aux[] = $o;

foreach ( $description_aux AS $da ) {
	$rx = strtolower(str_replace('\\','\\\\',$da->rx));
	$value = trim($da->value);
	$sql = "SELECT `id`,{$da->property},'{$value}' FROM `entry` WHERE `catalog`={$catalog_id} AND lower(ext_desc) RLIKE \"{$rx}\"";
	$sql .= " AND NOT EXISTS (SELECT * FROM auxiliary WHERE entry_id=entry.id AND aux_p={$da->property} AND aux_name='{$value}')";
	if ( $da->type_constraint!='' ) $sql .= " AND `type`='{$da->type_constraint}'";
	$sql = "INSERT IGNORE INTO auxiliary (entry_id,aux_p,aux_name) {$sql}" ;
	if ( $testing ) print "{$sql}\n";
	else $mnm->getSQL ( $sql ) ;
}

?>