#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

require_once dirname(__DIR__) . '/vendor/autoload.php';

$use_single_catalog = false ;
if ( isset($argv[1]) ) {
	$catalog = $argv[1] * 1 ;
	$use_single_catalog = true ;
}

$mnm = new MixNMatch\MixNMatch ;

function getSearch ( $query , $property , $q ) {
	$query .= " haswbstatement:{$property}={$q}" ;
	$url = "http://www.wikidata.org/w/api.php?action=query&list=search&format=json&srsearch=" . urlencode ( $query ) ;
	return json_decode ( file_get_contents ( $url ) ) ;
}

$used_catalogs = [] ;
$sql = "SELECT * FROM vw_aux WHERE aux_p IN (170,50) AND q is null" ;
if ( $use_single_catalog ) $sql .= " AND catalog={$catalog}" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$j = getSearch ( $o->ext_name , 'P'.$o->aux_p , $o->aux_name ) ;
	if ( count($j->query->search) == 0 ) continue ; # Nothing found

	$used_catalogs[$o->catalog] = $o->catalog ;

	# Single match
	if ( count($j->query->search) == 1 ) {
		$q = $j->query->search[0]->title ;
		$mnm->setMatchForEntryID ( $o->id , $q , 0 , true , true ) ;
		continue ;
	}

	# Multi-match
	$qs = [] ;
	foreach ( $j->query->search AS $v ) {
		$qs[] = preg_replace ( '/\D/' , '' , $v->title ) ;
	}
	$sql = "INSERT IGNORE INTO multi_match (entry_id,catalog,candidates,candidate_count) VALUES ({$o->id},{$o->catalog},'" . implode ( ',' , $qs ) . "'," . count($qs) . ")" ;
	$mnm->getSQL ( $sql ) ;
}

# Unnecessary, but just in case...
foreach ( $used_catalogs AS $catalog_id ) {
	$catalog = new MixNMatch\Catalog ( $catalog_id , $mnm ) ;
	$catalog->updateStatistics();
	$catalog->useAutomatchers(0);
}

?>
