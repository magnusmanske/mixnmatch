#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); # |E_ALL
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

if ( !isset ( $argv[1] ) ) die ( "Needs argument : catalog_id\n" ) ;
$catalog_id = $argv[1] * 1 ;


$am = new MixNMatch\AutoMatch ;
$am->automatch_by_search ( $catalog_id ) ;

?>
