#!/usr/bin/php
<?PHP

# NOTE: This can be run efficiently for updates

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

$catalog_id = ($argv[1]??0)*1 ;
if ( $catalog_id == 0 ) die ( "USAGE: {$argv[0]} CATALOG_ID [ENTRY_ID to test]\n" ) ;
$entry_id = ($argv[2]??0)*1 ;

$mnm = new MixNMatch\MixNMatch ;
$catalog = $mnm->loadCatalog($catalog_id,true) ;

$image_pattern = $catalog->data()->image_pattern;
$sql = "SELECT * FROM entry WHERE catalog={$catalog_id} AND (q is null or user=0) AND NOT EXISTS (SELECT * FROM kv_entry WHERE entry_id=entry.id AND kv_key='image_url') ORDER BY `random`" ;
if ( $entry_id>0 ) $sql = "SELECT * FROM entry WHERE id={$entry_id}";
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) {
	$image_url = $mnm->get_image_url_from_ext_url($o,$image_pattern);
	if ( $entry_id>0 ) print "{$o->id} => {$image_url}\n";
	sleep(2);
}


?>