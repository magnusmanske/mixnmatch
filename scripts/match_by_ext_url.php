#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';

function normalize_url ( $url ) {
	return preg_replace ( '|^https:|' , 'http:' , $url ) ;
}

$mnm = new MixNMatch\MixNMatch ;

if ( !isset($argv[1]) ) die ( "Needs catalog parameter\n" ) ;
$catalog = $argv[1] * 1 ;

$url2id = [] ;
$pattern = '' ;
$sql = "SELECT id,ext_url FROM entry WHERE (q IS NULL or user=0) AND catalog={$catalog} AND ext_url!=''" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$url = normalize_url ( $o->ext_url ) ;
	if ( $pattern == '' ) {
		$pattern = $url ;
		continue ;
	}
	if ( $pattern == '*' ) break ;
	while ( $pattern != '' ) {
		if ( $pattern == substr($url,0,strlen($pattern)) ) break ;
		$pattern = substr($pattern,0,strlen($pattern)-1) ;
	}
	if ( $pattern == '' ) $pattern = '*' ;
	$url2id[$url] = $o->id ;
}

if ( $pattern == '' or $pattern == '*' ) die(print "Could not find common URL pattern\n");

$pattern1 = $pattern ;
$pattern2 = preg_replace ( '|^http:|' , 'https:' , $pattern1 ) ;
#print "URL pattern:\n{$pattern1}\n{$pattern2}\n" ;

$id2q = [] ;
$sparql = "SELECT DISTINCT ?q ?url { { ?q wdt:P973 ?url } UNION { ?q p:P1343/pq:P2699 ?url } FILTER ( STRSTARTS(STR(?url),'{$pattern1}') || STRSTARTS(STR(?url),'{$pattern2}') ) }" ;
#print "{$sparql}\n" ;
$j = $mnm->tfc->getSPARQL ( $sparql ) ;
foreach ( $j->results->bindings AS $b ) {
	$q = $mnm->tfc->parseItemFromURL($b->q->value) ;
	$url = normalize_url ( $b->url->value ) ;
	if ( !isset($url2id[$url]) ) continue ;
	$id = $url2id[$url] ;
	$id2q[$id][] = $q ;
	//print "{$q} => {$url}\n" ;
}

#print_r($id2q) ;

foreach ( $id2q AS $id => $qs ) {
	if ( count($qs) != 1 ) continue ;
	$q = $qs[0] ;
	#print "{$id}:{$q}\n" ;
	$mnm->setMatchForEntryID ( $id , $q , 4 , true , false ) ;
}

?>