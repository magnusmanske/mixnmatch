#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

require_once dirname(__DIR__) . '/vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;

$bad_catalogs = [ 70 ] ;

# Get valid catalogs
$catalogs = [] ;
$sql = "SELECT id FROM catalog WHERE active=1 AND has_person_date='yes' AND id NOT IN (" . implode(',',$bad_catalogs) . ")" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) $catalogs[] = $o->id ;
$catalogs = implode ( ',' , $catalogs ) ;

#$sql_catalogs = "SELECT id FROM catalog WHERE active=1 AND id NOT IN (" . implode(',',$bad_catalogs) . ")" ;
$sql = "SELECT SQL_NO_CACHE ext_name,count(*) AS cnt,group_concat(entry_id) AS entry_ids,concat(year_born,'-',year_died) AS dates FROM entry,person_dates WHERE entry.id=entry_id AND (q IS NULL or user=0) AND catalog IN ({$catalogs}) AND year_born!='' AND year_died!='' GROUP BY ext_name,year_born,year_died HAVING cnt>=4" ;
$result = $mnm->getSQL ( $sql ) ;
$sql = "INSERT INTO common_names_human (name,cnt,entry_ids,dates) VALUES " ;
$first = true ;
while($o = $result->fetch_object()){
	if ( preg_match ( '/untitled/' , $o->ext_name ) ) continue ;
	if ( $first ) $first = false ;
	else $sql .= ',' ;
	$sql .= "('" . $mnm->escape($o->ext_name) . "',{$o->cnt},'{$o->entry_ids}','{$o->dates}')" ;
}

$sql_clear = "TRUNCATE common_names_human" ;
$result = $mnm->getSQL ( $sql_clear ) ;

$result = $mnm->getSQL ( $sql ) ;

?>