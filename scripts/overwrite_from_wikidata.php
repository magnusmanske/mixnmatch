#!/usr/bin/php
<?PHP

# This will find (SPARQL) mismatches between MnM and WD, and update MnM accordingly
# It can also generate QS commands to add new statements to WD, but we have that functionality already on the website

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

require_once dirname(__DIR__) . '/vendor/autoload.php';

$catalog = 0 ;
if ( isset($argv[1]) ) $catalog = $argv[1] * 1 ;
if ( $catalog == 0 ) die ( "USAGE: {$argv[0]} [CATALOG]\n" ) ;

$mnm = new MixNMatch\MixNMatch ;

$ci = $mnm->loadCatalog ( $catalog , true ) ;
if ( $ci->data()->active != 1 ) die ( "Catalog {$catalog} is not active\n" ) ;
if ( !isset($ci->data()->wd_prop) or isset($ci->data()->wd_qual) ) die ( "Catalog {$catalog} has no property, or has qualifier\n" ) ;

$sparql = "SELECT ?q ?v { ?q wdt:P{$ci->data()->wd_prop} ?v }" ;
$j = $mnm->tfc->getSPARQL ( $sparql ) ;
if ( !isset($j) or $j === FALSE or $j == null or !isset($j->results) or !isset($j->results->bindings) ) die ( "JSON query failure\n" ) ;

$id2q = [] ;
foreach ( $j->results->bindings AS $b ) {
	$q = $mnm->tfc->parseItemFromURL ( $b->q->value ) ;
	$q = preg_replace ( '|\D|' , '' , $q ) ; # Numeric
	$v = $b->v->value ;
	$id2q[$v] = $q ;
}
unset ( $j ) ;

$qs = [] ;
$sql = "SELECT id,ext_id,q FROM entry WHERE catalog={$catalog} AND q>0 AND user>0" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	if ( isset($id2q[$o->ext_id]) ) {
		if ( $id2q[$o->ext_id] == $o->q ) {
			# Same q
		} else {
			#print "Updating {$mnm->root_url}/#/entry/{$o->id} from https://www.wikidata.org/wiki/Q{$o->q} " ;
			#print " to https://www.wikidata.org/wiki/Q{$id2q[$o->ext_id]}\n" ;
			$mnm->setMatchForEntryID ( $o->id , $id2q[$o->ext_id] , 4 , false , false ) ;
		}
	} else { # On MnM but not on WD
		#print "Entry {$mnm->root_url}/#/entry/{$o->id} has https://www.wikidata.org/wiki/Q{$o->q} but Wikidata has not\n" ;
		#$qs[] = "Q{$o->q}\tP{$ci->data()->wd_prop}\t\"{$o->ext_id}\"" ;
	}
}

if ( count($qs) > 0 ) {
	print "QuickStatements commands:\n\n" ;
	print join ( "\n" , $qs ) . "\n\n" ;
}

?>