#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';

if ( !isset($argv[1]) ) die ( "CATALOG required\n" ) ;
$catalog_id = $argv[1] * 1 ;
if ( $catalog_id <= 0 ) die ( "Bad catalog {$argv[1]}\n" ) ;
$testing = isset($argv[2]) and $argv[2] == '1' ;

$mnm = new MixNMatch\MixNMatch () ;
$scraper = new MixNMatch\BespokeScraper ( $catalog_id , $mnm ) ;
$scraper->loadCodeFragment() ;
$entries_were_changed = $scraper->processCatalog($testing) ;

if ( !$testing and $entries_were_changed ) {
	$catalog = new MixNMatch\Catalog ( $catalog_id , $mnm ) ;
	$catalog->updateStatistics() ;
	$job_id = $mnm->queue_job($catalog_id,'auxiliary_matcher');
	$mnm->queue_job($catalog_id,'automatch_by_search',$job_id);
	$job_id = $mnm->queue_job($catalog_id,'update_person_dates');
	$mnm->queue_job($catalog_id,'match_person_dates',$job_id);
}
?>