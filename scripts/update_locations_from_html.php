#!/usr/bin/php
<?PHP

# THIS CAN BE RUN AGAIN EFFICIENTLY TO READ COORDINATES FOR NEW ADDITIONS

#error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
#ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

use OpenLocationCode\OpenLocationCode; # For 221

abstract class UpdateLocationFromHTML extends MixNMatch\Helper {
	public $testing = false ;
	public $silent = false ; # For testing only
	public $query_limit = 50 ; # For testing only

	function __construct($catalog_id,$mnm='') {
		$this->catalog_id = $catalog_id ;
		$this->mnm = $mnm==''?new MixNMatch\MixNMatch:$mnm ;
		$this->use_curl = true ;
	}

	protected function update_entry_generator() {
		$sql = "SELECT * FROM entry WHERE catalog={$this->catalog_id}" ;
		if ( $this->testing ) $sql .= " LIMIT {$this->query_limit}" ;
		else $sql .= " AND NOT EXISTS (SELECT * FROM location WHERE entry_id=entry.id)" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) yield $o ;
		yield from [] ;
	}

	protected function log ( $s ) { if ( $this->testing and !$this->silent ) print "{$s}\n" ; }

	public function update() {
		$coordinates_added = 0 ;
		$main_pattern = $this->get_main_pattern() ;
		foreach ( $this->update_entry_generator() AS $o ) {
			$url = $this->preprocess_url($o) ;
			if ( !isset($url) ) {
				$this->log("Pre-processing URL failed for {$o->id}: {$o->ext_url}") ;
				continue ;
			}
			if ( $url != $o->ext_url ) { # Update URL in MnM
				if ( $this->update_mnm_url($o) ) {
					$sql = "UPDATE entry SET ext_url='".$this->mnm->escape($url)."' WHERE id={$o->id}" ;
					$this->mnm->getSQL ( $sql ) ;
				}
				$o->ext_url = $url ;
			}

			$this->log("Checking {$o->id}: {$o->ext_url}") ;

			$coordinates = $this->process_special($o) ;
			if ( !isset($coordinates) and isset($main_pattern) ) {
				$html = $this->load_from_object ( $o ) ;
				if ( !preg_match($main_pattern,$html,$m) ) {
					$this->log("No pattern match for {$o->id}") ;
					continue ;
				}
				$coordinates = $this->process_entry ( $o , $html , $m ) ;
			}

			if ( is_array($coordinates) and count($coordinates)==2 ) {
				$this->log("Would set {$o->id} to {$coordinates[0]} / {$coordinates[1]}") ;
				if ( !$this->testing ) $this->mnm->setLocation ( $o->id , $coordinates[0] , $coordinates[1] ) ;
				$coordinates_added += 1 ;
			} else {
				$this->log("Could not process pattern match for {$o->id}") ;
			}
		}
		return $coordinates_added ;
	}

	protected function can_update () { return true ; } # Can the entire catalog update quickly?
	protected function process_special ( $o ) {}
	protected function update_mnm_url ( $o ) { return false ; }
	protected function load_from_object ( $o ) { return $this->load_from_url($o->ext_url) ; }
	protected function load_from_url ( $url ) {
		if ( $this->catalog_id == 5737 ) {
			$ret = file_get_contents($url);
			$ret = gzdecode($ret);
			return $ret ;
		}
		return $this->get_contents_from_url($url);
	}
	protected function preprocess_url ( $o ) { return $o->ext_url ; }
	abstract protected function get_main_pattern () ;
	abstract protected function process_entry ( $o , $html , $m ) ;

	public static function get_available_catalogs ( $mnm ) {
		$catalogs = [] ;
		$sql = "SELECT DISTINCT `catalog` FROM code_fragments WHERE `function`='COORDS_FROM_HTML'" ;
		$result = $mnm->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) $catalogs[] = $o->catalog ;
		return $catalogs ;
	}

	public static function test_all_catalogs($mnm) {
		$test_set_size = 2 ;
		$catalogs = UpdateLocationFromHTML::get_available_catalogs($mnm) ;
		foreach ( $catalogs AS $catalog_id ) {
			print "TESTING CATALOG {$catalog_id}..." ;
			$classname = self::load_catalog_class($catalog_id,$mnm);
			$u = new $classname ( $catalog_id , $mnm ) ;
			$u->testing = true ;
			$u->silent = true ;
			$u->query_limit = $test_set_size ;
			$coordinates_added = $u->update();
			if ( $coordinates_added == $test_set_size ) print " OK!\n" ;
			else print " {$coordinates_added} of expected {$test_set_size} coordinates found.\n" ;
		}
	}

	public static function update_all_catalogs($mnm) {
		$catalogs = UpdateLocationFromHTML::get_available_catalogs($mnm) ;
		foreach ( $catalogs AS $catalog_id ) {
			$classname = self::load_catalog_class($catalog_id,$mnm);
			$u = new $classname ( $catalog_id , $mnm ) ;
			if ( !$u->can_update() ) continue ;
			print "UPDATING CATALOG {$catalog_id}..." ;
			$coordinates_added = $u->update();
			print " {$coordinates_added} coordinates added.\n" ;
		}
	}

	public static function load_catalog_class($catalog_id,$mnm) {
		$classname = "UpdateLocationFromHTML{$catalog_id}" ;
		$sql = "SELECT * FROM code_fragments WHERE `catalog`={$catalog_id} AND `function`='COORDS_FROM_HTML'" ;
		$result = $mnm->getSQL ( $sql ) ;
		if ( $o = $result->fetch_object() ) {
			$code = "class {$classname} extends UpdateLocationFromHTML {\n{$o->php}\n}" ;
			eval ( $code ) ;
			return $classname ;
		} else {
			print "No class {$classname} exists.\n" ;
			self::show_available_catalogs($mnm);
			exit ( 0 ) ;
		}
	}

	public static function show_available_catalogs($mnm) {
		$catalogs = self::get_available_catalogs($mnm) ;
		print "Available catalogs: ".implode(', ',$catalogs)."\n" ;
	}

}

# ________________________________________________________________________________________________________________________


$mnm = new MixNMatch\MixNMatch ;

if ( !isset($argv[1]) ) {
	print "USAGE: {$argv[0]} CATALOG_ID [1(=testing)]\n" ;
	UpdateLocationFromHTML::show_available_catalogs($mnm);
}
else if ( $argv[1] == 'test_all' ) UpdateLocationFromHTML::test_all_catalogs($mnm);
else if ( $argv[1] == 'update_all' ) UpdateLocationFromHTML::update_all_catalogs($mnm);
else {
	$catalog_id= $argv[1]*1 ;
	$classname = UpdateLocationFromHTML::load_catalog_class($catalog_id,$mnm) ;
	$u = new $classname ( $catalog_id , $mnm ) ;
	if ( isset($argv[2]) and $argv[2]=='1' ) $u->testing = true ;
	$coordinates_added = $u->update();
	print "{$coordinates_added} coordinates were added to catalog {$catalog_id}.\n" ;
}

?>