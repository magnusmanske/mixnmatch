#!/usr/bin/php
<?PHP

$testing = 0 ;

$year_patterns = [
	294 => '|(\d{4})|' ,
	486 => '|(\d{4})|' ,
	781 => '|(\d{4})|' ,
	1066 => '|of (\d{4})|' ,
	1337 => '|\((\d{4})\)|' ,
	1548 => '|\((\d{4})\)|' ,
	1740 => '| (\d{4}),|' ,
	1757 => '|(\d{4})|' ,
	#1810 => '|^[A-Z]+ *. *(\d{4}) *,|' ,
	1976 => '|^(\d{4})|' ,
	2002 => '|^(\d{4})|' ,
	2043 => '|^(\d{4})|' ,
	2048 => '|(\d{4})|' ,
	2194 => '|^(\d{4})|' ,
	2508 => '|\((\d{4})\)|' ,
	2509 => '| (\d{4}),|' ,
	2511 => '|(\d{4})$|' ,
	2526 => '|\((\d{4})\)|' ,
	2852 => '|Year: (\d{4})|' ,
	2929 => '|, (\d{4})$|' ,
	2949 => '|(\d{4})$|' ,
	3137 => '| {\d{4})\)$|' ,
	3141 => '|- (\d{4}) -|' ,
	3268 => '|^(\d{4})|' ,
] ;

$year_props = [] ; # Default: P577

require_once dirname(__DIR__) . '/vendor/autoload.php';

if ( !isset($argv[1] ) ) die ( "Catalog ID required\n" ) ;
$catalog = $argv[1] * 1 ; # eg 3268
if ( $catalog == 0 ) die ( "Bad catalog ID {$argv[1]}\n" ) ;

$mnm = new MixNMatch\MixNMatch ;

function item_matches_year ( $q , $year_from_description ) {
	global $mnm , $year_props , $catalog , $testing ;
	if ( $year_from_description*1 == 0 ) return false ; # Paranoia
	#if ( $testing ) print "MATCHING {$q} against year {$year_from_description}\n" ;
	$i = $mnm->wil->getItem ( $q ) ;
	if ( !isset($i) ) return false ;
	$prop = 'P577' ;
	if ( isset($year_props[$catalog]) ) $prop = $year_props[$catalog] ;
	#if ( $testing ) print "USING PROP {$prop}\n" ;
	$pd = $i->getClaims ( $prop ) ;
	if ( count($pd) == 0 ) return false ;
	foreach ( $pd AS $d ) {
		$timestamp = $d->mainsnak->datavalue->value->time ;
		if ( !preg_match ( '/^\+(\d+)/' , $timestamp , $m ) ) continue ;
		$year = $m[1] ;
		#if ( $testing ) print "COMPARING {$year_from_description} and {$year}\n" ;
		if ( $year*1 == $year_from_description*1 ) return true ;
	}
	return false ;
}

function setMatch ( $id , $q ) {
	global $mnm , $testing ;
	if ( $testing ) print "https://mix-n-match.toolforge.org/#/entry/{$id} => https://www.wikidata.org/wiki/{$q}\n" ;
	else $mnm->setMatchForEntryID ( $id , $q , 4 , true , false ) ;
}

function extractYearFromDescription ( $desc ) {
	global $year_patterns , $catalog , $testing ;
	if ( !isset($year_patterns[$catalog]) ) die ( "No year_pattern set for {$catalog}\n" ) ;
	#if ( $testing ) print "DESC: {$desc} => " ;
	if ( preg_match ( $year_patterns[$catalog] , $desc , $m ) ) {
		$year = $m[1] ;
		#if ( $testing ) print "{$year}\n" ;
		return $year ;
	} else {
		#if ( $testing ) print "NOPE\n" ;
	}

}


function check_automatches () {
	global $mnm , $catalog , $testing;
	$sql = "SELECT id,ext_desc,q FROM entry WHERE catalog={$catalog} AND user=0" ;
	if ( $testing ) print "{$sql}\n" ;
	$result = $mnm->getSQL ( $sql ) ;
	$entries = [] ;
	$qss = [] ;
	while($o = $result->fetch_object()) {
		$entries[$o->id] = $o ;
		$q = 'Q'.$o->q ;
		$qss[$q] = $q ;
	}
	$qss = array_chunk ( $qss , 500 ) ;
	foreach ( $qss AS $qs ) {
		$mnm->wil = new WikidataItemList ;
		$mnm->wil->loadItems ( $qs ) ;

		foreach ( $entries AS $e ) {
			$the_year = extractYearFromDescription ( $e->ext_desc ) ;
			if ( !isset($the_year ) ) continue ;
			if ( !item_matches_year('Q'.$e->q,$the_year) ) continue ;
			setMatch ( $e->id , 'Q'.$e->q ) ;
		}
	}
	unset ( $qqs ) ;
}

function check_multimatches () {
	global $mnm , $catalog , $testing;
	$sql = "SELECT entry_id AS id,ext_desc,candidates FROM multi_match,entry WHERE entry.catalog={$catalog} AND (user=0 OR user is null) AND entry.id=entry_id" ;
	if ( $testing ) print "{$sql}\n" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) {
		$the_year = extractYearFromDescription ( $o->ext_desc ) ;
		if ( !isset($the_year ) ) continue ;
		$entries[$o->id] = $o ;
		$qs = [] ;
		foreach ( explode ( ',' , $o->candidates ) AS $q ) {
			$q = 'Q'.$q ;
			$qs[$q] = $q ;
		}
		$mnm->wil = new WikidataItemList ;
		$mnm->wil->loadItems ( $qs ) ;
		$matches = [] ;
		foreach ( $qs AS $q ) {
			if ( item_matches_year($q,$the_year) ) $matches[] = $q ;
		}
		if ( count($matches) != 1 ) continue ;
		$q = $matches[0] ;
		setMatch ( $o->id , $q ) ;
	}
}

check_automatches() ;
check_multimatches() ;
$mnm->queue_job($catalog,'microsync');

?>