#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;

$catalogs = [1337,2043,3457,6195,6196,486,6471];
if ( isset($argv[1]) ) $catalogs = [$argv[1]*1];
$types = ["Q11424"];


$sql = 'SELECT id,ext_name,`type`,REGEXP_REPLACE(ext_desc,"^.*?(\\\\d{4}).*$","\\\\1") AS year
FROM entry
WHERE catalog IN ('.implode(',',$catalogs).')
AND (q is null or user=0)
AND ext_desc RLIKE "\\\\d{4}"
AND `type` IN ("'.implode('","',$types).'")' ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) {
	$label = $o->ext_name ;
	$year = $o->year ;
	$type = $o->type ;

	$sparql = <<<SPARQL
	SELECT DISTINCT ?item
		WITH {
		  SELECT * WHERE {
		    BIND ("{$label}" AS ?searchfor)
		  }
		} AS %p
		WITH {
		  SELECT ?item
		  WHERE {
		    INCLUDE %p
		    BIND (CONCAT("\"", ?searchfor, "\"") AS ?searchstr)
		    SERVICE wikibase:mwapi {
		      bd:serviceParam wikibase:endpoint "www.wikidata.org" .
		      bd:serviceParam wikibase:api "Generator" .
		      bd:serviceParam mwapi:generator "search" .
		      bd:serviceParam mwapi:gsrsearch ?searchstr .
		      bd:serviceParam mwapi:gsrlimit "max" .
		      bd:serviceParam mwapi:gsrnamespace "0" .
			  bd:serviceParam mwapi:gsrprop "" .
		      ?item wikibase:apiOutputItem mwapi:title .
		    }
		  }
		} AS %i
		WHERE {
		  INCLUDE %i
		  INCLUDE %p
		  ?item (wdt:P31/(wdt:P279*)) wd:{$type}; rdfs:label ?itemLabel ; wdt:P577 ?date .
		  FILTER(CONTAINS(LCASE(str(?itemLabel)), LCASE(?searchfor)))
		  FILTER ( year(?date)={$year} )
		}
	SPARQL;
	$items = $mnm->tfc->getSPARQLitems($sparql);
	if ( count($items)==0 ) {
		# print "No item found for: {$label} ({$year})\n";
		continue;
	}
	if ( count($items)>1 ) {
		# print "Multiple items found for: {$label} ({$year}) : ".implode(" ",$items)."\n";
		continue;
	}
	$q = $items[0];
	print "https://mix-n-match.toolforge.org/#/entry/{$o->id} => https://www.wikidata.org/wiki/{$q}\n";
	$mnm->setMatchForEntryID ( $o->id , $q , 4 , true , false ) ;
}

?>