#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';

function normalize_url ( $url ) {
	return preg_replace ( '|^https:|' , 'http:' , $url ) ;
}

$mnm = new MixNMatch\MixNMatch ;

if ( !isset($argv[1]) ) die ( "Needs catalog parameter\n" ) ;
$catalog_id = $argv[1] * 1 ;


$sql = "SELECT * FROM catalog WHERE id={$catalog_id}" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) $catalog = $o ;

if ( isset($catalog->wd_prop) ) die ( "Catalog #{$catalog} has a WD property\n") ;
if ( !isset($catalog->source_item) ) die ( "Catalog #{$catalog} has no source item\n") ;
$source_q = 'Q' . $catalog->source_item ;

$sql = "SELECT * FROM entry WHERE catalog={$catalog_id} AND q IS NOT NULL AND user>0 AND ext_url!=''" ;
$result = $mnm->getSQL ( $sql ) ;
$entries = [] ;
while($o = $result->fetch_object()) {
	$entries[$o->id] = $o ;
	$qs['Q'.$o->q] = 1 ;
}

$qs = array_keys($qs) ;
$mnm->wil->loadItems($qs) ;
foreach ( $entries AS $id => $e ) {
	$q = 'Q' . $e->q ;
	$i = $mnm->wil->getItem($q) ;
	if ( !isset($i) ) continue ;

	# Check P973
	$p973 = $i->getStrings("P973") ;
	foreach ( $p973 AS $k => $url ) $p973[$k] = normalize_url($url) ;
	if ( in_array(normalize_url($e->ext_url), $p973) ) continue ;

	# Check P1343
	if ( $i->hasTarget('P1343',$source_q) ) continue ;

	# Output command
	$qs_command = "{$q}\tP1343\t{$source_q}\tP2699\t\"{$e->ext_url}\"" ;
	print "{$qs_command}\n" ;
}

?>