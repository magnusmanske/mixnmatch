#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

$mnm = new MixNMatch\MixNMatch ;
$mnm->tfc->getQS('mixnmatch:taxonCreatorMatcher','',true) ;

function getOrCreateTaxon ( $name , $rank = '' ) {
	global $mnm , $commands ;
	$commands = [] ;
	$results = $mnm->getSearchResults ( '"'.$name.'"' , 'P31' , 'Q16521' ) ;
	if ( count($results) == 1 ) return $results[0]->title ;
	if ( count($results) > 1 ) {
		print "{$name}: " ;
		foreach ( $results AS $r ) print "https://www.wikidata.org/wiki/{$r->title} ; " ;
		print "\n" ;
		return ;
	}

	$commands = [ 'CREATE' ] ;
	$commands[] = "LAST	P31	Q16521" ;
	$commands[] = "LAST	P225	\"{$name}\"" ;
	if ( $rank != '' ) $commands[] = "LAST	P105	$rank" ;
	foreach ( ['en','de','fr','es','it','pt','nl'] AS $lang ) $commands[] = "LAST	L{$lang}	\"{$name}\"" ;
	return 'LAST' ;
}

function addOrCreateForNames ( &$names , $rank ) {
	global $mnm ;
	foreach ( $names AS $name ) {
		$q = getOrCreateTaxon ( $name , $rank ) ;
		if ( !isset($q) ) continue ;

		$entries = [] ;
		$sql = "SELECT * FROM entry WHERE ext_name='" . $mnm->escape($name) . "' AND `type`='Q16521' AND (q is null or user=0)" ;
		$result = $mnm->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			$entries[] = $o ;
			$catalog = $mnm->loadCatalog ( $o->catalog , true ) ;
			if ( !isset($catalog) or !isset($catalog->data()->wd_prop) or isset($catalog->data()->wd_qual) ) continue ;
			$commands[] = "{$q}	P".$catalog->data()->wd_prop."	\"{$o->ext_id}\"" ;
		}

		if ( $q != 'LAST' ) continue ; # HACK; ONLY NEW ITEMS
		$nq = $mnm->tfc->runCommandsQS ( $commands ) ;
		if ( $q == 'LAST' and !isset($nq) ) {
			print "FAILED TO RUN COMMANDS FOR {$name}:\n" ;
			print_r ( $commands ) ;
			continue ;
		}
		if ( $q == 'LAST' ) $q = $nq ;

		foreach ( $entries AS $o ) {
			$mnm->setMatchForEntryID ( $o->id , $q , 4 , true , false ) ;
		}
	}
}


$rows = [];
$sql = "SELECT * FROM common_names_taxon" ;
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) $rows[] = $o ;

foreach ( $rows AS $o ) {
	# Get all unmatched entries with this name
	$entry_ids = [];
	$sql = "SELECT id FROM entry WHERE ext_name='".$mnm->escape($o->name)."' AND q IS NULL AND `type`='Q16521'" ;
	$result = $mnm->getSQL ( $sql ) ;
	while ( $x = $result->fetch_object() ) $entry_ids[] = $x->id ;
	if ( count($entry_ids) < 3 ) continue;

	# Check if this exists on WD already
	$commands = [];
	$q = getOrCreateTaxon($o->name,'Q7432'); # Species only
	if ( $q != 'LAST' ) continue;

	# Create QS commands
	$commands = $mnm->getCreateItemCommandsForEntries($entry_ids);
	$has_taxon_rank = false;
	foreach ( $commands AS $c ) $has_taxon_rank = $has_taxon_rank || str_contains($c,'P105');
	if ( !$has_taxon_rank ) $commands[] = "LAST\tP105\tQ7432";
	// print_r($commands);

	# Create new item
	$nq = $mnm->tfc->runCommandsQS ( $commands ) ;
	if ( !isset($nq) or trim("{$nq}")=='Q' ) {
		print "Creation failed for {$o->name}\n";
		continue;
	}
	print "Created new item https://www.wikidata.org/wiki/{$nq}\n";

	# Match entries to new item
	foreach ( $entry_ids AS $entry_id ) {
		$mnm->setMatchForEntryID ( $entry_id , $nq , 4 , true , false ) ;
	}
}


/*
# Species
$names = [] ;
$sql = 'SELECT ext_name,count(*) AS cnt FROM entry WHERE catalog IN (566,648,500,1302,540,916,755,611,1174,1401,364,361,1005,231,1178,238,78,968,680,827,287,679,392,314,780,505,783,255) AND (q is null or user=0) AND `type`="Q16521" AND ext_name LIKE "% %" AND ext_name NOT LIKE "% % %" AND ext_name NOT LIKE "%×%" AND ext_name NOT LIKE "x %" GROUP BY ext_name HAVING cnt>1' ;
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) $names[] = $o->ext_name ;
addOrCreateForNames ( $names , 'Q7432' ) ;
*/
/*
# Subspecies
$names = [] ;
$sql = 'SELECT ext_name,count(*) AS cnt FROM entry WHERE catalog IN (566,648,500,1302,540,916,755,611,1174,1401,364,361,1005,231,1178,238,78,968,680,827,287,679,392,314,780,505,783,255) AND (q is null or user=0) AND `type`="Q16521" AND ext_name LIKE "% % %" AND ext_name NOT LIKE "% % % %" AND ext_name NOT LIKE "% var %" AND ext_name NOT LIKE "% var. %" AND ext_name NOT LIKE "%×%" AND ext_name NOT LIKE "x %" GROUP BY ext_name HAVING cnt>1' ;
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) $names[] = $o->ext_name ;
addOrCreateForNames ( $names , 'Q68947' ) ;
*/

?>