#!/usr/bin/php
<?PHP

namespace MixNMatch ;

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';


class WRDCsync {
	protected $last_timestamp = '20230607000000';
	protected $mnm ;

	function __construct ( $mnm = '' ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
	}

	protected function get_wrdc_api_responses($params) {
		$url = "https://wdrc.toolforge.org/api.php?format=jsonl&{$params}";
		$headers = ['Accept: text/csv'];
		set_time_limit(0);

		$agent = 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0';

		$fh = tmpfile();
		$callback = function ($ch, $data) use ($fh){ return fwrite($fh, $data); } ;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_VERBOSE, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_WRITEFUNCTION, $callback);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_exec($ch);
		curl_close($ch);

		# Flush and reset file position to start
		fflush($fh);
		fseek($fh,0);

		while (($line = fgets($fh)) !== false) {
			if ( $line=='' ) continue;
			yield json_decode($line);
		}
		fclose($fh);
		yield from [];
	}

	protected function yesterday() {
		return $this->mnm->getFutureTimestamp(-60*60*24);
	}

	public function sync_redirects() {
		$kv_key = 'wdrc_'.__FUNCTION__;
		$last_timestamp = $this->mnm->get_kv_value($kv_key,$this->yesterday());
		$new_timestamp = $last_timestamp;
		$redirects = [];
		foreach ( $this->get_wrdc_api_responses("action=redirects&since={$last_timestamp}") as $j ) {
			$from = preg_replace('|\D|','',$j->item);
			$to = preg_replace('|\D|','',$j->target);
			$redirects[$from] = $to ;
			if ( $new_timestamp < $j->timestamp ) $new_timestamp = $j->timestamp ;
		}
		if ( count($redirects)>0 ) {
			$old_ids = implode(',',array_keys($redirects));
			$sql = "SELECT `id`,`q` FROM `entry` WHERE `q` IN ({$old_ids})" ;
			$q_ids = [];
			$result = $this->mnm->getSQL ( $sql ) ;
			while($o = $result->fetch_object()){
				if ( !isset($q_ids[$o->q]) ) $q_ids[$o->q] = [];
				$q_ids[$o->q][] = $o->id;
			}
			foreach ( $q_ids AS $old_q => $ids ) {
				$ids = implode(',',$ids);
				$new_q = $redirects[$old_q];
				$sql = "UPDATE `entry` SET `q`={$new_q} WHERE `q`={$old_q} AND `id` IN ({$ids})" ;
				$this->mnm->getSQL ( $sql ) ;
			}
		}
		$this->mnm->set_kv_value($kv_key,$new_timestamp);
	}

	protected function update_catalogs ( $catalog_ids ) {
		foreach ( $catalog_ids AS $catalog_id ) {
			$catalog = new Catalog ( $catalog_id , $this->mnm );
			$catalog->updateStatistics();
		}
	}

	public function apply_deletions() {
		$kv_key = 'wdrc_'.__FUNCTION__;
		$last_timestamp = $this->mnm->get_kv_value($kv_key,$this->yesterday());
		$new_timestamp = $last_timestamp;
		$deleted_q = [];
		foreach ( $this->get_wrdc_api_responses("action=deletions&since={$last_timestamp}") as $j ) {
			$deleted_q[] = preg_replace('|\D|','',$j->item);
			if ( $new_timestamp < $j->timestamp ) $new_timestamp = $j->timestamp ;
		}
		if ( count($deleted_q)>0 ) {
			$deleted_q = implode(',',$deleted_q);
			$sql = "SELECT DISTINCT `catalog` FROM `entry` WHERE `q` IN ({$deleted_q})" ;
			$catalog_ids = [];
			$result = $this->mnm->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) $catalog_ids[] = $o->catalog;
			$sql = "UPDATE `entry` SET `q`=NULL,`user`=NULL,`timestamp`=NULL WHERE `q` IN ({$deleted_q})" ;
			$this->mnm->getSQL ( $sql ) ;
			$this->update_catalogs ( $catalog_ids ) ;
		}
		$this->mnm->set_kv_value($kv_key,$new_timestamp);
	}

	protected function get_prop2catalog_ids() {
		$prop2catalog_ids = [] ;
		$sql = "SELECT `id`,`wd_prop` FROM `catalog` WHERE `wd_prop` IS NOT NULL AND `wd_qual` IS NULL AND `active`=1" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			if ( !isset($prop2catalog_ids[$o->wd_prop]) ) $prop2catalog_ids[$o->wd_prop] = [];
			$prop2catalog_ids[$o->wd_prop][] = $o->id;
		}
		return $prop2catalog_ids ;
	}

	protected function sync_property($prop,$catalog_ids,$last_timestamp) {
		$highest_timestamp = $last_timestamp;
		if ( count($catalog_ids)==0 ) return $highest_timestamp ; # Paranoia

		# Get items to investigate
		$items_to_load = [];
		foreach ( $this->get_wrdc_api_responses("action=properties&properties={$prop}&since={$last_timestamp}") as $j ) {
			if ( !isset($j->item) ) continue;
			if ( $highest_timestamp < $j->timestamp ) $highest_timestamp = $j->timestamp;
			$items_to_load[$j->item] = $j->item ;
		}

		# Get the latest item revisions, and the string values for the property
		$propval2item = [];
		$wil = new \WikidataItemList;
		$wil->loadItems($items_to_load);
		foreach ( $items_to_load AS $q ) {
			$i = $wil->getItem($q);
			if ( !isset($i) ) continue ;
			foreach ( $i->getStrings($prop) AS $prop_value ) {
				if ( !isset($propval2item[$prop_value]) ) $propval2item[$prop_value] = [] ;
				$propval2item[$prop_value][$q] = preg_replace('|\D|','',$q)*1;
			}
		}

		# Get property values as set in MnM
		$propval_safe = [];
		foreach ( $propval2item AS $prop_value => $item_ids ) {
			$propval2item[$prop_value] = array_values($item_ids); # Convert form associative to simple array
			if ( count($item_ids) > 1 ) continue ; # TODO handle multiple items having the same property value
			$propval_safe[] = $this->mnm->escape($prop_value);
		}
		if ( count($propval_safe) == 0 ) return $highest_timestamp ;
		$catalog_ids_str = implode(',',$catalog_ids) ;
		$propval_safe_str = implode('","',$propval_safe) ;
		$sql = "SELECT * FROM `entry` WHERE `catalog` IN ({$catalog_ids_str}) AND `ext_id` IN (\"{$propval_safe_str}\")" ;
		$prelim_or_unmatched = [] ;
		$result = $this->mnm->getSQL ( $sql ) ;
		$found_identical = 0 ;
		while($o = $result->fetch_object()) {
			if ( !isset($propval2item[$o->ext_id]) ) continue ; # Paranoia
			if ( !isset($o->user) or $o->user==0 ) $prelim_or_unmatched[$o->id] = $o ;
			else if ( $o->user>0 and in_array($o->q,$propval2item[$o->ext_id]) ) $found_identical++ ; # No one cares but still...
			else {
				// TODO addIssue
				// Entry $o->id has property value $o->ext_id matched to $o->q but Wikidata has it on (one of) $propval2item[$o->ext_id]
			}
		}

		// TODO record entries missing in MnM?

		# Set full match for prelim-/auto-matched entries
		foreach ( $prelim_or_unmatched AS $entry_id => $o ) {
			$q = $propval2item[$o->ext_id][0];
			$this->mnm->setMatchForEntryID ( $entry_id , $q , 4 , true , false ) ;
			// print $this->mnm->getEntryURL($entry_id) . " => https://www.wikidata.org/wiki/Q{$q}\n" ;
		}

		return $highest_timestamp;
	}

	public function sync_properties() {
		$kv_key = 'wdrc_'.__FUNCTION__;
		$last_timestamp = $this->mnm->get_kv_value($kv_key,$this->yesterday());
		$new_timestamp = $this->mnm->getCurrentTimestamp();

		$prop2catalog_ids = $this->get_prop2catalog_ids();
		foreach ( $prop2catalog_ids AS $prop => $catalog_ids ) {
			$timestamp = $this->sync_property($prop,$catalog_ids,$last_timestamp);
			if ( $new_timestamp > $timestamp ) $new_timestamp = $timestamp; # Lowest of the highest
		}
		$this->mnm->set_kv_value($kv_key,$new_timestamp);
	}
}

$wdrc = new WRDCsync;
$wdrc->sync_redirects();
$wdrc->apply_deletions();
$wdrc->sync_properties();

?>