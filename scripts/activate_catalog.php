#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';

$cat_source = $argv[1] * 1 ;
$mode = isset($argv[2]) ? $argv[2] * 1 : 1 ;

if ( $cat_source == 0 ) die ( "USAGE: activate_catalog.php SOURCE_CATALOG_ID [0/1]\n" ) ;

$mnm = new MixNMatch\MixNMatch ;
$catalog = new MixNMatch\Catalog ( $cat_source , $mnm ) ;
$catalog->activate ( $mode ) ;

?>