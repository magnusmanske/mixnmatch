#!/usr/bin/php
<?PHP

# This can be run again to update any time.

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog_id = 6159 ;

$mnm = new MixNMatch\MixNMatch ;
$catalog = new MixNMatch\Catalog ( $catalog_id , $mnm ) ;
$ext_ids = $catalog->get_all_external_ids() ;

# Get author URLs from authors catalog
$sql = "SELECT id,ext_name,ext_url FROM entry WHERE catalog=2501" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) {
	// print_r($o);
	$html = file_get_contents($o->ext_url);
	preg_match_all('|<td><a href="/notices/oeuvre/(\d+)/">(.+?)</a></td>|', $html, $matches, PREG_SET_ORDER);
	// print_r($matches);
	foreach ( $matches AS $m) {
		$id = $m[1];
		$label = $m[2];
		if ( !isset($ext_ids[$id]) ) {
			$ext_ids[$id] = 1 ;
			$entry = (object) [
				'catalog' => $catalog_id ,
				'id' => $id ,
				'name' => $label ,
				'desc' => "Work by {$o->ext_name}" ,
				'url' => "https://pinakes.irht.cnrs.fr/notices/oeuvre/{$id}/",
				'aux' => [ ['P31','Q47461344'] ]
			];
			// print_r($entry);
			$entry_id = $mnm->addNewEntry($entry);
			$mnm->linkEntriesViaProperty($entry_id,50,$o->id); # Author
		}

	}
}

?>