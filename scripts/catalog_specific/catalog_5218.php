#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog_id = 5218 ;

$mnm = new MixNMatch\MixNMatch ;
$helper = new MixNMatch\Helper ( $catalog_id , $mnm ) ;

# Get existing
$existing = [] ;
$sql = "SELECT `ext_id` FROM `entry` WHERE `catalog`={$catalog_id}" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) $existing[$o->ext_id] = 1 ;

function load_page ( $url ) {
	global $had_that , $load_next , $helper ;
	$html = $helper->get_contents_from_url($url,true);
	if ( !preg_match_all('|\?taxid=(\d+)"|', $html, $m) ) return $html ;
	if ( !isset($m) or !isset($m[1]) or !is_array($m[1]) ) return $html ;
	foreach ( $m[1] AS $id ) {
		if ( isset($had_that[$id]) ) continue ;
		if ( in_array($id, $load_next) ) continue ;
		$load_next[] = $id ;
	}
	return $html ;
}

$had_that = [] ;
$load_next = ['1'] ;
#load_page("https://www.boldsystems.org/index.php/TaxBrowser_Home");
while ( count($load_next)>0 ) {
	$id = array_pop($load_next) ;
	if ( isset($had_that[$id]) ) continue ;
	$had_that[$id] = 1 ;
	$url = "https://www.boldsystems.org/index.php/Taxbrowser_Taxonpage?taxid={$id}" ;
	$html = load_page ( $url ) ;
	if ( $id == 1 ) continue ; # No adding root
	if ( preg_match('|<title>(.+?)[\<\|]|',$html,$m) ) $title = trim($m[1]) ;
	else continue ;

	$o = (object) [
		'catalog' => $catalog_id ,
		'id' => $id ,
		'name' => $title ,
		'desc' => '' ,
		'url' => $url ,
		'type' => 'Q16521'
	] ;

	if ( preg_match('|([a-z]+).wikipedia.org/wiki/(.+?)\"|',$html,$m) ) {
		$site = "{$m[1]}wiki" ;
		$wikipage = $m[2] ;
		$wd_url = "https://www.wikidata.org/w/api.php?format=json&action=wbgetentities&sites={$site}&titles=".urlencode($wikipage);
		$j = json_decode(file_get_contents($wd_url));
		if ( isset($j->entities) ) {
			$qs = array_keys((array)$j->entities) ;
			if ( count($qs) == 1 ) {
				$o->q = $qs[0] ;
			}
		}
	}

	$mnm->addNewEntry ( $o ) ;
}


?>