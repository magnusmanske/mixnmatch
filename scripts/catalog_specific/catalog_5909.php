#!/usr/bin/php
<?PHP

# This will update empty descriptions from the source, nothing more!

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog_id = 5909 ;

$mnm = new MixNMatch\MixNMatch ;
$catalog = new MixNMatch\Catalog ( $catalog_id , $mnm ) ;
$ext_ids = $catalog->get_all_external_ids() ;


function get_url($url) {
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true ) ;
	$html = curl_exec($ch);
	$html = mb_convert_encoding($html, 'html-entities', 'utf-8'); 
	return $html ;
}

function setLocation($entry_id,$url) {
	global $mnm ;
	$html = get_url($url) ;
	if ( preg_match('|https://maps.googleapis.com/maps/api/staticmap\?center=([0-9.]+),([0-9.]+)|',$html,$m) ) {
		$lat = $m[1]*1 ;
		$lon = $m[2]*1 ;
		$mnm->setLocation ( $entry_id , $lat , $lon ) ;
	}
}

function addEntry($id,$name,$region,$reqion_q) {
	global $catalog_id , $mnm , $ext_ids ;

	$type = "Q811979" ; # Architectural structure
	if ( preg_match('|^Πύργος |',$name) ) $type = 'Q12518' ;
	if ( preg_match('|^Κάστρο |',$name) ) $type = 'Q23413' ;
#	if ( preg_match('|^Fortification |',$name) ) $type = 'Q57821' ;
	if ( preg_match('|^Bo{0,1}urtzi |',$name) ) $type = 'Q895263' ;
	$o = (object) [
		'catalog' => $catalog_id ,
		'id' => $id ,
		'name' => $name ,
		'desc' => $region ,
		'url' => "https://www.kastra.eu/kastregr.php?kastro={$id}",
		'type' => $type ,
		'aux' => []
	];
	if ( $reqion_q!='' ) $o->aux[] = ["P131",$reqion_q] ;

	if ( isset($ext_ids[$id]) ) { # Had that
		setLocation ( $ext_ids[$id] , $o->url ) ; # Update location
		return ;
	}
	print "{$o->name}\n";
	$entry_id = $mnm->addNewEntry($o);
	setLocation ( $entry_id , $o->url ) ;
	$ext_ids[$id] = $entry_id ;
}

function getRegionQ($region) {
	global $mnm ;
	$results = $mnm->getSearchResults ( $region , "P31" , "Q1234255" ) ;
	if ( count($results)==1 ) return $results[0]->title ;
	return '' ;
}

for ( $region_id = 1 ; $region_id <= 15 ; $region_id++ ) {
	$url = "https://www.kastra.eu/fndre2gr.php?cstreg={$region_id}" ;
	$html = get_url($url) ;
	$dom = new DomDocument();
	$dom->loadHTML($html);
	$xpath = new DOMXpath($dom);
	$elements = $xpath->query("//table[@class='castr']/tr");
	if (is_null($elements)) {
		print "No elements\n" ;
		continue ;
	}
	$first = true ;
	foreach ($elements as $element) {
		if ( $first ) {
			$first = false ;
			continue ;
		}
		$e2 = $xpath->query("td/a",$element);
		if (count($e2)==0) {
			$current_heading = $element->nodeValue ;
			$current_heading_q = getRegionQ($current_heading);
			print "HEADING: {$current_heading}\n" ;
		} else if (count($e2)==1) {
			$a = $e2[0] ;
			$name = $a->nodeValue ;
			$id = "" ;
			foreach ( $a->attributes as $attribute ) {
				if ( $attribute->name!="href") continue ;
				if ( !preg_match('|\?kastro=(.+)$|',$attribute->value,$m) ) continue ;
				$id = $m[1] ;
			}
			addEntry($id,$name,$current_heading,$current_heading_q) ;
#			print "- {$name}:{$id}\n" ;
		} else {
			print "TOO MANY LINKS\n" ;
			continue ;
		}
	}
}

?>