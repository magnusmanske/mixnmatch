#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../vendor/autoload.php';

function get_url($url) {
	$command = <<<COMMAND
	curl -s '{$url}' --compressed -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/115.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: en-GB,en;q=0.7,de;q=0.3' -H 'Accept-Encoding: gzip, deflate, br' -H 'Referer: https://artuk.org/discover/artists' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' -H 'Sec-GPC: 1'
	COMMAND;
	$out = null;
	if ( !exec(trim($command),$out) ) {
		print "Could not get {$url}:\n" ;
		// print_r($out);
		return [''];
	}
	return $out;
}

function shuffle_assoc(&$array) {
	$keys = array_keys($array);
	shuffle($keys);
	foreach($keys as $key) $new[$key] = $array[$key];
	$array = $new;
}

# Sometimes loses DB connection. This re-connects as a fallback.
function addNewEntry($o) {
	global $mnm;
	try {
		$ret = $mnm->addNewEntry ( $o );
		return $ret ;
	} catch (Exception $e) {
		$mnm->dbmConnect(false);
		$mnm->dbmConnect(true);
		return $mnm->addNewEntry ( $o );
	}
}

function process_artists($url) {
	global $mnm, $artists, $catalog_id_artists;
	$out = get_url($url);
	$html = implode(" ",$out);
	if ( !preg_match_all('|<li class="item track" id="(.+?)">.*?<span class="title">(.+?)<|',$html,$matches,PREG_SET_ORDER) ) return;
	foreach ( $matches AS $m ) {
		$o = (object) [
			'catalog' => $catalog_id_artists ,
			'id' => $m[1] ,
			'name' => trim(preg_replace('|\s+|',' ',$m[2])) ,
			'desc' => '' ,
			'url' => "https://artuk.org/discover/artists/{$m[1]}" ,
			'type' => 'Q5'
		] ;
		if ( isset($artists[$o->id]) ) continue ;
		if ( preg_match('|^(.+?) \((.+)\)$|',$o->name,$n) ) {
			$o->name = $n[1] ;
			$o->desc = $n[2] ;
		}
		$artists[$o->id] = addNewEntry ( $o ) ;
	}
}

function artists_by_nationality() {
	# Get artist nationalities
	$url = 'https://artuk.org/discover/artists/view_as/grid';
	$out = get_url($url);
	$html = implode("\n",$out);
	if ( !preg_match_all('|<li><span class=" pass" data-value="(.+?)" data-group="nationality"|',$html,$m) ) {
		print "Nationalities no match\n" ;
		exit(0);
	}
	$nationalities = array_map(function ($a) { return strtolower(preg_replace('|[ \(\)]+|','-',$a)); }, $m[1]);
	shuffle($nationalities);

	# Get all artists per nationality
	foreach ( $nationalities AS $nationality ) {
		print "{$nationality}\n";
		foreach ( ["sort_by/name.keyword/order/asc/page","sort_by/name.keyword/order/desc/page","page"] AS $part ) {
			$url = "https://artuk.org/discover/artists/view_as/grid/search/nationality:{$nationality}/{$part}/200";
			print "{$url}\n" ;
			process_artists($url);
		}
	}

	// # The British, everyone
	// $num = 100 ;
	// process_artists('https://artuk.org/discover/artists/view_as/grid/search/nationality:british/sort_by/name.keyword/order/asc/page/'.$num);
	// process_artists('https://artuk.org/discover/artists/view_as/grid/search/nationality:british/sort_by/name.keyword/order/desc/page/'.$num);
	// process_artists('https://artuk.org/discover/artists/view_as/grid/search/nationality:british/page/'.$num);
}

function artists_by_pattern() {
	foreach(range('a','z') as $letter1){
		foreach(range('a','z') as $letter2){
			$url = "https://artuk.org/discover/artists/view_as/grid/search/keyword:{$letter1}{$letter2}/page/10";
			print "{$url}\n" ;
			process_artists($url);
		}
	}
}

$artist_url_cache = [];

function get_artist_for_artwork($artwork_url) {
	global $catalog_id_artists, $artist_url_cache;
	if ( isset($artist_url_cache[$url]) ) return $artist_url_cache[$url];
	$out = get_url($artwork_url);
	$html = implode(" ",$out);
	if ( !preg_match('|<h2 class="artist">.*?<a href="https://artuk.org/discover/artists/(.+?)">(.+?)</a>|',$html,$m) ) return;
	$o = (object) [
		'catalog' => $catalog_id_artists ,
		'id' => $m[1] ,
		'name' => trim(preg_replace('|\s+|',' ',$m[2])) ,
		'desc' => '' ,
		'url' => "https://artuk.org/discover/artists/{$m[1]}" ,
		'type' => 'Q5'
	] ;
	if ( preg_match('|^(.+?) \((.+)\)$|',$o->name,$n) ) {
		$o->name = $n[1] ;
		$o->desc = $n[2] ;
	}
	$artist_url_cache[$url] = $o ;
	return $o ;
}

function process_artworks_page($url) {
	global $mnm, $catalog_id_artworks, $ext_ids_artworks, $artists;
	$out = get_url($url);
	$html = implode(" ",$out);
	if ( !preg_match('|<ul class="listing-grid.*?>(.+?)</ul>|',$html,$m) ) return ;
	$html = $m[1];
	if ( !preg_match_all('|<li.*?>.*?<a href="https://artuk.org/discover/artworks/(.+?)">.*?<div class="title">(.+?)</div>(.*?)</li>|',$html,$matches,PREG_SET_ORDER) ) return ;
	foreach ( $matches AS $m ) {
		$o = (object) [
			'catalog' => $catalog_id_artworks ,
			'id' => $m[1] ,
			'name' => trim($m[2]) ,
			'desc' => trim(preg_replace('|\s+|',' ',strip_tags($m[3]))) ,
			'url' => "https://artuk.org/discover/artworks/{$m[1]}" ,
			'type' => 'Q838948',
			'aux' => [],
		] ;
		if ( preg_match('|^(.+?) *<span class="date">(.+?)</span> *$|',$o->name,$n) ) {
			$o->name = $n[1];
			$o->desc = "{$n[2]}; {$o->desc}";
			if ( preg_match('|^\d{4}$|',$n[2]) ) $o->aux[] = [ 'P571' , "+{$n[2]}-01-01T00:00:00Z/9" ] ;
		}
		$o->name = trim(preg_replace('|\s+|',' ',strip_tags($o->name))) ;
		if ( !isset($ext_ids_artworks[$o->id]) ) {
			$artwork_entry_id = addNewEntry ( $o ) ;
			print "Artwork: https://mix-n-match.toolforge.org/#/entry/{$artwork_entry_id}\n";
			$ext_ids_artworks[$o->id] = $artwork_entry_id;
		} else {
			$artwork_entry_id = $ext_ids_artworks[$o->id];
		}

		$artist = get_artist_for_artwork($o->url);
		if ( !isset($artist) ) continue; # Something went wrong getting the artist from the artwork page
		if ( isset($artists[$artist->id]) ) continue ; # We already have this artist
		$artists[$artist->id] = addNewEntry ( $artist ) ;
		print "Artist: https://mix-n-match.toolforge.org/#/entry/{$artists[$artist->id]}\n";
		$mnm->linkEntriesViaProperty($artwork_entry_id,'P170',$artists[$artist->id]);
	}
}

function process_random_artworks() {
	return process_artworks_page('https://artuk.org/discover/artworks');
}

function process_artworks_by_year() {
	$step = 50;
	foreach (range(0,1000,$step) as $year) {
		try {
			$year_to = $year+$step-1;
			$url = "https://artuk.org/discover/artworks/view_as/grid/search/date-from:{$year}--date-to:{$year_to}/page/200";
			print "{$url}\n" ;
			process_artworks_page($url);
		} catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
}

function artworks_by_artist() {
	global $mnm, $artists, $catalog_id_artworks, $ext_ids_artworks;
	// $artists = ['rembrandt-van-rijn-16061669'=>153450477] ; # TESTING
	shuffle_assoc($artists);
	$added = 0;
	foreach ( $artists as $artist_ext_id => $artist_entry_id ) {
		$url = "https://artuk.org/discover/artworks/search/actor:{$artist_ext_id}/page/500";
		$out = get_url($url);
		$html = implode(" ",$out);
		if ( !preg_match_all('|<li .*?>(.+?)</li>|',$html,$artworks) ) continue ;
		foreach ( $artworks[1] AS $artwork ) {
			if ( !preg_match('|<a id="(.+?)".*?<span class="title">(.*?)</span>(.*?)</div>|',$artwork,$m) ) continue ;
			$o = (object) [
				'catalog' => $catalog_id_artworks ,
				'id' => $m[1] ,
				'name' => trim($m[2]) ,
				'desc' => trim(preg_replace('|\s+|',' ',strip_tags($m[3]))) ,
				'url' => "https://artuk.org/discover/artworks/{$m[1]}" ,
				'type' => 'Q838948'
			] ;
			if ( isset($ext_ids_artworks[$o->id]) ) continue ;
			try {
				$artwork_entry_id = addNewEntry ( $o ) ;
				$added += 1 ;
				print "#{$added}: https://mix-n-match.toolforge.org/#/entry/{$artwork_entry_id}\n";
				$ext_ids_artworks[$o->id] = $artwork_entry_id;
				$mnm->linkEntriesViaProperty($artwork_entry_id,'P170',$artist_entry_id);
			} catch (Exception $e) {
				echo 'Caught exception: ',  $e->getMessage(), "\n";
			}
		}
	}
}

function update_authors() {
	global $catalog_artists;
	artists_by_nationality();
	artists_by_pattern();
	$catalog_artists->updateStatistics() ;
}

function match_artworks_with_creator() {
	global $mnm, $catalog_id_artworks ;
	$sql = "SELECT entry.id AS entry_id,ext_id,ext_name,creator.aux_name AS creator_q,substr(ts.aux_name,2,4) as `year` FROM entry,auxiliary creator,auxiliary ts WHERE catalog={$catalog_id_artworks} AND (q is NULL or user=0) AND creator.entry_id=entry.id AND creator.aux_p=170 AND ts.entry_id=entry.id AND ts.aux_p=571" ;
	$result = $mnm->getSQL ( $sql ) ;
	while (($o = $result->fetch_object())) {
		// $o = (object) ["ext_name"=>"Portrait figure of Elisabeth Frink","creator_q"=>"Q5423877","year"=>" 1956"]; # TESTING
		// if ( preg_match('|"|',$o->ext_name) ) continue ; # Artwork name contains double quotes, skip TODO FIXME
		$query = "\"{$o->ext_name}\" haswbstatement:P170={$o->creator_q}" ;
		// print "{$query}\n" ;
		$results = $mnm->getSearchResults($query);
		$items = [] ;
		foreach ( $results as $r ) $items[] = $r->title ;
		if ( count($items)==0 ) continue;

		$wil = new WikidataItemList ;
		$wil->loadItems ( $items );
		$year_matches = [];
		foreach ( $items AS $q) {
			$i = $wil->getItem ( $q ) ;
			if ( !isset($i) ) continue ;
			if ( $i->getLabel('en')!=$o->ext_name ) continue ; # Exact label match
			$claims = $i->getClaims("P571");
			if ( count($claims)!=1 ) continue;
			$claim = array_pop($claims);
			try {
				$ts = $claim->mainsnak->datavalue->value->time ;
			} catch (Exception $e) {
				continue;
			}
			if ( !preg_match('|^\+(\d{4})-|',$ts,$m) ) continue ;
			$year = $m[1] ;
			if ( $year!=$o->year ) continue ;
			print "Match for https://mix-n-match.toolforge.org/#/entry/{$o->entry_id} => https://www.wikidata.org/wiki/{$q}\n" ;
			$mnm->setMatchForEntryID ( $o->entry_id , $q , 4 , true , false ) ;
		}
	}
}

function collections_from_artworks() {
	global $mnm, $catalog_id_collections , $ext_ids_artworks ;
	$catalog = new MixNMatch\Catalog ( $catalog_id_collections , $mnm ) ;
	$ext_ids_collections = $catalog->get_all_external_ids() ;
	foreach ( $ext_ids_artworks AS $ext_id=>$artwork_entry_id ) {
		$url = "https://artuk.org/discover/artworks/{$ext_id}";
		$out = get_url($url);
		$html = implode(" ",$out);
		if ( !preg_match_all('|<div class="info">\s*<h1>(.+?)</h1>\s*<a href="https://artuk.org/visit/collection/(.+?)" class="link">|',$html,$matches,PREG_SET_ORDER) ) continue;
		foreach ( $matches AS $m ) {
			$o = (object) [
				'catalog' => $catalog_id_collections ,
				'id' => $m[2] ,
				'name' => trim($m[1]) ,
				'desc' => '' ,
				'url' => "https://artuk.org/visit/collection/".urlencode($m[2]) ,
				'type' => 'Q7328910'
			] ;
			if ( isset($ext_ids_collections[$o->id]) ) continue ;
			$ext_ids_collections[$o->id] = addNewEntry($o);
			$mnm->linkEntriesViaProperty($artwork_entry_id,'P195',$ext_ids_collections[$o->id]);
		}
	}
}


# Init

$mnm = new MixNMatch\MixNMatch ;

$catalog_id_artists = 2 ; # Artists
$catalog_id_artworks = 2936; # Artwork
$catalog_id_collections = 5995 ; # Collections

$catalog_artists = new MixNMatch\Catalog ( $catalog_id_artists , $mnm ) ;
$artists = $catalog_artists->get_all_external_ids() ;
// update_authors();

# Get artworks
// match_artworks_with_creator(); exit(0);

$catalog = new MixNMatch\Catalog ( $catalog_id_artworks , $mnm ) ;
$ext_ids_artworks = $catalog->get_all_external_ids() ;


// collections_from_artworks();

// while ( 1 ) process_random_artworks();
// process_artworks_by_year(); exit(0);
// artworks_by_artist();

$catalog->updateStatistics() ;

?>