#!/usr/bin/php
<?PHP

# THIS CAN UPDATE TEH CATALOG, ONLY TAKES A FEW SECONDS

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog_id = 5335 ;

$mnm = new MixNMatch\MixNMatch ;

# Get existing
$existing = [] ;
$sql = "SELECT `ext_id` FROM `entry` WHERE `catalog`={$catalog_id}" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) $existing[$o->ext_id] = 1 ;

$html = file_get_contents('https://museweb.dcs.bbk.ac.uk/browseproperties');
$html = str_replace("\n", ' ', $html) ;
$html = preg_replace('/^.+var museums=/','',$html) ;
$html = preg_replace('/];.*$/',']',$html) ;
$j = json_decode($html) ;

foreach ( $j AS $m ) {
	$o = (object) [
		'catalog' => $catalog_id ,
		'id' => $m[0] ,
		'name' => trim($m[1]) ,
		'desc' => $m[2] ,
		'url' => "https://museweb.dcs.bbk.ac.uk/Museum/{$m[0]}" ,
		'location' => (object) ["lat"=>$m[3],"lon"=>$m[4]],
		'type' => 'Q33506'
	] ;
	if ( isset($existing[$o->id]) ) continue ;
	$mnm->addNewEntry ( $o ) ;
	$existing[$o->id] = 1 ;	
}

?>