#!/usr/bin/php
<?PHP

# This will update empty descriptions from the source, nothing more!

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog = 3642 ;

$mnm = new MixNMatch\MixNMatch ;

$sql = "SELECT * FROM entry WHERE catalog={$catalog} AND ext_desc=''" ;
$result = $mnm->getSQL ( $sql ) ;
while ($o = $result->fetch_object()) {
	$url = $o->ext_url ;
	$html = file_get_contents ( $url ) ;
	$query = "|'.@([^.']+\.htm\#".$o->ext_id.")'|" ;
	if ( !preg_match ( $query , $html , $m ) ) continue ;
	$url = 'http://www.gcatholic.org/saints/data/' . $m[1] ;
	$html = file_get_contents ( $url ) ;
	$query = '|<tr id="'.$o->ext_id.'">(.+?)</table>|' ;
	if ( !preg_match ( $query , $html , $m ) ) continue ;
	$text = str_replace ( '<' , ' <' , $m[1] ) ;
	$text = strip_tags ( $text ) ;
	$text = trim ( preg_replace ( '|\s+|' , ' ' , $text ) ) ;
	if ( $text == '' ) continue ;
	$mnm->setDescriptionForEntryID ( $o->id , $text ) ;
}

?>