#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;

$catalog_collection = 3353 ;
$catalog_object = 3354 ;
$catalog_person = 3355 ;
$new_collections_only = false ; # for initial seeding

$sql = "SELECT * FROM entry WHERE catalog={$catalog_collection} AND (q is null or q>0)" ;
#$sql .= " AND id=86375159" ; # TESTING
$collections = [] ;
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) $collections[] = $o ;

$known_object_ids = [] ;
$sql = "SELECT ext_id FROM entry WHERE catalog={$catalog_object}" ;
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) $known_object_ids[$o->ext_id] = 1 ;

$known_person_ids = [] ;
$sql = "SELECT ext_id FROM entry WHERE catalog={$catalog_person}" ;
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) $known_person_ids[$o->ext_id] = 1 ;

foreach ( $collections AS $collection ) {
	$page = 1 ;
	$max_pages = 1 ;
	while ( $page <= $max_pages ) {
		$url = "http://www.nationaltrustcollections.org.uk/results?Collections={$collection->ext_id}&Page={$page}" ;
		#print "Loading {$url}\n" ;
		$html = file_get_contents($url) ;
		$html = preg_replace ( '/\s+/' , ' ' , $html ) ;
		if ( preg_match ( '|&Page=(\d+)" rel="nofollow">&raquo;</a>|' , $html , $m ) ) {
			$max_pages = $m[1] * 1 ;
		}
		if ( FALSE !== preg_match_all('|<a href="/object/(\d+)">.*?<h3>(.*?)</h3> *<p>(.*?)</p>.*?</a>|',$html,$objects,PREG_SET_ORDER) ) {
			foreach ( $objects AS $o ) {
				# Create object
				$e = (object) [
					'catalog' => $catalog_object ,
					'id' => trim($o[1]) ,
					'name' => trim($o[2]) ,
					'desc' => trim($o[3]) ,
					'type' => 'Q17537576'
				] ;
				if ( isset($known_object_ids[$e->id]) and $new_collections_only ) {
					$page = $max_pages+5 ;
					break ;
				}
				if ( isset($known_object_ids[$e->id]) ) continue ;
				$known_object_ids[$e->id] = 1 ;
				$e->url = "http://www.nationaltrustcollections.org.uk/object/{$e->id}" ;
				$e->name = preg_replace ( '|[ \.;,]+$|' , '' , $e->name ) ;
				
				# Add object
				if ( $e->name == '' or $e->id == '' ) continue ;
				$entry_id_object = $mnm->addNewEntry ( $e ) ;

				# Add collection relationship
				$mnm->linkEntriesViaProperty ( $entry_id_object , 'P195' , $collection->id ) ;

				if ( $e->desc == '' ) continue ; # No creator

				# Create person
				$basename = html_entity_decode ( $e->desc ) ;
				$e = (object) [
					'catalog' => $catalog_person ,
					'id' => $basename ,
					'name' => $basename ,
					'desc' => '' ,
					'type' => 'Q5'
				] ;
				if ( isset($known_person_ids[$e->id]) ) continue ;
				$known_person_ids[$e->id] = 1 ;
				$e->url = "http://www.nationaltrustcollections.org.uk/results?Maker=".urlencode($e->id) ;
				if ( preg_match ( '|^(.+?) *(\(.*)$|' , $e->name , $m ) ) { # Split dates etc into description
					$e->name = trim($m[1]) ;
					$e->desc = trim($m[2]," \t\n\r\0\x0B\(\)") ;
				}
				if ( preg_match ( '|^(.+?) *(\[.*)$|' , $e->name , $m ) ) { # Split dates etc into description
					$e->name = trim($m[1]) ;
					$e->desc = trim($m[2]," \t\n\r\0\x0B\(\)") . '; ' . $e->desc ;
				}
				if ( preg_match ( '/^(Sir|Dr\.*) (.+?)$/' , $e->name , $m ) ) { # Remove "Sir"
					$e->name = $m[2] ;
				}
				if ( preg_match ( '/(University|workshop| and |Ltd|Limited|magazine|pottery|style|manufact|fluid|Co\.|\bart\b|photo|industr|\binc\b|union|manner|circle|company|\b[Tt]he\b|attributed|&|probably|society|factory|unknown|artist|\bpossibl[ye]\b|\bafter\b|british|school|\bcourt\b)/' , $e->name ) ) {
					unset ( $e->type ) ;
				}

				# Add person
				if ( $e->name == '' or $e->id == '' ) continue ;
				$entry_id_person = $mnm->addNewEntry ( $e ) ;

				# Add creator relationship
				$mnm->linkEntriesViaProperty ( $entry_id_object , 'P170' , $entry_id_person ) ;
			}
		} else {
			$max_pages = -1 ;
			print "Failed at {$url}\n" ;
		}
		$page++ ;
	}
}

$mnm->updateCatalogs ( [ $catalog_person , $catalog_object ] ) ;
$job_id = $mnm->queue_job ( $catalog_person , 'update_person_dates' ) ;
$mnm->queue_job ( $catalog_person , 'match_person_dates' , $job_id ) ;
$mnm->queue_job ( $catalog_person , 'automatch_by_search' ) ;
$mnm->queue_job ( $catalog_object , 'automatch_by_search' ) ;

?>