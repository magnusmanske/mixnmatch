#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog_id = 2844 ;

function process_file ( $filename ) {
	global $existing , $mnm , $catalog_id ;
	$text = file_get_contents($filename) ;
	$text = explode ( "\n" , $text ) ;
	$sql = [] ;
	$affected = 0 ;
	foreach ( $text AS $line ) {
		if ( !preg_match ( '|^(\d+)\t(.+?)\t(.*)$|' , $line , $m ) ) continue ;
		$o = (object) [
			'catalog' => $catalog_id ,
			'id' => $m[1] ,
			'name' => $m[2] ,
			'desc' => $m[3] ,
			'url' => "http://www.namenforschung.net/id/name/{$m[1]}" ,
			'type' => 'Q101352'
		] ;
		if ( isset($existing[$o->id]) ) continue ;
		$mnm->addNewEntry ( $o ) ;
		$affected++ ;
		$existing[$o->id] = 1 ;
	}
	return $affected ;
}

$mnm = new MixNMatch\MixNMatch ;

# Get existing
$existing = [] ;
$sql = "SELECT `ext_id` FROM `entry` WHERE `catalog`={$catalog_id}" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) $existing[$o->ext_id] = 1 ;

# Get all current
$affected = 0 ;
$affected += process_file ( 'http://www.namenforschung.net/alle.csv' ) ;
$affected += process_file ( 'http://www.namenforschung.net/neu.csv' ) ;


if ( $affected > 0 ) {
	$catalog = new MixNMatch\Catalog ( $catalog_id , $mnm ) ;
	$catalog->updateStatistics() ;
	$mnm->queue_job ( $catalog_id , 'microsync' ) ;
	$mnm->queue_job ( $catalog_id , 'automatch' ) ;
}

?>