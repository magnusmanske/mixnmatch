#!/usr/bin/php
<?php
// Load the HTML from the URL
$url = 'https://inpress.lib.uiowa.edu/feminae/Default.aspx';
$html = file_get_contents($url);

// Create a DOMDocument object and load the HTML
$dom = new DOMDocument();
$dom->loadHTML($html);

// Find all the <tr> elements in the table with class "basic_table"
$table = $dom->getElementById('basic_table');
$rows = $table->getElementsByTagName('tr');

// Loop through the rows
foreach ($rows as $row) {
  // Get the link and extract the name and ID
  $link = $row->getElementsByTagName('a')->item(0);
  $name = $link->nodeValue;
  $id = $link->getAttribute('href');

  // Get the description from the <td> element
  $description = $row->getElementsByTagName('td')->item(1)->nodeValue;

  // Print the data
  echo "Name: $name\n";
  echo "ID: $id\n";
  echo "Description: $description\n";
}
?>

