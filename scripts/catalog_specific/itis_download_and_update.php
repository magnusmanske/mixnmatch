#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog_id = 4543 ;
$mnm = new MixNMatch\MixNMatch ;
$data_dir = '/data/project/mix-n-match/scripts/catalog_specific' ;
$tmp_file = "{$data_dir}/itis.sqlite.zip" ;

# Download current file
if ( 1 ) {
	$cmd = "curl -s -o ${tmp_file} https://www.itis.gov/downloads/itisSqlite.zip" ;
	exec($cmd) ;
}

# Unpack archive
if ( file_exists($tmp_file) ) {
	$cmd = "unzip -qq ${tmp_file}" ;
	exec($cmd) ;
	unlink($tmp_file) ;
}

# Find sqlite file
$sqlite_dir = '' ;
$sqlite_file = '' ;
$files = scandir ( $data_dir ) ;
foreach ( $files as $file ) {
	if ( !preg_match('|^itisSqlite(\d+)$|',$file,$m) ) continue ;
	$sqlite_dir = "${data_dir}/${file}" ;
	$sqlite_file = "${sqlite_dir}/ITIS.sqlite" ;
}
if ( $sqlite_file == '' ) die ( "Could not find sqlite directory in ${data_dir}.\n") ;

# Get existing
$existing = [] ;
$sql = "SELECT `ext_id` FROM `entry` WHERE `catalog`={$catalog_id}" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) $existing[$o->ext_id] = 1 ;

# Read entries and add new ones
$changed = false ;
$sqlite = new SQLite3($sqlite_file);
$sql = "SELECT * FROM longnames" ;
$results = $sqlite->query($sql);
while ($row = $results->fetchArray()) {
	if ( isset($existing[$row['tsn']]) ) continue ;
	$o = (object) [
		'catalog' => $catalog_id ,
		'id' => $row['tsn'] ,
		'name' => $row['completename'] ,
		'url' => 'https://www.itis.gov/servlet/SingleRpt/SingleRpt?search_topic=TSN&search_value='.$row['tsn'] ,
		'type' => 'Q16521'
	] ;
	$mnm->addNewEntry ( $o ) ;
	$changed = true ;
}
$sqlite->close();
exec ( "rm -rf ${sqlite_dir}" ) ;

if ( $changed ) {
	$catalog = new MixNMatch\Catalog ( $catalog_id , $mnm ) ;
	$catalog->updateStatistics() ;
	$jid = $mnm->queue_job ( $catalog_id , 'taxon_matcher' ) ;
	$mnm->queue_job ( $catalog_id , 'automatch' , $jid ) ;
	$mnm->queue_job ( $catalog_id , 'microsync' , $jid ) ;
}

?>