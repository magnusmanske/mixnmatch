#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog_authors = 3274 ;
$catalog_works = 3275 ;

$mnm = new MixNMatch\MixNMatch ;

# Get existing work IDs
$work_ids = [] ;
$sql = "SELECT ext_id FROM entry WHERE catalog={$catalog_works}" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) $work_ids[$o->ext_id] = $o->ext_id ;

# Get authors
$sql = "SELECT * FROM entry WHERE catalog={$catalog_authors}" ;
#$sql .= " AND id=85868044" ; # TESTING
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$author_entry_id = $o->id ;
	$url = $o->ext_url ;
	$html = file_get_contents ( $url ) ;
	$html = preg_replace ( '/\s+/' , ' ' , $html ) ;
	if ( !preg_match_all ( '|<li>\s*<a href="\.\./\.\./(.+?)\.html">(.+?)</a>(.*?)</li>|' , $html , $matches , PREG_SET_ORDER ) ) continue ;
	foreach ( $matches AS $m ) {
		$o = (object) [
			'catalog' => $catalog_works ,
			'id' => $m[1] ,
			'url' => "https://www.projekt-gutenberg.org/{$m[1]}.html" ,
			'name' => trim(html_entity_decode($m[2])) ,
			'desc' => trim(html_entity_decode($m[3])) ,
			'type' => 'Q47461344'
		] ;
		if ( isset($work_ids[$o->id]) ) continue ;
		$work_entry_id = $mnm->addNewEntry ( $o ) ;
		$mnm->linkEntriesViaProperty ( $work_entry_id , 'P50' , $author_entry_id ) ;
		$work_ids[$o->id] = $o->id ;
	}
}


?>