#!/usr/bin/php
<?PHP

# This will update empty descriptions from the source, nothing more!

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../public_html/php/gpoint/gpoint.php';
require_once dirname(__DIR__) . '/../vendor/autoload.php';

function utmToLatLng($zone, $easting, $northing, $northernHemisphere=true) {
    if (!$northernHemisphere) $northing = 10000000 - $northing;

    $a = 6378137;
    $e = 0.081819191;
    $e1sq = 0.006739497;
    $k0 = 0.9996;

    $arc = $northing / $k0;
    $mu = $arc / ($a * (1 - pow($e, 2) / 4.0 - 3 * pow($e, 4) / 64.0 - 5 * pow($e, 6) / 256.0));

    $ei = (1 - pow((1 - $e * $e), (1 / 2.0))) / (1 + pow((1 - $e * $e), (1 / 2.0)));

    $ca = 3 * $ei / 2 - 27 * pow($ei, 3) / 32.0;

    $cb = 21 * pow($ei, 2) / 16 - 55 * pow($ei, 4) / 32;
    $cc = 151 * pow($ei, 3) / 96;
    $cd = 1097 * pow($ei, 4) / 512;
    $phi1 = $mu + $ca * sin(2 * $mu) + $cb * sin(4 * $mu) + $cc * sin(6 * $mu) + $cd * sin(8 * $mu);

    $n0 = $a / pow((1 - pow(($e * sin($phi1)), 2)), (1 / 2.0));

    $r0 = $a * (1 - $e * $e) / pow((1 - pow(($e * sin($phi1)), 2)), (3 / 2.0));
    $fact1 = $n0 * tan($phi1) / $r0;

    $_a1 = 500000 - $easting;
    $dd0 = $_a1 / ($n0 * $k0);
    $fact2 = $dd0 * $dd0 / 2;

    $t0 = pow(tan($phi1), 2);
    $Q0 = $e1sq * pow(cos($phi1), 2);
    $fact3 = (5 + 3 * $t0 + 10 * $Q0 - 4 * $Q0 * $Q0 - 9 * $e1sq) * pow($dd0, 4) / 24;

    $fact4 = (61 + 90 * $t0 + 298 * $Q0 + 45 * $t0 * $t0 - 252 * $e1sq - 3 * $Q0 * $Q0) * pow($dd0, 6) / 720;

    $lof1 = $_a1 / ($n0 * $k0);
    $lof2 = (1 + 2 * $t0 + $Q0) * pow($dd0, 3) / 6.0;
    $lof3 = (5 - 2 * $Q0 + 28 * $t0 - 3 * pow($Q0, 2) + 8 * $e1sq + 24 * pow($t0, 2)) * pow($dd0, 5) / 120;
    $_a2 = ($lof1 - $lof2 + $lof3) / cos($phi1);
    $_a3 = $_a2 * 180 / pi();

    $latitude = 180 * ($phi1 - $fact1 * ($fact2 + $fact3 + $fact4)) / pi();

    if (!$northernHemisphere) $latitude = -$latitude;

    if ( $zone>0 ) $longitude = 6 * $zone - 183.0 - $_a3;
    else $longitude = 3 - $_a3;
    // $longitude = (($zone > 0) and (6 * $zone - 183.0) or 3.0) - $_a3;

    return [$latitude, $longitude];

}


$catalog_id = 5958 ;

$mnm = new MixNMatch\MixNMatch ;
#$catalog = new MixNMatch\Catalog ( $catalog_id , $mnm ) ;

if ( true ) { # Update P131
	$sql = "select ext_desc,group_concat(id) AS ids,count(*) as cnt from entry WHERE catalog={$catalog_id}
		AND id NOT IN (SELECT id FROM vw_aux WHERE catalog={$catalog_id} AND aux_p=131)
		group by ext_desc 
		order by cnt desc" ;
	$result = $mnm->getSQL($sql) ;
	while($o = $result->fetch_object()) {
		$results = $mnm->getSearchResults ( $o->ext_desc , "P31" , "Q2074737" ) ;
		if ( count($results)==0 ) continue;
		$q = $results[0]->title ;
		$ids = explode(',',$o->ids);
		print("{$o->ext_desc} => {$q}\n");
		#print_r($ids);
		foreach ( $ids AS $id ) $mnm->setAux($id,131,$q);
	}
}

if ( true ) { # Update locations
	$sql = "SELECT * FROM entry WHERE catalog={$catalog_id} AND id NOT IN (SELECT id FROM vw_location WHERE catalog={$catalog_id})";
	$result = $mnm->getSQL($sql) ;
	while($o = $result->fetch_object()) {
		$url = $o->ext_url;
		$html = file_get_contents($url);
		if ( !preg_match('|href="gmaps\.php\?x=(.+?)&y=(.+?)&|', $html,$m) ) continue;
		$easting = preg_replace('|,.*$|','',$m[1])*1;
		$northing = preg_replace('|,.*$|','',$m[2])*1;
		$ret = utmToLatLng(30,$easting,$northing);
		$lat = $ret[0];
		$lon = $ret[1];
		if ( $lon>=-1.5 ) { # Zone switching hack
			$ret = utmToLatLng(29,$easting,$northing);
			$lat = $ret[0];
			$lon = $ret[1];
		}
		$mnm->setLocation ( $o->id , $lat , $lon ) ;
	}
}

?>