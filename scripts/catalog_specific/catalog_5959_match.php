#!/usr/bin/php
<?PHP

# This can be run again to update any time.

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog_id = 5959 ;

$mnm = new MixNMatch\MixNMatch ;
#$catalog = new MixNMatch\Catalog ( $catalog_id , $mnm ) ;

$sql = "SELECT * FROM entry WHERE q IS NULL AND catalog={$catalog_id}" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) {
	$html = file_get_contents($o->ext_url);
	if ( !preg_match('|href="http://en.wikipedia.org/wiki/(.+?)"|',$html,$m) ) continue;
	$url = "https://www.wikidata.org/w/api.php?format=json&action=wbgetentities&sites=enwiki&titles={$m[1]}" ;
	$j = json_decode(file_get_contents($url));
	$items = array_keys((array)$j->entities);
	$q = $items[0];
	if ( $q=='-1' ) continue ;
	print ("{$o->ext_name} => {$q}\n");
	$mnm->setMatchForEntryID ( $o->id , $q , 4 , true , false ) ;
}

?>