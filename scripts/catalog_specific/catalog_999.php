#!/usr/bin/php
<?PHP

# This can be run again to update any time.

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog_id = 999 ;

$mnm = new MixNMatch\MixNMatch ;

$catalog = new MixNMatch\Catalog ( 71 , $mnm ) ; # Authors
$ext_ids = $catalog->get_all_external_ids();

$sql = "SELECT id,ext_url FROM entry WHERE catalog={$catalog_id} AND NOT EXISTS (SELECT * FROM mnm_relation WHERE entry_id=entry.id AND property=50)" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) {
	$html = file_get_contents($o->ext_url);
	preg_match_all('|<a href="/ebooks/author/(\d+)" rel="marcrel:aut"|', $html, $matches, PREG_SET_ORDER);
	foreach ( $matches AS $m) {
		$author_ext_id = $m[1];
		if ( !isset($ext_ids[$author_ext_id]) ) continue ;
		$author_entry_id = $ext_ids[$author_ext_id];
		// print "{$o->id} => 50 => {$author_entry_id}\n";
		$mnm->linkEntriesViaProperty($o->id,50,$author_entry_id);
	}
}

?>