#!/usr/bin/php
<?PHP

# This will update empty descriptions from the source, nothing more!

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog_id = 3943 ;

$mnm = new MixNMatch\MixNMatch ;
$catalog = new MixNMatch\Catalog ( $catalog_id , $mnm ) ;
$ext_ids = $catalog->get_all_external_ids() ;

# UPSTREAM API ONLY ALLOWS FOR THE FIRST 10K ENTRIES

$start = 0 ;
$max = 1; # Will be replaced in first loop
$batch_size = 1000;
while ( 1 ) {
	print "{$start} of {$max}\n" ;
	if ( $start > $max ) break;
	$command = <<<COMMAND
	curl 'https://artuk.org/discover/artists/search/nationality:austrian/page/2?_ajax=1' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/115.0' -H 'Accept: application/json, text/javascript, */*; q=0.01' -H 'Accept-Language: en-GB,en;q=0.7,de;q=0.3' -H 'Accept-Encoding: gzip, deflate, br' -H 'X-Requested-With: XMLHttpRequest' -H 'DNT: 1' -H 'Connection: keep-alive'
	COMMAND;
	$out = null;
	if ( !exec(trim($command),$out) ) break;
	$j = json_decode($out[0]);
	if ( !isset($j) or !isset($j->totalHit) ) {
		print_r($out);
		sleep(15);
		continue;
	}
	$max = $j->totalHit;
	$start += $batch_size;
	foreach ( $j->hits as $h ) {
		$o = (object) [
			'catalog' => $catalog_id ,
			'id' => $h->id ,
			'name' => strip_tags($h->botanicalName) ,
			'desc' => $h->entityDescription ,
			'url' => " https://www.rhs.org.uk/Plants/{$h->id}/wd/Details" ,
			'type' => 'Q16521'
		] ;
		if ( isset($ext_ids[$o->id]) ) continue;
		if ( preg_match('|^(.+?) *\((.+)\)$|',$o->name,$m) ) {
			$o->name = $m[1] ;
			$o->desc = "({$m[2]}) {$o->desc}" ;
		}
		$mnm->addNewEntry ( $o ) ;
	}
	// sleep(5);
}

$catalog->updateStatistics() ;
$mnm->queue_job ( $catalog_id , 'taxon_matcher' ) ;

?>