#!/usr/bin/php
<?PHP

# This can be run again to update any time.

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog_id = 5959 ;

$mnm = new MixNMatch\MixNMatch ;
$catalog = new MixNMatch\Catalog ( $catalog_id , $mnm ) ;
$ext_ids = $catalog->get_all_external_ids() ;

$id = '';
$label = '' ;
$desc = '' ;
foreach(file("https://www.eionet.europa.eu/gemet/exports/latest/en/gemet-definitions.rdf") as $line) {
	if ( preg_match('|<rdf:Description rdf:about="concept/(\d+)">|',$line,$m) ) $id = $m[1] ;
	else if ( preg_match('|<skos:prefLabel>(.+?)</skos:prefLabel>|',$line,$m) ) $label = $m[1] ;
	else if ( preg_match('|<skos:definition>(.+?)</skos:definition>|',$line,$m) ) $desc = $m[1] ;
	else if ( preg_match('|</rdf:Description>|',$line,$m) ) {
		if ( $id!='' and $label!='' ) {
			if ( !isset($ext_ids[$id]) ) {
				$ext_ids[$id] = 1 ;
				$o = (object) [
					'catalog' => $catalog_id ,
					'id' => $id ,
					'name' => $label ,
					'desc' => $desc ,
					'url' => "https://www.eionet.europa.eu/gemet/en/concept/{$id}"
				];
				$mnm->addNewEntry($o);
			}
		}
		$id = '';
		$label = '' ;
		$desc = '' ;
	}
}

?>