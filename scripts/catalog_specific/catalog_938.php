#!/usr/bin/php
<?PHP

# Adds mnm_relation for P50, parsed from from author pages.
# It does not add any entries.
# This can be run again to update any time, but should not be necessary.

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog_id = 938 ;

$mnm = new MixNMatch\MixNMatch ;

$catalog = new MixNMatch\Catalog ( $catalog_id , $mnm ) ;
$ext_ids_work = $catalog->get_all_external_ids() ;

$catalog = new MixNMatch\Catalog ( 934 , $mnm ) ;
$ext_ids_author = $catalog->get_all_external_ids() ;

print json_encode($ext_ids_work)."\n\n";
print json_encode($ext_ids_author)."\n\n";

exit(0);

# Get author URLs from authors catalog
$sql = "SELECT id,ext_url FROM entry WHERE catalog=934" ;
$sql .= " AND ext_id='2682'"; # TESTING FIXME
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) {
	print "{$o->ext_url}\n";
	$html = file_get_contents($o->ext_url);
	print "{$html}\n";
	preg_match_all ( '|href="show_title\.php\?tid=(\d+)&aid=(\d+)"|',$html,$matches, PREG_SET_ORDER);
	foreach ( $matches AS $m) {
		$work_ext_id = $m[1];
		$author_ext_id = $m[2];
		print "{$work_ext_id}\t{$author_ext_id}\n";
		if ( !isset($ext_ids_work[$work_ext_id]) ) {
			$mnm->print_err("No work {$work_ext_id}");
			continue;
		}
		if ( !isset($ext_ids_author[$author_ext_id]) ) {
			$mnm->print_err("No author {$author_ext_id}");
			continue;
		}
		$sql = "INSERT IGNORE INTO mnm_relation (entry_id,property,target_entry_id) VALUES ({$ext_ids_work[$work_ext_id]},50,{$ext_ids_author[$author_ext_id]})" ;
		print "{$sql}\n\n";
		// $mnm->getSQL ( $sql ) ;
	}

}

?>