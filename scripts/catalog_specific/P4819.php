#!/usr/bin/php
<?PHP

# THIS CAN BE USED TO QUICKLY UPDATE THE CATALOG!

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$update_mode = true ; # Will stop at the first known ID received
$catalog_id = 4837 ;
$mnm = new MixNMatch\MixNMatch ;
$catalog = new MixNMatch\Catalog ( $catalog_id , $mnm ) ;
$known_ids = $catalog->get_all_external_ids() ;


#curl 'https://portrattarkiv.se/endpoints/latest.php' -X POST -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0' -H 'Accept: application/json, text/plain, */*' -H 'Accept-Language: en-GB,en;q=0.7,de;q=0.3' --compressed -H 'Content-Type: text/plain' -H 'Origin: https://portrattarkiv.se' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Referer: https://portrattarkiv.se/latest' -H 'Sec-Fetch-Dest: empty' -H 'Sec-Fetch-Mode: cors' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-GPC: 1' --data-raw '{"size":30,"from":60}'


#$agent = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0';
$url = 'https://portrattarkiv.se/endpoints/latest.php' ;
$max = 100000 ; # Dummy to be changed later
$pos = 0 ;
$size = 1000 ;
while ( $pos < $max ) {
	/*
	$fields = [
		"from" => $pos ,
		"size" => $size
	];
	#$fields_string = http_build_query($fields);
	$ch = curl_init();

	//set the url, number of POST vars, POST data
	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_POST, true);
	#curl_setopt($ch, CURLOPT_USERAGENT, $agent);
	#curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
	curl_setopt($ch, CURLOPT_POSTFIELDS, urlencode("{\"size\":{$size},\"from\":{$pos}}"));
	curl_setopt($ch, CURLOPT_HTTPHEADER, [
		'Accept: application/json, text/plain',
		'Content-Type: text/plain',
		'Origin: https://portrattarkiv.se',
		'DNT: 1',
		'Connection: keep-alive',
		'Referer: https://portrattarkiv.se/latest',
		'Sec-Fetch-Dest: empty',
		'Sec-Fetch-Mode: cors',
		'Sec-Fetch-Site: same-origin',
		'Sec-GPC: 1'
	]);

	//So that curl_exec returns the contents of the cURL; rather than echoing it
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

	//execute post
	$result = curl_exec($ch);
	*/

	$cmd = <<<EOD
curl -s 'https://portrattarkiv.se/endpoints/latest.php' -X POST -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0' -H 'Accept: application/json, text/plain, */*' -H 'Accept-Language: en-GB,en;q=0.7,de;q=0.3' --compressed -H 'Content-Type: text/plain' -H 'Origin: https://portrattarkiv.se' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Referer: https://portrattarkiv.se/latest' -H 'Sec-Fetch-Dest: empty' -H 'Sec-Fetch-Mode: cors' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-GPC: 1' --data-raw '{"size":
EOD;
	$cmd .= "{$size},\"from\":{$pos}}'" ;
	$result = exec ( $cmd ) ;

	$j = json_decode($result);
	$max = $j->hits->total ;
	$new_entries = 0 ;
	foreach ( $j->hits->hits AS $h ) {
		$o = (object) [
			'catalog' => $catalog_id ,
			'id' => $h->_id ,
			'url' => "https://portrattarkiv.se/details/{$h->_id}" ,
			'name' => "{$h->_source->FirstName} {$h->_source->LastName}" ,
			'desc' => [] ,
			'type' => ($h->_type=='Person'?'Q5':'')
		] ;
		if ( isset($known_ids[$o->id]) ) { # Had that, sorted by last ones first, so stop
			if ( !$update_mode ) continue ;
			$pos = $max+1 ;
			break ;
		}
		if ( ($h->_source->BirthDate??'') != '' ) $o->desc[] = "born {$h->_source->BirthDate}" ;
		else if ( ($h->_source->BirthYear??'') != '' ) $o->desc[] = "born {$h->_source->BirthYear}" ;
		if ( ($h->_source->DeathDate??'') != '' ) $o->desc[] = "died {$h->_source->DeathDate}" ;
		else if ( ($h->_source->DeathYear??'') != '' ) $o->desc[] = "died {$h->_source->DeathYear}" ;
		if ( isset($h->_source->Facts) and isset($h->_source->Facts->PortraitBioText) ) {
			$o->desc[] = $h->_source->Facts->PortraitBioText[0] ;
		}
		$o->desc = implode ( '; ' , $o->desc ) ;
		$known_ids[$o->id] = $o->id ;
		$mnm->addNewEntry ( $o ) ;
		$new_entries += 1 ;
	}
	$pos += $size ;
	print "--- {$pos} of {$max}\n" ;
}

?>