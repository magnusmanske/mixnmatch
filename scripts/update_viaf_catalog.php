#!/usr/bin/php
<?php

$catalog = 2050 ;

require_once dirname(__DIR__) . '/vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;
$dbl = $mnm->tfc->openDBtool ( 'mixnmatch_large_catalogs_p' ) ;

function refreshCandidateTable () {
	global $mnm , $dbl ;
	$min_fields_not_blank = 10 ;

	$sql = "DROP TABLE viaf_counts" ;
	$mnm->tfc->getSQL ( $dbl , $sql ) ;
	$sql = "CREATE TABLE `viaf_counts` ( `ext_id` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',  `fields_not_empty` int(11) NOT NULL DEFAULT '0', PRIMARY KEY (`ext_id`), KEY `fields_not_empty` (`fields_not_empty`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" ;
	$mnm->tfc->getSQL ( $dbl , $sql ) ;
	$sql = "INSERT IGNORE INTO viaf_counts (ext_id,fields_not_empty) SELECT ext_id,(if(`B2Q`='',0,1)+if(`BAV`='',0,1)+if(`BIBSYS`='',0,1)+if(`BLBNB`='',0,1)+if(`BNC`='',0,1)+if(`BNCHL`='',0,1)+if(`BNE`='',0,1)+if(`BNF`='',0,1)+if(`BNL`='',0,1)+if(`CYT`='',0,1)+if(`DBC`='',0,1)+if(`DNB`='',0,1)+if(`EGAXA`='',0,1)+if(`ERRR`='',0,1)+if(`FAST`='',0,1)+if(`ICCU`='',0,1)+if(`ISNI`='',0,1)+if(`JPG`='',0,1)+if(`KRNLK`='',0,1)+if(`LAC`='',0,1)+if(`LC`='',0,1)+if(`LNB`='',0,1)+if(`LNL`='',0,1)+if(`MRBNR`='',0,1)+if(`N6I`='',0,1)+if(`NDL`='',0,1)+if(`NII`='',0,1)+if(`NKC`='',0,1)+if(`NLA`='',0,1)+if(`NLB`='',0,1)+if(`NLI`='',0,1)+if(`NLP`='',0,1)+if(`NLR`='',0,1)+if(`NSK`='',0,1)+if(`NSZL`='',0,1)+if(`NTA`='',0,1)+if(`NUKAT`='',0,1)+if(`PERSEUS`='',0,1)+if(`PTBNP`='',0,1)+if(`RERO`='',0,1)+if(`SELIBR`='',0,1)+if(`SRP`='',0,1)+if(`SUDOC`='',0,1)+if(`SWNL`='',0,1)+if(`VLACC`='',0,1)+if(`W2Z`='',0,1)+if(`WKP`='',0,1)+if(`XA`='',0,1)+if(`XR`='',0,1)) AS fields_not_empty FROM viaf WHERE q IS NULL HAVING fields_not_empty>=" . $min_fields_not_blank ;
	$mnm->tfc->getSQL ( $dbl , $sql ) ;
}

function checkVIAFinWikidata () { 
	global $mnm , $dbl , $catalog ;
	$chunk_size = 100 ;

	$viaf_ids = [] ;
	$sql = "SELECT ext_id FROM viaf_counts" ;
	$result = $mnm->tfc->getSQL ( $dbl , $sql ) ;
	while($o = $result->fetch_object()) $viaf_ids[] = $o->ext_id ;
	$chunks = array_chunk ( $viaf_ids , $chunk_size ) ;
	foreach ( $chunks AS $chunk ) {
		if ( count($chunk) == 0 ) continue ; # Paranoia
		$sparql = "SELECT ?q ?id { VALUES ?id { '".implode("' '",$chunk)."'} . ?q wdt:P214 ?id }" ;
		$j = $mnm->tfc->getSPARQL ( $sparql ) ;
		foreach ( $j->results->bindings AS $b ) {
			$viaf_id = $b->id->value ;
			$q = $mnm->tfc->parseItemFromURL ( $b->q->value ) ;
			$q = preg_replace('/\D/','',$q) * 1 ;

			# Update mix'n'match catalog
			$ts = $mnm->getCurrentTimestamp() ;
			$sql = "UPDATE entry SET user=4,q={$q},timestamp='{$ts}' WHERE catalog={$catalog} AND ext_id='{$viaf_id}' AND (q IS NULL or user=0)" ;
			$mnm->getSQL ( $sql ) ;

			# Update large catalogs:viaf table
			$sql = "UPDATE viaf SET q={$q} WHERE ext_id='{$viaf_id}' AND q IS NULL" ;
			$mnm->tfc->getSQL ( $dbl , $sql ) ;

			# Update large catalogs:viaf_counts table
			$sql = "DELETE FROM viaf_counts WHERE ext_id='{$viaf_id}'" ;
			$mnm->tfc->getSQL ( $dbl , $sql ) ;
		}
	}
}

function updateVIAFcatalog () {
	global $mnm , $dbl , $catalog ;

	$in_catalog = [] ;
	$sql = "SELECT ext_id FROM entry WHERE catalog={$catalog}" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) $in_catalog[$o->ext_id] = $o->ext_id ;

	$add_ids = [] ;
	$sql = "SELECT ext_id FROM viaf_counts" ;
	$result = $mnm->tfc->getSQL ( $dbl , $sql ) ;
	while($o = $result->fetch_object()) {
		if ( isset($in_catalog[$o->ext_id]) ) continue ;
		$add_ids[] = $o->ext_id ;
	}

	foreach ( $add_ids AS $viaf_id ) {
		$url = "https://viaf.org/viaf/{$viaf_id}/viaf.json" ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		if ( !isset($j->nameType) ) continue ;
		if ( !isset($j->mainHeadings) ) continue ;
		if ( !isset($j->mainHeadings->data) ) continue ;
		$o = (object) [ 'catalog' => $catalog , 'id' => $viaf_id , 'name' => '' , 'desc' => '' , 'type' => '' , 'url' => 'https://viaf.org/viaf/'.$viaf_id.'/' ] ;
		if ( $j->nameType == 'Personal' ) $o->type = 'Q5' ;
		if ( $j->nameType == 'Corporate' ) $o->type = 'Q43229' ;
		if ( isset($j->deathDate) and $j->deathDate!='0' ) $o->desc = "died:{$j->deathDate}|{$o->desc}" ;
		if ( isset($j->birthDate) and $j->birthDate!='0' ) $o->desc = "born:{$j->birthDate}|{$o->desc}" ;
		if ( isset($j->mainHeadings->data->text) ) $o->name = $j->mainHeadings->data->text ;
		else {
			foreach ( $j->mainHeadings->data AS $d ) {
				if ( !isset($d->text) ) continue ;
				if ( $o->name == '' ) $o->name = $d->text ;
				else if ( $o->desc == '' ) $o->desc = $d->text ;
				else $o->desc .= '; ' . $d->text ;
			}
		}

		$o->name = preg_replace ( '/[ 0-9\-\.\(\)]{3,}/' , ' ' , $o->name ) ;
		if ( preg_match ( '/^(.+?)\s*\.\s*$/' , $o->name , $m ) ) $o->name = $m[1] ;
		while ( preg_match ( '/^(.+), +(.+)$/' , $o->name , $m ) ) $o->name = "{$m[2]} {$m[1]}" ;

		if ( $o->name == '' ) $o->name = "VIAF {$viaf_id}" ;

		$entry_id = $mnm->addNewEntry ( $o ) ;
		$mnm->setAux ( $entry_id , 214 , $viaf_id ) ;
	}
	$sql = "UPDATE entry SET ext_name=regexp_replace(ext_name,'[ 0-9\-\.\(\)]{3,}',' ') WHERE catalog={$catalog} AND ext_name like '%1%'" ;
	$mnm->getSQL ( $sql ) ;
	$sql = "UPDATE entry SET ext_name=regexp_replace(ext_name,'^(.+), +(.+)$','\\2 \\1') WHERE catalog=2050 AND ext_name LIKE '%, %'" ;
	$mnm->getSQL ( $sql ) ;
	exec ( '/data/project/mix-n-match/scripts/person_dates/update_person_dates.php '.$catalog ) ;
}

#refreshCandidateTable() ;
#checkVIAFinWikidata() ;
updateVIAFcatalog() ;

?>
