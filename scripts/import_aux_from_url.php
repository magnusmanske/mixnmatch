#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

if ( !isset($argv[1]) ) die ( "USAGE: {$argv[0]} CATALOG_ID [testing(0,1)=0]\n" ) ;

$catalog = $argv[1] ;
if ( $catalog != 'random' ) {
	$catalog = $catalog * 1 ;
	if ( $catalog == 0 ) die ( "Bad catalog {$argv[1]}\n" ) ;
}

$testing = false ;
if ( isset($argv[2]) and $argv[2] == '1' ) $testing = true ;

$uta = new MixNMatch\UrlToAux ( '' , $testing ) ;

$uta->run ( $catalog ) ;

$uta->mnm->show_proc_info();

?>