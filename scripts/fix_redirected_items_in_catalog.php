#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
require_once dirname(__DIR__) . '/vendor/autoload.php';

if ( !isset($argv[1]) ) die("USAGE: {$argv[0]} [CATALOG_ID|ALL]\n");

$maintenance = new MixNMatch\Maintenance ;

$catalogs = $maintenance->getActiveCatalogIDsFromString($argv[1]);
foreach ( $catalogs AS $catalog_id ) {
	foreach ( $maintenance->getFullyMatchedEntryBatches($catalog_id) AS $qs ) {
		$maintenance->fixRedirectedItems($qs);
		$maintenance->unlinkDeletedItems($qs);
	}
}

?>