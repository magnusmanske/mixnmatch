#!/usr/bin/php
<?PHP
declare(strict_types=1);

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); # E_ALL|
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

$maintenance = new MixNMatch\Maintenance ( $mnm ) ;
$maintenance->updatePropertyCache();

?>