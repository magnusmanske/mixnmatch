#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

$type = 'Q5' ;

require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once ( "/data/project/mix-n-match/manual_lists/large_catalogs/shared.php" ) ;
#require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

$good_props = [ 213,214,227,846,244,236,434,496 ] ; # Not used, just as a note...

if ( !isset($argv[2]) ) die ("USAGE: create_entries_with_shared_aux.php [PROPERTY_ID or 'all'] MIN_IN_AUX [Qtype|Q5]\n" ) ;
if ( $argv[1] == 'all' ) $properties = $good_props ;
else {
	$property = preg_replace ( '/\D/' , '' , $argv[1] ) * 1 ;
	if ( $property <= 0 ) die ( "Bad property ID {$argv[1]}\n" ) ;
	$properties = [ $property ] ;
}
$min_aux = $argv[2] * 1 ;
if ( $min_aux == 0 ) die ( "Bad min_in_aux {$argv[2]}\n" ) ;
if ( isset($argv[3]) ) $type = $argv[3] ;
$allow_automatched = true ;
$testing = false ;
$verbose = true ;

$lc = new largeCatalog (2) ; # VIAF
$lc->prop2field[2]['P213'] = 'ISNI' ; // Activating for this specific purpose
$lc->prop2field[2]['P1006'] = 'NTA' ; // Activating for this specific purpose

$mnm = new MixNMatch\MixNMatch ;

$mnm->tfc->getQS('mixnmatch:CreateNewItemsWithSharedMetadata','',true) ;

# Active catalogs only
$catalogs = [] ;
$sql = 'SELECT id FROM catalog WHERE active=1' ;
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) $catalogs[] = $o->id ;
$catalogs = implode ( ',' , $catalogs ) ;


$sql = "SELECT aux_p,aux_name,count(DISTINCT catalog) AS cnt,group_concat(id) AS ids FROM vw_aux WHERE aux_p IN (" . implode(',',$properties) . ")" ;
if ( $allow_automatched ) $sql .= " AND (q IS NULL or user=0)" ;
else $sql .= " AND q IS NULL" ;
$sql .= " AND catalog IN ($catalogs)" ;
if ( $type != '' ) $sql .= " AND `type`='{$type}'" ;
$sql .= " GROUP BY aux_p,aux_name HAVING cnt>={$min_aux}" ;
if ( $testing ) $sql .= " LIMIT 5" ;
#if ( $testing ) print "{$sql}\n" ;
$sql .= " LIMIT 5";
#print "{$sql}\n" ; exit(0);

$result = $mnm->getSQL ( $sql ) ;
if ( $verbose ) print "Starting...\n" ;
while ( $o = $result->fetch_object() ) {
	if ( $verbose ) print_r($o);
	$property = $o->aux_p ;
	$ids = explode ( ',' , $o->ids ) ;
	$prop_value = trim($o->aux_name) ;
	if ( $prop_value == '' ) continue ;

	$items = $mnm->getSearchResults('',"P{$property}",$prop_value);
	if ( count($items) == 1 ) {
		$q = $items[0]->title ;
		if ( $testing ) print "Matching to existing item:\n" ;
		foreach ( $ids AS $num => $entry_id ) {
			if ( $testing ) print $mnm->getEntryURL($entry_id) . " => https://www.wikidata.org/wiki/{$q}\n" ;
			else $mnm->setMatchForEntryID ( $entry_id , $q , 4 , true , false ) ;
		}
		if ( $verbose ) print "Matched to existing items\n" ;
		continue ;
	}
	if ( count($items) > 1 ) {
		print "Multiple items with P{$property}:'{$prop_value}' :\n" ;
		print_r ( $items ) ;
		continue ;
	}

	$all_right = true ;
	$joined_commands = [] ;
	foreach ( $ids AS $num => $id ) {
		try {
			$entry = new MixNMatch\Entry ( $id , $mnm ) ;
			$entry = $entry->core_data() ;
		} catch (Exception $e) {
			continue ;
		}
		if ( $verbose ) print "Generating commands for {$id}..." ;
		$commands = $mnm->getCreateItemForEntryCommands ( $entry , $lc , true ) ;
		if ( $verbose ) print "done.\n" ;
		if ( !isset($commands) ) {
			if ( !$testing and preg_match('|/entry/(\d+) might already exist as ["(Q\d+)"]|',$mnm->last_error,$m) and count($mnm->possible_items)==1) {
				$mnm->setMatchForEntryID ( $id , $mnm->possible_items[0] , 4 , true , false ) ;
				if ( $verbose ) print "Setting " . $mnm->getEntryURL($id) . " as https://www.wikidata.org/wiki/{$mnm->possible_items[0]}\n" ;
				$all_right = false ;
				continue ;
			}
			print "PROBLEM CREATING COMMANDS: {$mnm->last_error}\n" ;
			print_r ( $entry ) ;
			$all_right = false ;
			break ;
		}
		foreach ( $commands AS $c ) {
			if ( $c == 'CREATE' ) continue ;
			if ( $num > 0 and preg_match ( '/^LAST\t[LD]/' , $c ) ) continue ; # Only labels/descriptions from first set
			if ( !preg_match ( '/^(LAST\t.+?\t[^\t]+)(.*?)$/' , $c , $m ) ) continue ;
			$statement = $m[1] ;
			$references = $m[2] ;
			if ( isset($joined_commands[$statement]) ) {
				if ( $references != '' ) $joined_commands[$statement] .= $references ;
			} else {
				$joined_commands[$statement] = $c ;
			}
		}
	}
	if ( !$all_right ) {
		if ( $verbose ) print "PROBLEMS WITH " . join(',',$ids) . "\n" ;
		continue ;
	}
	$commands = array_values ( $joined_commands ) ;
	array_unshift ( $commands , 'CREATE' ) ;
	if ( $testing ) {
		print_r ( $commands ) ;
		continue ;
	}

	$q = $mnm->tfc->runCommandsQS ( $commands ) ;
	if ( $verbose ) print "CREATED NEW ITEM FOR " . join(',',$ids) . "\n" ;
	if ( isset($q) ) {
		foreach ( $ids AS $id ) {
			$mnm->setMatchForEntryID ( $id , $q , 4 , true ) ;
		}
	}

}

?>
