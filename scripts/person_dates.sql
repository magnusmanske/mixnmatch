connect s51434__mixnmatch_p;

DROP TABLE IF EXISTS tmp1;

CREATE TABLE tmp1 AS
SELECT ext_name,
catalog,
if(q IS NULL OR user=0,0,1) as matched,
concat(ext_name,'|',year_born,'-',year_died) as nbd,
entry_id
FROM entry,person_dates 
WHERE entry_id=entry.id AND year_born!='' AND year_died!=''
AND catalog NOT IN (SELECT id FROM catalog where active=0);

CREATE INDEX tmp1a ON tmp1 (nbd);

DELETE FROM tmp1 WHERE nbd IN (select nbd from tmp1 where matched=1);

TRUNCATE common_names_dates;

INSERT IGNORE INTO common_names_dates (`name`,cnt,entry_ids,dates)
SELECT ext_name,count(DISTINCT catalog) as cnt,group_concat(entry_id),regexp_replace(nbd,'^.*\\|','')
FROM tmp1
GROUP BY nbd
HAVING cnt>=3;

DROP TABLE tmp1;
