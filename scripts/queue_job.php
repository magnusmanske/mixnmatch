#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';

if ( !isset($argv[1] ) ) die ( "action required\n" ) ;
$action = $argv[1] ;

$mnm = new MixNMatch\MixNMatch ;

$sql = "select * from `jobs` where `action`='{$action}' and `status`='DONE' order by `last_ts` LIMIT 1" ;
$result = $mnm->getSQL ( $sql ) ;
if($o = $result->fetch_object()){
	$job_id = $mnm->queue_job ( $o->catalog , $action ) ;
	print "Updated {$action} job #{$job_id} for https://mix-n-match.toolforge.org/#/catalog/{$o->catalog}\n" ;
}

?>