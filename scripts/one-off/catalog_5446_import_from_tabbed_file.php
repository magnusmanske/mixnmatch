#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;

$had_id = [] ;
$sql = "SELECT ext_id FROM entry WHERE catalog=5446" ;
$result = $mnm->getSQL ( $sql ) ;
while ($o = $result->fetch_object()) $had_id[$o->ext_id] = 1 ;

$catalog_id = 5446 ;
$filename = '/data/project/mix-n-match/manual_lists/J9U_personal_names.csv.mnm' ;
$handle = fopen($filename, "r");
while (($data = fgetcsv($handle, 100000, "\t")) !== FALSE) {
	$o = (object) [
		'catalog' => $catalog_id ,
		'id' => $data[0] ,
		'name' => $data[1] ,
		'desc' => $data[2] ,
		'url' => 'http://uli.nli.org.il/F/?func=find-b&local_base=NLX10&find_code=UID&request='.$data[0] ,
		'born' => $data[3] ,
		'died' => $data[4] ,
		'aux' => [] ,
		'type' => 'Q5'
	] ;
	if ( isset($had_id[$o->id]) ) continue ;
	if ( $data[5] != '' ) $o->aux[] = [ 'P244' , $data[5] ] ;
	try {
		$mnm->addNewEntry ( $o ) ;
		$had_id[$o->id] = 1 ;
	} catch(Exception $e) {
		echo $e->getMessage();
	}
}


?>