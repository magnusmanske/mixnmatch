#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';


# ____________________________________________________________________________________________________

$cc = new MixNMatch\CrossCheck ( isset($argv[2]) );
if ( !isset($argv[1]) ) die ("USAGE: {$argv[0]} CATALOG_ID\n" ) ;

if ( $argv[1] == 'all' ) {
	$cc->run_all_catalogs();
	exit(0);
}

$catalog_id = $argv[1]*1 ;
if ( $catalog_id == 0 ) $cc->run_random_catalog();
else $cc->run_catalog ( $catalog_id ) ;

$cc->mnm->show_proc_info();


?>