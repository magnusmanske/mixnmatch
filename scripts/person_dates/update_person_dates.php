#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/../vendor/autoload.php';

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_ALL);

if ( !isset($argv[1]) ) die ( "Requires catalog number\n" ) ;
$catalog_id = $argv[1] * 1 ;

$pd = new MixNMatch\PersonDates ( $catalog_id ) ;
$pd->loadCodeFragment() ;
$pd->clearOldDates() ;
$pd->updateDatesFromDescription() ;

?>