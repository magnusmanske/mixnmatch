#!/usr/bin/php
<?PHP

ini_set('memory_limit','3000M');

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$apdtw = new MixNMatch\AddPersonDatesToWikidata() ;
$apdtw->run() ;

print "Memory used: " ;
$apdtw->mnm->show_proc_info(($argv[1]??0)*1);

?>