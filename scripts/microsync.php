#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
require_once dirname(__DIR__) . '/vendor/autoload.php';

#if ( !isset($argv[1]) ) die("USAGE: {$argv[0]} CATALOG_ID\n");

# Init
$ms = new MixNMatch\MicroSync ;
$todo = $ms->init_catalogs() ;

$the_catalog = $argv[1] ;
$specific_catalog = isset($the_catalog) ;
if ( $the_catalog == 'random' ) $the_catalog = $ms->get_random_catalog();
if ( $specific_catalog && !isset($ms->catalogs[$the_catalog]) ) exit(0); # Bad catalog

foreach ( $todo AS $catalog_id => $prop ) {
	if ( $specific_catalog and $the_catalog!=$catalog_id ) continue ;
	$ms->checkCatalog ( $catalog_id , $prop ) ;
}

$ms->finalize($specific_catalog) ;

?>