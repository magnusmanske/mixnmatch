#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

require_once dirname(__DIR__) . '/vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;

$use_single_catalog = false ;
if ( isset($argv[1]) ) {
	$catalog = $argv[1] * 1 ;
	$use_single_catalog = true ;
}

function getSearch ( $query ) {
	#if ( isset($property) and isset($q) ) $query .= " haswbstatement:{$property}={$q}" ;
	$url = "http://www.wikidata.org/w/api.php?action=query&list=search&format=json&srlimit=500&srsearch=" . urlencode ( $query ) ;
	#print "{$url}\n" ;
	return json_decode ( file_get_contents ( $url ) ) ;
}

$sql = "SELECT entry.*,mnm_relation.target_entry_id,(SELECT q FROM entry WHERE entry.id=mnm_relation.target_entry_id AND user>0 AND q IS NOT NULL AND q>0) AS target_q FROM entry,mnm_relation WHERE entry.id=entry_id AND property=50 AND q IS NULL" ;
if ( $use_single_catalog ) $sql .= " AND catalog={$catalog}" ;
$sql .= " HAVING target_q IS NOT NULL" ;

$author2work_q = [] ;
$used_catalogs = [] ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$query = preg_replace ( '/\(.+?\)/' , '' , $o->ext_name ) ;
	$j = getSearch ( $query ) ;
	if ( count($j->query->search) == 0 ) continue ; # Nothing found

	#print_r ( $o ) ;
	#print_r ( $j ) ;

#	if ( !preg_match('|^\+0*(\d+)|',$o->inception,$m) ) continue ;
#	$year = $m[1] ;

	$qs_search = [] ;
	foreach ( $j->query->search AS $v ) {
		$q = 'Q' . preg_replace ( '/\D/' , '' , $v->title ) ;
		$qs_search[$q] = $q ;
	}

	$author_q = "Q{$o->target_q}" ;
	$qs_sparql = [] ;
	if ( isset($author2work_q[$author_q]) ) {
		$qs_sparql = $author2work_q[$author_q] ;
	} else {
		$sparql = "SELECT ?work { ?work wdt:P50 wd:{$author_q} }" ; # ; wdt:P571 ?inception FILTER ( year(?inception)={$year} ) 
		#print "{$sparql}\n" ;
		$qs_sparql = $mnm->tfc->getSPARQLitems ( $sparql , 'work' ) ;
		$author2work_q[$author_q] = $qs_sparql ;
	}


	$qs = [] ;
	foreach ( $qs_sparql AS $q ) {
		if ( !isset($qs_search[$q]) ) continue ;
		$qs[$q] = $q ;
	}

	#print_r ( $qs ) ;
	if ( count($qs) == 0 ) continue ;

	$used_catalogs[$o->catalog] = $o->catalog ;

	if ( count($qs) == 1 ) {
		$q = array_keys($qs) ;
		$q = $q[0] ;
		$mnm->setMatchForEntryID ( $o->id , $q , 0 , true , true ) ;
		continue ;
	}

	$qs2 = [] ;
	foreach ( $qs AS $q ) $qs2[] = preg_replace ( '/\D/' , '' , $q ) ;
	$sql = "INSERT IGNORE INTO multi_match (entry_id,catalog,candidates,candidate_count) VALUES ({$o->id},{$o->catalog},'" . implode ( ',' , $qs2 ) . "'," . count($qs2) . ")" ;
	$mnm->getSQL ( $sql ) ;
}

# Unnecessary, but just in case...
foreach ( $used_catalogs AS $catalog_id ) {
	$catalog = new MixNMatch\Catalog ( $catalog_id , $mnm ) ;
	$catalog->updateStatistics();
	$catalog->useAutomatchers(0);
}


?>
