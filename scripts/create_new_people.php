#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

$maintenance = new MixNMatch\Maintenance ;
$maintenance->createNewPeople( ($argv[1]??10)*1 );

?>