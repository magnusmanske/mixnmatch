#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

function followURLandReturnHTML ( $url ) {
	$ch = curl_init();
	curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
	curl_setopt($ch, CURLOPT_URL, $url);
//	curl_setopt($ch, CURLOPT_HEADER, TRUE);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	$a = curl_exec($ch);
#	print_r ( curl_error($ch) ) ;
	return $a ;
}

$overwrite = false ;
if ( !isset($argv[1]) ) die ( "Udage: import_aux_from_url.php CATALOG_ID [overwrite=0/1|0]\n" ) ;
$catalog = $argv[1] * 1 ;
if ( $catalog == 0 ) die ( "Bad catalog {$argv[1]}\n" ) ;
if ( isset($argv[2]) and $argv[2]*1 == 1 ) $overwrite = true ;

$mnm = new MixNMatch\MixNMatch ;

$sql = "SELECT * FROM entry WHERE catalog={$catalog}" ; #  AND ext_url!=''
if ( !$overwrite) $sql .= " AND NOT EXISTS (SELECT * FROM `blob` WHERE entry_id=entry.id)" ;
#$sql .= " AND (user=0 or q is null)" ;
#$sql .= " AND id=2057751" ; # TESTING
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) {

	$ts = $mnm->getCurrentTimestamp() ;
	$format = '' ;
	$url = $o->ext_url ;
	if ( $o->catalog == 49 ) {
		$url = "http://vocab.getty.edu/download/jsonld?uri=http://vocab.getty.edu/tgn/{$o->ext_id}.jsonld" ;
		$format = 'jsonld' ;
	}

	if ( $url == '' ) {
		# TODO log error
		print "Bad URL for:\n" ; print_r ( $o ) ;
		continue ;
	}

	$data = followURLandReturnHTML ( $url ) ;
	if ( !isset($data) or $data === null or $data == '' ) {
		# TODO log error
		print "No result for {$url}\n" ;
		continue ;
	}

	$sql = "REPLACE INTO `blob` (`entry_id`,`format`,`data`,`timestamp`) VALUES ({$o->id}," ;
	$sql .= '"' . $mnm->escape($format) . '",' ;
	$sql .= '"' . $mnm->escape($data) . '",' ;
	$sql .= '"' . $mnm->escape($ts) . '"' ;
	$sql .= ")" ;

	$mnm->getSQL ( $sql ) ;
}

?>