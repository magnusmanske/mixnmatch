#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

$toolname = 'mixnmatch_fix_gnd' ;

require_once dirname(__DIR__) . '/../vendor/autoload.php';
require_once ( "/data/project/mix-n-match/public_html/php/wikidata.php" ) ;
require_once ( "/data/project/quickstatements/public_html/quickstatements.php" ) ;

function getQS () {
	global $toolname ;
	$qs = new QuickStatements() ;
	$qs->use_oauth = false ;
	$qs->bot_config_file = "/data/project/mix-n-match/reinheitsgebot.conf" ;
	$qs->toolname = $toolname ;
	$qs->sleep = 5 ;
	$qs->use_command_compression = true ;
	return $qs ;
}

function runCommandsQS ( $commands ) {
	global $qs ;
	if ( count($commands) == 0 ) return ;
	$commands = implode ( "\n" , $commands ) ;
	$tmp = $qs->importData ( $commands , 'v1' ) ;
#	print_r ( $tmp ) ; exit(0);
	$qs->runCommandArray ( $tmp['data']['commands'] ) ;
}

function fixSingleConstraintViolations ( $q ) {
	global $tfc , $wil , $total_commands_executed ;
	$i = $wil->getItem ( $q ) ;
	if ( !isset($i) ) return ;
	$values = $i->getStrings ( 'P227' ) ;
	$rdfs = [] ;
	foreach ( $values AS $v ) {
		$rdfs[$v] = file_get_contents ( "https://d-nb.info/gnd/{$v}/about/lds" ) ;
	}
	$commands = [] ;
	foreach ( $rdfs AS $id => $rdf ) {
		if ( !preg_match ( '/gndo:gndIdentifier "(.+?)" ;/' , $rdf , $m ) ) continue ;
		$id2 = $m[1] ;
		if ( $id == $id2 ) continue ; // Identity
		if ( isset($rdfs[$id2]) ) {
			$commands[] = "-$q\tP227\t\"$id\" /* This ID was redirected to {$id2}, which is already present */" ; // {$id} deprecated, but {$id2} is already in item
		} else {
#			$commands[] = "" ; // {$id} deprecated, new ID {$id2} not in item
		}
	}
	$total_commands_executed += count($commands) ;
	runCommandsQS ( $commands ) ;
}

$wil = new WikidataItemList ;
$tfc = new ToolforgeCommon ;
$qs = getQS() ;
$total_commands_executed = 0 ;

$sparql = 'SELECT (sample(?item) AS ?q) (count(?id) AS ?cnt) { ?item wdt:P227 ?id } GROUP BY ?item HAVING (?cnt>1)' ;
$items = $tfc->getSPARQLitems ( $sparql , 'q' ) ;
#$items = [ 'Q99231' ] ;

$wil->loadItems ( $items ) ;
foreach ( $items AS $q ) fixSingleConstraintViolations($q) ;

print "{$total_commands_executed} edits done.\n" ;

?>