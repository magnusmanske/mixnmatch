#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
require_once dirname(__DIR__) . '/../vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;

if ( ($argv[1]??'')=='hourly' ) {
	try {
		$maintenance = new MixNMatch\Maintenance ( $mnm ) ;
		$maintenance->updatePropertyCache();
	} catch(Exception $e) {
		echo $e->getMessage();
	}

	try {
		print "Auxiliary\n" ;
		$aux = new MixNMatch\Auxiliary ( false , $mnm ) ;
		print "addAuxiliaryToWikidata\n" ;
		$aux->addAuxiliaryToWikidata ( 0 ) ;
		print "matchEntriesViaAuxiliary\n" ;
		$aux->matchEntriesViaAuxiliary ( 0 , true ) ;
		print "internalAuxiliaryMatcher\n" ;
		$aux->internalAuxiliaryMatcher ( 0 ) ;
	} catch(Exception $e) {
		echo $e->getMessage();
	}

	try {
		print "TaxonMatcher\n" ;
		$tm = new MixNMatch\TaxonMatcher('new',$mnm) ;
		$tm->run();
	} catch(Exception $e) {
		echo $e->getMessage();
	}

	/* IN RUST
	try {
		print "MicroSync\n" ;
		$ms = new MixNMatch\MicroSync ;
		$catalogs = $ms->init_catalogs() ;
		$iterations_left = 5 ;
		while ( $iterations_left > 0 ) {
			$catalog_id = $ms->get_random_catalog();
			if ( !isset($catalogs[$catalog_id]) ) continue ;
			$ms->checkCatalog ( $catalog_id , $catalogs[$catalog_id] ) ;
			$iterations_left -= 1 ;
		}
	} catch(Exception $e) {
		echo $e->getMessage();
	}
	*/

	try {
		# 140Mi
		$apdtw = new MixNMatch\AddPersonDatesToWikidata() ;
		$apdtw->run() ;
	} catch(Exception $e) {
		echo $e->getMessage();
	}

	try {
		print "PersonMatcher\n" ;
		$pm = new MixNMatch\PersonMatcher ( false , true , false , $mnm ) ;
		foreach ( $pm->candidateEntriesGenerator(0) AS $o ) $pm->process_entry ( $o ) ;
	} catch(Exception $e) {
		echo $e->getMessage();
	}

	$mnm->show_proc_info();
}

if ( ($argv[1]??'')=='daily' ) {
	try {
		$maintenance = new MixNMatch\Maintenance ( $mnm ) ;
		$maintenance->updateAuxCandidates();
		$maintenance->updatePaintersList();
		$maintenance->updateIssues();
		$maintenance->updatePropsTodo();
		$maintenance->deleteMultimatchesForFullyMatchedEntries();
		$maintenance->unlinkDisambiguationItems(); # Random subset
		$maintenance->importRelationsIntoAux();
		$maintenance->crossmatchViaAux();
	} catch(Exception $e) {
		echo $e->getMessage();
	}

	try {
		$spc = new MixNMatch\SyncPropCatalog($mnm);
		$spc->run();
	} catch(Exception $e) {
		echo $e->getMessage();
	}

	try {
		$st = new MixNMatch\StatementText ( $mnm ) ;
		$st->run_everything(); # fix_aux_values
	} catch(Exception $e) {
		echo $e->getMessage();
	}

	try {
		$cc = new MixNMatch\CrossCheck ( $mnm );
		print "CrossCheck\n" ;
		$cc->run_all_catalogs();
	} catch(Exception $e) {
		echo $e->getMessage();
	}

	try {
		$tm = new MixNMatch\TaxonMatcher('random',$mnm) ;
		print "TaxonMatcher\n" ;
		$tm->run();
		# $tm->generateTaxonCandidatesTable(); # Long running query, disabled for now
	} catch(Exception $e) {
		echo $e->getMessage();
	}

	$mnm->show_proc_info();
}

if ( ($argv[1]??'')=='weekly' ) {
	try {
		$maintenance = new MixNMatch\Maintenance ( $mnm ) ;
		$maintenance->matchPeopleByKnownNameDates();
		$maintenance->updateCommonNames();
	} catch(Exception $e) {
		echo $e->getMessage();
	}

	$mnm->show_proc_info();
}

if ( ($argv[1]??'')=='monthly' ) {
}

if ( ($argv[1]??'')=='test' ) {
	$mnm = new MixNMatch\MixNMatch ;
	$maintenance = new MixNMatch\Maintenance ( $mnm ) ;
	$maintenance->importRelationsIntoAux();
	$mnm->show_proc_info();
}

?>
