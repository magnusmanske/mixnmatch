#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../vendor/autoload.php';

class PeopleGender {
	public $bad_name_patterns = [
		'[\*\(\)\[\]\,\.\£\!\+\#\$\/“”:@ⓔ0-9]',
		"^Schleswig-Holstein.*$",
		"&quot;",
		"^Chr$",
		"^the$"
	] ;

	function __construct( $mnm = null ) {
		if ( isset($mnm) ) $this->mnm = $mnm ;
		else $this->mnm = new MixNMatch\MixNMatch ;
	}

	protected function is_bad_name ( $name ) {
		if ( strlen($name) < 3 ) return true ;
		foreach ( $this->bad_name_patterns AS $pattern ) {
			$pattern = "|{$pattern}|" ;
			if ( preg_match($pattern,$name) ) return true ;
		}
		return false ;
	}
	protected function is_good_name ( $name ) {
		return !$this->is_bad_name($name) ;
	}

	protected function get_sql_patterns ( $name ) {
		$sql_patterns = [ "`{$name}` LIKE '%\"%'" ] ;
		foreach ( $this->bad_name_patterns AS $pattern ) {
			$pattern = str_replace('\\','\\\\',$pattern) ;
			$sql_patterns[] = "`{$name}` RLIKE \"{$pattern}\"" ;
		}
		return $sql_patterns ;
	}

	protected function get_bad_given_names_in_table () {
		$sql_patterns = $this->get_sql_patterns("name") ;
		$sql = "SELECT * FROM `given_name` WHERE " . implode(" OR ",$sql_patterns) ;
		$ret = [] ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $ret[$o->id] = $o ;
		return $ret ;
	}

	protected function normalize_name ( $given_name ) {
		$fixed_name = trim($given_name) ;
		$fixed_name = preg_replace('|^[“”\\* ]+|','',$fixed_name) ;
		$fixed_name = preg_replace('|[“”\,\. ]+$|','',$fixed_name) ;
		$fixed_name= ucfirst($fixed_name) ;
		return $fixed_name ;
	}

	protected function change_or_merge_given_name ( $given_name_id , $new_name ) {
		$existing_id = 0 ;
		$new_name_escaped = $this->mnm->escape($new_name) ;
		$sql = "SELECT * FROM `given_name` WHERE `name`=\"{$new_name_escaped}\"" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $existing_id = $o->id ;
		if ( $existing_id == 0 ) { # New name does not exist
			$sql = "UPDATE `given_name` SET `name`=\"{$new_name_escaped}\" WHERE `id`={$given_name_id}" ;
			$this->mnm->getSQL ( $sql ) ;
		} else { # New name does exist
			$sql = "UPDATE `entry2given_name` SET `given_name_id`={$existing_id} WHERE `given_name_id`={$given_name_id}" ;
			$this->mnm->getSQL ( $sql ) ;
			$sql = "DELETE FROM `given_name` WHERE `id`={$given_name_id}" ;
			$this->mnm->getSQL ( $sql ) ;
		}
	}

	public function remove_bad_given_names () {
		$given_names = $this->get_bad_given_names_in_table() ;
		if ( count($given_names)==0 ) return ; # Nothing to do
		foreach ( $given_names AS $id => $o ) {
			$new_name = $this->normalize_name($o->name) ;
			if ( !$this->is_bad_name($new_name) ) {
				$this->change_or_merge_given_name ( $id , $new_name ) ;
				unset($given_names[$id]) ;
			}
		}
		$given_name_ids = implode(',',array_keys($given_names));
		if ( count($given_names)==0 ) return ; # Nothing to do
		$sql = "DELETE FROM `entry2given_name` WHERE `given_name_id` IN ({$given_name_ids})" ;
		$this->mnm->getSQL ( $sql ) ;
		$sql = "DELETE FROM `given_name` WHERE `id` IN ({$given_name_ids})" ;
		$this->mnm->getSQL ( $sql ) ;
	}

	protected function get_or_create_given_name_ids ( $given_names ) {
		if ( count($given_names) == 0 ) return [] ;
		$ret = [] ;
		$given_names_escaped = [] ;
		foreach ( $given_names AS $name ) $given_names_escaped[] = $this->mnm->escape($name) ;
		$sql = "SELECT * FROM `given_name` WHERE `name` IN (\"".implode('","',$given_names_escaped)."\")" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $ret[$o->name] = $o->id ;
		foreach ( $given_names AS $name ) {
			if ( isset($ret[$name]) ) continue ;
			$ret[$name] = $this->create_new_given_name($name) ;
		}
		return $ret ;
	}

	protected function create_new_given_name ( $given_name ) {
		$given_name_escaped = $this->mnm->escape ( $given_name ) ;
		$sql = "INSERT IGNORE INTO `given_name` (`name`) VALUES (\"{$given_name_escaped}\")" ;
		$this->mnm->getSQL ( $sql ) ;
		return $this->mnm->dbm->insert_id ;
	}

	public function add_new_entries () {
		$batch_size = 2000 ;
		$sql = "SELECT max(`entry_id`) AS `entry_id` FROM `entry2given_name`" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $last_entry_id = $o->entry_id ;
		$sql_patterns = $this->get_sql_patterns("ext_name") ;
		$sql = "SELECT `id` AS `entry_id`,`ext_name` AS `name` FROM `entry` WHERE `id`>{$last_entry_id} AND `type`='Q5' AND `q` IS NULL AND NOT (".implode(" OR ",$sql_patterns).") ORDER BY `entry_id` LIMIT {$batch_size}" ;
		$given_name2entry_id = [] ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$given_name = preg_replace ( '| .*$|' , '' , $o->name ) ;
			$given_name = $this->normalize_name($given_name) ;
			if ( !$this->is_bad_name($given_name) ) $given_name2entry_id[$given_name][] = $o->entry_id ;
		}
		if ( count($given_name2entry_id) == 0 ) return ;
		$given_name2given_name_id = $this->get_or_create_given_name_ids(array_keys($given_name2entry_id));
		$sql_parts = [] ;
		foreach ( $given_name2given_name_id AS $given_name => $given_name_id ) {
			if ( !isset($given_name2entry_id[$given_name]) ) continue ;
			foreach ( $given_name2entry_id[$given_name] AS $entry_id ) {
				$sql_parts[] = "({$entry_id},rand(),{$given_name_id})" ;
			}
			unset($given_name2entry_id[$given_name]);
		}
		$sql = "INSERT IGNORE INTO `entry2given_name` (`entry_id`,`random`,`given_name_id`) VALUES ".implode(",",$sql_parts) ;
		$this->mnm->getSQL ( $sql ) ;
		if ( count($given_name2entry_id)==0 ) return ;
		print "Could not create given names for: \n- ".implode("\n- ",array_keys($given_name2entry_id))."\n" ;
	}

	protected function get_names_for_gender ( $qid ) {
		$ret = [] ;
		$sparql = "SELECT ?qLabel { ?q wdt:P31 wd:{$qid} . SERVICE wikibase:label { bd:serviceParam wikibase:language 'en' } }" ;
		foreach ( $this->mnm->tfc->getSPARQL_TSV($sparql) AS $row ) {
			$label = $row['qLabel'] ;
			if ( preg_match('|^Q\d+$|',$label) ) continue ;
			$ret[] = $label ;
		}
		return $ret ;
	}

	protected function set_given_name_gender ( $gender , $names ) {
		if ( count($names)==0 ) return ;
		foreach ( $names AS $k => $name ) $names[$k] = $this->mnm->escape($name) ;
		$sql = "UPDATE `given_name` SET `gender`='{$gender}' WHERE `gender`='unknown' AND `name` IN (\"".implode('","',$names)."\")" ;
		$this->mnm->getSQL ( $sql ) ;
	}

	public function update_names_gender_from_wikidata () {
		$female_names = $this->get_names_for_gender('Q11879590') ;
		$male_names = $this->get_names_for_gender('Q12308941') ;
		$ambiguous_names = $this->get_names_for_gender('Q3409032') ;
		$ambiguous_names2 = array_intersect($male_names,$female_names);
		$ambiguous_names = array_unique(array_merge($ambiguous_names,$ambiguous_names2));
		$female_names = array_diff($female_names,$ambiguous_names);
		$male_names = array_diff($male_names,$ambiguous_names);
		$this->set_given_name_gender('female',$female_names);
		$this->set_given_name_gender('male',$male_names);
		$this->set_given_name_gender('ambiguous',$ambiguous_names);
	}

	public function clear_matched_or_nonhumans () {
		$sql = "SELECT `entry_id` FROM `entry`,`entry2given_name` WHERE `entry`.`id`=`entry_id` AND (`type`!='Q5' OR `user`>=1)" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		$entry_ids = [] ;
		while($o = $result->fetch_object()) $entry_ids[] = $o->entry_id ;
		if ( count($entry_ids)==0 ) return ;
		$sql = "DELETE FROM `entry2given_name` WHERE `entry_id` IN (".join(',',$entry_ids).")" ;
		$this->mnm->getSQL ( $sql ) ;
	}
}

$command = $argv[1]??'' ;

$pg = new PeopleGender ;
if ( $command=='add_new_entries' ) $pg->add_new_entries() ;
if ( $command=='remove_bad_names' ) $pg->remove_bad_given_names() ;
if ( $command=='update_names_gender' ) $pg->update_names_gender_from_wikidata() ;
if ( $command=='clear_matched_or_nonhumans' ) $pg->clear_matched_or_nonhumans() ;
if ( $command=='all' ) {
	$pg->add_new_entries() ;
	$pg->remove_bad_given_names() ;
	$pg->clear_matched_or_nonhumans() ;
	$pg->update_names_gender_from_wikidata() ;
}

?>