#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';

$maintenance = new MixNMatch\Maintenance ;
$maintenance->unlinkDisambiguationItems($argv[1]);

?>