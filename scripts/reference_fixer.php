#!/usr/bin/php
<?PHP

exit(0); # DEACTIVATED FOR POTENTIALLY ADDING MULTIPLE REFERENCE PARTS INSTEAD OF SINGLE REFERENCE

# Will take a QID and check it for reference URLs replacable by property/IDs
# Will also split multiple values for reference URLs in single "reference groups"

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); # E_ALL|
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

class ReferenceFixer {
	public $simulating = false ;
	public $summary = "Fixing references as part of Mix'n'match cleanup" ;
	public $wikidata_api = "https://www.wikidata.org/w/api.php" ;
	public $batch_size = 5 ;
	private $user_agent = "Mix'n'match reference fixer" ;# 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:10.0.2) Gecko/20100101 Firefox/10.0.2' ;
	private $hardcoded_patterns = [
		"|^https?://www\.biodiversitylibrary\.org/creator/(.+?)/*$|" => "P4081" ,
		"|^https?://trove.nla.gov.au/people/(\d+).*$|" => "P1315" ,
		"|^https?://openlibrary\.org/authors/(.+?)/.*$|" => "P648" ,
		"|^https?:\/\/www\.biusante\.parisdescartes\.fr\/histoire\/biographies\/index\.php\?cle=(\d+)|" => "P5375" ,
		"|^https?:\/\/biusante\.parisdescartes\.fr\/histoire\/biographies\/index\.php\?cle=(\d+)|" => "P5375" ,
		"|^https?://bibliotheque.academie-medecine.fr/membres/membre/\?mbreid=(\d+).*$|" => "P3956" ,
		"|^https?://www.artnet.com/artists/([^/]+).*$|" => "P3782" ,
		"|^https?://www.mutualart.com/Artist/[^/]+/([^/]+).*$|" => "P6578" ,
		"|^https?://en.isabart.org/person/(\d+).*$|" => "P6844" ,
		"|^http?://www.sikart.ch/KuenstlerInnen.aspx\?id=(\d+).*$|" => "P781"
	] ;


	function __construct( $mnm = null ) {
		if ( isset($mnm) ) $this->mnm = $mnm ;
		else $this->mnm = new MixNMatch\MixNMatch ;
		$this->stated_in = $this->load_stated_in() ;
		$this->url_pattern2property = $this->load_formatter_urls();
	}

	public function update_candidates () {
		$dbwd = $this->mnm->openWikidataDB() ;
		# Page creations by User:Reinheitsgebot for mix'n'match
		$sql = 'SELECT DISTINCT page_title FROM page WHERE page_namespace=0 AND page_id IN (SELECT rev_page FROM revision WHERE rev_comment_id=502688778 AND rev_id IN (SELECT revactor_rev FROM revision_actor_temp WHERE revactor_actor=169) AND rev_parent_id=0)' ;
		$result = $this->mnm->tfc->getSQL ( $dbwd , $sql ) ;
		$sql = [] ;
		$sql = "INSERT IGNORE INTO `reference_fixer` (`q`,`done`) VALUES (0,1)" ;
		while($o = $result->fetch_object()) {
			$q = substr($o->page_title,1)*1 ;
			$sql .= ",({$q},0)" ;
		}
		$this->mnm->getSQL ( $sql ) ;
	}

	protected function getBotConfig () {
		return parse_ini_file ( "/data/project/mix-n-match/bot.ini" ) ;
	}

	protected function getBotAPI ( $force_login = false ) {
		if ( !$force_login and isset($this->bot_api) and $this->bot_api->isLoggedIn() ) return $this->bot_api ;

		$bot_config = $this->getBotConfig() ;
		$api = new \Mediawiki\Api\MediawikiApi( $this->wikidata_api );
		if ( $force_login or !$api->isLoggedin() ) {
			if ( isset($bot_config['user']) ) $username = $bot_config['user'] ;
			if ( isset($bot_config['pass']) ) $password = $bot_config['pass'] ;
			if ( !isset($username) or !isset($password) ) return false ;
			$x = $api->login( new \Mediawiki\Api\ApiUser( $username, $password ) );
			if ( !$x ) return false ;
		}
		$this->bot_api = $api ;
		return $api ;	
	}

	protected function load_formatter_urls () {
		$ret = [] ;
		$sparql = 'select ?property ?formatterurl where { 
			{ ?property p:P1630 [ps:P1630 ?formatterurl]; rdf:type wikibase:Property } UNION 
			{ ?property p:P1921 [ps:P1921 ?formatterurl]; rdf:type wikibase:Property }
		}' ;
		$j = $this->mnm->tfc->getSPARQL ( $sparql ) ;
		foreach ( $j->results->bindings AS $b ) {
			$property = preg_replace ( '|^.+/|' , '' , $b->property->value ) ;
			$pattern = trim($b->formatterurl->value) ;
			if ( preg_match('|\bdoi.org/\$1$|',$pattern) ) continue ;
			if ( !preg_match('|^https?:|',$pattern) ) continue ;
			if ( $pattern == '$1' ) continue ; # WTF? P8009
			$pattern = preg_quote ( $pattern ) ;
			$pattern = str_replace ( '\\$1' , '(.+)' , $pattern ) ;
			$pattern = preg_replace ( '|^https?\\\\:|' , 'https?:' , $pattern ) ;
			$pattern = preg_replace ( '|/$|' , '/{0,1}' , $pattern ) ;
			$pattern = "|^{$pattern}$|" ;
			$ret[$pattern][$property] = $property ;
		}
		foreach ( $ret AS $k => $v ) {
			if ( count((array)$v) > 1 ) {
				#print "Collision on {$k}: ".json_encode(array_values($v))."\n" ;
				unset($ret[$k] ) ;
			} else $ret[$k] = array_values($v)[0] ;
		}
		foreach ( $this->hardcoded_patterns AS $k => $v ) $ret[$k] = $v ;
		return $ret ;
	}

	protected function load_stated_in () {
		$ret = [] ;
		$sparql = 'SELECT ?property ?stated_in WHERE { ?property rdf:type wikibase:Property . ?property wdt:P9073 ?stated_in }' ;
		$j = $this->mnm->tfc->getSPARQL ( $sparql ) ;
		foreach ( $j->results->bindings AS $b ) {
			$property = preg_replace ( '|^.+/|' , '' , $b->property->value ) ;
			$this->stated_in = preg_replace ( '|^.+/|' , '' , $b->stated_in->value ) ;
			if ( isset($ret[$property]) ) continue ;
			$ret[$property] = $this->stated_in ;
		}
		return $ret ;
	}

	protected function improved_reference_snak ( $ref ) {
		if ( $ref->snaktype != 'value' ) return ;
		if ( !isset($ref->datavalue) ) return ;
		if ( !isset($ref->datavalue->value) ) return ;
		if ( $ref->datavalue->type != 'string' ) return ;
		$url = $ref->datavalue->value ;
		$matches = [] ;
		foreach ( $this->url_pattern2property AS $pattern => $property ) {
			if ( !preg_match($pattern,$url,$m) ) continue ;
			$value = $m[1] ;
			$value = urldecode ( $value ) ;
			$value = preg_replace('|\&.*$|','',$value) ; # Trailing other parameters
			$value = preg_replace('|/$|','',$value) ; # Trailing slash
			$value = preg_replace('|_\(Dizionario_Biografico\)$|','',$value) ; # P1986
			$value = trim($value) ;
			if ( $value == '' ) continue ;
			$matches[$property][$value] = $value ;
		}
		if ( count((array)$matches) == 0 ) return ; # No matching property found
		if ( count((array)$matches) > 1 ) return ; # Multiple properties, not touching this!
		$property = array_keys($matches)[0] ;
		$values = array_values($matches)[0] ;
		if ( count($values) != 1 ) return ; # Multiple possible values, not touching that
		$value = array_values($values)[0] ;

		$ret = [] ;

		# Stated in
		if ( isset($this->stated_in[$property]) ) {
			$si = $this->stated_in[$property] ;
			$si_numeric = substr ( $si , 1 ) ;
			$ret[] = (object) [
				"snaktype" => "value" ,
				"property" => "P248" ,
				"datavalue" => (object) [
					"value" => (object) [
	                    "entity-type" => "item",
	                    "numeric-id" => $si_numeric,
	                    "id" => $si
	                ],
	                "type" => "wikibase-entityid"
				] ,
				"datatype" => "wikibase-item"
			] ;
		}

		$ret[] = (object) [
			"snaktype" => "value" ,
			"property" => $property ,
			"datavalue" => (object) [
				"value" => $value ,
				"type" => "string"
			] ,
			"datatype" => "external-id"
		] ;
		return $ret ;
	}

	protected function new_reference_group ( $reference_snaks ) {
		$ret = (object) [] ;
		$ret->snaks = (object) [] ;
		$order = [] ;
		foreach ( $reference_snaks AS $snak ) {
			if ( isset($snak->hash) ) unset ( $snak->hash ) ;
			$property = $snak->property ;
			if ( !isset($ret->snaks->$property) ) $ret->snaks->$property = [] ;
			$ret->snaks->$property[] = $snak ;
			$order[$property] = $property ;
		}
		$order = array_values($order) ;

		# P248 first
		if (($key = array_search("P248", $order)) !== false) {
			unset($order[$key]) ;
			$order = array_values($order) ;
			array_unshift($order, "P248") ;
		}

		$ret->{"snaks-order"} = $order ;

		return $ret ;
	}

	protected function check_reference_group ( $j ) {
		if ( !isset($j->snaks) ) return ; # No reference snaks
		if ( !isset($j->snaks->{'P854'}) ) return ; # No reference URL snaks

		# TODO check for generic properties like dates and proceed but use them
		if ( count((array)$j->snaks) > 1 ) return ; # Other properties than P854, aborting

		$multiple = count($j->snaks->{'P854'})>1 ;
		$new_reference_groups = [] ;
		$changed = $multiple ;
		$old_refs = $j->snaks->{'P854'} ;
		foreach ( $old_refs as $url_reference ) {
			$new_ref = $this->improved_reference_snak ( $url_reference ) ;
			if ( isset($new_ref) ) {
				$changed = true ;
				$new_reference_groups[] = $this->new_reference_group($new_ref) ;
			} else {
				$ref = json_decode ( json_encode ( $url_reference ) ) ;
				$new_reference_groups[] = $this->new_reference_group([$ref]) ;
			}
		}
		if ( $changed ) return $new_reference_groups ;
	}

	protected function api_action ( $action , $params ) {
		if ( $this->simulating ) {
			print "{$action}: ".json_encode($params)."\n" ;
			return ;
		}
		$api = $this->getBotAPI() ;
		$params['format'] = 'json' ;
		$params['bot'] = '1' ;
		$params['token'] = $api->getToken() ;
		$params['summary'] = $this->summary ;
		try {
			sleep(1);
			$api->postRequest( new \Mediawiki\Api\SimpleRequest( $action, $params ) ) ;
			return true ;
		} catch (Exception $e) {
			$msg = $e->getMessage() ;
			print json_encode($params)."\n" ;
			print "{$msg}\n" ;
			return false ;
		}
	}

	protected function add_reference_group ( $statement_id , $rg ) {
		$params = [
			"statement" => $statement_id ,
			"snaks" => json_encode($rg->snaks) ,
			"snaks-order" => json_encode($rg->{"snaks-order"})
		] ;
		return $this->api_action ( "wbsetreference" , $params ) ;
	}

	protected function remove_reference_group ( $statement_id , $ref_ids ) {
		$params = [
			"statement" => $statement_id ,
			"references" => implode('|',$ref_ids)
		] ;
		return $this->api_action ( "wbremovereferences" , $params ) ;
	}

	protected function is_self_reference ( $statement , $rg ) {
		if ( !isset($statement->mainsnak) ) return false ;
		if ( $statement->mainsnak->datatype != 'external-id' ) return false ;
		$other_snak_props = false ;
		$ret = false ;
		foreach ( $rg->snaks AS $property => $value ) {
			if ( in_array($property, ["P248","P813"]) ) continue ; # Ignore
			if ( $property == $statement->mainsnak->property ) $ret = true ;
			else return false ; # Other properties, can't remove
		}
		return $ret ;
	}

	protected function check_statement ( $statement ) {
		if ( !isset($statement->references) ) return ;
		$remove_references = [] ;
		foreach ( $statement->references as $reference_group ) {
			if ( $this->is_self_reference($statement,$reference_group) ) {
				$remove_references[] = $reference_group->hash ;
				continue ;
			}
			$new_reference_groups = $this->check_reference_group ( $reference_group ) ;
			if ( !isset($new_reference_groups) ) continue ;

			foreach ( $new_reference_groups AS $rg ) {
				# external ID with itself as reference? Don't add!
				if ( $this->is_self_reference($statement,$rg) ) continue ;

				if ( !$this->add_reference_group ( $statement->id , $rg ) ) return ;
			}
			$remove_references[] = $reference_group->hash ;
		}
		if ( count($remove_references) > 0 ) {
			$this->remove_reference_group ( $statement->id , $remove_references ) ;
		}
	}

	protected function check_json ( $item ) {
		if ( !isset($item->claims) ) return ;
		foreach ( $item->claims AS $property => $statements ) {
			foreach ( $statements AS $statement ) {
				$this->check_statement ( $statement ) ?? [] ;
			}
		}
	}

	public function check_item ( $q ) {
		$url = "https://www.wikidata.org/w/api.php?action=wbgetentities&format=json&ids={$q}" ;
		$json_reply = json_decode(file_get_contents($url));
		$this->check_json ( $json_reply->entities->$q ) ;
	}

	public function check_revision ( $url ) {
		#$url = "https://www.wikidata.org/w/api.php?action=query&prop=revisions&titles=Q109833604&rvslots=*&rvprop=timestamp|user|comment|content&rvuser=Reinheitsgebot&format=json" ;
		#$rf->check_revision($url);
		$json_reply = json_decode(file_get_contents($url));
		foreach ( $json_reply->query->pages AS $k => $v ) {
			$j = json_decode($v->revisions[0]->slots->main->{'*'});
		}
		$this->check_json ( $j ) ;

	}

	public function check_next () {
		while ( 1 ) {
			$sql = "SELECT * FROM `reference_fixer` WHERE `done`=0 ORDER BY `q` DESC LIMIT {$this->batch_size}" ;
			$result = $this->mnm->getSQL ( $sql ) ;
			$found = false ;
			while($o = $result->fetch_object()) {
				$found = true ;
				$this->check_item ( "Q{$o->q}" ) ;
				$this->mnm->getSQL ( "UPDATE `reference_fixer` SET `done`=1 WHERE `q`={$o->q}" ) ; # Mark as done
			}
			if ( !$found ) break ; # All done
		}
		print "All done!\n" ;
	}

}

$rf = new ReferenceFixer();

if ( isset($argv[2]) ) $rf->simulating = true ;

if ( !isset($argv[1]) ) $rf->check_next() ;
else if ( $argv[1] == 'update' ) $rf->update_candidates() ;
else $rf->check_item($argv[1]); # "Q109887499"

?>
