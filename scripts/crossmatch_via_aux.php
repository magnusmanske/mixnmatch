#!/usr/bin/php
<?PHP

/*
This script can find entries with the same auxiliary data (VIAF or GND), where some entries are manually matched against Wikidata, and apply the same match to the ones that are not matched yet.
*/

require_once dirname(__DIR__) . '/vendor/autoload.php';


$maintenance = new MixNMatch\Maintenance () ;
$maintenance->crossmatchViaAux() ;

$maintenance->mnm->show_proc_info();

?>