#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

class Description2AuxiliaryParser {
	public $mnm ;
	public $min_entries_for_label = 10 ;
	public $verbose = true ;
	public $testing = false ;
	protected $catalog_id ;
	protected $cached_results = [
		"nl" => [
			"Parijs" => "Q90" ,
			"Delft" => "Q690" ,
			"Eindhoven" => "Q9832" ,
			"Enschede" => "Q10002" ,
			"Düsseldorf" => "Q1718" ,
			"'s-Hertogenbosch" => "Q2766547" ,
			"Engeland" => "Q21" ,
			"Gent" => "Q1296"
		] ,
		"fr" => [
			"Etterbeek" => "Q192859",
			"Paris" => "Q90" ,
			"Rio de Janeiro" => "Q8678" ,
			"Tours" => "Q288" ,
			"Casablanca" => "Q7903" ,
			"Antibes" => "Q126898" ,
			"San Diego" => "Q16552" ,
			"New-York" => "Q60" ,
			"Metz" => "Q22690" ,
			"Berlin" => "Q64" ,
			"Tōkyō" => "Q1490"
		]
	] ;
	protected $language = 'en' ; # Default

	/**
	 * Constructor
	 *
	*/
	public function __construct ( /*int*/ $catalog_id , $mnm = null ) {
		$this->catalog_id = $catalog_id * 1 ;
		if ( !isset($mnm) ) $this->mnm = new MixNMatch\MixNMatch ;
		$this->catalog = new MixNMatch\Catalog ( $catalog_id , $this->mnm ) ;
		$this->cd = $this->catalog->data();
		if ( isset($this->cd->search_wp) and $this->cd->search_wp!='' ) $this->language = $this->cd->search_wp ;
	}

	# "pattern" needs to cover the whole description (^$)
	public function parse_part_match ( $catalog_id , $pattern , $type = null , $skip_existing_aux_p = null ) {
		$label2entries = [] ;
		$sql = 'SELECT id,regexp_replace(ext_desc,"'.$pattern.'","\\\\1") AS place FROM entry';
		$sql .= ' WHERE catalog='.$catalog_id ;
		if ( isset($type) ) $sql .= " AND `type`='{$type}'" ;
		$sql .= ' AND ext_desc rlike "'.$pattern.'"' ;
		if ( isset($skip_existing_aux_p) ) {
			$skip_existing_aux_p *= 1 ;
			$sql .= " AND NOT EXISTS (SELECT * FROM auxiliary WHERE entry_id=entry.id AND aux_p={$skip_existing_aux_p})" ;
		}
		if ( $this->testing ) print "{$sql}\n" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) $label2entries[$o->place][] = $o->id ;
		return $label2entries ;
	}

	protected function log ( $msg ) {
		if ( $this->verbose ) print "{$msg}\n" ;
	}

	protected function find_matching_item ( $qs , $label ) {
		if ( count($qs) == 0 ) return $this->log ( "Not found: {$label}" ) ;
		if ( count($qs) == 1 ) return $qs[0] ; # Easy case
		$wil = new \WikidataItemList ;
		$wil->loadItems ( $qs ) ;
		$qs2 = [] ;
		foreach ( $qs AS $q ) {
			$i = $wil->getItem($q) ;
			if ( !isset($i) ) continue ;
			$item_label = $i->getLabel($this->language);
			if ( $label == $item_label ) {
				$qs2[] = $q ;
				continue ;
			}
		}
		if ( count($qs) == 0 ) $this->log ( "Search match but no label match: {$label}" ) ;
		if ( count($qs2) == 1 ) return $qs2[0] ; # Label match
		$this->log ( "Multiple values: {$label} => ".implode(", ",$qs2) ) ;
	}

	public function convert_label_to_item ( $label , $property = '' , $prop_value = '' ) {
		$label = trim($label);
		if ( !isset($this->cached_results[$this->language]) ) $this->cached_results[$this->language] = [] ;
		$cache_key = $label ;
		if ( isset($this->cached_results[$this->language][$cache_key]) ) return $this->cached_results[$this->language][$cache_key] ; # Generic case, hardcoded
		$cache_key = "{$label}\t{$property}\n{$prop_value}" ;
		if ( isset($this->cached_results[$this->language][$cache_key]) ) return $this->cached_results[$this->language][$cache_key] ;
		$qs = array_values ( $this->mnm->getCachedWikidataSearch ( $label , $property , $prop_value ) ) ;
		$q_or_undef = $this->find_matching_item ( $qs , $label ) ;
		$this->cached_results[$this->language][$cache_key] = $q_or_undef ;
		return $q_or_undef ;
	}

	public function set_aux_for_entries ( $entry_ids , $aux_p , $aux_name ) {
		if ( !isset($aux_name) or $aux_name=='' ) return ;
		if ( count($entry_ids) == 0 ) return ;
		$aux_p *= 1 ;
		$aux_name_safe = $this->mnm->escape($aux_name) ;
		$sql = [] ;
		foreach ( $entry_ids AS $entry_id ) {
			$sql[] = "({$entry_id},{$aux_p},'{$aux_name_safe}')" ;
		}
		$sql = "INSERT IGNORE INTO auxiliary (entry_id,aux_p,aux_name) VALUES " . implode(',',$sql) ;
		if ( $this->testing ) print count($entry_ids) . " entries => P{$aux_p} = $aux_name\n" ;
		else $this->mnm->getSQL ( $sql ) ;
	}

	public function set_aux_for_labels ( $label , $entry_ids , $aux_p , $property = '' , $prop_value = '' ) {
		$q = $this->convert_label_to_item ( $label , $property , $prop_value ) ;
		$this->set_aux_for_entries ( $entry_ids , $aux_p , $q ) ;
	}

	public function set_aux_from_pattern ( $pattern , $aux_p , $type = null , $property = '' , $prop_value = '' ) {
		$label2entries = $this->parse_part_match ( $this->catalog_id , $pattern , $type , $aux_p ) ;
		foreach ( $label2entries AS $label => $entry_ids ) {
			if ( count($entry_ids) < $this->min_entries_for_label ) continue ;
			$this->set_aux_for_labels ( $label , $entry_ids , $aux_p , $property , $prop_value ) ;
		}
	}

	public function test ( $testing = true ) {
		$this->testing = $testing ;
		$this->verbose = $testing ;
	}

}

$catalog_patterns = [
	"15" => [ "Q5" => [
			"19" => '^.*, *(.+?)\. Gestorven.*$' , # Place of birth
			"20" => '^.* Gestorven:[^.,]+, *([^.]+).*$' , # Place of death
		]
	] ,
	"439" => [ "Q5" => [
			"19" => '^.*Né\\\\(e\\\\) à : *(.+?)[,:].*$' , # Place of birth
			"20" => '^.*Mort\\\\(e\\\\) à : *(.+?)[,:].*$' , # Place of death
		]
	]
] ;

$property_p31s = [
	"19" => [ 'Q1549591' , 'Q515' ] ,
	"20" => [ 'Q1549591' , 'Q515' ] ,
] ;


if ( !isset($argv[1]) ) die ("USAGE: {$argv[0]} CATALOG_ID\n" ) ;
$catalog_id = $argv[1]*1 ;
$dap = new Description2AuxiliaryParser ( $catalog_id ) ;
if ( isset($argv[2]) ) $dap->test() ;

foreach ( $catalog_patterns[$catalog_id]??[] AS $type => $pattern_list ) {
	foreach ( $pattern_list AS $prop => $pattern ) {
		foreach ( $property_p31s[$prop]??[] AS $p31 ) {
			$dap->set_aux_from_pattern ( $pattern , (int)$prop , $type , 'P31' , $p31 ) ;
		}
	}
}

/*
if ( $catalog_id == 15 ) {
	$dap->set_aux_from_pattern ( '^.*, *(.+?)\. Gestorven.*$' , 19 , 'Q5' , 'P31' , 'Q1549591' ) ; # Place of birth
	$dap->set_aux_from_pattern ( '^.*, *(.+?)\. Gestorven.*$' , 19 , 'Q5' , 'P31' , 'Q515' ) ; # Place of birth
	$dap->set_aux_from_pattern ( '^.* Gestorven:[^.,]+, *([^.]+).*$' , 20 , 'Q5' , 'P31' , 'Q1549591' ) ; # Place of death
	$dap->set_aux_from_pattern ( '^.* Gestorven:[^.,]+, *([^.]+).*$' , 20 , 'Q5' , 'P31' , 'Q515' ) ; # Place of death
} else if ( $catalog_id == 439 ) {
	$dap->set_aux_from_pattern ( '^.*Né\\\\(e\\\\) à : *(.+?)[,:].*$' , 19 , 'Q5' , 'P31' , 'Q1549591' ) ; # Place of birth
	$dap->set_aux_from_pattern ( '^.*Né\\\\(e\\\\) à : *(.+?)[,:].*$' , 19 , 'Q5' , 'P31' , 'Q515' ) ; # Place of birth
	$dap->set_aux_from_pattern ( '^.*Né\\\\(e\\\\) à : *(.+?)[,:].*$' , 20 , 'Q5' , 'P31' , 'Q1549591' ) ; # Place of birth
	$dap->set_aux_from_pattern ( '^.*Mort\\\\(e\\\\) à : *(.+?)[,:].*$' , 20 , 'Q5' , 'P31' , 'Q515' ) ; # Place of birth
}
*/
?>