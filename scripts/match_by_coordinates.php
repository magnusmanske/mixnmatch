#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

require_once dirname(__DIR__) . '/vendor/autoload.php';

$cm = new MixNMatch\CoordinateMatcher ( $argv[1]??'0' , $argv[2]??'' ) ;
$cm->verbose = false;
$cm->match_all_entries() ;

?>