#!/usr/bin/php
<?PHP

# INCOMPLETE, NOT IN USE

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

require_once dirname(__DIR__) . '/vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;

# BNF
$sql = "SELECT entry.id,bnf_person.q,entry.catalog FROM entry,s51434__mixnmatch_large_catalogs_p.bnf_person
WHERE `type`='Q5'
AND entry.q IS NULL
AND bnf_person.name=ext_name AND bnf_person.q is not null
AND catalog IN (SELECT id FROM catalog WHERE active=1)
GROUP BY entry.id
HAVING count(*)=1";
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$mnm->setMatchForEntryID($o->id,$o->q,0,true,false);
}


# Auxiliary data

# BNF via name/dates
$sql = "INSERT IGNORE INTO auxiliary (entry_id,aux_p,aux_name) 
SELECT entry_id,268,ext_id FROM vw_dates,s51434__mixnmatch_large_catalogs_p.bnf_person
WHERE vw_dates.born!='' AND vw_dates.died!=''
AND vw_dates.q is null
AND vw_dates.ext_name=bnf_person.name
AND vw_dates.born=bnf_person.born
AND vw_dates.died=bnf_person.died";
$mnm->getSQL ( $sql ) ;


# VIAF via BNF via name/dates
$sql = "INSERT IGNORE INTO auxiliary (entry_id,aux_p,aux_name) 
SELECT entry_id,214,viaf FROM vw_dates,s51434__mixnmatch_large_catalogs_p.bnf_person
WHERE vw_dates.born!='' AND vw_dates.died!=''
AND vw_dates.q is null
AND vw_dates.ext_name=bnf_person.name
AND vw_dates.born=bnf_person.born
AND vw_dates.died=bnf_person.died
AND viaf!=''";
$mnm->getSQL ( $sql ) ;

?>