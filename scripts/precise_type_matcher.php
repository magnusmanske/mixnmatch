#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';

if ( !isset($argv[1]) ) die ( "Requires catalog\n" ) ;
$catalog = $argv[1] * 1 ;
# TODO type(s) in $argv[2]

$mnm = new MixNMatch\MixNMatch ;

$language = 'de' ;

$changed = false ;
$sql = "SELECT * FROM entry WHERE catalog={$catalog} AND (q IS null or user=0) AND `type`!=''" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$results = $mnm->getSearchResults ( $o->ext_name , 'P31' , $o->type ) ;
	if ( count($results) != 1 ) continue ;
	$q = $results[0]->title ;

	# Paranoia
	$mnm->wil->loadItem($q) ;
	$i = $mnm->wil->getItem($q) ;
	if ( !isset($i) ) continue ;
	if ( !$i->hasTarget('P31',$o->type) ) continue ;
	if ( $i->hasTarget('P31','Q4167410') ) continue ; # Disambig page
	$label = $i->getLabel($language,true) ;
	if ( $label != $o->ext_name ) {
		#print "{$label} != {$o->ext_name}\n" ;
		continue ;
	}

	#print "{$o->ext_name}: {$q}\n" ;
	$mnm->setMatchForEntryID ( $o->id , $q , 4 , true , false ) ;
	$changed = true ;
}

if ( $changed ) {
	$mnm->queue_job ( $catalog , 'microsync' ) ;
}

?>