#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';

$create_new_items = true ;
$beacon_file = 'http://www.historische-kommission-muenchen-editionen.de/beacond/bbld.php?beacon&full=1' ;
$prop_id1 = 'P227' ; # PND/GND
$prop_id2 = 'P2580' ; # Baltisches Biographisches Lexikon digital ID


# NOTE: id2 is the PRIMARY ID
#________________________________________________________________________________________________________________________

$toolname = "Mix'n'match:BEACON sync" ;

function getQS () {
	global $toolname ;
	$qs = new QuickStatements() ;
	$qs->use_oauth = false ;
	$qs->bot_config_file = "/data/project/mix-n-match/reinheitsgebot.conf" ;
	$qs->toolname = $toolname ;
	$qs->sleep = 1 ;
	$qs->use_command_compression = true ;
	return $qs ;
}

// $commands = [] , one QS V1 command per element, no newlines
function runCommandsQS ( $commands ) {
	global $qs ;
	if ( count($commands) == 0 ) return ;
	$commands = implode ( "\n" , $commands ) ;
	$tmp_uncompressed = $qs->importData ( $commands , 'v1' ) ;
	$tmp['data']['commands'] = $qs->compressCommands ( $tmp_uncompressed['data']['commands'] ) ;
	$qs->runCommandArray ( $tmp['data']['commands'] ) ;
	if ( !isset($qs->last_item) ) return ;
	$last_item = 'Q' . preg_replace ( '/\D/' , '' , "{$qs->last_item}" ) ;
	return $last_item ;
}

function addDateCommand ( $prop , $d , &$commands ) {
	if ( !isset($d) or $d == '' ) return ;
	if ( preg_match ( '/^\d{1,4}$/' , $d ) ) $commands[] = "LAST\t{$prop}\t+{$d}-01-01T00:00:00Z/9" ;
	if ( preg_match ( '/^\d{1,4}-\d{2}$/' , $d ) ) $commands[] = "LAST\t{$prop}\t+{$d}-01T00:00:00Z/10" ;
	if ( preg_match ( '/^\d{1,4}-\d{2}-\d{2}$/' , $d ) ) $commands[] = "LAST\t{$prop}\t+{$d}T00:00:00Z/11" ;
}

function addCommandsFromMixnmatch ( $catalog , $extid , $label , &$commands ) {
	global $mnm ;
	if ( $catalog == '' ) return true ;
	$sql = "SELECT * FROM entry WHERE catalog={$catalog} AND ext_id='" . $mnm->escape($extid) . "'" ;
	$result = $mnm->getSQL ( $sql ) ;
	if ($o = $result->fetch_object()) {
		$entry_id = $o->id ;
		if ( isset($o->q) AND $o->user != 0 ) {
			print "{$mnm->root_url}/#/entry/{$entry_id} has a match!\n" ;
			return false ;
		}
	} else {
		$commands[] = "LAST\tLen\t\"{$label}\"" ;
		return true ;
	}

	$label = $o->ext_name ;
	$commands[] = "LAST\tLen\t\"{$label}\"" ;
	$desc = trim($o->ext_desc) ;
	if ( $desc != '' ) $commands[] = "LAST\tDen\t\"{$desc}\"" ;
	if ( $o->type != '' ) $commands[] = "LAST\tP31\t{$o->type}" ;
	if ( $o->type != 'Q5' ) return true ; // No human, no point looking for dates

	$sql = "SELECT * FROM vw_dates WHERE entry_id={$entry_id}" ;
	$result = $mnm->getSQL ( $sql ) ;
	if ($o = $result->fetch_object()) {
		addDateCommand ( 'P569' , $o->born , $commands ) ;
		addDateCommand ( 'P570' , $o->died , $commands ) ;
	}
	return true ;
}


$mnm = new MixNMatch\MixNMatch ;
$qs = getQS() ;

# Find catalog associated with this property, if any
$catalog = '' ;
$sql = "SELECT id FROM catalog WHERE active=1 AND wd_qual IS NULL AND wd_prop=" . preg_replace('/\D/','',$prop_id2) ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) $catalog = $o->id ;

# Find all items with this property, and optionally the other property
$id2_value2item = [] ;
$sparql = "SELECT ?q ?v1 ?v2 ?r".rand()." { ?q wdt:{$prop_id2} ?v2 OPTIONAL { ?q wdt:{$prop_id1} ?v1 } }" ;
$j = $mnm->tfc->getSPARQL ( $sparql ) ;
foreach ( $j->results->bindings AS $b ) {
	$q = $mnm->tfc->parseItemFromURL ( $b->q->value ) ;
	if ( isset($b->v1) ) $id1_value2item[$b->v1->value] = $q ;
	$id2_value2item[$b->v2->value] = $q ;
}

# Get and parse BEACON file
$commands = [] ;
$rows = explode ( "\n" , file_get_contents ( $beacon_file ) ) ;
foreach ( $rows AS $row ) {
	$row = trim ( $row ) ;
	if ( preg_match ( '/^#/' , $row ) ) continue ;
	$parts = explode ( '|' , $row ) ;
	if ( count($parts) != 3 ) continue ; # Paranoia
	$id1 = $parts[0] ;
	$id2 = $parts[1] ;
	$text = $parts[2] ;

	// Does an item with this ID2 already exists?
	if ( isset($id2_value2item[$id2]) ) {
		if ( isset($id1_value2item[$id1]) ) {
			if ( $id1_value2item[$id1] != $id2_value2item[$id2] ) {
				print "Mismatch: {$id1_value2item[$id1]} has {$prop_id1}:{id1} but {$id2_value2item[$id2]} has {$prop_id2}:{id2}\n" ;
			}
		} else {
			$commands[] = $id2_value2item[$id2] . "\t" . $prop_id1 . "\t\"" . $id1 . "\" /* Adding via BEACON data for {$prop_id2} from {$beacon_file} */" ;
		}
		continue ;
	}

	// No such item yet, but maybe it exists for ID1?
	$sparql = "SELECT ?q { ?q wdt:{$prop_id1} '{$id1}' }" ;
	$items = $mnm->tfc->getSPARQLitems ( $sparql ) ;
	if ( count($items) > 1 ) {
		print "MULTIPLE ITEMS FOR {$prop_id1}:{$id1}\n" ;
		print_r ( $items ) ;
		continue ;
	}
	if ( count($items) == 1 ) {
		$q = array_pop ( $items ) ;
		$commands[] = "{$q}\t{$prop_id2}\t\"{$id2}\"" ;
		continue ;
	}

	// No such item, create one!
	if ( !$create_new_items ) continue ;
	$commands[] = "CREATE" ;
	$commands[] = "LAST\t{$prop_id1}\t\"{$id1}\"" ;
	$commands[] = "LAST\t{$prop_id2}\t\"{$id2}\"" ;
	if ( !addCommandsFromMixnmatch ( $catalog , $id2 , $text , $commands ) ) continue ;
}

#print_r ( $commands ) ;
runCommandsQS ( $commands ) ;

?>