#!/usr/bin/bash

# Person candidates
cat /data/project/mix-n-match/scripts/person_dates.sql | sql tools

# Taxon candidates
cat /data/project/mix-n-match/scripts/taxa.sql | sql tools

# Artwork candidates
cat /data/project/mix-n-match/scripts/artwork.sql | sql tools
