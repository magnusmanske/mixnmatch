#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;

if ( !isset($argv[1]) ) die ( "USAGE: invalidate_404s.php CATALOG_ID [skip_fully_matched=1]\n" ) ;
$catalog = $argv[1] * 1 ;

$sql = "SELECT id,ext_url FROM entry WHERE catalog={$catalog}" ;
if ( isset($argv[2]) and $argv[1]=='1' ) $sql .= " AND (user=0 OR q IS NULL)" ; # Don't change set ones
#$sql .= " AND id=915329" ; # TESTING
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) {
	$mnm->check404($o->id,$o->catalog,$o->ext_url) ;
}

?>