#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

if ( !isset($argv[1]) ) die ("USAGE: {$argv[0]} CATALOG [PROPERTY=P569] [YEAR_IS_ENOUGH=0]\n") ;

$prop = 'P569' ; // What to match; P569 / P570
$year_is_enough = false ;

$catalog = $argv[1] ;
if ( isset($argv[2]) ) $prop = $argv[2] ;
if ( isset($argv[3]) ) $year_is_enough = $argv[3] * 1 ;

$pd = new MixNMatch\PersonDates ( $catalog ) ;
$pd->birthdayMatcher ( $prop , $year_is_enough ) ;

?>