#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';

if ( isset($argv[1]) ) $catalog_id = $argv[1] * 1 ;
else die ( "Catalog ID required as parameter 1\n" ) ;
if ( isset($argv[2]) ) $url_pattern = $argv[2] ;
else die ( "URL pattern required as parameter 2\n" ) ;
if ( strpos($url_pattern, '$1')===false ) die("URL pattern '{$url_pattern}' does not contain '$1'\n");
$testing = $testing = (isset($argv[3]) and $argv[3]=='1') ; ;

$mnm = new MixNMatch\MixNMatch ;

$sql = "SELECT id,ext_id,ext_url FROM entry WHERE catalog={$catalog_id}" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$new_url = str_replace('$1', $o->ext_id, $url_pattern) ;
	$sql = "UPDATE entry SET ext_url='".$mnm->escape($new_url)."' WHERE id={$o->id}" ;
	if ( $testing ) print "{$sql}\n" ;
	else $mnm->getSQL ( $sql ) ;
}

?>