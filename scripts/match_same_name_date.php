#!/usr/bin/php
<?PHP

# Finds two entries with identical names, birth and death dates, where one is manually matched, and the other is not.
# Then uses the match of the first to update the other

require_once dirname(__DIR__) . '/vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;

$updated_catalogs = [] ;

$sql = "
SELECT e1.entry_id,e1.catalog,e1.born,e1.died,e2.q FROM vw_dates e1,vw_dates e2
WHERE e1.ext_name=e2.ext_name
AND e1.catalog IN (SELECT id FROM catalog WHERE active=1)
AND e2.catalog IN (SELECT id FROM catalog WHERE active=1)
AND e1.born=e2.born
AND e1.died=e2.died
AND (e1.q IS NULL or e1.user=0)
AND e2.user>0
AND e2.q>0
HAVING (e1.born!='' and e1.died!='') OR (e1.born='' and length(e1.died)=10) OR (e1.died='' and length(e1.born)=10)
" ;

$cnt = 0 ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){

	# Paranoia
	if ( $o->born == '' and strlen($o->died) < 10 ) continue ;
	if ( $o->died == '' and strlen($o->born) < 10 ) continue ;

	$ts = $mnm->getCurrentTimestamp();
	$sql = "UPDATE entry SET `q`={$o->q},`user`=3,`timestamp`='{$ts}' WHERE id={$o->entry_id} AND (q IS NULL or user=0)" ;
	$mnm->getSQL ( $sql ) ;
	$updated_catalogs[$o->catalog] = 1 ;
	$cnt++ ;
}

print "{$cnt} entries matched\n" ;

foreach ( $updated_catalogs AS $catalog => $dummy ) {
	$mnm->queue_job ( $catalog , 'microsync' ) ;
}

?>