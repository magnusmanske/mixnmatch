#!/usr/bin/php
<?PHP

# Scans entry descriptions of specific catalogs, tries to find nationalities of people,
# and adds them to the auxiliary table

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

function flush_sql ( $sql ) {
	global $mnm ;
	if ( count($sql) == 0 ) return [] ;
	$sql = "INSERT IGNORE INTO auxiliary (entry_id,aux_p,aux_name) VALUES " . implode(',',$sql) ;
	$mnm->getSQL ( $sql ) ;
	return [] ;
}

$mnm = new MixNMatch\MixNMatch ;

if ( !isset($argv[1]) ) exit(0); # TODO
$catalog_id = $argv[1] * 1 ;

$sparql = "SELECT ?q ?demonym { ?q wdt:P1549 ?demonym MINUS { ?q wdt:P1366 [] } MINUS { ?q wdt:P31 wd:Q41710 } MINUS { ?q wdt:P31 wd:Q82794 } }" ;
$j = $mnm->tfc->getSPARQL ( $sparql ) ;
$demonym2q = [] ;
foreach ( $j->results->bindings AS $b ) {
	$q = preg_replace("|^.+/|",'',$b->q->value) ;
	$qnum = substr($q,1)*1 ;
	if ( $qnum > 10000 ) continue ; # Hard filter
	if ( in_array($q, ["Q842","Q220"]) ) continue ;
	$demonym = strtolower($b->demonym->value) ;
	$demonym2q[$demonym][$q] = $q ;
}
foreach ( $demonym2q AS $demonym => $qs ) {
	if ( count($qs) == 1 ) $demonym2q[$demonym] = array_values($qs)[0] ;
	else unset($demonym2q[$demonym]); # 2 or more, not using
}
# Hardcoded
$demonym2q["american"] = "Q30" ;
$demonym2q["yugoslavian"] = "Q36704" ;
$demonym2q["ukrainian"] = "Q212" ;

$sql = "SELECT * FROM entry WHERE catalog={$catalog_id} AND ext_desc!='' AND `type`='Q5'" ;
$sql .= " AND NOT EXISTS (SELECT * FROM auxiliary WHERE entry_id=entry.id AND aux_p=27)" ;
$result = $mnm->getSQL ( $sql ) ;
$sql = [] ;
while ( $o = $result->fetch_object() ) {
	$desc = strtolower($o->ext_desc) ;
	$matches = [] ;
	foreach ( $demonym2q AS $demonym => $q ) {
		if ( strstr($desc, $demonym) ) $matches[$q] = $q ;
	}
	if ( count($matches) == 0 ) continue ;
	if ( count($matches)>1 ) {
		#print "{$desc}: ".implode(',',$matches)."\n" ;
		continue ;
	}
	$q = array_values($matches)[0] ;
	$sql[] = "({$o->id},27,'{$q}')" ;
	if ( count($sql) >= 100 ) $sql = flush_sql($sql) ;
}
$sql = flush_sql($sql) ;

?>