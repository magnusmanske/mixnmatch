<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once ( "/data/project/mix-n-match/tests/shared.php" ) ;
use PHPUnit\Framework\TestCase;

class MixnMatchHelperTest extends TestCase {

    protected $mnm ;
    protected $mnm_helper ;

    protected function setUp(): void {
        $this->mnm = new MixNMatch\MixNMatch () ;
        $this->mnm_helper = new MixNMatch\Helper ( 1 , $this->mnm ) ;
    }

    public function testConstructor () {
        $this->assertSame(1,$this->mnm_helper->get_catalog());
    }

    public function test_setCodeFragment1 () {
        $cf = (object) [] ;
        $this->expectExceptionMessage('MixNMatch\Helper::setCodeFragment: Trying to set a code fragment with bad/missing success flag');
        $this->mnm_helper->setCodeFragment($cf);
    }

    public function test_setCodeFragment2 () {
        $cf = (object) [ 'success'=>0 ] ;
        $this->expectExceptionMessage('MixNMatch\Helper::setCodeFragment: Trying to set a code fragment with bad/missing success flag');
        $this->mnm_helper->setCodeFragment($cf);
    }

    public function test_setCodeFragment3 () {
        $cf = (object) [ 'success'=>1 ] ;
        $this->expectExceptionMessage('MixNMatch\Helper::setCodeFragment: Trying to set a code fragment with bad/missing is_active flag');
        $this->mnm_helper->setCodeFragment($cf);
    }

    public function test_setCodeFragment4 () {
        $cf = (object) [ 'success'=>1 , 'is_active'=>0 ] ;
        $this->expectExceptionMessage('MixNMatch\Helper::setCodeFragment: Trying to set a code fragment with bad/missing is_active flag');
        $this->mnm_helper->setCodeFragment($cf);
    }

    public function test_setCodeFragment5 () {
        $cf = (object) [ 'success'=>1 , 'is_active'=>1 , 'php' => '$xyz=1;' ] ;
        $this->mnm_helper->setCodeFragment($cf);
        $this->assertSame($cf,$this->mnm_helper->get_code_fragment());
    }

    public function test_loadCodeFragment () { # TODO other classes as well, with actual code fragments
        $this->expectExceptionMessage('MixNMatch\Helper::loadCodeFragment: no cf_function');
        $this->mnm_helper->loadCodeFragment();
    }

    public function test_parse_date() {
        $method = getMethod('MixNMatch\Helper','parse_date');
        $this->assertSame('2020',$method->invokeArgs($this->mnm_helper, ['2020']));
        $this->assertSame('0001',$method->invokeArgs($this->mnm_helper, ['1']));
        $this->assertSame('0012',$method->invokeArgs($this->mnm_helper, ['12']));
        $this->assertSame('0123',$method->invokeArgs($this->mnm_helper, ['123']));
        $this->assertSame('1974-05',$method->invokeArgs($this->mnm_helper, ['5. 1974']));
        $this->assertSame('1974-05-24',$method->invokeArgs($this->mnm_helper, ['24.  5. 1974']));
        $this->assertSame('1974-05-24',$method->invokeArgs($this->mnm_helper, ['May 24, 1974']));
        #$this->assertSame('1974-05',$method->invokeArgs($this->mnm_helper, ['May 1974'])); # Currently does not work, but not really important
    }

    public function test_rewriteGlobalMethods() {
        $method = getMethod('MixNMatch\Helper','rewriteGlobalMethods');
        $this->assertSame('$a=2;',$method->invokeArgs($this->mnm_helper, ['$a=2;']));
    }

}




/*
Misc notes
    public function testPushAndPop()
    {
        $this->expectException('Exception') ;
        $this->expectExceptionMessage('MixNMatch\Helper::setCodeFragment: Trying to set a code fragment with bad/missing success flag');

        $stack = [];
        $this->assertSame(0, count($stack));

        array_push($stack, 'foo');
        $this->assertSame('foo', $stack[count($stack)-1]);
        $this->assertSame(1, count($stack));

        $this->assertSame('foo', array_pop($stack));
        $this->assertSame(0, count($stack));
    }
*/
