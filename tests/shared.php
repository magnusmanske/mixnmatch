<?php

# To get to protected and private methods
function getMethod($class_name,$name) {
  $class = new ReflectionClass($class_name);
  $method = $class->getMethod($name);
  $method->setAccessible(true);
  return $method;
}


?>