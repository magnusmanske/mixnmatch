<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once ( "/data/project/mix-n-match/tests/shared.php" ) ;
use PHPUnit\Framework\TestCase;

class PersonDatesTest extends TestCase {

    protected $mnm ;
    protected $mnm_helper ;

    protected function setUp(): void {
        $this->mnm = new MixNMatch\MixNMatch ;
        $this->mnm_helper = new MixNMatch\PersonDates ( 1 , $this->mnm ) ;
    }

    public function test_rewriteGlobalMethods() {
        $method = getMethod('MixNMatch\PersonDates','rewriteGlobalMethods');
        
        // Neutral functionality already tested in MixnMatchHelperTest

        $this->assertSame('bml("foo");',$method->invokeArgs($this->mnm_helper, ['bml("foo");']));
        $this->assertSame('ml_("foo");',$method->invokeArgs($this->mnm_helper, ['ml_("foo");']));
        $this->assertSame('$this->try_get_three_letter_month("foo");',$method->invokeArgs($this->mnm_helper, ['ml("foo");']));

        $this->assertSame(';xdp("foo");',$method->invokeArgs($this->mnm_helper, [';xdp("foo");']));
        $this->assertSame(';dp_("foo");',$method->invokeArgs($this->mnm_helper, [';dp_("foo");']));
        $this->assertSame(';$this->parse_date("foo");',$method->invokeArgs($this->mnm_helper, [';dp("foo");']));
    }

    public function test_processEntry() {
        $cf = (object) [
            'success' => 1 ,
            'is_active' => 1,
            'php' => 'if ( preg_match ( "/;\s*(\d{3,4})-(\d{3,4})$/" , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;' ,
            'json' => json_decode('{}') ,

        ] ;
        $this->mnm_helper->setCodeFragment($cf) ;

        $o = (object) [
            'id' => 12345,
            'ext_desc' => 'foo-bar; 1234-1301' ,
            'user' => 2 ,
            'q' => 1 ,
            'type' => 'Q5'
        ] ;

        $commands = $this->mnm_helper->processEntry($o) ;
        $this->assertSame(count($commands),1);
        $command = $commands[0] ;
        $this->assertTrue(isset($command));
        $this->assertSame(12345,$command->getEntryID()) ;
        $this->assertSame('1234',$command->getData()->born) ;
        $this->assertSame('1301',$command->getData()->died) ;
        $this->assertTrue($command->getData()->is_matched);

        $o->ext_desc = 'foo-bar; 1234-13' ;
        $commands = $this->mnm_helper->processEntry($o) ;
        $this->assertSame(count($commands),0);
    }
}

?>