<?php

# ./vendor/bin/phpunit tests/EntryTest.php

use PHPUnit\Framework\TestCase;

require_once ( "/data/project/mix-n-match/tests/shared.php" ) ;

class EntryTest extends TestCase {

    protected function setUp(): void {
    }

    public function test_constructor_id() {
        $entry = new MixNMatch\Entry ( 12345 ) ;
        $this->assertSame($entry->id(),12345);
        $entry = new MixNMatch\Entry ( "67890" ) ;
        $this->assertSame($entry->id(),67890);
    }

    public function test_constructor_object() {
        $entry_array = [ 'id'=>12345 ] ;
        $entry = new MixNMatch\Entry ( $entry_array ) ;
        $this->assertSame($entry->id(),12345);
        
        $entry_array = [ 'id'=>'12345','name'=>'foo ','desc'=>' bar','url'=>' https://x.y.z','timestamp'=>'20210101170000 ','born'=>' 1923','died'=>'19801213 '] ; # TODO more
        $entry = new MixNMatch\Entry ( $entry_array ) ;
        foreach ( $entry_array AS $k => $v ) {
            $this->assertSame(''.$entry->$k(),trim(''.$v));
        }

        $entry = new MixNMatch\Entry ( (object) $entry_array ) ;
        foreach ( $entry_array AS $k => $v ) {
            $this->assertSame(''.$entry->$k(),trim(''.$v));
        }
        
    }

    public function test_loadEntryFromDatabase() {
        $mnm = new MixNMatch\MixNMatch ;
        $entry = new MixNMatch\Entry ( 2712 , $mnm ) ;
        $entry->loadEntryFromDatabase() ;
        $this->assertSame($entry->name(),'Fletcher Christian');
    }

    public function test_loadMetadataFromDatabase() {
        $mnm = new MixNMatch\MixNMatch ;
        foreach ( ['aliases','descriptions','mnm_relation','multi_match','auxiliary','location','person_dates'] AS $table_name ) { # ,'' 'location', 'person_dates',
            $sql = "SELECT `entry_id` FROM `{$table_name}` LIMIT 1" ;
            $result = $mnm->getSQL ( $sql ) ;
            if ( $o = $result->fetch_object() ) {
                $entry_id = $o->entry_id ;
                $entry = new MixNMatch\Entry ( $entry_id , $mnm ) ;
                $entry->loadMetadataFromDatabase() ;
                if ( $table_name == 'location' ) {
                    $loc = $entry->location() ;
                    $metadata = ( isset($loc) and isset($loc->lat) and isset($loc->lon) ) ?["OK"]:[];
                } else if ( $table_name == 'person_dates' ) $metadata = ($entry->born().$entry->died()!='')?["OK"]:[];
                else if ( $table_name == 'auxiliary' ) $metadata = $entry->aux() ;
                else $metadata = $entry->$table_name() ;
                $this->assertTrue(count($metadata)>0);
            } else {
                throw new \Exception("test_loadEntryFromDatabase: {$table_name}") ;
            }
        }
    }

}

?>