<?PHP

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class UrlToAux {
	public $mnm ;
	public $testing = false ;
	public $catalogs_requiring_curl = [3849] ;
	protected $last_helper = []  ;
	protected $random_catalog_blacklist = [
		150 , # FAST, no need (VIAF), I guess
		2050 , # VIAF
		3047 , # BNF
	] ;


	function __construct( $mnm = '' , $testing = false ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
		$this->testing = $testing ;
	}

	protected function get_contents_from_url ( $catalog_id , $url ) {
		if ( !isset($this->last_helper[$catalog_id]) ) {
			$this->last_helper[$catalog_id] = new Helper ( $catalog_id , $this->mnm ) ;
			if ( in_array($catalog_id, $this->catalogs_requiring_curl) ) $this->last_helper[$catalog_id]->use_curl = true ;
		}
		return $this->last_helper[$catalog_id]->get_contents_from_url ( $url ) ;
	}

	protected function setAux ( $entry_id , $aux_p , $aux_name ) {
		if ( $this->testing ) print "P{$aux_p}:{$aux_name}\n" ;
		else {
			try {
				$this->mnm->setAux ( $entry_id , $aux_p , $aux_name ) ;
			} catch ( Exception $e ) {
				$this->mnm->dbmConnect(true);
				$this->mnm->setAux ( $entry_id , $aux_p , $aux_name ) ;
			}
			if ( $aux_p == 31 ) {
				$sql = "UPDATE entry SET `type`='" . $this->mnm->escape($aux_name) . "' WHERE `type`='' AND id={$entry_id}" ;
				$this->mnm->getSQL($sql);
			}
		}
		return true ;
	}

	public function run ( $catalog ) {
		$dates_have_changed = false ;

		$kv = [] ;
		if ( $catalog!='random' ) {
			$result = $this->mnm->getSQL ( "SELECT * FROM kv_catalog WHERE catalog_id={$catalog}" ) ;
			while ( $o = $result->fetch_object() ) $kv[$o->kv_key] = $o->kv_value ;
		}

		$sql = "SELECT * FROM entry WHERE catalog={$catalog} AND ext_url!=''" ;
		$sql .= " AND (user=0 or q is null)" ; # Don't query for already matched entries
		if ( isset($kv['aux_from_url_type_filter']) ) $sql .= " AND `type`='{$kv['aux_from_url_type_filter']}'" ;
		if ( isset($argv[3]) ) $sql .= " AND ext_id='{$argv[3]}'" ;
		if ( $catalog == 'random' ) {
			$r = rand()/getrandmax()  ;
			$sql = "SELECT * FROM entry WHERE (user=0 or q is null) AND `type`='Q5' AND random>{$r} HAVING ext_url!='' AND catalog IN (SELECT id FROM catalog WHERE active=1) AND catalog NOT IN (".implode(',',$this->random_catalog_blacklist).") ORDER BY random LIMIT 50" ;
		}
		if ( $this->testing and $catalog!='random' ) print "{$sql}\n" ;

		$result = $this->mnm->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			$url = $o->ext_url ;
			$url = preg_replace ( '| |' , '%20' , $url ) ;
			if ( $catalog == 4292 ) $url = "https://data.nlg.gr/data/authority/record{$o->ext_id}.csv" ;

			if($this->testing and $catalog!='random') print "{$url}\n" ;
			$html = $this->get_contents_from_url ( $o->catalog , $url ) ;
			#$html = @file_get_contents ( $url ) ;
			$html = preg_replace ( '|\s+|' , ' ' , $html ) ; # Simple spaces
		#	if($this->testing) print "{$html}\n" ;
			$found = false ;

			# GND 
			if (
				preg_match ( '|href=["\']https{0,1}://d-nb\.info/gnd/(\d+X{0,1})["\']|' , $html , $m ) or # GND
				preg_match ( '|https{0,1}://tools.wmflabs.org/persondata/p/gnd/(\d+X{0,1})|i' , $html , $m ) or # GND via persondata
				preg_match ( '|https{0,1}://viaf.org/viaf/sourceID/DNB\|(\d+X{0,1})|i' , $html , $m ) or # GND via VIAF
				preg_match ( '|http://mmlo.de/Q/GND=(\d+X{0,1})|i' , $html , $m ) # Special
				) {
				$found = $this->setAux ( $o->id , 227 , $m[1] ) ;
			}

			# VIAF
			if (
				preg_match ( '|\bhttps{0,1}://viaf\.org/viaf/(\d+)|' , $html , $m ) or
				preg_match ( '|\bhttps{0,1}://viaf\.org/(\d+)|' , $html , $m )
				) {
				$found = $this->setAux ( $o->id , 214 , $m[1] ) ;
			}

			# CERL
			if (
				preg_match ( '|\bhttps{0,1}://thesaurus.cerl.org/record/(cnp\d+)|' , $html , $m ) 
				) {
				$found = $this->setAux ( $o->id , 1871 , $m[1] ) ;
			}

			# ISNI
			if (
				preg_match ( '|\bhttps{0,1}://isni.org/isni/(\d{4})(\d{4})(\d{4})(\d{4})|' , $html , $m )
				) {
				$id = "{$m[1]} {$m[2]} {$m[3]} {$m[4]}" ;
				$found = $this->setAux ( $o->id , 213 , $id ) ;
			}

			# MacTutor (P1563)
			if (
				preg_match ( '|\bhttps{0,1}://www-gap\.dcs\.st-and\.ac\.uk/\~history/Biographies/(.+?)\.html|' , $html , $m ) or
				preg_match ( '|\bhttps{0,1}://www-history\.mcs\.st-andrews\.ac\.uk/Biographies/(.+?)\.html|' , $html , $m )
				) {
				$found = $this->setAux ( $o->id , 1563 , $m[1] ) ;
			}

			# GeoNames (P1566)
			if (
				preg_match ( '|\bhttps{0,1}://www.geonames.org/(\d+)|' , $html , $m )
				) {
				$found = $this->setAux ( $o->id , 1566 , $m[1] ) ;
			}

			# Pleiades place (P1584)
			if (
				preg_match ( '|\bhttps{0,1}://pleiades.stoa.org/places/(\d+)|' , $html , $m )
				) {
				$found = $this->setAux ( $o->id , 1584 , $m[1] ) ;
			}

			# DBLP (P2456)
			if (
				preg_match ( '|\bhttps{0,1}://dblp\.uni-trier\.de/pid/(.+?)["\']|' , $html , $m )
				) {
				$found = $this->setAux ( $o->id , 2456 , $m[1] ) ;
			}

			# Discogs artist
			if ( preg_match('|\bhttps{0,1}://www\.discogs\.com/artist/(\d+)"|' , $html , $m ) ) {
				$found = $this->setAux ( $o->id , 1953 , $m[1] ) ;
			}

			# MusicBrains artist
			if ( preg_match('|\bhttps{0,1}://musicbrainz\.org/artist/([a-z0-9-]+)"|' , $html , $m ) ) {
				$found = $this->setAux ( $o->id , 434 , $m[1] ) ;
			}

			# Historic Scotland ID
			if ( preg_match('|\bhttps{0,1}://portal\.historic-scotland\.gov\.uk/designation/([A-Z]{1,2}\d+)|' , $html , $m ) ) {
				$found = $this->setAux ( $o->id , 709 , $m[1] ) ;
			}

			# Wikidata
			if (
				$catalog != 3402 and 
				preg_match ( '|\bhttps{0,1}://www\.wikidata\.org/wiki/(Q\d+)|' , $html , $m ) 
				) {
				if ( $this->testing ) print "WD:{$m[1]}\n" ;
				else $this->mnm->setMatchForEntryID ( $o->id , $m[1] , 4 , true , false ) ;
				$found = true ;
			}

			# National Gallery of Art artist ID (P2252)
			if ( preg_match ( '|<A TARGET="_blank" HREF="http://www.nga.gov/content/ngaweb/Collection/artist-info\.(\d+)\.html|' , $html , $m ) ) { 
				$found = $this->setAux ( $o->id , 2252 , $m[1] ) ;
			}

			if ( $catalog == 2376 ) {
				// Insert subset into TLG catalog
				if ( preg_match ( '|href="/catalog/urn:cts:greekLit:tlg(\d{4})"|' , $html , $m ) ) {
					$e = [
						'catalog' => 2377,
						'id' => $m[1] ,
						'url' => 'http://data.perseus.org/catalog/urn:cts:greekLit:tlg'.$m[1] ,
						'name' => $o->ext_name,
						'desc' => $o->ext_desc,
						'type' => 'Q5'
					] ;
					if ( !$this->testing) $this->mnm->addNewEntry($e) ;
					else print_r($e) ;
					$found = true ;
				}
			}

			if ( $catalog == 3589 ) {
				$lat = '' ;
				$lon = '' ;
				if ( preg_match ( '|<div class="bold">Latitude</div>.*?<div class="bold">(.+?)</div>|' , $html , $m ) ) $lat = $m[1] ;
				if ( preg_match ( '|<div class="bold">Longitude</div>.*?<div class="bold">(.+?)</div>|' , $html , $m ) ) $lon = $m[1] ;
				if ( $lat != '' and $lon != '' ) {
					if ( $this->testing ) print "{$o->id}: {$lat}/{$lon}\n" ;
					else $this->mnm->setLocation ( $o->id , $lat , $lon ) ;
				}
			}

			if ( $catalog == 'random' and $found ) {
				print "$url\n" ;
				#print_r ( $o ) ;
				$sql = "SELECT * FROM auxiliary WHERE entry_id={$o->id}" ;
				$result2 = $this->mnm->getSQL ( $sql ) ;
				while ( $o2 = $result2->fetch_object() ) {
					print "Has P{$o2->aux_p}:'{$o2->aux_name}'\n" ;
				}
				$sql = "SELECT count(*) AS cnt FROM auxiliary,entry WHERE catalog={$o->catalog} AND entry_id=entry.id" ;
				$result2 = $this->mnm->getSQL ( $sql ) ;
				if ( ($o2 = $result2->fetch_object()) ) {
					print "Catalog https://mix-n-match.toolforge.org/#/catalog/{$o->catalog} has {$o2->cnt} auxiliary entries in total\n" ;
				} else {
					print "Catalog https://mix-n-match.toolforge.org/#/catalog/{$o->catalog} has no auxiliary entries so far\n" ;
				}
				print "----\n\n" ;
			}
		}

		if ( $catalog != 'random' ) {
			$jid = $this->mnm->queue_job($catalog*1,'auxiliary_matcher');
			$this->mnm->queue_job($catalog*1,'microsync',$jid,'',0,0,'LOW_PRIORITY');
			if ( $dates_have_changed ) {
				$jid = $this->mnm->queue_job($catalog*1,'update_person_dates');
				$this->mnm->queue_job($catalog*1,'match_person_dates',$jid);
			}
		}

	}
}


?>