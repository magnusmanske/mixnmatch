<?PHP

namespace MixNMatch ;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class CoordinateMatcher {
	public $mnm ;
	public $simulate = false ;
	public $catalog_id = 'random' ;
	public $default_max_distance = '500m' ;
	public $max_automatch_distance = 0.1 ; # 100m
	public $max_results_for_random_catalogs = 5000 ;
	public $min_levenshtein_distance = 6;
	public $verbose = false ;
	public $permissions = [] ;
	public $bad_catalogs = [] ;
	public $catalogs_to_microsync = [] ;
	public $use_single_catalog = false ;
	protected $subclass_tree = [];

	function __construct ( $catalog_id , $simulate = '' , $mnm = '' ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
		if ( $catalog_id != 'random' ) $catalog_id = (int) $catalog_id ;
		if ( $catalog_id == 0 ) $catalog_id = 'random' ;
		else $this->use_single_catalog = true ;
		$this->catalog_id = $catalog_id ;
		$this->simulate = $simulate=='1' ;
		if ( $this->simulate ) $this->verbose = true ;
		if ( $this->simulate ) print "SIMULATING!\n" ;
		$this->load_permissions() ;
		$this->check_bad_catalogs() ;
		$this->catalog = new \MixNMatch\Catalog($this->catalog_id,$this->mnm);
	}

	function language() {
		return $this->catalog->data()->search_wp;
	}

	function check_bad_catalogs () {
		if ( in_array($this->catalog_id,$this->bad_catalogs) ) {
			throw new \Exception(__METHOD__.": Catalog {$catalog_id} is in the list of Bad Catalogs for CoordinateMatcher!");
		}
	}

	function get_quickstatements_object () {
		global $toolname ;
		$qs = new \QuickStatements() ;
		$qs->use_oauth = false ;
		$qs->bot_config_file = "/data/project/mix-n-match/reinheitsgebot.conf" ;
		$qs->toolname = $toolname ;
		$qs->sleep = 1 ;
		$qs->use_command_compression = true ;
		return $qs ;
	}

	// $commands = [] , one QS V1 command per element, no newlines
	function run_quickstatement_commands ( $commands ) {
		if ( !isset($this->qs) ) $this->qs = $this->get_quickstatements_object() ;
		$commands = implode ( "\n" , $commands ) ;
		$tmp_uncompressed = $this->qs->importData ( $commands , 'v1' ) ;
		$tmp['data']['commands'] = $this->qs->compressCommands ( $tmp_uncompressed['data']['commands'] ) ;
		$this->qs->runCommandArray ( $tmp['data']['commands'] ) ;
		if ( !isset($this->qs->last_item) ) return ;
		$last_item = 'Q' . preg_replace ( '/\D/' , '' , "{$this->qs->last_item}" ) ;
		return $last_item ;
	}


	function load_permissions() {
		$this->permissions = [] ; 
		$sql = "SELECT * FROM `kv_catalog` WHERE `kv_key` IN ('allow_location_match','allow_location_create','allow_location_operations','location_distance','location_force_same_type')" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			if ( $o->kv_key == 'location_distance' ) {
				$this->permissions[$o->kv_key][$o->catalog_id] = $o->kv_value  ;
			} else {
				$this->permissions[$o->kv_key][$o->kv_value][] = $o->catalog_id ;
			}
		}
		$this->bad_catalogs = $this->permissions['allow_location_operations']['no'] ;
	}

	function load_subclass_tree($q) {
		if ( trim(preg_replace('|\D|','',$q))=='' ) return [];
		$this->mnm->wil->sanitizeQ ( $q ) ;
		if ( isset($this->subclass_tree[$q]) ) return $this->subclass_tree[$q];
		$sparql = "SELECT ?q { ?q wdt:P279* wd:{$q} }";
		try {
			$this->subclass_tree[$q] = $this->mnm->tfc->getSPARQLitems ( $sparql , 'q' ) ;
			// print "SUBCLASS TREE: {$q} => ".json_encode($this->subclass_tree[$q])."\n";
		} catch (Exception $e) {
			$this->subclass_tree[$q] = [$q];
		}
		return $this->subclass_tree[$q];
	}

	function try_match_via_type ( $entry , $results ) {
		if ( $entry->type=='' ) return false;
		$type_items = $this->load_subclass_tree($entry->type);

		# Load candidate items
		$items = [];
		foreach ( $results AS $r ) {
			if ( $r->ns==0 ) {
				$q = $r->title;
				$this->mnm->wil->sanitizeQ ( $q ) ;
				$items[] = $q;
			}
		}
		if ( count($items)==0 ) return false;
		$this->mnm->wil->loadItems ( $items ) ;

		# Check for type
		$matches = [];
		foreach ( $items AS $q ) {
			$i = $this->mnm->wil->getItem ( $q ) ;
			if ( !isset($i) ) continue ;
			foreach ( $type_items AS $ti ) {
				if ( $i->hasTarget("P31",$ti) ) $matches[] = $q ;
			}
		}
		$this->mnm->wil->deleteItems($items);
		if ( count($matches)>1 ) $this->mnm->setMultiMatch($entry->id,$matches);
		if ( count($matches)!=1 ) return false;

		$q = $matches[0];
		if ( $this->simulate ) print "https://mix-n-match.toolforge.org/#/entry/{$entry->id}: TYPE MATCH FOUND: http://www.wikidata.org/wiki/{$q}\n";
		else $this->mnm->setMatchForEntryID ( $entry->id , $q , 0 , true , false ) ;
		return true;
	}

	function similar_name_prep(&$s) {
		$s = trim($s);
		$s = preg_replace('/^(Kirche|Wehrkirche|Kirchenburg) /i','',$s);
		$s = preg_replace('| *fortified church *|i',' ',$s);
		$s = preg_replace('|^(.+) castle|i','Castle $1',$s);
		$s = preg_replace('|^(.+) tower|i','Tower $1',$s);
		$s = preg_replace('|^Torre |i','Tower ',$s);
		$s = preg_replace('/^(Kasteel|Castello|Burgruine|Burgstall|Abgegangene Burg) /i','Burg ',$s);
		$s = preg_replace('|\bSchloss\b|i','Burg',$s);
		$s = preg_replace('|^Burg |i','Castle ',$s);
		$s = trim(strtolower($s));
	}

	function is_name_similar_enough($s1,$s2) {
		// print "{$s1} / {$s2} => " ;
		$this->similar_name_prep($s1);
		$this->similar_name_prep($s2);
		// print "{$s1} / {$s2}\n" ;
		$distance = levenshtein($s1,$s2);
		return ($distance>=$this->min_levenshtein_distance);
	}


	function try_match_via_wikidata_search ( $entry , $results ) {
		$matches = [] ;
		$ext_name = strtolower($entry->ext_name) ;
		$ext_name = preg_replace ( '| *\(.*$|' , '' , $ext_name ) ;
		foreach ( $results AS $r ) {
			$titles = explode ( "\n" , strtolower($r->snippet) ) ;
			if ( in_array($ext_name, $titles) ) {
				$matches[] = $r->title ;
			} else {
				foreach ( $titles AS $t ) {
					if ( $this->is_name_similar_enough($ext_name,$t) ) {
						$matches[] = $r->title ;
						break ;
					}
				}
			}
		}
		if ( count($matches)==1 ) {
			$q = $matches[0] ;
			print "Matching https://mix-n-match.toolforge.org/#/entry/{$entry->id} to https://www.wikidata.org/wiki/{$q}\n" ;
			if ( !$this->simulate ) $this->mnm->setMatchForEntryID ( $entry->id , $q , 4 , true , false ) ;
			$this->catalogs_to_microsync["{$o->catalog}"] = true ;
			return true ;
		} else if ( count($matches)>1 ) {
			if ( $this->simulate ) {
				print "WARNING: https://mix-n-match.toolforge.org/#/entry/{$entry->id} seems to match:\n" ;
				foreach ( $matches AS $m ) print "- https://www.wikidata.org/wiki/{$m}\n" ;
			} else {
				// $q = $matches[0] ;
				// $this->mnm->setMatchForEntryID ( $entry->id , $q , 0 , true , false ) ;
				$this->mnm->setMultiMatch($entry->id,$matches);
			}
		}
		return false ;
	}

	# Returns true if no matches found
	function try_match_via_sparql_query ( $entry , $max_distance ) {
		# Check SPARQL (thorough)
		$lang = $this->language();
		$sparql = "SELECT ?place ?location ?distance ?p31 ?label WHERE {
		    SERVICE wikibase:around { 
		      ?place wdt:P625 ?location . 
		      bd:serviceParam wikibase:center 'Point({$entry->lon} {$entry->lat})'^^geo:wktLiteral . 
		      bd:serviceParam wikibase:radius '{$max_distance}' . 
		      bd:serviceParam wikibase:distance ?distance .
		    }
		    OPTIONAL { ?place wdt:P31 ?p31 }
		    OPTIONAL { ?place rdfs:label ?label FILTER (lang(?label) = \"{$lang}\") }
		} ORDER BY (?distance) LIMIT 500" ;
		if ( $this->verbose ) print "{$sparql}\n\n" ;
		$j = $this->mnm->tfc->getSPARQL ( $sparql , 'place' ) ;
		$bindings = ($j->results->bindings??[]) ;
		foreach ( $bindings as $b ) {
			if ( $b->distance->value > $this->max_automatch_distance ) break ;
			if ( !isset($b->p31) or !isset($b->p31->value) ) continue ;
			if ( ($b->p31->type??'') != 'uri' ) continue ;
			$p31 = preg_replace('|^.+/|','',$b->p31->value);
			if ( $p31 != $entry->type ) continue ;
			$q = preg_replace('|^.+/|','',$b->place->value);
			if ( $q == "Q{$entry->q}" ) break ; # Already the target

			if ( isset($b->label) and isset($b->label->value) and $b->label->value!='' ) {
				$ext_name = strtolower($entry->ext_name) ;
				$label = strtolower($b->label->value);
				if ( $this->is_name_similar_enough($ext_name,$label) ) {
					print "https://mix-n-match.toolforge.org/#/entry/{$entry->id} => {$b->place->value} AS AUTOMATCH (distance {$distance})\n" ;
					if ( !$this->simulate ) $this->mnm->setMatchForEntryID ( $entry->id , $q , 0 , true , false ) ;
				}
			}
			break ;
		}
		return count($bindings) == 0 ;
	}

	function entry_generator () {
		$sql = "SELECT * FROM `vw_location` WHERE (`q` IS NULL OR `user`=0) AND `ext_name`!=''" ;
		if ( $this->use_single_catalog ) {
			$sql .= " AND `catalog`={$this->catalog_id}" ;
			// $sql .= " AND id=167539338"; # TESTING FIXME
		} else {
			$r = rand()/getrandmax() ;
			$sql .= " AND `catalog` NOT IN (".implode(',',$this->bad_catalogs).")" ;
			$sql .= " AND `random`>={$r} ORDER BY `random` LIMIT {$this->max_results_for_random_catalogs}" ;
		}

		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) yield $o ;
		yield from [] ;
	}

	function get_max_distance_sparql_for_entry ( $o ) {
		$max_distance = $this->permissions['location_distance'][$o->catalog] ?? $this->default_max_distance ;
		$max_distance_sparql = 1 ; # km
		if ( preg_match("/^([0-9.]+)km$/",$max_distance,$m) ) $max_distance_sparql = $m[1]*1 ;
		else if ( preg_match("/^([0-9.]+)m$/",$max_distance,$m) ) $max_distance_sparql = ((float)$m[1])/1000 ;
		return [$max_distance,$max_distance_sparql] ;
	}

	protected function get_prop_value_from_entry ( $o ) {
		$prop = '' ;
		$value = '' ;
		$force_same_type = in_array($o->catalog, $this->permissions['location_force_same_type']['yes']) ;
		if ( $force_same_type ) {
			$prop = 'P31' ;
			$value = $o->type ;
		}
		return [$prop,$value] ;
	}

	public function match_all_entries () {
		foreach ( $this->entry_generator() AS $o ) {
			list($prop,$value) = $this->get_prop_value_from_entry($o) ;
			list($max_distance,$max_distance_sparql) = $this->get_max_distance_sparql_for_entry ( $o ) ;

			# Check search (quick)
			$query = "nearcoord:{$max_distance},{$o->lat},{$o->lon}" ;
			$types = $this->load_subclass_tree($o->type);
			// if ( $this->simulate ) print "{$query}: ".json_encode($types)."\n";
			$sr = $this->mnm->getSearchResults ( $query , $prop , $types ) ;
			// if ( $this->simulate ) print_r($sr);
			if ( count($sr) > 0 ) {
				if ( in_array($o->catalog, $this->permissions['allow_location_match']['yes']) ) {
					$found = $this->try_match_via_wikidata_search ( $o , $sr );
					if ( !$found ) $found = $this->try_match_via_sparql_query ( $o , $max_distance_sparql ) ;
					if ( !$found ) $found = $this->try_match_via_type ( $o , $sr ) ;
				}
			} else { # No match found
				if ( in_array($o->catalog, $this->permissions['allow_location_create']['yes']) ) {
					if ( $this->try_match_via_sparql_query ( $o , $max_distance_sparql ) ) {
						$this->create_new_item_for_entry ( $o ) ;
					}
				}
			}
		}
		$this->microsync_catalogs() ;
	}

	protected function create_new_item_for_entry ( $o ) {
		if ( $this->verbose ) {
			print_r($o);
			print "https://wikishootme.toolforge.org/#lat={$o->lat}&lng={$o->lon}&zoom=15&layers=mixnmatch,wikidata_image,wikidata_no_image\n" ;
		}

		$commands = $this->mnm->getCreateItemForEntryCommands($o) ;
		if ( !isset($commands) ) return ;
		if ( $this->verbose ) print_r($commands);
		if ( !$this->simulate ) {
			$q = $this->run_quickstatement_commands($commands) ;
			$this->mnm->setMatchForEntryID ( $o->id , $q , 4 , true , false ) ;
		} else $q = "DUMMY" ;
		print "Matching https://mix-n-match.toolforge.org/#/entry/{$o->id} to https://www.wikidata.org/wiki/{$q} (NEW); catalog {$o->catalog}\n" ;		
	}

	protected function microsync_catalogs () {
		foreach ( $this->catalogs_to_microsync as $catalog_id => $dummy ) {
			print "Syncing {$catalog_id}\n" ;
			if ( !$this->simulate ) $this->mnm->queue_job($catalog_id,'microsync');
		}
	}

}


?>