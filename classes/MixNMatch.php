<?PHP

namespace MixNMatch;

require_once ( '/data/project/mix-n-match/public_html/php/ToolforgeCommon.php' ) ;
require_once ( '/data/project/mix-n-match/public_html/php/wikidata.php' ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;


class MixNMatch {
	public $dbm , $dbm_ro, $dbwd ;
	public $use_db_ro = false; // Use false if the ro-replica is lagging
	public $last_error = '' ;
	public $last_entry = [] ;
	public $testing = false ;
	public $tfc ;
	public $wil ;
	public $similar_languages = ['en','de','fr','es','it','pt','nl','gl','sv','fi'] ;
	public $possible_items ; # Set from getCreateItemForEntryCommands
	public $use_persistent_connection = false ;
	public $root_url = 'https://mix-n-match.toolforge.org' ;
	public $max_execution_time ; # MySQL SELECT MAX_EXECUTION_TIME

	private $mysql_log_file = '/data/project/mix-n-match/mysql_query.log' ;
	private $mysql_log_after_seconds = 5 ;
	private $user_cache = [] ;
	private $catalogs = [] ;
	private $wikidata_search_results = [] ;
	private $default_units = [
		'P2044'=>11573 # Metre
	] ;
	private $language_code_replace = ["no"=>"nb","cat"=>"ca","cz"=>"cs"] ;

	
	function __construct( $persistent=false ) {
		$this->use_persistent_connection = $persistent ;
		$this->tfc = new \ToolforgeCommon('mix-n-match') ;
		$this->tfc->tool_user_name = 'mix-n-match';
		$this->tfc->use_db_cache = false ;
		$this->dbm = $this->openMixNMatchDB() ;
		// $this->dbm_ro = $this->openMixNMatchDB_ro() ;
		$this->wil = new \WikidataItemList ;
	}

	public function dbmConnect($connection=true) {
		if ( $connection ) $this->dbm = $this->openMixNMatchDB() ;
		else unset ( $this->dbm ) ;
	}

	public function closeWikidataDatabase() {
		if ( !isset($this->dbwd) ) return ;
		unset($this->dbwd);
	}

	private function openMixNMatchDB () {
		$db = $this->tfc->openDBtool ( 'mixnmatch_p' , '' , '' , $this->use_persistent_connection ) ;
		if ( $db === false ) die ( "Cannot access DB: " . $o['msg'] ) ;
		$db->set_charset("utf8") ;
		return $db ;
	}

	private function openMixNMatchDB_ro () {
		$db = $this->tfc->openDBtool_ro ( 'mixnmatch_p' , '' , '' , $this->use_persistent_connection ) ;
		if ( $db === false ) die ( "Cannot access DB: " . $o['msg'] ) ;
		$db->set_charset("utf8") ;
		return $db ;
	}

	private function logError ( $msg = 'Unspecified error' ) {
		$this->last_error = $msg ;
		return false ;
	}

	public function escape ( $s ) {
		return $this->dbm->real_escape_string ( $s ) ;
	}

	public function getSQL ( $sql ) {
		if ( isset($this->max_execution_time) and preg_match('|^SELECT [^/]|i',$sql) ) {
			$sql = preg_replace('|^SELECT |i',"SELECT /*+ MAX_EXECUTION_TIME({$this->max_execution_time}) */ ",$sql);
		}

		if ( $this->use_db_ro and preg_match('|^SELECT |',$sql) and !preg_match('|/\* *NO_RO *\*/|',$sql) ) return $this->getSQL_ro($sql);
		else return $this->getSQL_rw($sql);
	}

	protected function getSQL_rw ( $sql ) {
		$reconnects_left = 4 ;
		while ( $reconnects_left > 0 ) {
			if ( !isset($this->dbm) ) $this->dbm = $this->openMixNMatchDB() ;
			try {
				$start = hrtime(true) ;
				$ret = $this->tfc->getSQL ( $this->dbm , $sql , 2 ) ;
				$end = hrtime(true) ;
				$seconds_elapsed = ($end-$start)/1e+9 ;
				if ( isset($this->mysql_log_file) and $seconds_elapsed > $this->mysql_log_after_seconds ) { # Log query
					if ( !preg_match('|\bAGAINST\b|',$sql) ) { # Filter out search
						$seconds_elapsed = (int)$seconds_elapsed ;
						file_put_contents($this->mysql_log_file,"{$seconds_elapsed}: {$sql}\n", FILE_APPEND);
					}
				}
				return $ret ;
			} catch (Exception $e) {
				unset ( $this->dbm ) ;
				$reconnects_left-- ;
				sleep ( 5 ) ;
			}
		}
	}

	protected function getSQL_ro ( $sql ) {
		$reconnects_left = 4 ;
		while ( $reconnects_left > 0 ) {
			if ( !isset($this->dbm_ro) ) $this->dbm_ro = $this->openMixNMatchDB_ro() ;
			try {
				$start = hrtime(true) ;
				$ret = $this->tfc->getSQL ( $this->dbm_ro , $sql , 2 ) ;
				$end = hrtime(true) ;
				$seconds_elapsed = ($end-$start)/1e+9 ;
				if ( isset($this->mysql_log_file) and $seconds_elapsed > $this->mysql_log_after_seconds ) { # Log query
					if ( !preg_match('|\bAGAINST\b|',$sql) ) { # Filter out search
						$seconds_elapsed = (int)$seconds_elapsed ;
						file_put_contents($this->mysql_log_file,"{$seconds_elapsed}: {$sql}\n", FILE_APPEND);
					}
				}
				return $ret ;
			} catch (Exception $e) {
				unset ( $this->dbm_ro ) ;
				$reconnects_left-- ;
				sleep ( 5 ) ;
			}
		}
	}

	# CATALOG METHODS

	# Wrapper
	public function loadCatalog ( $catalog_id , $return_catalog_object = true ) {
		$catalog = new Catalog ( $catalog_id , $this ) ;
		try {
			$this->catalogs[$catalog_id] = $catalog->data();
			return $catalog ;
		} catch (Exception $e) {
			$this->last_error = $e->getMessage() ;
		}
	}

	public function getAllCatalogIDs () {
		$catalog_ids = [] ;
		$sql = "SELECT /* ".__METHOD__." */ id from catalog" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $catalog_ids[] = $o->id ;
		return $catalog_ids ;
	}

	public function updateCatalogs ( $catalogs ) {
		foreach ( $catalogs AS $catalog_id ) {
			try {
				$catalog = new Catalog ( $catalog_id , $this ) ;
				$catalog->updateStatistics() ;
			} catch (exception $e) {
				// Ignore
			}
		}
	}



	# USER METHODS


	public function isUserBlocked ( $user_name ) {
		$uid = $this->getOrCreateUserID ( $user_name ) ;
		if ( $uid == -1 ) return false ; // Paranoia
		$user_key = $this->getUserKey ( $user_name ) ;
		if ( !isset($this->user_cache[$user_key]) ) return false ; // Paranoia
		if ( time() - $this->user_cache[$user_key]['last_block_check'] > 60*15 ) { // Cache block check for 15 min
			$this->user_cache[$user_key]['is_blocked'] = $this->checkUserBlock ( $user_name ) ;
		}
		return $this->user_cache[$user_key]['is_blocked'] ;
	}

	/* moved */
	private function checkUserBlock ( $user_name ) {
		$url = "https://www.wikidata.org/w/api.php?action=query&list=users&ususers=".urlencode($user_name)."&usprop=blockinfo&format=json" ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		return isset ( $j->query->users[0]->blockid ) ;
	}

	/* moved */
	private function getUserKey ( $user_name ) {
		return str_replace ( ' ' , '_' , trim($user_name) ) ;
	}

	public function getOrCreateUserID ( $user_name_original ) {
		$user_key = $this->getUserKey ( $user_name_original ) ;
		if ( isset($this->user_cache[$user_key]) ) return $this->user_cache[$user_key]['id'] ;

		$user_name = $this->escape($user_name_original) ;
		$last_block_check = time() ;
		$sql = "INSERT IGNORE user (name,last_block_check) VALUES ('{$user_name}','{$last_block_check}')" ;
		$this->getSQL ( $sql ) ;
		$user_id = -1 ;
		$sql = "SELECT /* ".__METHOD__." */ * FROM user WHERE name='{$user_name}'" ;
		$result = $this->getSQL ( $sql ) ;
		if ($o = $result->fetch_object()) {
			$user_id = $o->id ;
			$this->user_cache[$user_key] = [
				"id" => $o->id ,
				"last_block_check" => $o->last_block_check ,
				"is_blocked" => $this->checkUserBlock ( $user_name_original )
			] ;
		}
		return $user_id ;
	}

	# Returns true if match was stored in database, false otherwise
	public function addUserLog ( $action , $entry_id , $user_id , $q = -1 ) {
		$entry_id *= 1 ;
		$user_id *= 1 ;
		if ( $entry_id == 0 ) return $this->logError ( "Bad entry ID" ) ;

		if ( !isset($q) or $q < 1 ) $q = 'null' ;
		else $q = preg_replace ( '/\D/' , '' , "$q" ) ;

		$ts = $this->getCurrentTimestamp() ;
		$sql = "INSERT INTO log (`action`,`entry_id`,`user`,`timestamp`,`q`) VALUES ('".$this->escape($action)."',{$entry_id},{$user_id},'$ts',{$q})" ;
		$result = $this->getSQL ( $sql ) ;
		return true ;
	}





	public function sanitizePersonName ( $name ) {
		$ret = $name ;
		$ret = preg_replace ( '/^(Sir|Mme|Dr|Mother|Father)\.{0,1} /' , '' , $ret ) ;
		$ret = preg_replace ( '/\b[A-Z]\. /' , ' ' , $ret ) ; // M. Y. Wiener
		$ret = preg_replace ( '/ (\&) /' , ' ' , $ret ) ;
		$ret = preg_replace ( '/\(.+?\)/' , ' ' , $ret ) ;
		$ret = trim ( preg_replace ( '/\s+/' , ' ' , $ret ) ) ;
		return $ret ;
	}

	public function getSimplifiedName ( $name ) {
		$ret = $name ;
		$ret = trim ( preg_replace ( '/\s*\(.*?\)\s*/' , ' ' , $ret ) ) ; # (...)
		$ret = preg_replace ( '/[, ]+(Jr\.{0,1}|Sr\.{0,1}|PhD\.{0,1}|MD|M\.D\.)$/' , '' , $ret ) ;
		$ret_old = "!$ret" ;
		while ( $ret != $ret_old ) {
			$ret_old = $ret ;
			$ret = preg_replace ( '/^(Sir|Baron|Baronesse{0,1}|Graf|Gräfin|Prince|Princess|Dr\.|Prof\.|Rev\.)\s+/' , '' , $ret ) ;
		}
		$ret = trim ( preg_replace ( '/\s*(Ritter|Freiherr)\s+/' , ' ' , $ret ) ) ;
		if ( !preg_match ( '/^(\S+)$/' , $ret ) and !preg_match ( '/^(\S+) (\S+)$/' , $ret ) ) {
			$ret = preg_replace ( '/^(\S+) .*?(\S+)$/' , '$1 $2' , $ret ) ;
		}
		return $ret ;
	}

	private function extendSearchQuery ( $query , $property = '' , $value = '' ) {
		$query = trim ( $query ) ;
		if ( $property != '' and is_array($value) and count($value)>0 ) {
			$parts = [];
			foreach ( $value AS $v ) $parts[] = "{$property}={$v}";
			$parts = implode('|',$parts);
			if ( preg_match ( '/ /' , $value ) ) $query = trim("{$query} haswbstatement:\"{$parts}\"") ;
			else $query = trim("{$query} haswbstatement:{$parts}") ;
		} else if ( $property != '' and $value != '' ) {
			if ( preg_match ( '/ /' , $value ) ) $query = trim("{$query} haswbstatement:\"{$property}={$value}\"") ;
			else $query = trim("{$query} haswbstatement:{$property}={$value}") ;
		}
		return $query ;
	}

	# `$value` can be a string or an array
	public function getSearchResults ( $query , $property = '' , $value = '' ) {
		$query = $this->extendSearchQuery ( $query , $property , $value ) ;
		// print "{$query}\n";
		$ret = [] ;
		$sroffset = 0 ;
		$results = [] ;
		while ( 1 ) {
			$url = "https://www.wikidata.org/w/api.php?action=query&list=search&titlesnippet&srnamespace=0&srlimit=500&format=json&srsearch=" . urlencode($query) ;
			if ( $sroffset > 0 ) $url .= "&sroffset={$sroffset}";
			#print "{$url}\n" ;
			$result = @file_get_contents ( $url ) ;
			if ( !isset($result) or $result === null ) {
				$this->logError ( "MixNMatch::getSearchResults : query failed: {$url}" ) ;
				return $ret ;
			}
			$j = @json_decode ( $result ) ;
			if ( !isset($j) or $j === null or !isset($j->query) or !isset($j->query->search) ) {
				$this->logError ( "MixNMatch::getSearchResults : bad JSON: {$url}" ) ;
				return $ret ;
			}
			foreach ( $j->query->search AS $r ) $results[] = $r ;
			if ( $property != 'P31' or (is_array($value) or $value != 'Q5') ) break ; # No continue for non-persons
			else if ( isset($j->continue->sroffset) ) $sroffset = $j->continue->sroffset*1 ;
			else break ;
		}
		return $results ;
	}

	public function getCachedWikidataSearch ( $query , $property = '' , $value = '' , $store_in_cache = true ) {
		$query = $this->extendSearchQuery ( $query , $property , $value ) ;
		if ( isset($this->wikidata_search_results[$query]) ) return $this->wikidata_search_results[$query] ;
		$items = [] ;
		$url = "https://www.wikidata.org/w/api.php?format=json&action=query&list=search&srnamespace=0&srsearch=".urlencode($query) ;
		$j = json_decode(file_get_contents($url)) ;
		if ( isset($j->query) and isset($j->query->search) ) {
			foreach ( $j->query->search as $result ) $items[] = $result->title ;
		}
		if ( $store_in_cache ) $this->wikidata_search_results[$query] = $items ;
		return $items ;
	}

	# Optional; aux as array of arrays like ['P123','the value']
	# Optional: q can be a unset, a single value (then set as user=4), or an array (then added as multi_match)
	# Optional: location can be an object (lat,lon)
	# Optional: born/died
	public function addNewEntry ( $o ) {
		if ( is_array($o) ) $o = (object) $o ;
		if ( !is_object($o) ) die ( "mnm::addNewEntry - not an object\n" ) ;
		foreach ( ['catalog','id','name'] AS $k ) {
			if ( !isset($o->$k) or trim($o->$k) == '' ) die ( "mnm::addNewEntry - object requires ->{$k}\n" ) ;
		}
		$q = 0 ;
		if ( isset($o->q) ) {
			if ( is_array($o->q) ) $q = $o->q ;
			else if ( preg_match('/^Q(\d+)$/i',$o->q,$m) ) $q = $m[1] * 1 ;
		}
		$sql = "INSERT IGNORE INTO entry (catalog,ext_id,ext_url,ext_name,`type`,random" ;
		if ( !is_array($q) and $q > 0 ) $sql .= ",`q`,`user`,`timestamp`" ;
		$sql .= ") VALUES (" ;
		$sql .= $this->escape(trim($o->catalog)) . "," ;
		$sql .= "'" . $this->escape(trim($o->id)) . "'," ;
		if ( isset($o->url) ) $sql .= "'" . $this->escape(trim($o->url)) . "'," ;
		else $sql .= "''," ;
		$sql .= "'" . $this->escape($o->name) . "'," ;
		if ( isset($o->type) ) $sql .= "'" . $this->escape(trim($o->type)) . "'," ;
		else $sql .= "''," ;
		$sql .= "rand()" ;
		if ( !is_array($q) and $q > 0 ) $sql .= ",{$q},4,'" . $this->getCurrentTimestamp() . "'" ;
		$sql .= ")" ;
		$this->getSQL ( $sql ) ;
		$entry_id = $this->dbm->insert_id ;
		if ( $entry_id*1 <= 0 ) throw new \Exception(__METHOD__.': Could not create new entry for '.json_encode($o) ) ; 
		if ( isset($o->desc) ) $this->setDescriptionForEntryID ( $entry_id , $o->desc ) ;
		if ( isset($o->aux) ) {
			foreach ( $o->aux AS $a ) $this->setAux ( $entry_id , $a[0] , $a[1] ) ;
		}
		if ( is_array($q) ) $this->setMultiMatch ( $entry_id , $q ) ;

		# log entry creation
		$this->logEntryCreation($entry_id);

		if ( isset($o->mnm_relations) and is_array($o->mnm_relations) ) {
			foreach ( $o->mnm_relations AS $mr ) {
				if ( count($mr)==2 ) {
					$this->linkEntriesViaProperty( $entry_id , $mr[0] , $mr[1] );
				}
			}
		}

		if ( isset($o->statement_text) and is_array($o->statement_text) ) {
			$sql = [];
			foreach ( $o->statement_text AS $st ) {
				if ( count($st)==2 ) {
					$property = preg_replace('/\D/','',$st[0]) * 1 ;
					$text = $this->escape(trim($st[1]));
					if ( $property>0 and $text!='' ) $sql[] = "({$entry_id},{$property},'{$text}')";
				}
			}
			if ( count($sql)>0 ) {
				$sql = "INSERT IGNORE INTO `statement_text` (`entry_id`,`property`,`text`) VALUES ". implode(',',$sql);
				$this->getSQL ( $sql ) ;
			}
		}


		if ( !is_array($q) and $q > 0 ) {
			$sql = "UPDATE person_dates SET is_matched=1 WHERE entry_id={$entry_id}" ;
			$this->getSQL ( $sql ) ;
		}

		if ( isset($o->location) and isset($o->location->lat) and isset($o->location->lon) ) {
			$this->setLocation ( $entry_id , $o->location->lat , $o->location->lon ) ;
		}

		$born = $o->born ?? '' ;
		$died = $o->died ?? '' ;
		if ( $born.$died != '' ) $this->setPersonDates ( $entry_id , $born , $died ) ;

		$this->setMatchStatus ( $entry_id , 'UNKNOWN' ) ;

		return $entry_id ;
	}

	public function logEntryCreation ( $entry_id ) {
		if ( $entry_id*1 <= 0 ) return false ; # Paranoia
		$entry_id = $this->escape($entry_id);
		$sql = "INSERT INTO `entry_creation` (`entry_id`,`timestamp`) VALUES ({$entry_id},now())" ;
		$sql .= " ON DUPLICATE KEY UPDATE `timestamp`=now()" ;
		$this->getSQL ( $sql ) ;
	}

	public function setMultiMatch ( $entry_id , $candidates ) {
		$entry_id *= 1 ;
		if ( $entry_id <= 0 ) return ;

		if ( !is_array($candidates) ) $candidates = [] ;
		foreach ( $candidates AS $k => $v ) {
			if ( $v*1 == 0 ) unset ( $candidates["{$k}"] ) ;
		}

		$sql = '' ;
		$candidates_count = count ( $candidates ) ;
		if ( $candidates_count > 1 and $candidates_count < 10 ) { # More than one, not too many to be useful
			$candidates = implode ( ',' , $candidates ) ;
			$candidates = preg_replace ( '/[^0-9,]/' , '' , $candidates ) ;
			$catalog_sql = "SELECT /* ".__METHOD__." */ catalog FROM entry WHERE id={$entry_id}" ;
			$sql = "REPLACE INTO `multi_match` (entry_id,catalog,candidates,candidate_count) VALUES ({$entry_id},({$catalog_sql}),'$candidates',$candidates_count)" ;
		} else {
			$sql = "DELETE FROM `multi_match` WHERE entry_id={$entry_id}" ;
		}
		$this->getSQL ( $sql ) ;
	}

	public function clearPersonDates ( $catalog ) {
		$catalog *= 1 ;
		$sql = "DELETE person_dates FROM person_dates INNER JOIN entry ON entry_id=entry.id WHERE catalog={$catalog}" ;
		$this->getSQL ( $sql ) ;
	}

	public function isValidDate ( $date ) {
		if ( !isset($date) ) return false ;
		if ( preg_match('|^\d{4}$|',$date) ) return true ;
		if ( preg_match('|^\d{4}-\d{2}$|',$date) ) return true ;
		if ( preg_match('|^\d{4}-\d{2}-\d{2}$|',$date) ) return true ;
		return false ;
	}


	public function setPersonDates ( $entry_id , $born , $died ) {
		if ( !isset($entry_id) ) return ;
		$entry_id = $entry_id * 1 ;
		if ( $entry_id == 0 ) return ;
		if ( $born.$died == '' ) return ; // No dates to set
		if ( !$this->isValidDate($born) and $born != '' ) return ;
		if ( !$this->isValidDate($died) and $died != '' ) return ;
		$sql = "INSERT INTO person_dates (entry_id,born,died) VALUES ($entry_id," ;
		$sql .= "'" . $this->escape($born) . "'," ;
		$sql .= "'" . $this->escape($died) . "')" ;
		$sql .= " ON DUPLICATE KEY UPDATE " ;
		$sql .= "born='" . $this->escape($born) . "'," ;
		$sql .= "died='" . $this->escape($died) . "'" ;
		$this->getSQL ( $sql ) ;
	}

	public function linkEntriesViaProperty ( $entry_id , $property , $target_entry_id ) {
		$entry_id *= 1 ;
		$target_entry_id *= 1 ;
		$property = preg_replace('/\D/','',$property) * 1 ;

		# Do entries exist?
		$sql = "SELECT /* ".__METHOD__." */ count(*) AS cnt FROM entry WHERE id IN ($entry_id,$target_entry_id)" ;
		$result = $this->getSQL ( $sql ) ;
		$cnt = 0 ;
		if($o = $result->fetch_object()) $cnt = $o->cnt ;
		if ( $cnt < 2 ) return ; # TODO error?

		$sql = "INSERT IGNORE INTO mnm_relation (entry_id,property,target_entry_id) VALUES ($entry_id,$property,$target_entry_id)" ;
		$this->getSQL ( $sql ) ;
	}

	public function setLocation ( $entry_id , $lat , $lon ) {
		$entry_id *= 1 ;
		if ( $entry_id == 0 ) return ;
		$lat *= 1 ;
		$lon *= 1 ;
		if ( $lat==0 and $lon == 0 ) return ;
		$sql = "INSERT IGNORE INTO `location` (entry_id,lat,lon) VALUES ($entry_id,$lat,$lon)" ;
		$this->getSQL ( $sql ) ;

		# Set "has_locations" for catalog
		$sql = "SELECT /* ".__METHOD__." */ kv_catalog.* FROM `kv_catalog`,`entry` WHERE `catalog_id`=`catalog` AND `entry`.`id`={$entry_id} AND `kv_key`='has_locations'" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			if ( $o->kv_value=='yes' ) return ; # Already has
			# Change to "yes"
			$sql = "UPDATE `kv_catalog` SET `kv_value`='yes' WHERE `id`={$o->id}" ;
			$this->getSQL ( $sql ) ;
			return ;
		}
		# Nothing yet, add "yes"
		$sql = "INSERT IGNORE INTO `kv_catalog` (catalog_id,kv_key,kv_value) SELECT `catalog`,'has_locations','yes' FROM `entry` WHERE `entry`.`id`={$entry_id}" ;
		$this->getSQL ( $sql ) ;
	}

	public function set_image_url($entry_id,$url) {
		$v = $this->escape($url);
		$sql = "REPLACE INTO `kv_entry` (entry_id,kv_key,kv_value) VALUES ({$entry_id},'image_url','{$v}')" ;
		$this->getSQL ( $sql ) ;
	}

	public function get_image_url_from_ext_url($entry,$image_pattern) {
		try {
			if ( $entry->catalog==6149 ) $url = "https://api.navigart.fr/14/artworks/{$entry->ext_id}";
			else $url = $entry->ext_url;
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true ) ;
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); // Ignore faulty SSL certificate
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // Ignore faulty SSL certificate
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0' ) ;
			$html = curl_exec($ch);
			// $e = curl_error($ch);
			// print "ERROR: {$e}\n";
		} catch (\Exception $e) {
			return '';
		}
		$ext_img = '';
		if ( $entry->catalog==6149 ) {
			try {
				$j = json_decode($html);
				$fn = $j->results[0]->_source->ua->medias[0]->file_name;
				$size = 300;
				$ext_img = "https://images.navigart.fr/{$size}/{$fn}";
			} catch (\Exception $e) {
				return '';
			}
		} else {
			$html = preg_replace("| *\n *|",' ',$html);
			$pattern = '|'.str_replace('|','\\|',$image_pattern).'|';
			if ( preg_match($pattern,$html,$m) ) {
				$ext_img = trim($m[1]);
				if ( preg_match('|^//|',$ext_img) ) $ext_img = "https:{$ext_img}";
				if ( !preg_match('|^https{0,1}://|',$ext_img) ) {
					$url_root = preg_replace('|^(https{0,1}://.+?)/.*$|','$1',$entry->ext_url).'/';
					$ext_img = $url_root.preg_replace('|^/+|','',$ext_img);
				}
	 		}
	 	}
		$this->set_image_url($entry->id,$ext_img);
		return $ext_img;
	}

	public function setAux ( $entry_id , $property , $value ) {
		$entry_id *= 1 ;
		if ( $entry_id == 0 ) return ;
		$property = preg_replace ( '/\D/' , '' , "{$property}" ) ;
		if ( $property == '' ) return ;
		$value = trim ( $value ) ;
		if ( $value == '' ) return ;
		$value = $this->fixPropertyValueFromLCtoWikidata ( $property , $value ) ;
		$sql = "INSERT IGNORE INTO auxiliary (entry_id,aux_p,aux_name) VALUES ({$entry_id},{$property},'".$this->escape($value)."')" ;
		$this->getSQL ( $sql ) ;
	}

	public function setAlias ( $entry_id , $label , $language = '' , $user_id = 0 ) {
		$entry_id *= 1 ;
		if ( $entry_id == 0 ) return ;
		$user_id *= 0 ;
		$label = trim($label) ;
		$language = trim(strtolower($language)) ;
		if ( $label == '' ) return ;
		$sql = "INSERT IGNORE INTO aliases (entry_id,language,label,added_by_user) VALUES ({$entry_id},'".$this->escape($language)."','".$this->escape($label)."',{$user_id})" ;
		$this->getSQL ( $sql ) ;
	}

	public function setLanguageDescription ( $entry_id , $label , $language = '' , $user_id = 0 ) {
		$entry_id *= 1 ;
		if ( $entry_id == 0 ) return ;
		$user_id *= 0 ;
		$label = trim($label) ;
		$language = trim(strtolower($language)) ;
		if ( $label == '' ) return ;
		$sql = "INSERT IGNORE INTO descriptions (entry_id,language,label,added_by_user) VALUES ({$entry_id},'".$this->escape($language)."','".$this->escape($label)."',{$user_id})" ;
		$this->getSQL ( $sql ) ;
	}

	public function getItemForEntryID ( $entry_id ) {
		$entry_id *= 1 ;
		$sql = "SELECT /* ".__METHOD__." */ q FROM entry WHERE id={$entry_id}" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) return $o->q ;
	}

	public function avoidEntryAutomatch ( $entry_id , $q = -1 ) {
		$entry_id *= 1 ;
		$sql = "SELECT /* ".__METHOD__." */ * FROM `log` WHERE `entry_id`={$entry_id}" ;
		if ( isset($q) and $q != -1 ) $sql .= " AND (q IS NULL OR q=".preg_replace('/\D/','',$q).")" ;
		if ( isset($q) and $q != -1 ) $sql .= " AND (q=".preg_replace('/\D/','',$q).")" ; # Huh?
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) return true ;
		return false ;
	}

	public function sanitizeQ ( &$q ) {
		if ( !isset($q) or $q === null ) return ;
		if ( preg_match ( '/^[PQ](\d+)$/' , "$q" , $m ) ) $q = $m[1] ;
		$q = intval ( $q ) ;
	}

	# Returns true if match was stored in database, false otherwise
	public function setMatchForEntryID ( $entry_id , $q , $user_id , $no_overwrite_manual = false , $allow_q_zero = true ) {
		$entry_id *= 1 ;
		$this->sanitizeQ ( $q ) ;
		$user_id *= 1 ;

		if ( $entry_id <= 0 ) return $this->logError ( "Bad entry ID" ) ;
		if ( $q == 0 and !$allow_q_zero ) return $this->logError ( "Trying to set q to zero" ) ;

		// Get existing
		try {
			$entry = new Entry ( $entry_id , $this ) ;
			$entry = $entry->core_data() ;
		} catch (Exception $e) {
			return $this->logError ( "Entry #{$entry_id} not found." ) ;
		}

		if ( $q > 0 and User::isAutoMatchUser ( $user_id ) and $this->avoidEntryAutomatch ( $entry_id , $q ) ) return $this->logError ( "Entry #{$entry_id} was removed before." ) ;

		return $this->setMatchForEntryObject ( $entry , $q , $user_id , $no_overwrite_manual , $allow_q_zero ) ;
	}

	# Returns true if match was stored in database, false otherwise
	public function setMatchForCatalogExtID ( $catalog , $ext_id , $q , $user_id , $no_overwrite_manual = false , $allow_q_zero = true ) {
		$catalog *= 1 ;
		$ext_id = $this->escape ( $ext_id ) ;
		$this->sanitizeQ ( $q ) ;
		$user_id *= 1 ;

		if ( $q == 0 and !$allow_q_zero ) return $this->logError ( "Trying to set q to zero" ) ;

		// Get existing
		$entry = '' ;
		$sql = "SELECT /* ".__METHOD__." */ * FROM entry WHERE catalog={$catalog} AND ext_id='{$ext_id}' LIMIT 1" ; # By table constraints, there can only be one
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $entry = $o ;
		if ( $entry == '' ) return $this->logError ( "External ID '{$ext_id}' in catalog #{$catalog} not found." ) ;

		$this->last_entry = $entry ;

		return $this->setMatchForEntryObject ( $entry , $q , $user_id , $no_overwrite_manual ) ;
	}

	# Returns true if match was stored in database, false otherwise
	public function removeMatchForEntryID ( $entry_id ,  $user_id ) {
		$entry_id *= 1 ;
		$user_id *= 1 ;
		if ( $entry_id <= 0 ) return $this->logError ( "Bad entry ID" ) ;

		try {
			$entry = new Entry ( $entry_id , $this ) ;
			$entry = $entry->core_data() ;
		} catch (Exception $e) {
			return $this->logError ( "Entry #{$entry_id} not found." ) ;
		}

		$removed_q = $entry->q ;
		if ( !$this->setMatchForEntryObject ( $entry , null , null ) ) return false ;
		if ( !$this->addUserLog ( 'remove_q' , $entry_id , $user_id , $removed_q ) ) return false ;
		return true ;
	}

	# Removes all matches to a specific item
	public function removeMatchesToItem ( $q , $user_id , $remove_auto_matches=true , $remove_manual_matches=true ) {
		$this->sanitizeQ ( $q ) ;
		if ( $q <= 0 ) return ; # Paranoia

		# Remove auto-matches
		if ( $remove_auto_matches ) {
			$sql = "UPDATE entry SET `q`=NULL,`user`=NULL,`timestamp`=NULL WHERE q={$q} AND user<=0" ;
			$this->getSQL ( $sql ) ;
		}

		# Remove manual matches
		if ( $remove_manual_matches ) {
			$sql = "SELECT /* ".__METHOD__." */ * FROM entry WHERE q={$q}" ;
			$result = $this->getSQL ( $sql ) ;
			while($o = $result->fetch_object()){
				$this->removeMatchForEntryID ( $o->id , $user_id ) ;
			}
		}
	}

	public function getCurrentTimestamp () {
		return date ( 'YmdHis' ) ;
	}

	public function getFutureTimestamp ( $add_seconds ) {
		$t = time() + $add_seconds*1 ;
		return date ( 'YmdHis' , $t ) ;
	}

	# Returns true if match was stored in database, false otherwise
	# Private function, passed paramaters expected to be sanitized
	private function setMatchForEntryObject ( $entry , $q , $user_id , $no_overwrite_manual = false , $allow_q_zero = false ) {
		$ts = $this->getCurrentTimestamp() ;
		$this->sanitizeQ ( $q ) ;

		# Paranoia
		if ( $user_id === null and $q !== null ) return $this->logError ( "#{$entry->id}: Q is $q but user is null" ) ;
		if ( $user_id !== null and $user_id == 0 and $entry->user == 0 and $q == $entry->q ) return true ; # Auto-match replacing same auto-match, no need to update
		if ( !$allow_q_zero and $user_id == 4 and $q <= 0 ) return false ; # No auto-matching as "N/A"; might be overly stringent

		# Prepare update overview
		$add_column = '' ;
		if ( $user_id == 0 ) $add_column = 'autoq' ;
		else if ( $q === null ) $add_column = 'noq' ;
		else if ( $q == 0 ) $add_column = 'na' ;
		else if ( $q == -1 ) $add_column = 'nowd' ;
		else if ( $user_id > 0 ) $add_column = 'manual' ;

		$reduce_column = '' ;
		if ( (!isset($entry->q) or $entry->q === null) and (isset($q) and $q !== null) ) $reduce_column = 'noq' ;
		else if ( $entry->q == 0 ) $reduce_column = 'na' ;
		else if ( $entry->q == -1 ) $reduce_column = 'nowd' ;
		else if ( $entry->user == 0 ) $reduce_column = 'autoq' ;

		# Set match
		$q_sql = ($q===null)?'null':$q ;
		$user_sql = ($user_id===null)?'null':$user_id ;
		$ts_sql = ($q === null and $user_id === null) ?'null':"'$ts'" ;
		$sql = "UPDATE entry SET q={$q_sql},user={$user_sql},`timestamp`={$ts_sql} WHERE id={$entry->id}" ;
		if ( $no_overwrite_manual ) $sql .= " AND (user is null or user=0 or q=-1)" ;
		$this->getSQL ( $sql ) ;
		if ( $this->dbm->affected_rows == 0 ) return $this->logError ( "No changes written." ) ;

		# Clean up
		if ( $user_id !== 0 ) {
			$sql = "DELETE FROM multi_match WHERE entry_id={$entry->id}" ;
			$this->getSQL ( $sql ) ;
		}

		$status = 'UNKNOWN' ;
		#if ( $q !== null and $q <= 0 ) $status = 'N/A' ;
		$this->setMatchStatus ( $entry->id , $status , $ts ) ;
		$is_matched = ( $user_id!==null and $user_id>0 and $q!==null AND $q>0 ) ? 1 : 0 ;

		# Update person_dates
		if ( $entry->type == 'Q5' ) {
			$sql = "UPDATE person_dates SET is_matched={$is_matched} WHERE entry_id={$entry->id}" ;
			$this->getSQL ( $sql ) ;
		}

		# Update auxiliary
		$sql = "UPDATE `auxiliary` SET `entry_is_matched`=" . ($is_matched?1:0) . " WHERE entry_id={$entry->id}" ;
		$this->getSQL ( $sql ) ;

		# Update statement_text
		$sql = "UPDATE `statement_text` SET `entry_is_matched`=" . ($is_matched?1:0) . " WHERE entry_id={$entry->id}" ;
		$this->getSQL ( $sql ) ;

		# Update overview
		if ( $add_column == '' and $reduce_column == '' ) return true ; # No overview update, but match still took place
		$sql = "UPDATE overview SET " ;
		if ( $add_column != '' ) $sql .= " {$add_column}={$add_column}+1" ;
		if ( $add_column != '' and $reduce_column != '' ) $sql .= "," ;
		if ( $reduce_column != '' ) $sql .= " {$reduce_column}={$reduce_column}-1" ;
		$sql .= " WHERE catalog={$entry->catalog}" ;
		$this->getSQL ( $sql ) ;

		# Queue for reference fixing, just in case
		$this->fix_references_in_item($q);

		return true ;
	}

	public function setMatchStatus ( $entry_id , $status = 'UNKNOWN' , $ts = '' )  {
		$entry_id *= 1 ;
		if ( $entry_id <= 0 ) return ;
		$status = $this->escape ( $status ) ;
		if ( $ts == '' ) $ts = $this->getCurrentTimestamp() ;
		$ts = $this->escape ( $ts ) ;
		$catalog_query = "SELECT /* ".__METHOD__." */ entry.catalog FROM entry WHERE entry.id={$entry_id}" ;
		$sql = "INSERT INTO `wd_matches` (`entry_id`,`status`,`timestamp`,`catalog`) VALUES ({$entry_id},'{$status}','{$ts}',({$catalog_query})) ON DUPLICATE KEY UPDATE `status`='{$status}',`timestamp`='{$ts}'" ;
		$this->getSQL ( $sql ) ;
	}

	public function getMatchStatus ( $entry_id ) {
		$entry_id *= 1 ;
		if ( $entry_id <= 0 ) return ;
		$sql = "SELECT /* ".__METHOD__." */ * FROM `wd_matches` WHERE `entry_id`={$entry_id}" ;
		$result = $this->getSQL ( $sql ) ;
		if ( $o = $result->fetch_object() ) return $o->status ;
	}

	# If $randomize==true, a random subset of $limit size will be taken from all entries with matching $statuses
	public function getEntriesWithWdMatches ( $statuses = [] , $limit = -1 , $randomize = false ) {
		$ret = [] ;
		if ( count($statuses) == 0 ) return $ret ;
		$limit *= 1 ;
		foreach ( $statuses AS $k => $v ) $statuses[$k] = $this->escape($v) ;
		$statuses = implode ( "','" , $statuses ) ;
		$sql = "SELECT /* ".__METHOD__." */ entry.* FROM entry,wd_matches WHERE entry_id=entry.id AND wd_matches.status IN ('{$statuses}')" ;
		if ( $randomize ) {
			$r = $this->rand() ;
			$sql .= " HAVING random>={$r} ORDER BY random" ;
		}
		if ( $limit > 0 ) $sql .= " LIMIT {$limit}" ;
		$result = $this->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) $ret[$o->id] = $o ;
		return $ret ;
	}

	public function isGNDundifferentiatedPerson ( $id ) {
		if ( is_array($id) ) {
			if ( count($id) == 1 ) return $this->isGNDundifferentiatedPerson ( $id[0] ) ;
			$this->logError ( "Mixnmatch::isGNDundifferentiatedPerson." . json_encode($id) ) ;
			return true ; // ????
		}
		$url = "http://d-nb.info/gnd/{$id}/about/lds" ;
		$rdf = @file_get_contents ( $url ) ;
		if ( $rdf === false ) return false ; # 404 => not undifferentiated...
		return preg_match ( '/gndo:UndifferentiatedPerson/' , $rdf ) ;
	}

	public function hasPropertyEverEditedInItem ( $q , $prop ) {
		$this->wil->sanitizeQ ( $q ) ;
		$this->wil->sanitizeQ ( $prop ) ;
		$sql = "SELECT /* ".__METHOD__." */ count(*) AS cnt from page,revision_compat WHERE page_title='$q' AND page_namespace=0 AND page_id=rev_page AND rev_comment LIKE '%Property:{$prop}%' LIMIT 1" ;
		$this->openWikidataDB() ;
		$result = $this->tfc->getSQL ( $this->dbwd , $sql ) ;
		if($o = $result->fetch_object()) return true ;
		return false ;
	}

	# this queues references to be fixed by reference_fixer.php, it doesn't actually fix them directly, because slow
	public function fix_references_in_item ( $q ) {
		# Not necessary anymore for most things
		try {
			$q_numeric = (int) preg_replace('|\D|','',"{$q}") ;
			if ( $q <= 1 ) return ;
			$sql = "INSERT INTO `reference_fixer` (`q`,`done`) VALUES ({$q_numeric},0) ON DUPLICATE KEY UPDATE `done`=0" ;
			$this->getSQL($sql);
		} catch (Exception $e) {
			# so what
		}
	}

	public function openWikidataDB ( $force_new_connection = false ) {
		if ( $force_new_connection or !isset($this->dbwd) ) $this->dbwd = $this->tfc->openDB ( 'wikidata' , 'wikidata' ) ; # TODO persistent - is it safe?
		return $this->dbwd ;
	}

	# Large catalogs value fix
	public function fixPropertyValueFromLCtoWikidata ( $prop , $value ) {
		if ( preg_match('/^\d+$/',"{$prop}") ) $prop = "P{$prop}" ; # 214 => "P214"
#		if ( $prop == 'P1368' ) $value = 'LNC10-' . $value ;
		if ( $prop == 'P1368' ) $value = preg_replace ( '/^(.*)(\d{9})$/' , '$2' , $value ) ;
		if ( $prop == 'P1207' ) $value = preg_replace ( '/\s/' , '' , $value ) ;
		if ( $prop == 'P244' ) $value = preg_replace ( '/\s/' , '' , $value ) ;
		if ( $prop == 'P213' ) $value = preg_replace ( '/\s/' , '' , $value ) ;
		return $value ;
	}


	public function date2expression ( $d , $is_human_date = false ) {
		$ret = '' ;
		if ( preg_match ( '/^\d+-\d{2}-\d{2}$/' , $d ) ) $ret = "+{$d}T00:00:00Z/11" ;
		else if ( preg_match ( '/^\d+-\d{2}$/' , $d ) ) $ret = "+{$d}-01T00:00:00Z/10" ;
		else if ( preg_match ( '/^\d+$/' , $d ) ) $ret = "+{$d}-01-01T00:00:00Z/9" ;
		if ( $ret == '' ) {
			$this->logError ( "Bad date: {$d}" ) ;
			return ;
		}
		$ret = preg_replace('|^(.+?)-00(T.+)/11$|',"$1-01$2/10",$ret) ; # Day => Month
		$ret = preg_replace('|^(.+?)-00-\d\d(T.+)/11$|',"$1-01-01$2/9",$ret) ; # Month => Year
		if ( $is_human_date ) {
			$now = $this->date2expression ( date ( 'Y-m-d' ) ) ;
			if ( $ret > $now ) return ; # Date in the future
		}
		return $ret ;
	}

	public function fixStringForQS ( $s ) {
		if ( strlen($s) > 250 ) $s = substr ( $s , 0 , 250 ) ;
		return $s ;
	}

	protected function fixURL ( $url ) {
		$url = str_replace ( ' ' , '%20' , $url ) ;
		$url = str_replace ( '[' , '%5B' , $url ) ;
		$url = str_replace ( ']' , '%5D' , $url ) ;
		return $url ;
	}

	public function asciify($s) {
		return iconv("utf-8","ascii//TRANSLIT",$s);
	}

	# "Scores" human names and their usability as labels; requires uppercase letters but prefers the one with most lowercase
	protected static function compare_human_names ( $a , $b ) {
		$lca = preg_match_all('|[a-z]|', $a, $dummy) ;
		$lcb = preg_match_all('|[a-z]|', $b, $dummy) ;
		$uca = preg_match_all('|[A-Z]|', $a, $dummy) ;
		$ucb = preg_match_all('|[A-Z]|', $b, $dummy) ;
		if ( $uca == 0 ) return 1 ;
		if ( $ucb == 0 ) return -1 ;
		return $lcb - $lca ;
	}

	# Returns an array of strings with QS commands
	public function getCreateItemCommandsForEntries ( $entry_ids , $lc = null , $verbose = false , $default_entry = 0 ) {
		require_once ( '/data/project/mix-n-match/manual_lists/large_catalogs/shared.php' ) ;
		if ( $lc == null ) $lc = new \largeCatalog ( 2 ) ;

		$cmds = [] ;
		$label_pattern = '|^[LADS]|' ;

		$preferred_labels = [] ;
		$preferred_labels_done = false ;

		foreach ( $entry_ids AS $entry_id ) {
			# Sanity check
			$entry_id = "{$entry_id}"*1 ;
			if ( $entry_id <= 0 ) throw new \Exception(__METHOD__.': Bad entry ID in '.json_encode($entry_ids) ) ; 

			# Get entry
			$entry = new Entry ( $entry_id , $this ) ;
			if ( !isset($entry) ) throw new \Exception(__METHOD__.": Entry '{$entry_id}' not in database") ;
			$entry_cd = $entry->core_data() ;

			if ( $default_entry == $entry->id() ) {
				$label = '"'.$entry->name().'"' ;
				$label_lc = strtolower($label) ;
				$preferred_labels[$label_lc] = [] ;
				$preferred_labels[$label_lc][$label] = $label ;
				$preferred_labels_done = true ;
			}
			
			# Get QS commands for this entry
			$commands = $this->getCreateItemForEntryCommands ( $entry_cd , $lc ) ;
			if ( !isset($commands) ) throw new \Exception(__METHOD__.": {$this->last_error}") ;
			
			# Merge commands into shared structure $cmds
			foreach ( $commands AS $command ) {
				if ( $command == 'CREATE' )  continue ;
				$c = explode ( "\t" , $command ) ;
				if ( count($c) < 3 ) throw new \Exception(__METHOD__.": '{$command}' not recognized") ;
				if ( $c[0] != 'LAST' ) throw new \Exception(__METHOD__.": '{$command}' does not start with 'LAST'") ;
				$action = trim($c[1]) ;
				$value = trim($c[2]) ;
				if ( $value == '' or $value == '""' ) continue ; # Paranoia
				if ( preg_match($label_pattern,$action) ) $key = "{$action}" ;
				else $key = "{$action}\t{$value}" ;
				if ( !isset($cmds[$key]) ) $cmds[$key] = (object) [ "action"=>$action , "refs"=>[] , "values"=>[] ] ;
				$cmds[$key]->values[$value] = $value ;
				if ( count($c) > 3 and !preg_match($label_pattern,$action) ) { # No refs for LADS
					$references = array_slice ( $c , 3 ) ;
					$ref_key = implode ( "\t" , $references ) ;
					$cmds[$key]->refs[$ref_key] = $references ;
				}
			}

		}

		# Multiple sites check
		foreach ( $cmds AS $cmd ) {
			if ( !preg_match('|^S|',$cmd->action) ) continue ;
			if ( count((array)$cmd->values)==1 ) continue ;
			throw new \Exception(__METHOD__.": Multiple pages on same wiki: ".json_encode($cmd)) ;
		}

		# Human labels: upper-/lowercase
		if ( $preferred_labels_done ) {
			$label_lc = array_keys($preferred_labels)[0] ;
			$label = array_values($preferred_labels)[0] ;
			foreach ( $cmds AS $cmd ) {
				# Find label(s) in the similar_languages group
				if ( !preg_match('|^L(.+)$|',$cmd->action,$m) ) continue ;
				if ( !in_array($m[1], $this->similar_languages) ) continue ;
				# Copy label(s) over to aliases
				$cmd_key = "A{$m[1]}" ;
				if ( !isset($cmds[$cmd_key]) ) $cmds[$cmd_key] = (object) [ "action"=>$cmd_key , "refs"=>[] , "values"=>[] ] ;
				foreach ( $cmd->values AS $k => $v ) $cmds[$cmd_key]->values[$k] = $v ;
				# Replace label with default
				$cmd->values = [] ;
				$cmd->values[$label_lc] = $label ;
			}
		} else if ( isset($cmds["P31\tQ5"]) ) { # Human
			foreach ( $cmds AS $cmd ) {
				if ( !preg_match('|^[LA]|',$cmd->action) ) continue ;
				foreach ( $cmd->values AS $label ) {
					$label_lc = strtolower($label) ;
					if ( !isset($preferred_labels[$label_lc]) ) $preferred_labels[$label_lc] = [] ;
					$preferred_labels[$label_lc][$label] = $label ;
				}
			}

			foreach ( $preferred_labels AS $label_lc => $labels ) {
				usort ( $labels , [MixNMatch::class,'compare_human_names'] ) ;
				$preferred_labels[$label_lc] = $labels[0] ;
			}
		}

		# Convert some labels to aliases
		foreach ( $cmds AS $cmd ) {
			if ( !preg_match('|^L|',$cmd->action) ) continue ;
			$labels = array_values($cmd->values) ;
			
			if ( isset($cmds["P31\tQ5"]) ) { # Human
				foreach ( $labels AS $k => $label ) {
					$label_lc = strtolower($label) ;
					if ( isset($preferred_labels[$label_lc]) ) $labels[$k] = $preferred_labels[$label_lc] ;
				}
				$labels = array_unique ( $labels ) ;
				$cmd->values = $labels ;
			}

			if ( count((array)$cmd->values)==1 ) continue ;

			$label = array_shift($labels);
			$cmd->values = [ $label=>$label ] ;
			$alias_key = preg_replace('|^L|','A',$cmd->action) ;
			if ( !isset($cmds[$alias_key]) ) $cmds[$alias_key] = (object) [ "action"=>$alias_key , "refs"=>[] , "values"=>[] ] ;
			foreach ( $labels AS $alias ) $cmds[$alias_key]->values[$alias] = $alias ;
		}

		# Ensure "main" languages are covered for humans
		if ( isset($cmds["P31\tQ5"]) and count($preferred_labels) > 0 ) {
			$label_lc = array_keys($preferred_labels)[0] ;
			if ( in_array($label_lc, $this->similar_languages) ) { # Do not copy Greek, Russian, Hebrew etc to all languages
				$default_label = array_values($preferred_labels)[0] ;
				foreach ( $this->similar_languages AS $lang ) {
					if ( isset($cmds["L{$lang}"]) ) continue ;
					$cmds["L{$lang}"] = (object) ["action"=>"L{$lang}", "values"=>[], "refs"=>[] ] ;
					$cmds["L{$lang}"]->values[$default_label] = $default_label ;
					#print "Setting L{$lang} to {$default_label}\n" ;
				}
			}
		}

		# Merge descriptions
		$trim_desc = function(string $d): string {return trim(preg_replace('|^"(.+)"$|','$1',$d)," \n\r\t\v\0|;.,");};
		foreach ( $cmds AS $key => $cmd ) {
			if ( !preg_match('|^D|',$cmd->action) ) continue ;
			if ( count((array)$cmd->values)==1 ) continue ;
			$desc = array_map($trim_desc, $cmd->values) ;
			$desc = implode ( "; " , $desc ) ;
			$cmd->values = [ $desc=>$desc ] ;
		}

		# Check that no description is identical to its language label
		$labels = [] ;
		$descs = [] ;
		foreach ( $cmds AS $cmd ) {
			$language = '' ;
			if ( preg_match('|^[LD](\S+)|',$cmd->action,$m) ) $language = $m[1] ;
			if ( $language == '' ) continue ;
			if ( preg_match('|^L|',$cmd->action) ) $labels[$language] = array_values($cmd->values) ;
			if ( preg_match('|^D|',$cmd->action) ) $descs[$language] = array_values($cmd->values) ;
		}
		foreach ( $descs AS $language => $d_values ) {
			if ( !isset($labels[$language]) ) continue ;
			$l_values = $labels[$language] ;
			if ( count($d_values) > 1 ) throw new Exception(__METHOD__.': Multiple descriptions for '.$language ) ;
			if ( count($l_values) > 1 ) throw new Exception(__METHOD__.': Multiple labels for '.$language ) ;
			if ( $d_values[0] != $l_values[0] ) continue ;
			foreach ( $cmds AS $k => $cmd ) {
				if ( $cmd->action == "D{$language}" ) unset ( $cmds[$k] ) ;
			}
		}

		# Ensure no alias is the same as the label
		foreach ( $cmds AS $cmd ) {
			if ( !preg_match('|^L(.+)$|',$cmd->action,$m) ) continue ;
			$lang = $m[1] ;
			$key = "A{$lang}" ;
			if ( !isset($cmds[$key]) ) continue ;
			foreach ( $cmd->values AS $kl => $vl ) {
				foreach ( $cmds[$key]->values AS $ka => $va ) {
					if ( $vl==$va ) unset ( $cmds[$key]->values[$ka] ) ;
				}
			}
		}

		
		# Create joined QS commands
		$qs = ['CREATE'] ; # Array of strings
		foreach ( $cmds AS $cmd ) {
			foreach ( $cmd->values AS $value ) {
				$c = [ 'LAST' , $cmd->action , $value ] ;
				$c = implode ( "\t" , $c ) ;
				foreach ( $cmd->refs AS $refs ) {
					$first = true ;
					while ( count($refs) > 1 ) {
						$k = array_shift($refs) ;
						$v = array_shift($refs) ;
						if ( $first and preg_match('|^S\d+$|',$k) ) {
							$k = "!{$k}" ;
							$first = false ;
						}
						$c .= "\t{$k}\t{$v}" ;
					}
				}
				$qs[] = $c ;
			}
		}
		return $qs ;
	}

	public function getCreateItemForEntryCommands ( $entry , $lc = '' , $verbose = false ) {
		$this->possible_items = [] ;
		$commands = [] ;

		$catalog_object = $this->loadCatalog ( $entry->catalog , true ) ;
		$cd = $catalog_object->data();

		$source = '' ;
		$source_via_property = isset($cd->wd_prop) and !isset($cd->wd_qual);
		/*if ( in_array($cd->wd_prop,[10]) { # exclude specific props for reference, eg P10=video
			$source_via_property = false;
		} else */
		if ( $source_via_property ) {
			# Try "stated in" from property
			$prop = "P{$cd->wd_prop}" ;
			$this->wil->loadItems ( [$prop] ) ;
			$i = $this->wil->getItem ( $prop ) ;
			# TODO exclude datatype=commonsMedia
			$use_source = true;
			if ( isset($i) ) {
				if ( $i->j->datatype=='commonsMedia' ) $use_source = false;
				else {
					$claims = $i->getClaims("P9073") ; # "stated in"
					if ( count($claims) > 0 ) {
						$stated_in = $i->getTarget($claims[0]) ; # First one
						if ( isset($stated_in) ) $source .= "\tS248\t{$stated_in}" ;
					}
				}
			}

			# Property:ID
			if ( $use_source ) $source .= "\tS{$cd->wd_prop}\t\"{$entry->ext_id}\"" ;

		} else if ( $entry->ext_url != '' ) {
			$source = "\tS854\t\"" . $this->fixURL($entry->ext_url) . '"' ;
			# $source .= "\tS813\t" . $this->date2expression(date('Y-m-d')) ; # Deactivated as per https://www.wikidata.org/wiki/Topic:V2tl70hbgsvundhx
		}

		# Entry creation date, assuming it will roughly correlate with web request
		# ERROR: LEADS TO
		# "P813":[{"snaktype":"value","property":"P813","datavalue":{"type":"unknown","text":""}}]}}
		$sql = "SELECT /* ".__METHOD__." */ * FROM `entry_creation` WHERE `entry_id`={$entry->id}" ;
		$result = $this->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			$date = substr ( $o->timestamp , 0 , 12 ) ;
			$p813 = "\tS813\t" . $this->date2expression($date) ;
		}

		# P31
		if ( preg_match ( '/^Q\d+$/' , $entry->type ) ) $commands[] = "LAST\tP31\t{$entry->type}" ;

		# Label
		$lang = $cd->search_wp ;
		if ( isset($this->language_code_replace[$lang]) ) $lang = $this->language_code_replace[$lang] ;

		$labels_in_language = [ $lang => strtolower($entry->ext_name) ] ;
		$had_that_label = [ $lang => 1 ] ;
		$commands[] = "LAST\tL{$lang}\t\"" . $this->fixStringForQS($entry->ext_name) . '"' ;
		if ( $entry->type == 'Q5' and in_array ( $lang , $this->similar_languages ) ) {
			foreach ( $this->similar_languages AS $l ) {
				if ( $l == $lang ) continue ;
				$commands[] = "LAST\tL{$l}\t\"" . $this->fixStringForQS($entry->ext_name) . '"' ;
				$had_that_label[$l] = 1 ;
				$labels_in_language[$l] = strtolower($entry->ext_name) ;
			}
		}


		# Aliases
		$sql = "SELECT /* ".__METHOD__." */ * FROM aliases WHERE entry_id={$entry->id}" ;
		$result = $this->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			if ( $o->language == '' ) $o->language = $lang ; # Fallback
			$key = 'A' ;
			if ( !isset($had_that_label[$o->language]) ) {
				$had_that_label[$o->language] = 1 ;
				$key = 'L' ; # Use first alias as label, if none is set in that language
			}
			if ( $key=='A' and $o->language == $lang and ( $entry->ext_name == $o->label or $labels_in_language[$lang] == strtolower($o->label) ) ) continue ;
			if ( $key == 'L' ) $labels_in_language[$lang] = strtolower($o->label) ;
			$commands[] = "LAST\t{$key}{$o->language}\t\"" . $this->fixStringForQS($o->label) . '"' ;
		}

		# Descriptions
		$sql = "SELECT /* ".__METHOD__." */ * FROM descriptions WHERE entry_id={$entry->id}" ;
		$result = $this->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			if ( $o->label == '' ) continue ;
			if ( $o->language == '' ) continue ; #$o->language = $lang ; # Fallback
			if ( $labels_in_language[$o->language]??'' == strtolower($o->label) ) continue ;
			$commands[] = "LAST\tD{$o->language}\t\"" . $this->fixStringForQS($o->label) . '"' ;
		}

		# Description
		$desc = $this->getDescriptionForEntryObject ( $entry ) ;
		if ( $desc != '' and $cd->use_description_for_new ) {
			if ( $labels_in_language[$lang]??'' != strtolower($desc) )
				$commands[] = "LAST\tD{$lang}\t\"" . $this->fixStringForQS($desc) . '"' ;
		}

		# Location
		$sql = "SELECT /* ".__METHOD__." */ * FROM `location` WHERE entry_id={$entry->id}" ;
		$result = $this->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			$commands[] = "LAST\tP625\t@{$o->lat}/{$o->lon}{$source}" ;
		}

		# Dates
		if ( $entry->type == 'Q5' ) {
			$sql = "SELECT /* ".__METHOD__." */ * FROM person_dates WHERE entry_id={$entry->id}" ;
			$result = $this->getSQL ( $sql ) ;
			while ( $o = $result->fetch_object() ) {
				if ( $o->born != '' ) {
					$de = $this->date2expression($o->born,true) ;
					if ( isset($de) ) $commands[] = "LAST\tP569\t{$de}{$source}" ;
				}
				if ( $o->died != '' ) {
					$de = $this->date2expression($o->died,true) ;
					if ( isset($de) ) $commands[] = "LAST\tP570\t{$de}{$source}" ;
				}
			}
		}

		# external IDs
		$external_ids = [] ;
		if ( $source_via_property ) $external_ids["P{$cd->wd_prop}"][$entry->ext_id] = [$entry->ext_id] ;

		# Aux
		$conditions = [] ;
		$sql = "SELECT /* ".__METHOD__." */ * FROM auxiliary WHERE entry_id={$entry->id}" ;
		$result = $this->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			$prop = 'P' . $o->aux_p ;
			if ( in_array($prop, ["P569","P570"]) ) continue; // Dates TODO FIXME
			$external_ids[$prop][$o->aux_name] = [$o->aux_name,$source] ;
			if ( !preg_match ( '/^Q\d+$/' , $o->aux_name ) ) { # String value HACKISH FIXME
				$value = $this->fixPropertyValueFromLCtoWikidata ( $prop , $o->aux_name ) ;
				if ( $lc != '' ) {
					if ( $prop == $lc->getMainProp() ) $conditions[] = "ext_id='".$this->escape($o->aux_name)."'" ;
					foreach ( $lc->prop2field[$lc->getCatalogID()] AS $p => $col ) {
						if ( $p != $prop ) continue ;
						$conditions[] = "$col='".$this->escape($o->aux_name)."'" ;
					}
				}
			}
		}

		# From large catalog
		if ( count($conditions) > 0 ) {
			$table = $lc->getCat()->table ;
			$conditions = array_unique ( $conditions ) ;
			$sql = "SELECT /* ".__METHOD__." */ * FROM `$table` WHERE ((" . implode ( ") OR (" , $conditions ) . "))" ;
			if ( $verbose ) print "\n{$sql}\n" ;
			$result = $this->tfc->getSQL ( $lc->db , $sql ) ;
			while ( $o = $result->fetch_object() ) {
				$prop = $lc->getMainProp() ;
				$value = $this->fixPropertyValueFromLCtoWikidata ( $prop , $o->ext_id ) ;
				$external_ids[$prop][$value] = [$value,$source] ;
				foreach ( $lc->prop2field[$lc->getCatalogID()] AS $prop => $col ) {
					$value = trim ( $o->$col ) ;
					if ( $value == '' ) continue ;
					$value = $this->fixPropertyValueFromLCtoWikidata ( $prop , $value ) ;
					$external_ids[$prop][$value] = [$value] ;
				}
			}
		}

		if ( isset($external_ids['P213']) ) {
			foreach ( $external_ids['P213'] AS $k => $v ) {
				$external_ids['P213'][$k] = str_replace(' ','',$v);
			}
		}

		if ( isset($external_ids['P227']) ) {
			foreach ( $external_ids['P227'] AS $k => $gnd ) {
				if ( $this->isGNDundifferentiatedPerson($gnd) ) unset ( $external_ids['P227'][$k] ) ;
			}
			if ( count($external_ids['P227']) == 0 ) unset ( $external_ids['P227'] ) ;
		}

		# Check external IDs via SPARQL, convert to commands
		$this->wil->loadItems ( array_keys($external_ids) ) ;
		$sparql_conditions = [] ;
		foreach ( $external_ids AS $prop => $values ) { # $prop is Pxxx
			$i = $this->wil->getItem ( $prop ) ;
			if ( !isset($i) ) continue ; // Property does not exist on Wikidata??
			if ( !isset($i->j) ) continue ; # No JSON?
			if ( !isset($i->j->datatype) ) continue ; # Odd but happens?
			foreach ( $values AS $dummy => $v ) {
				$value = trim($v[0]) ;
				if ( $value == '' ) continue ;
				$s = isset($v[1]) ? $v[1] : '' ;
				$is_string_value = !($i->j->datatype=='wikibase-item') ;
				if ( $i->j->datatype == 'external-id' ) $sparql_conditions[] = "?q wdt:{$prop} '{$value}'" ;
				if ( $i->j->datatype == 'quantity' ) {
					$is_string_value = false ;
					if ( isset($this->default_units[$prop]) ) $value = "{$value}U{$this->default_units[$prop]}" ;
				}
				if ( $prop == 'P625' ) $commands[] = "LAST\t{$prop}\t@{$value}{$s}" ;
				else if ( $i->j->datatype == 'time' ) $commands[] = "LAST\t{$prop}\t{$value}{$s}" ;
				else if ( $is_string_value ) $commands[] = "LAST\t{$prop}\t\"{$value}\"{$s}" ;
				else $commands[] = "LAST\t{$prop}\t{$value}{$s}" ;
			}
		}
		if ( count($sparql_conditions) > 0 ) {
			$sparql = "SELECT /* ".__METHOD__." */ DISTINCT ?q { { " . implode ( " } UNION { " , $sparql_conditions ) . " } }" ;
			if ( $verbose ) print "\n{$sparql}\n" ;
			try {
				$items = $this->tfc->getSPARQLitems ( $sparql , 'q' ) ;
			} catch (Exception $e) {
				$items = [] ;
			}
			if ( count($items) > 0 ) {
				$this->possible_items = $items ;
				if ( count($items)==1 ) $this->setMatchForEntryID ( $entry->id , $items[0] , 0 , true , false ) ;
				$this->logError ( $this->getEntryURL($entry->id)." might already exist as " . json_encode($items) ) ;
				return ;
			}
		}

		# Described by source/URL, for catalogs with no property
		if ( !isset($cd->wd_prop) # No catalog property
			and !isset($cd->wd_qual) # No catalog qualifier
			and !isset($external_ids['P1343']) # No "described by source" already
			and !isset($external_ids['P973']) # No "described at URL" already
			and isset($entry->ext_url) # Entry has an external URL
			and $entry->ext_url != ''  # that is not empty
		) {
			$url = $this->fixURL($entry->ext_url) ;
			if ( isset($cd->source_item) ) $commands[] = "LAST\tP1343\tQ{$cd->source_item}\tP2699\t\"{$url}\"" ;
			else $commands[] = "LAST\tP973\t\"{$url}\"" ;
		}

		$this->extend_commands_from_gnd ( $commands ) ;

		$commands = array_unique ( $commands ) ;
		array_unshift ( $commands , 'CREATE' ) ;
		return $commands ;
	}

	protected function extend_commands_from_gnd ( &$commands ) {
		$gnd = '' ;
		foreach ( $commands AS $command ) {
			if ( !preg_match('|\tP227\t"(.+?)"|',$command,$m) ) continue ;
			$gnd = $m[1] ;
		}
		if ( $gnd == '' ) return ; # No GND
		$url = "https://wikidata-todo.toolforge.org/gnd.php?action=get_qs&gnd={$gnd}" ;
		$j = json_decode(file_get_contents($url)) ;
		if ( $j->status != 'OK' ) return ;
		foreach ( $j->qs AS $command ) {
			if ( !preg_match('|^(.+?\t.+?\t[^\t]+)|',$command,$m) ) continue ;
			$found = false ;
			$base_command = $m[1] ;
			foreach ( $commands AS $c ) {
				if ( $base_command != substr($c,0,strlen($base_command)) ) continue ;
				$found = true ;
			}
			if ( $found ) continue ;
			$commands[] = $command ;
		}
	}

	# JOBS METHODS

	protected function quote ( $s ) {
		return "'{$s}'" ;
	}

	public function get_job ( $catalog_id , $action ) {
		$sql = "SELECT /* ".__METHOD__." */ * FROM `jobs` WHERE `catalog`=".($catalog_id*1)." AND `action`='".$this->escape($action)."'" ;
		return $this->getSQL($sql)->fetch_object() ;
	}

	public function get_job_by_id ( $job_id ) {
		$job_id *= 1 ;
		$sql = "SELECT /* ".__METHOD__." */ * FROM `jobs` WHERE `id`={$job_id}" ;
		return $this->getSQL($sql)->fetch_object() ;
	}

	public function queue_job ( $catalog_id , $action , $depends_on = 0 , $json = '' , $seconds = 0 , $user_id = 0 , $status = 'TODO' ) {
		$depends_on *= 1 ;
		$seconds *= 1 ;
		$catalog_id *= 1 ;
		$user_id *= 1 ;
		$keys = ['catalog','action','status','last_ts'] ;
		$values = [ $catalog_id , $this->quote($this->escape($action)) , $this->quote($this->escape($status)) , $this->quote($this->getCurrentTimestamp()) ] ;
		$job = $this->get_job($catalog_id,$action) ;
		if ( isset($json) and $json != '' ) {
			$keys[] = 'json' ;
			$values[] = $this->quote($this->escape($json)) ;
		}
		if ( isset($depends_on) and $depends_on>0 ) {
			$keys[] = 'depends_on' ;
			$values[] = $depends_on ;
		} else if ( isset($job) and $job->depends_on != $depends_on ) {
			$keys[] = 'depends_on' ;
			$values[] = $job->depends_on ;
		}
		if ( isset($seconds) and $seconds>0 ) {
			$keys[] = 'repeat_after_sec' ;
			$values[] = $seconds ;
		}
		if ( isset($user_id) and $user_id>0 ) {
			$keys[] = 'user_id' ;
			$values[] = $user_id ;
		}
		if ( isset($job) ) {
			$keys[] = 'id' ;
			$values[] = $job->id ;
		}
		$sql = "REPLACE INTO `jobs` (`" . implode('`,`',$keys) . "`) VALUES (" . implode(',',$values) . ")" ;
		$this->getSQL($sql) ;
		$job_id = $this->dbm->insert_id;
		$this->reset_dependent_job_status ( [$job_id] ) ;
		return $job_id ;
	}

	protected function reset_dependent_job_status ( $job_ids ) {
		$had_that = [] ;
		while ( count($job_ids) > 0 ) {
			foreach ( $job_ids AS $id ) $had_that[$id] = $id ;
			$sql = "SELECT /* ".__METHOD__." */ * FROM `jobs` WHERE `depends_on` IN (" . implode($job_ids) . ")" ;
			$sql_after = "UPDATE `jobs` SET `status`='TODO' WHERE `depends_on` IN (" . implode($job_ids) . ")" ;
			$result = $this->getSQL($sql) ;
			$job_ids = [] ;
			while ( $o = $result->fetch_object() ) {
				if ( isset($had_that[$o->id]) ) continue ;
				$job_ids[] = $o->id ;
			}
			$this->getSQL($sql_after);
		}
	}

	protected function schedule_job($job_id,$seconds) {
		$ts = $this->getFutureTimestamp($seconds*1) ;
		$sql = "UPDATE `jobs` SET `next_ts`='{$ts}' WHERE `id`={$job_id}" ;
		$this->getSQL($sql);
	}

	public function set_job_status(&$job,$status,$note='') {
		$ts = $this->getCurrentTimestamp() ;
		$sql = "UPDATE `jobs` SET `status`='".$this->escape($status)."'" ;
		if ( isset($note) and $note != '' ) $sql .= ",`note`='".$this->escape($note)."'" ;
		$sql .= ",`last_ts`='{$ts}' WHERE `id`={$job->id}" ;
		$this->getSQL($sql);
		$job->status = $status ;
		$job->last_ts = $ts ;

		# Update next run time
		if ( isset($job->repeat_after_sec) and $job->repeat_after_sec*1>0 and ($status=='DONE' or $status=='FAILED') ) {
			$this->schedule_job($job->id,$job->repeat_after_sec*1) ;
		}
	}

	// This should only ever been run manually, and technically shouldn't be necessary at all, keeping the code just in case...
	protected function update_all_missing_scheduled_jobs() {
		$sql = "SELECT /* ".__METHOD__." */ id,repeat_after_sec,last_ts FROM jobs WHERE repeat_after_sec IS NOT NULL AND repeat_after_sec>0 AND (next_ts='' OR next_ts<last_ts)";
		$result = $this->getSQL($sql) ;
		while ( $o = $result->fetch_object() ) {
			$parsed = date_parse($o->last_ts) ;
			$t = mktime(
		        $parsed['hour'], 
		        $parsed['minute'], 
		        $parsed['second'], 
		        $parsed['month'], 
		        $parsed['day'], 
		        $parsed['year']
			);
			$t += $o->repeat_after_sec*1 ;
			$t = date ( 'YmdHis' , $t ) ;
			# print "{$o->id} : {$o->last_ts} => {$t}\n" ;
			$sql = "UPDATE jobs SET next_ts='{$t}' WHERE id={$o->id}" ;
			$this->getSQL($sql);
		}
	}




	public function addIssue ( $entry_id , $type , $json ) {
		$entry_id *= 1 ;
		if ( $entry_id <= 0 ) return ;
		if ( !is_string($json) ) $json = json_encode($json) ;
		$sql = "INSERT IGNORE INTO `issues` (`entry_id`,`type`,`json`,`random`,`catalog`) SELECT {$entry_id},'".$this->escape($type)."','".$this->escape($json)."',rand(),`catalog` FROM `entry` WHERE `id`={$entry_id}" ;
		$this->getSQL ( $sql ) ;
	}

	public function get_kv_value ( $key , $default = '' ) {
		$sql = "SELECT /* ".__METHOD__." */ * FROM `kv` WHERE `kv_key`='".$this->escape($key)."'" ;
		$result = $this->getSQL($sql) ;
		while ( $o = $result->fetch_object() ) return $o->kv_value ;
		return $default ;
	}

	public function set_kv_value ( $key , $value ) {
		$sql = "INSERT INTO `kv` (`kv_key`,`kv_value`) VALUES ('".$this->escape($key)."','".$this->escape($value)."')" ;
		$sql .= " ON DUPLICATE KEY UPDATE `kv_value`='".$this->escape($value)."'" ;
		$this->getSQL($sql) ;
	}


	# CODE FRAGMENTS METHODS

	public function touchCodeFragment ( $function , $catalog ) {
		$catalog *= 1 ;
		$sql = "UPDATE `code_fragments` SET `last_run`=NOW() WHERE `function`='".$this->escape($function)."' AND `catalog`={$catalog}" ;
		$this->getSQL($sql);
	}

	public function hasCodeFragment ( $function , $catalog , $require_active = true ) {
		$catalog *= 1 ;
		$ret = (object) [ 'success' => false ] ;
		$sql = "SELECT /* ".__METHOD__." */ * FROM `code_fragments` WHERE `function`='".$this->escape($function)."' AND `catalog`={$catalog}" ;
		if ( $require_active ) $sql .= " AND `is_active`=1" ;
		$result = $this->getSQL($sql) ;
		if ( $o = $result->fetch_object() ) return true ;
		return false ;
	}

	public function loadCodeFragment ( $function , $catalog , $require_active = true ) {
		$catalog *= 1 ;
		$ret = (object) [ 'success' => false ] ;
		$sql = "SELECT /* ".__METHOD__." */ * FROM `code_fragments` WHERE `function`='".$this->escape($function)."' AND `catalog`={$catalog}" ;
		if ( $require_active ) $sql .= " AND `is_active`=1" ;
		$result = $this->getSQL($sql) ;
		if ( $o = $result->fetch_object() ) {
			if ( !isset($o->json) or $o->json=='' ) $o->json = '{}' ;
			$o->json = json_decode($o->json);
			if ( !isset($o->json) or $o->json===null or $o->json===false ) {
				$this->logError ( "Bad JSON in catalog {$catalog} for '{$function}'" ) ;
				return $ret ;
			}
			$o->php = trim ( $o->php ) ;
			$o->success = true ;
			$ret = $o ;
		}
		return $ret ;
	}

	public function saveCodeFragment ( $fragment ) {
		$php = $this->escape($fragment->php) ;
		$json = $this->escape($fragment->json) ;
		$is_active = $fragment->is_active ? 1 : 0 ;
		$note = $this->escape($fragment->note) ;

		// Update existing code fragment
		if ( isset($fragment->id) and $fragment->id*1>0 ) {
			$id = $fragment->id*1 ;
			$sql = "UPDATE `code_fragments` SET `php`='{$php}',`json`='{$json}',`is_active`={$is_active},`note`='{$note}' WHERE `id`={$id}" ;
			$this->getSQL ( $sql ) ;
			return $id ;
		}

		// Create new code fragment
		$function = $this->escape($fragment->function) ;
		$catalog = $fragment->catalog * 1 ;
		if ( $catalog <= 0 ) throw new Exception(__METHOD__.': No/bad catalog' ) ; 
		$sql = "INSERT IGNORE INTO `code_fragments` (`function`,`catalog`,`php`,`json`,`is_active`,`note`) VALUES ('{$function}',{$catalog},'{$php}','{$json}',{$is_active},'{$note}')" ;
		$this->getSQL ( $sql ) ;
		return $this->dbm->insert_id ;
	}





	public function getEntryURL ( $entry_id ) {
		return "{$this->root_url}/#/entry/{$entry_id}" ;
	}

	// THIS IS COPIED CODE FROM manual_lists/large_catalogs/shared.php
	public function hasPropertyEverBeenRemovedFromItem ( $q , $prop , $value = '' ) {
		$this->wil->sanitizeQ ( $q ) ;
		$this->wil->sanitizeQ ( $prop ) ;
		if ( $value == '' ) $propquery = "Property:{$prop}%" ;
		else $propquery = "Property:{$prop}]]: " . $this->escape($value) . "%" ;
		$sql = "SELECT /* ".__METHOD__." */ count(*) AS cnt from page,revision_compat WHERE page_title='$q' AND page_namespace=0 AND page_id=rev_page AND (" ;
		$sql .= "rev_comment LIKE '%wbremoveclaims-remove%{$propquery}' OR " ;
		$sql .= "(rev_comment LIKE '%wbcreateclaim-create%{$propquery}' AND rev_user_text='Reinheitsgebot')" ; # Bot added this prop before, but probably was reverted
		$sql .= ")" ;
		$this->openWikidataDB() ;
		$result = $this->tfc->getSQL ( $this->dbwd , $sql ) ;
		while($o = $result->fetch_object()) {
			if ( $o->cnt >= 1 ) return true ;
		}
		return false ;
	}

	public function sanitizeShortText ( $text ) {
		return substr(trim($text),0,250) ;
	}

	public function getOrCreateTextID ( $text ) {
		$text = $this->sanitizeShortText ( $text ) ;
		if ( $text == '' ) return 0 ; # Shortcut

		# Try to find existing text
		$sql = "SELECT /* ".__METHOD__." */ `id` FROM `texts` WHERE `t`='" . $this->escape($text) . "'" ;
		$result = $this->getSQL($sql) ;
		if ( $o = $result->fetch_object() ) return $o->id ;

		# Insert new text
		$sql = "INSERT IGNORE INTO `texts` (`t`) VALUES ('" . $this->escape($text) . "')" ;
		$this->getSQL($sql) ;
		$id = $this->dbm->insert_id ;
		return $id ;
	}

	// BELOW IS PART OF AN EXPERIMENT TO REPLACE entry.ext_desc WITH a table texts(id,t)

	// UNUSED
	/*
	public function getTextFromID ( $text_id ) {
		$text_id *= 1 ;
		if ( $text_id == 0 ) return '' ; # Shortcut
		$sql = "SELECT `t` FROM `texts` WHERE `id`={$text_id}" ;
		$result = $this->getSQL($sql) ;
		if ( $o = $result->fetch_object() ) return $o->t ;
		return '' ; # Paranoia default
	}
	*/

	public function setDescriptionForEntryID ( $entry_id , $desc ) {
		$entry_id *= 1 ;
		$desc = $this->sanitizeShortText ( $desc ) ;
		$desc = @\iconv('UTF-8', 'UTF-8//IGNORE', $desc);

		$sql_parts = [] ;
		$sql_parts[] = "`ext_desc`='" . $this->escape($desc) . "'" ;

		# UNUSED
		#$sql_parts[] = "`desc_id`=" . $this->getOrCreateTextID ( $desc ) ;

		$sql = "UPDATE `entry` SET " . implode ( ',' , $sql_parts ) . " WHERE `id`={$entry_id}" ;

		return $this->getSQL ( $sql ) ;
	}

	public function descriptionIsEmptySQL () {
		return ' ext_desc="" ' ;
	}

	public function descriptionIsNotEmptySQL () {
		return ' ext_desc!="" ' ;
	}

	public function getDescriptionForEntryObject ( $entry ) {
		if ( isset($entry->ext_desc) ) return $entry->ext_desc ;
		#if ( isset($entry->desc_id) ) return $this->getTextFromID ( $entry->desc_id ) ;
		return '' ; # Paranoia fallback
	}

	# END OF EXPERIMENT TO REPLACE entry.ext_desc WITH a table texts(id,t)

	public function rand() {
		return rand() / getrandmax() ;
	}

	public function getUUID() {
	    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
	        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
	        mt_rand( 0, 0xffff ),
	        mt_rand( 0, 0x0fff ) | 0x4000,
	        mt_rand( 0, 0x3fff ) | 0x8000,
	        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ) ) ;
	}

	public function check404 ( $entry_id , $catalog = 0 , $url = '' ) {
		$entry_id *= 1 ;
		if ( $entry_id <= 0 ) return ;

		# Load URL if empty
		if ( !isset($catalog) or !isset($url) or $catalog == 0 or $url == '' ) {
			try {
				$entry = new Entry ( $entry_id , $this ) ;
				$o = $entry->core_data() ;
			} catch (Exception $e) {
				return $this->logError ( "check404: Entry #{$entry_id} not found." ) ;
			}

			if ( isset($o) and isset($o->ext_url) ) $url = $o->ext_url ;
			else return ;
			if ( isset($o) and isset($o->catalog) ) $catalog = $o->catalog ;
			else return ;
		}
		if ( !isset($catalog) or !isset($url) or $catalog == 0 or $url == '' ) return;

		$is_404 = false ;

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true ) ;
		$html = curl_exec($ch);
		if (!curl_errno($ch)) {
			$code = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
			if ( $code == 404 ) $is_404 = true ;
		}

		if ( !$is_404 ) {
			$html = preg_replace ( '|\s+|s' , ' ' , $html ) ;
		}

		if ( $is_404 ) {}
		else if ( !isset($html) or $html === null or trim($html) == '' ) {
			$is_404 = true ;
		} else if ( preg_match ( '|<h1> *404 *</h1>|' , $html ) ) {
			$is_404 = true ;
		} else if ( $catalog == 177 ) {
			$is_404 = preg_match ( '/Die angeforderte Seite ist nicht vorhanden/' , $html ) ;
		} else if ( $catalog == 292 ) {
			$is_404 = preg_match ( '|<span>ANNULLED</span>|' , $html ) ;
		} else {
			# TODO fallback - detect 404
		}

		if ( !$is_404 ) return ;
		$this->setMatchForEntryID ( $entry_id , 0 , 4 , false , true ) ; # N/A
	}

	public function getQsCommentForEntry ( $entry_id ) {
		$entry_id *= 1 ;
		if ( $entry_id <= 0 ) return '' ;
		return "\t/* from " . $this->getEntryURL($entry_id) . " */" ;
	}

	protected $wiki_logins = [] ;

	public function loginToWiki ( $api_url , $wiki_user , $wiki_pass ) {
		if ( isset($this->wiki_logins[$api_url]) ) return ;

		$api = new \Mediawiki\Api\MediawikiApi( $api_url );
		$api->login( new \Mediawiki\Api\ApiUser( $wiki_user, $wiki_pass ) );
		$services = new \Mediawiki\Api\MediawikiFactory( $api );

		$this->wiki_logins[$api_url] = (object) [ 'api' => $api , 'services' => $services ] ;
	}

	public function setWikipageText ( $api_url , $page_title , $new_wikitext , $summary = '' ) {
		if ( !isset($this->wiki_logins[$api_url]) ) throw new Exception(__METHOD__.": Not logged in to {$api_url}" ) ;

		$services = $this->wiki_logins[$api_url]->services ;
		#$new_wikitext = utf8_encode($new_wikitext) ; # TODO to prevent MD5 fail with MW API; OK to do always?
		$content = new \Mediawiki\DataModel\Content( $new_wikitext );
		$page_title = str_replace(' ','_',$page_title) ;

		# TODO summary

		$page = $services->newPageGetter()->getFromTitle( $page_title );
		$revisions = (array) $page->getRevisions() ;
		$revision = array_pop ( $revisions ) ;
		$revision = array_pop ( $revision ) ;
		if ( $revision == null ) { # Create new page
			if ( trim($new_wikitext) == '' ) return ; # Not creating blank page

			$title = new \Mediawiki\DataModel\Title( $page_title );
			$identifier = new \Mediawiki\DataModel\PageIdentifier( $title );
			$revision = new \Mediawiki\DataModel\Revision( $content, $identifier );
			$services->newRevisionSaver()->save( $revision );
		} else { # Update existing
			$old_wikitext = $revision->getContent()->getData() ;
			if ( trim($old_wikitext) == trim($new_wikitext) ) return ; # No change, no edit

			$revision = new \Mediawiki\DataModel\Revision( $content, $page->getPageIdentifier() );
			$services->newRevisionSaver()->save( $revision );
		}
	}

	# $redirected_items : [page_id=>'Q123',..]
	public function getRedirectTargets($redirected_items) {
		if ( count($redirected_items) == 0 ) return [] ;
		$q2q = [] ;
		$sql = "SELECT /* ".__METHOD__." */ * FROM `redirect` WHERE rd_from IN (".implode(',',array_keys($redirected_items)).") AND rd_namespace=0" ;
		$dbwd = $this->openWikidataDB();
		$result = $this->tfc->getSQL ( $dbwd , $sql ) ;
		while($o = $result->fetch_object()) {
			if ( !isset($redirected_items[$o->rd_from]) ) continue ;
			$source_q = $redirected_items[$o->rd_from] ;
			$target_q = $o->rd_title ;
			$q2q[$source_q] = $target_q ;
		}
		return $q2q;
	}

	public function show_proc_info() {
		$this->tfc->showProcInfo() ;
	}

	public function get_redirect_target ( $q ) {
		$this->sanitizeQ ( $q ) ;
		$sql = "SELECT /* ".__METHOD__." */ * FROM `redirect` WHERE rd_namespace=0 AND rd_from IN (SELECT page_id FROM page WHERE page_title='Q{$q}' AND page_namespace=0)" ;
		$dbwd = $this->openWikidataDB();
		$result = $this->tfc->getSQL ( $dbwd , $sql ) ;
		while($o = $result->fetch_object()) return $o->rd_title ;
	}

	public function print_err($msg) {
		fwrite(STDERR, $msg . PHP_EOL);
	}

} ;

?>
