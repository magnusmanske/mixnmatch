<?php

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

# ________________________________________________________________________________________________________________________
# HTMLtoDescription

final class HTMLtoDescription extends Helper {
	protected $rewrite_functions = [ 'clean_html'=>'clean_html' , 'DMStoDEC'=>'DMStoDEC' , 'dp'=>'parse_date' ] ;
	protected $cf_function = 'DESC_FROM_HTML' ;

	/**
	 * Constructor
	 *
	*/
	public function __construct ( /*int*/ $catalog , $mnm = '' ) {
		parent::__construct($catalog,$mnm);
		$this->use_curl = true ;
	}

	/**
	 * Tries to load the original URL and extract a description and metadata from the HTML
	 *
	 * @params object $o an entry object
	 * @return array of commands
	*/
	public function processEntry ( $o ) {
		$this->current_results = [] ;

		# Bespoke catalog-specifc voodoo
		if ( trim($o->ext_url) == '' ) return [] ;
		if ( $this->catalog == 1140 and !preg_match('/\d{4}/',$o->ext_desc) ) return [] ;
		$url = $this->rewriteURL ( $o->ext_url ) ;
		if ( $this->catalog == 1656 ) $url .= '&Niveau=bio' ;
		if ( $this->catalog == 1932 ) $url .= '?action=raw' ;

		$url = str_replace ( ' ' , '%20' , $url ) ;
		$html = $this->get_contents_from_url ( $url ) ;
		if ( $html === false or !is_string($html) ) return [] ;
		$encoding = mb_detect_encoding($html);
		if ($encoding) {
			if ( $encoding!='UTF-8') {
				//$html = iconv($encoding, 'UTF-8//IGNORE', $html);
			}
		}

		if ( $this->catalog == 1932 ) {}
		else $html = preg_replace ( '/\s+/' , ' ' , $html ) ; // Simple spaces

		# Run code fragment
		$born = '' ; // ISO date
		$died = '' ; // ISO date
		$change_type = [] ; // empty, or ['from','to']
		$change_name = [] ; // empty, or ['from','to']
		$location = [] ; // empty or [lat,lon]
		$location_texts = [] ; // multiple possible, each [property_number,'cleaned location text']
		$aux = [] ; // multiple possible, each [property_number,'value']
		$d = [] ;
		if ( !isset($this->code_fragment) ) throw new \Exception(__METHOD__.': No code fragment loaded') ;
		$php = $this->rewriteGlobalMethods ( $this->code_fragment->php ) ;
		if ( FALSE === eval($php) ) throw new \Exception(__METHOD__.": Error in code fragment:\n{$this->code_fragment->php}");

		$ret = $this->current_results ;
		if ( $born.$died != '' ) $ret[] = Command::setPersonDates ( $o->id , $born , $died ) ;
		if ( count($location)==2 ) $ret[] = Command::setLocation ( $o->id , $location[0] , $location[1] ) ;
		foreach ( $aux AS $a ) $ret[] = Command::setAux ( $o->id , $a[0] , $a[1] ) ;
		foreach ( $location_texts AS $lt ) $ret[] = Command::setLocationText ( $o->id , $lt[0]*1 , $lt[1] ) ;
		if ( count($change_name) == 2 and $change_name[0]!=$change_name[1] ) $ret[] = Command::setEntryName ( $o->id , $change_name[1] ) ;
		if ( count($change_type) == 2 ) $ret[] = Command::setEntryType ( $o->id , $change_type[1] ) ;

		$new_description = $this->get_new_description ( $o , $d ) ;
		if ( $o->ext_desc != $new_description ) $ret[] = Command::setDescription ( $o->id , $new_description ) ;

		$this->current_results = $ret ;
		return $ret ;
	}

	/**
	 * Tries to gennerate a new description, based on new information and the old description
	 *
	 * @param object $o entry object
	 * @param object $res result object from processEntry()
	 * @return string the new description
	*/
	private function get_new_description($o,$d) {
		if ( count($d) == 0 ) return $o->ext_desc ;
		if ( !is_array($d) ) $d = [$d] ;
		if ( implode('; ',$d) == $o->ext_desc ) return $o->ext_desc ;
		if ( $o->ext_desc != '' ) $d[] = $o->ext_desc ;

		$d = implode ( '; ' , $d ) ;
		$d = preg_replace ( '/<.+?>/' , ' ' , $d ) ; // Remove HTML tags
		$d = preg_replace ( '/\s+/' , ' ' , $d ) ; // Simple spaces
		$d = trim ( $d ) ;
		if ( strlen($d) > 250 ) {
			$d2 = mb_substr($d, 0, 250, 'UTF-8');
		}
		return $d ;
	}

	/**
	 * For specific catalogs, loads the given URL and extracts the useful one.
	 * 
	 * @param string $url The URL to rewrite
	 * @return string the modified or (in most cases) original URL
	*/
	private function rewriteURL ( $url ) {
		if ( $this->catalog == 2667 ) {
			$html = @file_get_contents ( $url ) ;
			if ( preg_match('|<a href="/modals/artist-bio\.inc\.php\?id=(\d+)|',$html,$m) ) {
				$url = "https://www.artbrokerage.com/modals/artist-bio.inc.php?id={$m[1]}" ;
				#print "Changing to $url\n" ;
			}
		}
		return $url ;
	}

	/**
	 * Checks if the JSON of the code fragment allows for descriptions to be amended
	 *
	 * @return bool can amend existing descriptions?
	*/
	public function descriptionPresentAllowed() {
		if ( !isset($this->code_fragment) ) throw new \Exception(__METHOD__.': No code_fragment loaded');
		if ( !isset($this->code_fragment->json->allow_descriptions_present) ) return false ;
		return $this->code_fragment->json->allow_descriptions_present ;
	}

	/**
	 * Returns a bespoke SQL query fragment from the code fragment JSON
	 *
	 * @return string SQL or blank
	*/
	public function getSQLaddendum() {
		if ( !isset($this->code_fragment) ) throw new \Exception(__METHOD__.': No code_fragment loaded');
		if ( !isset($this->code_fragment->json->sql_addendum) ) return '' ;
		return $this->code_fragment->json->sql_addendum ;
	}

	/**
	 * Converts DMS to DEC coordinates.
	 * Invoked from within a code fragment
	 *
	 * @param int $deg
	 * @param int $min
	 * @param int $sec
	 * @return float "GPS" decimal coordinate
	*/
	private function DMStoDEC($deg,$min,$sec) {
    	return $deg+((($min*60)+($sec))/3600);
	}    

	/**
	 * Cleans up HTML.
	 * Invoked from within a code fragment
	 *
	 * @param string $html HTML
	 * @return string cleaned-up HTML
	*/
	private function clean_html ( $html ) {
		$html = preg_replace ( '/&nbsp;/' , ' ' , $html ) ;
		$html = preg_replace ( '/<.+?>/' , ' ' , $html ) ;
		$html = preg_replace ( '/\s+/' , ' ' , $html ) ;
		$html = html_entity_decode ( $html ) ;
		return trim ( $html ) ;
	}

	public function getMainSQL() {
		if ( !isset($this->catalog) ) throw new \Exception(__METHOD__.': no catalog' ) ;
		if ( !isset($this->code_fragment) ) throw new \Exception(__METHOD__.': No code_fragment loaded');
		$sql = "SELECT * FROM entry WHERE catalog={$this->catalog}" ;
		$sql_addendum = $this->getSQLaddendum() ;
		if ( $sql_addendum != '' ) $sql .= $sql_addendum ;
		else if ( !$this->descriptionPresentAllowed() ) $sql .= " AND (q IS NULL or user=0) AND " . $this->mnm->descriptionIsEmptySQL() ;
		return $sql ;
	}

}

?>