<?php

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

# ________________________________________________________________________________________________________________________
# PersonDates

final class PersonDates extends Helper {
	private $month_in_other_languages ;
	private $q2month = [ 'Q108'=>'jan','Q109'=>'feb','Q110'=>'mar','Q118'=>'apr','Q119'=>'may','Q120'=>'jun','Q121'=>'jul','Q122'=>'aug','Q123'=>'sep','Q124'=>'oct','Q125'=>'nov','Q126'=>'dec' ] ;
	protected $rewrite_functions = [ 'ml'=>'try_get_three_letter_month' , 'dp'=>'parse_date' ] ;
	protected $cf_function = 'PERSON_DATE' ;

	/**
	 * Constructor
	 *
	*/
	public function __construct ( /*int*/ $catalog , $mnm = '' ) {
		parent::__construct($catalog,$mnm);
	}

	/**
	 * Tries to convert a month name into a three-letter month code
	 *
	 * @param $month string name of a month, in any language
	 * @return string three-letter standard form of the month, or the original string if unrecognized
	*/
	public function try_get_three_letter_month ( $month ) {
		$month = strtolower(trim($month)) ;
		if ( !isset($this->month_in_other_languages) ) $this->load_month_labels() ;
		if ( isset($this->month_in_other_languages[$month]) ) return $this->month_in_other_languages[$month] ;
		return $month ;
	}

	public function flushPersonDates ( $values ) {
		if ( count($values) == 0 ) return ;
		$sql = "INSERT IGNORE INTO person_dates (entry_id,born,died,is_matched) VALUES " . implode ( ',' , $values ) ;
		$this->mnm->getSQL ( $sql ) ;
		$this->updateHasPersonDates('yes') ;
		$this->touchCodeFragment();
	}

	public function updateDatesFromDescription () {
		$max_values_batch_size = 5000 ;
		$values = [] ;
		$sql = "SELECT id,ext_id,ext_name,ext_desc,q,user FROM `entry` WHERE `catalog`={$this->catalog} AND `type`='Q5'" ;
		$sql .= " AND " . $this->mnm->descriptionIsNotEmptySQL();
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			$commands = $this->processEntry ( $o ) ;
			foreach ( $commands AS $command ) {
				if ( $command->getAction() != Command::SET or $command->getTarget() != Command::PERSON_DATES ) continue ;
				$data = $command->getData() ;
				$entry_id = $command->getEntryID() ;
				$is_matched = $data->is_matched?1:0 ;
				$values[] = "({$entry_id},'{$data->born}','{$data->died}',{$is_matched})" ;
				if ( count($values) < $max_values_batch_size ) continue ;
				$this->flushPersonDates($values) ;
				$values = [] ;
			}
		}
		$this->flushPersonDates($values) ;

	}

	/**
	 * Tries to fix up some partially broken date formats
	 *
	 * @param &$d string A date, to be converted to an ISO date if possible
	*/
	public function fix_date_format ( &$d ) {
		$d = trim ( $d ) ;
		if ( $d == '' ) return ;
		$d = preg_replace ( '/^0+/' , '' , $d ) ; // Leading zeros
		if ( preg_match ( '/^(\d{3,4})-(\d)-(\d)$/' , $d , $m ) ) $d = $m[1].'-0'.$m[2].'-0'.$m[3] ;
		else if ( preg_match ( '/^(\d{3,4})-(\d\d)-(\d)$/' , $d , $m ) ) $d = $m[1].'-'.$m[2].'-0'.$m[3] ;
		else if ( preg_match ( '/^(\d{3,4})-(\d)-(\d\d)$/' , $d , $m ) ) $d = $m[1].'-0'.$m[2].'-'.$m[3] ;
		while ( preg_match ( '/^\d{1,3}-/' , $d ) ) $d = "0$d" ;
		while ( preg_match ( '/^\d{1,3}$/' , $d ) ) $d = "0$d" ;
		while ( preg_match ( '/^(.+)-00$/' , $d  , $m ) ) $d = $m[1] ;
	}


	/**
	 * Removes the existing person_dates for the current catalog, if applicable
	 *
	 * @throws Exception if no catalog or code fragment set
	*/
	public function clearOldDates() {
		if ( !isset($this->catalog) ) throw new \Exception(__METHOD__.': no catalog set');
		if ( !isset($this->code_fragment) ) throw new \Exception(__METHOD__.': No code fragment loaded' ) ;
		$j = $this->code_fragment->json ;
		if ( isset($j->clear_old_dates) and !$j->clear_old_dates ) return ;
		$this->mnm->clearPersonDates($this->catalog) ;
	}

	/**
	 * Tries to convert an entry object into birth/death dates
	 *
	 * @params object $o an entry object
	 * @return array of Command
	*/
	public function processEntry ( $o ) {
		# Run code fragment
		$born = '' ;
		$died = '' ;
		if ( !isset($this->code_fragment) ) throw new \Exception(__METHOD__.': No code fragment loaded' ) ;
		$php = $this->rewriteGlobalMethods ( $this->code_fragment->php ) ;
		if ( FALSE === eval($php) ) throw new \Exception(__METHOD__.": Error in code fragment:\n{$this->code_fragment->php}\n");
		
		# Cleanup
		$this->fix_date_format ( $born ) ;
		$this->fix_date_format ( $died ) ;
		
		# Year paranoia
		if ( $born!='' and $died!='' and (int)preg_replace('/-.*$/','',$born)*1 == (int)preg_replace('/-.*$/','',$died)*1 ) return [] ;
		if ( preg_match ( '/^\d+$/' , $born) and $born*1>2050 ) return [] ;
		if ( preg_match ( '/^\d+$/' , $died) and $died*1>2050 ) return [] ;
		if ( preg_match ( '/^(\d+)/' , $born , $m ) and preg_match ( '/^(\d+)/' , $died , $n ) ) {
			if ( $n[1]*1 - $m[1]*1 > 120 ) return [] ; // Older than 120
			if ( $m[1]*1 > $n[1]*1 ) return [] ; // born after death
		}

		if ( $born . $died == '' ) return [] ; // No need to update

		// Paranoia
		if ( preg_match ( '/-00-00$/' , $born ) or preg_match ( '/-00-00$/' , $died ) ) return [] ;
		$born = preg_replace ( '/-00-\d\d$/' , '' , $born ) ;
		$died = preg_replace ( '/-00-\d\d$/' , '' , $died ) ;
		if ( $born != '' and !preg_match ( '/^[0-9]+-[0-9]{1,2}-[0-9]{1,2}$/' , $born ) and !preg_match ( '/^[0-9]+-[0-9]{1,2}$/' , $born ) and !preg_match ( '/^[0-9]+$/' , $born ) ) return [] ;
		if ( $died != '' and !preg_match ( '/^[0-9]+-[0-9]{1,2}-[0-9]{1,2}$/' , $died ) and !preg_match ( '/^[0-9]+-[0-9]{1,2}$/' , $died ) and !preg_match ( '/^[0-9]+$/' , $died ) ) return [] ;

		if ( preg_match('/^(\d+)-(\d+)-(\d+)$/',$born,$m) ) {
			if ( $m[2] * 1 < 1 ) return [] ;
			if ( $m[2] * 1 > 12 ) return [] ;
		}
		if ( preg_match('/^(\d+)-(\d+)-(\d+)$/',$died,$m) ) {
			if ( $m[2] * 1 < 1 ) return [] ;
			if ( $m[2] * 1 > 12 ) return [] ;
		}
		if ( $born == $died ) return [] ; // Yeah no
	
		$is_matched = false ;
		if ( isset($o->user) and $o->user!=null and $o->user>0 and isset($o->q) and $o->q!=null and $o->q>0 ) $is_matched = true ;

		return [ Command::setPersonDates ( $o->id , $born , $died , $is_matched ) ] ;
	}

	/**
	 * Loads months labels mapping via SPARQL
	 * Invoked when first necessary by try_get_three_letter_month()
	 *
	*/	
	private function load_month_labels() {
		$this->month_in_other_languages = [] ;
		$sparql = 'SELECT DISTINCT ?q ?label (lang(?label) AS ?lang) { ?q wdt:P31 wd:Q47018901 ; rdfs:label ?label }' ;
		$j = $this->mnm->tfc->getSPARQL($sparql) ;
		foreach ( $j->results->bindings AS $b ) {
			$q = $this->mnm->tfc->parseItemFromURL($b->q->value) ;
			$label = trim(strtolower($b->label->value)) ;
			$language = trim(strtolower($b->lang->value)) ;
			if ( !isset($this->q2month[$q]) ) continue ;
			if ( $label == '' ) continue ;
			/* Duplicate key warnings, for debugging
			if ( isset($this->month_in_other_languages[$label]) and $this->month_in_other_languages[$label]!=$this->q2month[$q] ) {
				print "MONTHS WARNING: month_in_other_languages[{$label}] is '{$this->month_in_other_languages[$label]}' but {$language}:q2month[{$q}] is '{$this->q2month[$q]}'!\n" ;
			}
			*/
			$this->month_in_other_languages[$label] = $this->q2month[$q] ;
		}
	}

	public function updateHasPersonDates ( $value ) {
		$value = $this->mnm->escape ( $value ) ;
		$sql = "UPDATE catalog SET has_person_date='{$value}' WHERE id={$this->catalog} AND has_person_date!='{$value}'" ;
		$this->mnm->getSQL ( $sql ) ;
	}


	protected function findBirtdateMatch ( $entry_id , $born , $qs , $prop ) {
		$wil = new \WikidataItemList ;
		$wil->loadItems ( $qs ) ;
		$matches = [] ;
		$compare_field = '+' . ($prop=='P569'?$born:$died) ;
		foreach ( $qs AS $q ) {
			$q = 'Q' . preg_replace ( '/\D/' , '' , "$q" ) ;
			$i = $wil->getItem ( $q ) ;
			if ( !isset($i) ) continue ;
			$claims = $i->getClaims($prop) ;
			foreach ( $claims AS $c ) {
				if ( !isset($c->mainsnak) or !isset($c->mainsnak->datavalue) ) continue ;
				$time = $c->mainsnak->datavalue->value->time ;
				if ( substr($time,0,strlen($compare_field)) == $compare_field ) $matches[$q] = $q ;
			}
		}
		if ( count($matches) == 0 ) return ;
		if ( count($matches) > 1 ) {
			$this->mnm->addIssue ( $entry_id , 'WD_DUPLICATE' , $matches ) ;
			return ;
		}
		$q = array_pop ( $matches ) ;
		#print "{$mnm->root_url}/#/entry/{$entry_id} => https://www.wikidata.org/wiki/{$q}\n" ;
		$this->mnm->setMatchForEntryID ( $entry_id , $q , '4' , true , false ) ;
	}

	protected function matchAllBirthdaysEntries ( $sql , $prop , $year_is_enough , $the_field ) {
		if ( $year_is_enough ) $sql .= " AND {$the_field}!=''" ; # Anything goes
		else $sql .= " AND length({$the_field})=10" ; # Only full days
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			if ( $year_is_enough ) $o->$the_field = substr ( $o->$the_field , 0 , 4 ) ;
			$this->findBirtdateMatch ( $o->entry_id , $o->$the_field , explode (',',$o->qs) , $prop ) ;
		}
	}

	public function birthdayMatcher ( $prop , $year_is_enough ) {
		if ( $prop != 'P569' ) die ("THERE IS A BUG HERE, DO NOT USE"); # TODO FIXME

		$catalog_id = $this->catalog ;
		$the_field = $prop=='P569'?'born':'died' ;
		$other_field = $prop=='P569'?'died':'born' ;

		# MULTI-MATCH
		$sql = "SELECT multi_match.entry_id,born,died,candidates AS qs FROM vw_dates,multi_match WHERE {$other_field}='' AND (q IS NULL OR user=0) AND vw_dates.entry_id=multi_match.entry_id" ;
		if ( isset($catalog_id) ) $sql .= " AND multi_match.catalog={$catalog_id}" ;
		$this->matchAllBirthdaysEntries ( $sql , $prop , $year_is_enough , $the_field ) ;

		# AUTO-MATCH
		$sql = "SELECT entry_id,born,died,q qs FROM vw_dates WHERE {$other_field}='' AND (q is null or user=0)" ;
		if ( isset($catalog_id) ) $sql .= " AND catalog={$catalog_id}" ;
		$this->matchAllBirthdaysEntries ( $sql , $prop , $year_is_enough , $the_field ) ;
	}

}


?>