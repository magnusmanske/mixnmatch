<?PHP

namespace MixNMatch ;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class RecentChangesWatcher {
	public $qs_tool_name = 'mixnmatch:recent_changes_watcher' ;

	private $matches_via_properties_limit = 2000 ;
	private $redirect_limit = 5000 ;
	private $deleted_items_limit = 10000 ;
	private $sync_wd_matches_limit = 1000 ;
	private $sync_matches_to_wd_limit = 200 ;
	private $property_blacklist = [] ; # 'Pxxx'
	private $suspended_catalogs = [ # Catalog IDs
		150 , # From microsync.php
		506 , # Hard ignore, because data problem in source; from microsync.php
		1619 # DBID, temporary
	] ;
	private $respect_single_value_for_property = [
		'P1015' , # Bibsys
		'P1871', # https://www.wikidata.org/wiki/Wikidata:Project_chat#Please_protect_Q1339
		'P1580', # https://www.wikidata.org/wiki/Wikidata:Project_chat#Please_protect_Q1339
		'P535' , # https://www.wikidata.org/wiki/User_talk:Reinheitsgebot#Doppeleintr%C3%A4ge_bei_Findagrave
	] ;

	private $mnm ;
	private $aux ;
	private $testing ;
	private $prop2catalogs ;
	private $all_catalogs_with_props ;
	private $prop_value_q = [] ;
	private $person_dates = [] ;
	private $q2names = [] ;

	function __construct($testing=false,$mnm='') {
		$this->testing = $testing ;
		$this->aux = new Auxiliary ( $testing , $mnm ) ;
		$this->mnm = $this->aux->mnm ;
		$this->initializeCatlogsAndProperties() ;
	}

	private function log ( $message , $level = 1 ) {
		if ( !$this->testing and $level > 1 ) return ;
		print "{$message}\n" ;
	}

	private function isPropertyForAuxMatch ( $prop ) {
		$this->mnm->sanitizeQ ( $prop ) ;
		$prop = "P{$prop}" ;
		if ( $prop == 'P225' ) return true ; # HARDCODED POSITIVE
		if ( in_array ( $prop , $this->property_blacklist ) ) return false ;
		if ( in_array ( $prop , $this->aux->property_blacklist ) ) return false ;
		if ( in_array ( $prop , $this->aux->properties_that_have_external_ids) ) return true ;
		return false ;
	}

	public function checkMatchesViaProperties() {
		$this->aux->initializePropertyLists() ;

		# Get Recent Changes from Wikidata
		$dbwd = $this->mnm->openWikidataDB(true) ;
		$last_ts = $this->mnm->get_kv_value ( 'recent_changes_watcher:last_time' , '' ) ;
		$sql = "SELECT `rc_title`,`rc_timestamp` FROM `recentchanges` WHERE `rc_namespace`=0 AND `rc_timestamp`>'{$last_ts}' ORDER BY `rc_timestamp`" ;
		$sql .= " LIMIT {$this->matches_via_properties_limit}" ;
		$result = $this->mnm->tfc->getSQL ( $dbwd , $sql ) ;
		$items_edited = [] ;
		while($o = $result->fetch_object()) {
			$items_edited[$o->rc_title] = 1 ;
			if ( $last_ts < $o->rc_timestamp ) $last_ts = $o->rc_timestamp ;
		}
		$items_edited = array_keys ( $items_edited ) ;
		$this->log ( count($items_edited) . " items to check" , 2 ) ;

		$this->mnm->wil->loadItems ( $items_edited ) ;
		foreach ( $items_edited AS $q ) {
			$i = $this->mnm->wil->getItem ( $q ) ;
			if ( !isset($i) ) continue ; # TODO item doesn't exist - purge from MnM matches?
			$qnum = preg_replace ( '|\D|' , '' , $q ) * 1 ;
			if ( $qnum <= 0 ) continue ; # Paranoia

			# Checking properties
			$entries_with_match = [] ;
			if ( !isset($i->j) or !isset($i->j->claims) ) continue ;
			foreach ( $i->j->claims AS $prop => $claims ) {

				# Store external ID mapping; for checkAuxiliaryMatches
				$prop_values = $i->getStrings ( $prop ) ;
				if ( $this->isPropertyForAuxMatch($prop) ) {
					foreach ( $prop_values AS $value ) {
						$this->prop_value_q[$prop][$value][] = $q ;
					}
				}

				# Birth/death years; for matchPersonDates
				if ( ( $prop == 'P569' or $prop == 'P570' ) and $i->hasTarget('P31','Q5') ) {
					$claims = $i->getClaims($prop) ;
					foreach ( $claims AS $c ) {
						if ( !isset($c->mainsnak) ) continue ;
						if ( !isset($c->mainsnak->datatype) ) continue ;
						if ( !isset($c->mainsnak->datavalue) ) continue ;
						if ( $c->mainsnak->datatype != 'time' ) continue ;
						if ( $c->mainsnak->datavalue->type != 'time' ) continue ;
						if ( $c->mainsnak->datavalue->value->calendarmodel != 'http://www.wikidata.org/entity/Q1985727' ) continue ;
						if ( $c->mainsnak->datavalue->value->precision < 9 ) continue ;
						if ( !preg_match ( '|^\+(\d+)|' , $c->mainsnak->datavalue->value->time , $m ) ) continue ;
						$year = $m[1] ;
						$this->person_dates[$q][$prop][] = $year ;
					}
				}

				if ( !isset($this->prop2catalogs[$prop]) ) continue ;
				$catalogs = implode(',',$this->prop2catalogs[$prop]) ;

				# Check properties
				foreach ( $prop_values AS $value ) {
					$sql = "SELECT * FROM entry WHERE catalog IN ({$catalogs}) AND ext_id='".$this->mnm->escape($value)."'" ;
					$result = $this->mnm->getSQL ( $sql ) ;
					while($o = $result->fetch_object()) {
						if ( !isset($o->q) or !isset($o->user) or $o->user == NULL or $o->user == 0 ) {
							# Check if this is the only item with that property/value
							$search_result_items = $this->mnm->getCachedWikidataSearch ( '' , $prop , $value ) ;
							if ( count($search_result_items) > 1 ) {
								$this->mnm->addIssue ( $o->id , 'WD_DUPLICATE' , $search_result_items ) ;
								continue ;
							}

							# Set new match
							$this->log ( $this->mnm->getEntryURL($o->id) . " changed to https://www.wikidata.org/wiki/Q{$qnum} via {$prop}" , 2 ) ;
							if ( $this->mnm->setMatchForEntryID ( $o->id , $q , 4 , false , false ) ) $this->mnm->setMatchStatus ( $o->id , 'SAME' ) ;
							$entries_with_match["{$o->id}"] = 1 ;
						} else if ( $o->q == $qnum ) {
							if ( $this->mnm->getMatchStatus($o->id) != 'SAME' ) $this->mnm->setMatchStatus ( $o->id , 'SAME' ) ;
							$entries_with_match["{$o->id}"] = 1 ;
						} else {
							$this->log ( $this->mnm->getEntryURL($o->id) . " has Q{$o->q} but MnM says https://www.wikidata.org/wiki/Q{$qnum}" , 2 ) ;
							$this->mnm->setMatchStatus ( $o->id , 'DIFFERENT' ) ;
							$this->mnm->addIssue ( $o->id , 'MISMATCH' , ["Q{$o->q}",$q] ) ;
							$entries_with_match["{$o->id}"] = 0 ; # Different match, but seen it
						}
					}
				}
			}

			# Checking matches
			$sql = "SELECT * FROM `entry` WHERE `q`={$qnum} AND `user`>0" ;
			$result = $this->mnm->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) {
				if ( !isset($this->all_catalogs_with_props[$o->catalog]) ) continue ;
				if ( isset($entries_with_match[$o->id]) ) continue ;
				if ( $this->mnm->getMatchStatus($o->id) != 'WD_MISSING' ) $this->mnm->setMatchStatus ( $o->id , 'WD_MISSING' ) ;
			}

			# Check dates, cache labels/aliases; for matchPersonDates
			if ( count($this->person_dates[$q]??[]) == 2 ) {
				$names = [] ;
				foreach ( ($i->j->labels??[]) AS $lang => $v ) $names[$v->value] = 1 ;
				foreach ( ($i->j->aliases??[]) AS $lang => $texts ) {
					foreach ( $texts AS $v ) $names[$v->value] = 1 ;
				}
				if ( count($names) > 0 ) $this->q2names[$q] = array_keys($names) ;
			} else {
				unset ( $this->person_dates[$q] ) ;
			}
		}

		$this->mnm->set_kv_value ( 'recent_changes_watcher:last_time' , $last_ts ) ;
	}

	public function updateRedirectedItems() {
		$last_ts = $this->mnm->get_kv_value ( 'update_redirects:last_time' , '' ) ;

		# Get redirected items
		$redirected_items = [] ; # page_id => Qxxx
		$sql = "SELECT page.* FROM page,revision WHERE rev_page=page_id AND page_is_redirect=1 AND page_namespace=0 AND rev_timestamp>='{$last_ts}'" ;
		$sql .= " ORDER BY rev_timestamp" ;
		$sql .= " LIMIT {$this->redirect_limit}" ;
		$dbwd = $this->mnm->openWikidataDB(true) ;
		$result = $this->mnm->tfc->getSQL ( $dbwd , $sql ) ;
		while($o = $result->fetch_object()) {
			if ( $last_ts < $o->page_touched ) $last_ts = $o->page_touched ;
			$q = $o->page_title ;
			if ( !preg_match ( '|^Q(\d+)|' , $q , $m ) ) continue ; # Paranoia
			$q_num = $m[1] * 1 ;
			if ( $q_num <= 0 ) continue ; # Paranoia
			$redirected_items[$o->page_id] = $q ;
		}
		$this->mnm->set_kv_value ( 'update_redirects:last_time' , $last_ts ) ; # Updating DB
		$this->log ( count($redirected_items) . " redirects to check" , 2 ) ;
		if ( count($redirected_items) == 0 ) return ; # No redirects
		
		# Get redirect targets
		$q2q = $this->mnm->getRedirectTargets($redirected_items);
		if ( count($q2q) == 0 ) return ; # Nothing to update

		# Update redirects
		$redirects_updated = 0 ;
		foreach ( $q2q AS $source_q => $target_q ) {
			$source_q = preg_replace ( '|\D|' , '' , $source_q ) * 1 ;
			$target_q = preg_replace ( '|\D|' , '' , $target_q ) * 1 ;
			if ( $source_q * $target_q == 0 ) continue ; # Paranoia
			$sql = "UPDATE `entry` SET `q`={$target_q} WHERE `q`={$source_q}" ;
			$this->mnm->getSQL ( $sql ) ;
			$redirects_updated += $this->mnm->dbm->affected_rows ;
		}

		if ( $redirects_updated > 0 ) $this->log ( "{$redirects_updated} redirects updated" ) ;
	}

	public function unmatchDeletedItems () {
		$last_ts = $this->mnm->get_kv_value ( 'unmatch_deleted_items:last_time' , '' ) ;

		$dbwd = $this->mnm->openWikidataDB(true) ;
		$sql = "SELECT * FROM logging WHERE log_action='delete' AND log_namespace=0 AND log_timestamp>='{$last_ts}'" ;
		$sql .= " ORDER BY log_timestamp" ;
		$sql .= " LIMIT {$this->deleted_items_limit}" ;
		$result = $this->mnm->tfc->getSQL ( $dbwd , $sql ) ;
		while($o = $result->fetch_object()) {
			$this->log ( "unmatchDeletedItems:" . json_encode($o) , 2 ) ;
			$q = $o->log_title ;
			if ( !preg_match ( '|^Q(\d+)|' , $q , $m ) ) continue ; # Paranoia
			$q_num = $m[1] * 1 ;
			if ( $q_num <= 0 ) continue ; # Paranoia
			$sql = "SELECT * FROM page WHERE page_title='{$q}' AND page_namespace=0" ;
			$exists = false ;
			$is_redirect = false ;
			$redirect_target = '' ;
			$result2 = $this->mnm->tfc->getSQL ( $dbwd , $sql ) ;
			while ( $o2 = $result2->fetch_object() ) {
				$exists = true ;
				if ( $o2->page_is_redirect ) {
					$is_redirect = true ;
					$sql = "SELECT * FROM `redirect` WHERE rd_from={$o2->page_id} AND rd_namespace=0" ;
					$result2 = $this->mnm->tfc->getSQL ( $dbwd , $sql ) ;
					if ( $o2 = $result2->fetch_object() ) $redirect_target = $o2->rd_title ;
				}
			}

			if ( !$exists ) {
				$this->log ( "Removing matches to {$q}" , 2 ) ;
				$this->mnm->removeMatchesToItem ( $q , 4 ) ;
			} else if ( $is_redirect ) {
				$this->log ( "{$q} / {$q_num} : REDIRECTED => {$redirect_target}" , 2 ) ;
				if ( preg_match ( '|^Q(\d+)|' , $redirect_target , $m ) ) {
					# We have a redirect target
					$qr = $m[1] * 1 ;
					if ( $qr <= 0 ) continue ;
					$sql = "UPDATE `entry` SET `q`={$qr} WHERE `q`={$q_num}" ;
					$this->mnm->getSQL ( $sql ) ;
				} else {
					# We have no redirect target
					$this->log ( "{$q} : REDIRECTED BUT NO REDIRECT" ) ;
				}
			}

			if ( $last_ts = $o->log_timestamp ) $last_ts = $o->log_timestamp ;
		}

		$this->mnm->set_kv_value ( 'unmatch_deleted_items:last_time' , $last_ts ) ;
	}

	public function checkAuxiliaryMatches () {
		# DEPENDS ON RUNNING checkMatchesViaProperties first!
		$this->aux->initializePropertyLists() ;
		foreach ( $this->prop_value_q AS $prop => $value2qs ) {
			if ( !$this->isPropertyForAuxMatch($prop) ) continue ; # Paranoia
			$prop_num = preg_replace ( '|\D|' , '' , $prop ) * 1 ;
			if ( $prop_num <= 0 ) continue ; # Paranoia
			$values = [] ;
			foreach ( $value2qs AS $value => $qs ) {
				if ( count($qs) != 1 ) continue ; # Paranoia
				$values[] = $this->mnm->escape($value) ;
			}
			if ( count($values) == 0 ) continue ;
			$sql = "SELECT * FROM `vw_aux` WHERE `aux_p`={$prop_num} AND `aux_name` IN ('" . implode("','",$values) . "')" ;
			$sql .= " AND in_wikidata=0" ; # OK?
			$result = $this->mnm->getSQL ( $sql ) ;
			while ( $o = $result->fetch_object() ) {
				$value = $o->aux_name ;
				if ( !isset($value2qs->$value) ) continue ; # Paranoia
				$q = $value2qs->$value[0] ;
				if ( !isset($q) ) continue ; # Paranoia
				if ( isset($o->user) and $o->user>0 and $q == "Q{$o->q}" ) continue ;

				if ( isset($o->user) and $o->user>0 and $q != "Q{$o->q}" ) {
					$this->log ( $this->mnm->getEntryURL($o->id) . " has Q{$o->q} but {$prop}:'{$value}' says {$q}" , 2 ) ;
					$this->mnm->addIssue ( $o->id , 'MISMATCH' , ["Q{$o->q}",$q] ) ;
					continue ;
				}

				# Make sure it's the only item with that property/value on Wikidata
				$search_result_items = $this->mnm->getCachedWikidataSearch ( '' , $prop , $value ) ;
				if ( count($search_result_items) > 1 ) continue ;

				# Match entry to Wikidata
				$this->mnm->setMatchForEntryID ( $o->id , $q , 4 , false , false ) ;

				# Mark auxiliary as "in wikidata"
				$sql = "UPDATE `auxiliary` SET `in_wikidata`=1 WHERE id={$o->aux_id} AND in_wikidata=0" ;
				$this->mnm->getSQL ( $sql ) ;

				$this->log ( $this->mnm->getEntryURL($o->id) ." : {$prop}:'{$value}' => {$q} : " . json_encode($o) , 2 ) ;
			}
		}
		$this->prop_value_q = [] ; # Save memory
	}

	private function initializeCatlogsAndProperties() {
		# Get properties to watch for from MnM
		$this->prop2catalogs = [] ;
		$this->all_catalogs_with_props = [] ;
		$sql = "SELECT `id`,`wd_prop` FROM `catalog` WHERE `wd_prop` IS NOT NULL AND `wd_qual` IS NULL AND `active`=1" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			if ( $this->isCatalogSuspended($o->id) ) continue ;
			$prop = "P{$o->wd_prop}" ;
			if ( in_array ( $prop , $this->property_blacklist ) ) continue ;
			$this->prop2catalogs[$prop][] = $o->id ;
			$this->all_catalogs_with_props["{$o->id}"] = 1 ;
		}
	}

	private function isCatalogSuspended ( $catalog_id ) {
		return in_array ( $catalog_id , $this->suspended_catalogs ) ;
	}

	private function compareNames ( $name , $names ) {
		if ( in_array ( $name , $names ) ) return true ;
		$name = $this->mnm->getSimplifiedName ( $this->mnm->sanitizePersonName ( $name ) ) ;
		if ( in_array ( $name , $names ) ) return true ;
		return false ;
	}

	public function matchPersonDates() {
		# DEPENDS ON RUNNING checkMatchesViaProperties first!
		# All the Qs in person_dates have P31:Q5 
		$matched_entries = 0 ;
		foreach ( $this->person_dates AS $original_q => $prop2year ) {
			if ( !isset($this->q2names[$original_q]) ) continue ;
			if ( !isset($prop2year['P569']) or !isset($prop2year['P570']) ) continue ;
			$birth_years = array_unique ( $prop2year['P569'] ) ;
			$death_years = array_unique ( $prop2year['P570'] ) ;
			#if ( count($birth_years) != 1 or count($death_years) != 1 ) continue ;
			$by = implode ( "','" , $birth_years ) ;
			$dy = implode ( "','" , $death_years ) ;
			$sql = "SELECT * FROM vw_dates WHERE (q IS NULL OR user=0) AND year_born IN ('{$by}') AND year_died IN ('{$dy}')" ;
			$result = $this->mnm->getSQL ( $sql ) ;
			$entry2name = [] ;
			while ( $o = $result->fetch_object() ) {
				if ( $this->isCatalogSuspended($o->catalog) ) continue ;
				$entry2name[$o->entry_id] = $o->ext_name ;
			}

			# Search for names
			$found = [] ;
			foreach ( $entry2name AS $entry_id => $name ) {
				if ( $this->compareNames ( $name , $this->q2names[$original_q] ) ) $found[] = $entry_id ;
			}
			if ( count($found) == 0 ) continue ;
			$found = array_unique($found) ;
			$this->log ( "matchPersonDates: found {$original_q}" , 2 ) ;
			foreach ( $found AS $entry_id ) {
				$this->log ( "- as ".$this->mnm->getEntryURL($entry_id) , 2 ) ;
				$this->mnm->setMatchForEntryID ( $entry_id , $original_q , 3 , false , false ) ;
				$matched_entries++ ;
			}
		}
		$this->person_dates = [] ; # Save RAM
		if ( $matched_entries > 0 ) $this->log ( "{$matched_entries} entries matched via dates" , 1 ) ;
	}

	public function syncWdMatches () {
		$m = new Maintenance ( $this->mnm ) ;
		$m->fixupWdMatches() ;

		$this->syncWdMatchesByStatus ( 'UNKNOWN' ) ;
		$this->syncWdMatchesByStatus ( 'DIFFERENT' ) ;
	}

	# Returns array catalog_id => "Pxxx"
	private function getCatalog2prop ( $catalogs ) {
		$catalog2prop = [] ;
		$sql = "SELECT * FROM `catalog` WHERE `id` IN (" . implode(',',array_keys($catalogs)) . ") AND active=1 AND wd_prop IS NOT NULL AND wd_qual IS NULL" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			if ( $this->isCatalogSuspended($o->id) ) continue ;
			$catalog2prop[$o->id] = "P{$o->wd_prop}" ;
		}
		return $catalog2prop ;
	}

	private function getCatalogsFromEntries ( $entries ) {
		$catalogs = [] ;
		foreach ( $entries AS $o ) {
			if ( $this->isCatalogSuspended($o->catalog) ) continue ;
			$catalogs[$o->catalog] = $o->catalog ;
		}
		return $catalogs ;
	}

	private function getItemsFromEntries ( $entries , $mark_unmatched_as_unknown = false ) {
		$items = [] ;
		foreach ( $entries AS $o ) {
			if ( $this->isCatalogSuspended($o->catalog) ) continue ;
			if ( isset($o->q) and isset($o->user) and $o->user!=0 ) $items[] = "Q{$o->q}" ;
			else if ( $mark_unmatched_as_unknown ) $this->mnm->setMatchStatus ( $entry->id , 'UNKNOWN' ) ;
		}
		return $items ;
	}

	private function entryFilterGenerator ( $entries , $mark_unmatched_as_unknown = false ) {
		$items = $this->getItemsFromEntries ( $entries , $mark_unmatched_as_unknown ) ;
		if ( count($items) == 0 ) return ;
		$this->mnm->wil->loadItems ( $items ) ;
		unset ( $items ) ;
		$catalogs = $this->getCatalogsFromEntries ( $entries ) ;
		$catalog2prop = $this->getCatalog2prop ( $catalogs ) ;

		foreach ( $entries AS $entry ) {
			if ( !isset($catalog2prop[$entry->catalog]) ) {
				$this->mnm->setMatchStatus ( $entry->id , 'N/A' ) ;
				continue ;
			}
			if ( !isset($entry->q) or !isset($entry->user) or $entry->user==0 ) {
				$this->mnm->setMatchStatus ( $entry->id , 'N/A' ) ;
				continue ;
			}
			$prop = $catalog2prop[$entry->catalog] ;
			$q = "Q{$entry->q}" ;
			$i = $this->mnm->wil->getItem ( $q ) ;
			if ( !isset($i) ) {
				$this->mnm->setMatchStatus ( $entry->id , 'N/A' ) ;
				continue ;
			}
			$value = $entry->ext_id ;
			$values = $i->getStrings ( $prop ) ;
			if ( count($values) > 0 and $values != [$value] and in_array ( $prop , $this->respect_single_value_for_property ) ) {
				$this->mnm->setMatchStatus ( $entry->id , 'MULTIPLE' ) ;
				$j = [ 'wd' => $values , 'mnm' => $value ] ;
				$this->mnm->addIssue ( $entry->id , 'MULTIPLE' ,  $j ) ;
				continue ;
			}
			yield [ $entry , $value , $values , $prop , $q ] ;
		}
	}

	private function syncWdMatchesByStatus ( $status ) {
		$entries = $this->mnm->getEntriesWithWdMatches ( [$status] , $this->sync_wd_matches_limit ) ;
		foreach ( $this->entryFilterGenerator($entries,false) AS $x ) {
			list($entry,$value,$values,$prop,$q) = $x ;
			if ( in_array ( $entry->ext_id , $values ) ) $this->mnm->setMatchStatus ( $entry->id , 'SAME' ) ;
			else if ( count($values) > 0 ) $this->mnm->setMatchStatus ( $entry->id , 'DIFFERENT' ) ;
			else $this->mnm->setMatchStatus ( $entry->id , 'WD_MISSING' ) ;
		}
	}

	public function syncMatchesToWikidata () {
		$commands = [] ;
		$entries = $this->mnm->getEntriesWithWdMatches ( ['WD_MISSING'] , $this->sync_matches_to_wd_limit , true ) ;
		foreach ( $this->entryFilterGenerator($entries,true) AS $x ) {
			list($entry,$value,$values,$prop,$q) = $x ;
			if ( in_array ( $value , $values ) ) $this->mnm->setMatchStatus ( $entry->id , 'SAME' ) ;
			else {
				if ( $this->mnm->hasPropertyEverBeenRemovedFromItem ( $q , $prop , $value ) ) {
					$this->mnm->setMatchStatus ( $entry->id , 'PARANOIA' ) ;
					$this->mnm->check404 ( $entry->catalog , $entry->id , $entry->ext_url ) ;
				} else {
					$commands[] = "{$q}\t{$prop}\t\"{$value}\"" . $this->mnm->getQsCommentForEntry($entry->id);
				}
			}
		}
		$this->runCommandsQS ( $commands ) ;
	}

	private function runCommandsQS ( $commands ) {
		if ( count($commands) == 0 ) return ; // Nothing to do
		$qs = $this->mnm->tfc->getQS($this->qs_tool_name,'/data/project/mix-n-match/bot.ini') ;
		if ( $this->testing ) $qs->debugging = true ;
		$this->mnm->tfc->runCommandsQS ( $commands , $qs ) ;
	}

	# Batch-add entries to `wd_matches`
	public function addUnknownBatch () {
		$sql = 'INSERT IGNORE INTO wd_matches (entry_id,status,catalog,timestamp) SELECT id,"UNKNOWN",catalog,timestamp FROM entry WHERE user>0 AND q IS NOT NULL AND catalog IN (SELECT id FROM catalog WHERE active=1 AND wd_prop IS NOT NULL AND wd_qual IS NULL) AND NOT EXISTS (SELECT * FROM wd_matches WHERE entry_id=entry.id) LIMIT 2000' ;
		$this->mnm->getSQL ( $sql ) ;
	}

} ;

?>