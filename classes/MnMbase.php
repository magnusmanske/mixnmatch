<?PHP

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class MnMbase {
	protected $id ; # ID (int)
	protected $mnm ;


	public function setMnM ( $mnm ) {
		$this->mnm = $mnm ;
	}

	public function id() {
		return $this->id ;
	}

	protected function checkMnM() {
		if ( !isset($this->mnm) ) throw new \Exception(__CLASS__." class: ID {$this->id} : No MnM" ) ;
	}

	protected function checkValidID() {
		if ( !isset($this->id) ) throw new \Exception(__CLASS__." class: No ID set" ) ;
		if ( $this->id<=0 ) throw new \Exception(__CLASS__." class: ID '{$this->id}' is not valid for database requests" ) ;
	}

	protected function getSQL ( $sql ) {
		$this->checkMnM() ;
		return $this->mnm->getSQL ( $sql ) ;
	}

	protected function escape ( $s ) {
		$this->checkMnM() ;
		return $this->mnm->escape ( $s ) ;
	}

}