<?PHP

namespace MixNMatch ;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class ImportJSON {
	protected $mnm ;
	protected $catalog_id ;
	protected $j ;
	protected $new_entries ;

	function __construct ( $mnm = '' ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
	}

	public function process_file ( $filename ) {
		$this->j = json_decode ( file_get_contents ( $filename ) ) ;
		if ( $this->j === null ) throw new \Exception('Invalid JSON') ;
		$this->catalog_id = $this->j->catalog * 1 ;
		$this->new_entries = 0 ;

		$this->entries_not_in_master() ;
		$this->import_or_update() ;

		$catalog = new Catalog ( $this->catalog_id , $this->mnm ) ;
		$catalog->updateStatistics() ;
		$this->mnm->queue_job($this->catalog_id,'automatch_by_search');
		$this->mnm->queue_job($this->catalog_id,'automatch_from_other_catalogs');
	}

	protected function entries_not_in_master () {
		$entries = [] ;
		$ids = [] ;
		foreach ( $this->j->entries AS $e ) $ids[] = "'" . $this->mnm->escape ( $e->id ) . "'" ;
		if ( count($ids) == 0 ) return ;
		$id_chunks = array_chunk ( $ids , 2000 ) ;
		foreach ( $id_chunks AS $ids ) {
			$sql = "SELECT * FROM entry WHERE catalog=$this->catalog_id AND ext_id NOT IN (" . implode(',',$ids) . ")" ;
			$result = $this->mnm->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) $entries[$o->ext_id] = $o ;
		}
		if ( count($entries) == 0 ) return ;
		print count($entries) . " entries not in current master\n" ;
	}

	protected function get_or_create_entry ( $e ) {
		$entry = new Entry ( 0 , $this->mnm ) ;
		try {
			$entry->setFromExternalID ( $this->catalog_id , $e->id ) ;
			return $entry->core_data() ;
		} catch (\Exception $ex) {
			# Ignore, create new entry
		} 
		foreach ( ['url','name','desc','type'] AS $k ) {
			if ( !isset($e->$k) ) $e->$k = '' ;
		}
		if ( $e->type == 'person' ) $e->type = 'Q5' ;
		$e->type = strtoupper($e->type) ;
		$this->new_entries++ ;
		$e->catalog = $this->catalog_id ;
		$entry_id = $this->mnm->addNewEntry ( $e ) ;
		if ( isset($entry_id) ) {
			try {
				$entry = new Entry ( $entry_id , $this->mnm ) ;
				return $entry->core_data() ;
			} catch (\Exception $ex) {
				# Ignore, return unset
			}
		}
	}

	protected function set_q_auto ( &$o , $q ) {
		$o->q = $q ;
		$this->mnm->setMatchForEntryID ( $o->id , $q , 4 , true ) ;
	}

	protected function set_q_from_wiki_page ( $o , $a ) {
		// Resolve redirect, if any
		$title = urldecode ( str_replace('_',' ',$a->id) ) ;
		if ( preg_match('/[\#\:]/',$title) ) return ;
		$db2 = $this->mnm->tfc->openDBwiki ( $a->catalog , true ) ;
		$sql = "SELECT lt_title FROM page,pagelinks,linktarget WHERE pl_target_id=lt_id AND pl_from=page_id AND page_namespace=0 AND page_is_redirect=1 AND lt_namespace=0 AND page_title='" . $db2->real_escape_string ( str_replace(' ','_',$title) ) . "' LIMIT 1" ;
		$db2 = $this->mnm->tfc->openDBwiki ( $a->catalog , true ) ; // Yes, Tool Labs DB replicas might have timed out by now...
		$result = $this->mnm->tfc->getSQL ( $db2 , $sql ) ;
		while($o2 = $result->fetch_object()) $title = urldecode(str_replace('_',' ',$o2->lt_title)) ;
		$db2->close() ;
		if ( preg_match('/[\#\:]/',$title) ) return ;

		// Find Q
		$dbwd = $this->mnm->tfc->openDB ( 'wikidata' , 'wikidata' , true ) ;
		$sql = "SELECT * FROM wb_items_per_site WHERE ips_site_id='" . $dbwd->real_escape_string ( $a->catalog ) . "' AND ips_site_page='" . $this->mnm->escape ( $title ) . "'" ;
		$result = $this->mnm->tfc->getSQL ( $dbwd , $sql ) ;
		$q = '' ;
		while($o2 = $result->fetch_object()) $q = $o2->ips_item_id ;
		if ( $q == '' ) { // Not found
			print "Could not find Q number for " . $a->catalog . ":" . $title . "\n" ;
			return ;
		}

		// Update match
		$this->set_q_auto ( $o , $q ) ;
	}

	protected function set_aux_prop ( $o , $a , $aux ) {
		// Check if aux already exists
		$prop = preg_replace('/\D/','',$a->prop) ;
		$val = $a->id ;
		if ( trim($val) == '' ) return ;
		
		// Format hacks
		if ( $prop == '1415' ) {
			while ( strlen($val) < 6 ) $val = "0$val" ;
			if ( strlen($val) == 6 ) $val = "101$val" ;
		}
		
		$exists = false ;
		foreach ( $aux AS $a2 ) {
			if ( $a2->aux_p != $prop ) continue ;
			if ( $a2->aux_name != $val ) continue ;
			$exists = true ;
			break ;
		}
		if ( $exists ) return ;
		
		// Create new aux
		$n = (object) [] ;
		$n->aux_p = $prop ;
		$n->aux_name = $val ;
		$aux[] = $n ;
		$this->mnm->setAux ( $o->id , $prop , $val ) ;
	}

	protected function update_loc ( $e , $o ) {
		if ( !isset($e->loc) ) return ;
		if ( !isset($e->loc->lat) or trim($e->loc->lat) == '' ) return ;
		if ( !isset($e->loc->lon) or trim($e->loc->lon) == '' ) return ;
		$this->mnm->setLocation ( $o->id , $e->loc->lat , $e->loc->lon ) ;
	}

	protected function update_aux ( $e , $o ) { // This only adds aux data, does not remove it!
		if ( !isset($e->aux) ) return ;
		if ( count($e->aux) == 0 ) return ;
		
		// Load existing auxiliary data
		$aux = [] ;
		$sql = "SELECT * FROM auxiliary WHERE entry_id=" . $o->id ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o2 = $result->fetch_object()) $aux[] = $o2 ;
		
		foreach ( $e->aux AS $a ) {
			if ( isset ( $a->catalog ) ) {
			
				// Set q from Wikipedia
				if ( preg_match ( '/^.+wiki/' , $a->catalog ) and $o->q == '' ) {
					$this->set_q_from_wiki_page ( $o , $a ) ;
				}
				
			} else if ( isset ( $a->prop ) ) {
				$this->set_aux_prop ( $o , $a , $aux ) ;
			} else if ( isset ( $a->q ) ) { // Set known q; q must be a number, no leading "Q"
				$this->set_q_auto ( $o , $a->q ) ;
			} else {
				print "There's something odd about aux data:\n" ;
				print_r ( $e ) ; print "\n" ;
				print_r ( $a ) ; print "\n" ;
			}
		}
	}

	protected function import_or_update () {
		$cnt = 0 ;
		foreach ( $this->j->entries AS $e ) {
			if ( strlen ( $e->id ) > 90 ) continue ;
			if ( $e->name == 'Page Not Found' ) continue ;
			$cnt++ ;
			$o = $this->get_or_create_entry ( $e ) ;
			if ( !isset($o) ) {
				print "Tried to create " . $e->id . " but couldn't.\n" ;
				continue ;
			}
			// TODO check values
			if ( !isset($o->q) ) $this->update_aux ( $e , $o ) ;
			if ( isset($e->loc) ) $this->update_loc ( $e , $o ) ;
		}
		print "Processed $cnt entries. Added $this->new_entries new entries.\n" ;
	}
}

?>