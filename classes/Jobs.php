<?PHP

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';


class Jobs {
	public $mnm ;
	public $valid_job_types = [] ;
	public $job_types = [
		'all' => [
			'automatch', # CANDIDATE FOR RUST
			'match_by_coordinates', # CANDIDATE FOR RUST, low numbers

			'import_aux_from_url' , # User PHP code
			'update_descriptions_from_url' , # User PHP code
#			'bespoke_scraper' , # User PHP code
#			'generate_aux_from_description' , # User PHP code, run from rust
#			'update_person_dates', # User PHP code, run from rust
#			'fix_disambig', # Rust
#			'fix_redirected_items_in_catalog', # Rust
#			'microsync', # Rust
#			'autoscrape', # Rust
#			'auxiliary_matcher', # Rust
#			'aux2wd', # Rust
#			'automatch_by_sitelink', # Rust
#			'match_person_entries_by_name_and_dates', # Rust
#			'purge_automatches', # Rust
#			'match_person_dates', # Rust
#			'match_on_birthdate', # Rust
#			'automatch_from_other_catalogs', # Rust
		]
	] ;
	protected $script_dir = '/data/project/mix-n-match/scripts' ;
	protected $job_id_running = '' ;

	function __construct( $mnm = '' ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
	}

	# Resets running jobs that the script is responsible for to TODO status
	public function reset_running_jobs() {
		if ( count($this->valid_job_types)==0 ) return ;
		$sql = "UPDATE `jobs` SET `status`='TODO',`note`=NULL WHERE `status` IN ('RUNNING','FAILED') AND `status` IN ('".implode("','",$this->valid_job_types)."')" ;
			$this->mnm->getSQL ( $sql ) ;
	}

	protected function get_all_allowed_jobs() {
		$ret = [] ;
		foreach ( $this->job_types AS $k => $v ) $ret = array_merge($ret,$v) ;
		return $ret ;
	}

	public function run_next_inline_job() {
		$job = $this->get_next_job();
		if ( !isset($job) ) {
			sleep(5);
			return;
		}
		try {
			$this->mnm->set_job_status($job,'RUNNING');
			$this->run_job_inline($job);
			$this->mnm->closeWikidataDatabase() ;
			$this->mnm->dbmConnect(true);
			$this->mnm->set_job_status($job,'DONE');
		} catch(Exception $e) {
			$this->mnm->closeWikidataDatabase() ;
			$this->mnm->dbmConnect(true);
			$this->mnm->set_job_status($job,'FAILED',$e->getMessage());
			print "FAILED {$job->action} on catalog {$job->catalog}:\n".$e->getMessage()."\n\n" ;
			return ;
		} 
	}

	public function get_running_job_id() {
		return $this->job_id_running ;
	}

	public function reactivateOldJob ( $action ) {
		# Find oldest job for that action
		$sql = "SELECT * FROM `jobs` WHERE `action`='{$action}' AND `status`='DONE' ORDER BY `last_ts` LIMIT 1" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		if($o = $result->fetch_object()) $this->mnm->queue_job ( $o->catalog , $action ) ;
	}

	public function run_job_inline ( $job ) {
		$catalog_id = $job->catalog ;
		if ( !isset($catalog_id) or $catalog_id<=0 ) throw new \Exception("Job #{$job->id}: {$job->action} for bad catalog {$catalog_id}");
		if ( !in_array($job->action, $this->valid_job_types) ) throw new \Exception("Job #{$job->id}: {$job->action} is not valid");
		print "Attempting {$job->action} on catalog {$catalog_id}\n" ;
		if ( $job->action=='import_aux_from_url' ) {
			$uta = new UrlToAux ( $this->mnm ) ;
			$uta->run ( $catalog_id ) ;
		} else if ( $job->action=='autoscrape' ) {
			$autoscrape = new AutoScrape ( $this->mnm ) ;
			if ( !$autoscrape->loadByCatalog($catalog_id) ) throw new \Exception ( $autoscrape->error."\n" ) ;
			$autoscrape->max_urls_requested = 1 ;
			$autoscrape->scrapeAll() ;
		} else if ( $job->action=='automatch_by_search' ) {
			$am = new AutoMatch ;
			$am->automatch_by_search ( $catalog_id ) ;
		} else if ( $job->action=='generate_aux_from_description' ) {
			$d2a = new DescriptionToAux ( $catalog_id , $this->mnm ) ;
			if ( $d2a->hasCodeFragment() ) {
				$d2a->loadCodeFragment() ;
				$sql = "SELECT * FROM entry WHERE catalog={$catalog_id}" ;
				$result = $mnm->getSQL ( $sql ) ;
				while($o = $result->fetch_object()){
					$commands = $d2a->processEntry ( $o ) ;
					$d2a->enact ( $commands ) ;
				}
				$d2a->touchCodeFragment();
				$this->mnm->queue_job($catalog_id,'auxiliary_matcher');
			}
		} else if ( $job->action=='update_from_tabbed_file' ) {
			$uc = new UpdateCatalog ( $catalog_id , $this->mnm ) ;
			$uc->update_catalog() ;
			$uc->run_updates();
		} else if ( $job->action=='taxon_matcher' ) {
			$tm = new TaxonMatcher ( $catalog_id , $this->mnm ) ;
			$tm->run();
		} else if ( $job->action=='update_descriptions_from_url' ) {
			$h2d = new HTMLtoDescription ( $catalog_id , $this->mnm ) ;
			if ( $h2d->hasCodeFragment() ) {
				$h2d->loadCodeFragment() ;
				$sql = $h2d->getMainSQL() ;
				$result = $this->mnm->getSQL ( $sql ) ;
				while($o = $result->fetch_object()){
					$commands = $h2d->processEntry ( $o ) ;
					$h2d->enact ( $commands ) ;
				}
				$job_id = $this->mnm->queue_job($catalog_id,'update_person_dates');
				$this->mnm->queue_job($catalog_id,'match_person_dates',$job_id);
				$job_id = $this->mnm->queue_job($catalog_id,'generate_aux_from_description');
				$this->mnm->queue_job($catalog_id	,'auxiliary_matcher',$job_id);
			}
		} else if ( $job->action=='bespoke_scraper' ) {
			$scraper = new BespokeScraper ( $catalog_id , $this->mnm ) ;
			if ( $scraper->hasCodeFragment() ) {
				$scraper->loadCodeFragment() ;
				$entries_were_changed = $scraper->processCatalog() ;
				if ( $entries_were_changed ) {
					$catalog = new Catalog ( $catalog_id , $this->mnm ) ;
					$catalog->updateStatistics() ;
					$job_id = $this->mnm->queue_job($catalog_id,'auxiliary_matcher');
					$this->mnm->queue_job($catalog_id,'automatch_by_search',$job_id);
					$job_id = $this->mnm->queue_job($catalog_id,'update_person_dates');
					$this->mnm->queue_job($catalog_id,'match_person_dates',$job_id);
				}
			}
		} else if ( $job->action=='microsync' ) {
			$ms = new MicroSync($this->mnm) ;
			$catalogs_with_property = $ms->init_catalogs() ;
			if ( !isset($catalogs_with_property[$catalog_id]) ) return ; # No property, no sync
			$ms->checkCatalog ( $catalog_id , $catalogs_with_property[$catalog_id] ) ;
		} else if ( $job->action=='automatch' ) {
			$am = new AutoMatch($this->mnm) ;
			$am->verbose = false ;
			$am->automatch_simple ( $catalog_id ) ;
		} else if ( $job->action=='automatch_by_sitelink' ) {
			$am = new AutoMatch($this->mnm) ;
			$am->automatch_by_sitelink($catalog_id);
		} else if ( $job->action=='automatch_from_other_catalogs' ) {
			$am = new AutoMatch($this->mnm) ;
			$am->automatch_from_other_catalogs($catalog_id);
		} else if ( $job->action=='fix_disambig' ) {
			$maintenance = new Maintenance($this->mnm) ;
			$maintenance->unlinkDisambiguationItems($catalog_id);
		} else if ( $job->action=='fix_redirected_items_in_catalog' ) {
			$maintenance = new Maintenance($this->mnm) ;
			foreach ( $maintenance->getFullyMatchedEntryBatches($catalog_id) AS $items ) {
				$maintenance->fixRedirectedItems($items);
				$maintenance->unlinkDeletedItems($items);
			}
		} else if ( $job->action=='purge_automatches' ) {
			$maintenance = new Maintenance($this->mnm) ;
			$maintenance->purgeAutomatchesFromCatalog($catalog_id);
		} else if ( $job->action=='match_by_coordinates' ) {
			$cm = new CoordinateMatcher ( $catalog_id , 0 , $this->mnm ) ;
			$cm->match_all_entries() ;
		} else if ( $job->action=='aux2wd' ) {
			$aux = new Auxiliary ( false , $this->mnm ) ;
			$aux->addAuxiliaryToWikidata ( $catalog_id ) ;
		} else if ( $job->action=='auxiliary_matcher' ) {
			$aux = new Auxiliary ( false , $this->mnm ) ;
			$aux->matchEntriesViaAuxiliary ( $catalog_id ) ;
		} else if ( $job->action=='match_person_entries_by_name_and_dates' or $job->action=='match_person_dates' ) {
			$pm = new PersonMatcher ( false , true , false , $this->mnm ) ;
			foreach ( $pm->candidateEntriesGenerator($catalog_id) AS $o ) $pm->process_entry ( $o ) ;
			$this->mnm->queue_job($catalog_id,'microsync');
		} else if ( $job->action=='update_person_dates' ) {
			$pd = new PersonDates ( $catalog_id , $this->mnm ) ;
			if ( $pd->hasCodeFragment() ) {
				$pd->loadCodeFragment() ;
				$pd->clearOldDates() ;
				$pd->updateDatesFromDescription() ;
			}
		} else if ( $job->action=='match_on_birthdate' ) {
			$pd = new PersonDates ( $catalog_id , $this->mnm ) ;
			$pd->birthdayMatcher ( 'P569' , false ) ;
			$this->mnm->queue_job($catalog_id,'microsync');
		} else {
			throw new \Exception("Unknown long running action '{$job->action}' for catalog {$catalog_id} (job id #{$job->id})");
		}
		print "Finished {$job->action} on catalog {$catalog_id}\n\n" ;
	}

	public function get_next_job() {
		$ret = $this->get_next_high_priority_job();
		if ( !isset($ret) ) $ret = $this->get_next_dependent_job() ;
		if ( !isset($ret) ) $ret = $this->get_next_initial_job() ;
		if ( !isset($ret) ) $ret = $this->get_next_low_priority_job() ;
		if ( !isset($ret) ) $ret = $this->get_next_scheduled_job() ;
		return $ret ;
	}

	public function run_job_command(&$job) {
		$parameters = '' ;
		if ( isset($job->json) and $job->json!='' ) {
			$j = json_decode($job->json) ;
			if ( is_array($j) ) $parameters = implode ( ' ' , $j ) ; # parameters in JSON array
			# TODO else
		}

		$script_path1 = "{$this->script_dir}/{$job->action}.php";

		if ( $job->action == 'update_person_dates' ) {
			$this->run_command ( $job , "{$this->script_dir}/person_dates/update_person_dates.php {$job->catalog} {$parameters}" ) ;
		} else if ( $job->action == 'autoscrape' ) {
			$this->run_command ( $job , "{$this->script_dir}/autoscrape.php run {$job->catalog} force {$parameters}" ) ;
		} else if ( $job->action == 'match_person_dates' ) {
			$this->run_command ( $job , "{$this->script_dir}/match_person_entries_by_name_and_dates.php {$job->catalog} {$parameters}" ) ;
		} else if ( file_exists($script_path1) ) { # Try standard ones
			$this->run_command ( $job , "{$script_path1} {$job->catalog} {$parameters}" ) ; # TODO is this really safe?
		} else { # Nope
			$this->mnm->set_job_status($job,'FAILED');
		}
	}

	protected function extendJobQuerySQL ( &$sql ) {
		if ( count($this->valid_job_types)==0 ) return ;
		# Trusting valid_job_types to be properly escaped
		$cond = " AND `action` IN ('".implode("','",$this->valid_job_types)."')" ;
		if ( preg_match('/(.+?)\b(LIMIT|OFFSET|GROUP BY|ORDER BY)(.*)$/',$sql,$m) ) {
			$sql = "{$m[1]}{$cond} {$m[2]}{$m[3]}" ;
		} else {
			$sql .= $cond ;
		}
	}

	public function get_job_by_id($job_id) {
		$job_id *= 1 ;
		$sql = "SELECT * FROM `jobs` WHERE `id`={$job_id}" ;
		$this->extendJobQuerySQL($sql);
		return $this->mnm->getSQL($sql)->fetch_object() ;
	}


	protected function get_next_dependent_job() {
		$sql = "SELECT * FROM `jobs` WHERE `status`='TODO' AND `depends_on` IS NOT NULL AND `depends_on` IN (SELECT `id` FROM `jobs` WHERE `status`='DONE') ORDER BY last_ts LIMIT 1" ;
		$this->extendJobQuerySQL($sql);
		return $this->mnm->getSQL($sql)->fetch_object() ;
	}

	protected function get_next_initial_job() {
		$sql = "SELECT * FROM `jobs` WHERE `status`='TODO' AND `depends_on` IS NULL ORDER BY `last_ts` LIMIT 1" ;
		$this->extendJobQuerySQL($sql);
		return $this->mnm->getSQL($sql)->fetch_object() ;
	}

	protected function count_running_jobs() {
		$all_job_types = $this->get_all_allowed_jobs() ;
		$sql = "SELECT count(*) AS `cnt` FROM `jobs` where `status`='RUNNING' AND `action` NOT IN ('".implode("','",$all_job_types)."')" ;
		return $this->mnm->getSQL($sql)->fetch_object()->cnt*1 ;
	}

	protected function get_next_scheduled_job() {
		$ts = $this->mnm->getCurrentTimestamp() ;
		$sql = "SELECT * FROM `jobs` WHERE `status`='DONE' AND `next_ts`!='' AND `next_ts`<='{$ts}' ORDER BY `next_ts` LIMIT 1" ;
		$this->extendJobQuerySQL($sql);
		return $this->mnm->getSQL($sql)->fetch_object() ;
	}

	protected function get_next_high_priority_job() {
		$sql = "SELECT * FROM `jobs` WHERE `status`='HIGH_PRIORITY' AND `depends_on` IS NULL ORDER BY `last_ts` LIMIT 1" ;
		$this->extendJobQuerySQL($sql);
		return $this->mnm->getSQL($sql)->fetch_object() ;
	}

	protected function get_next_low_priority_job() {
		$sql = "SELECT * FROM `jobs` WHERE `status`='LOW_PRIORITY' AND `depends_on` IS NULL ORDER BY `last_ts` LIMIT 1" ;
		$this->extendJobQuerySQL($sql);
		return $this->mnm->getSQL($sql)->fetch_object() ;
	}

	protected function run_command(&$job,$cmd) {
		$this->mnm->set_job_status($job,'RUNNING');
		$ts = $this->mnm->getCurrentTimestamp();
		$this->mnm->dbmConnect(false);
		$this->log ( "{$ts} : RUN {$cmd}" ) ;
		$this->job_id_running = $job ;
		$result = exec(trim($cmd));
		$this->mnm->dbmConnect(true);
		$this->job_id_running = '' ;
		if ( $result === false ) {
			$this->mnm->set_job_status($job,'FAILED');
		} else {
			$this->mnm->set_job_status($job,'DONE');
		}
	}

	protected function log ( $msg ) {
		print "{$msg}\n" ;
	}

}

?>