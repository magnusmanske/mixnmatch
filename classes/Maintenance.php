<?php

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class Maintenance {
	public $mnm ;
	protected $lc ;
	private $testing = false ;
	private $disambiguation_indicator_items = ['Q4167410','Q4167410','Q4167836','Q22808320'] ;

	function __construct ( $mnm = '', $testing = false ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
		$this->testing = $testing ;
	}

	public function updateAuxCandidates () {
		$props_ext_mnm = [] ;
		$sql = "select distinct aux_p from auxiliary ORDER BY aux_p" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $props_ext_mnm[] = $o->aux_p;

		$props_ext = [] ;
		$sql = "select pi_property_id from wb_property_info WHERE pi_type='external-id' AND pi_property_id IN (".implode(',',$props_ext_mnm).") ORDER BY pi_property_id" ;
		$dbwd = $this->mnm->openWikidataDB() ;
		$result = $this->mnm->tfc->getSQL ( $dbwd , $sql ) ;
		while($o = $result->fetch_object()) $props_ext[] = $o->pi_property_id;
		#$dbwd->close();

		$min = 3; # enough for now, lower to 2 if we run out of candidates
		#$sql = "INSERT INTO aux_candidates SELECT aux_name,aux_p,count(*) AS cnt,sum(entry_is_matched) AS matched,group_concat(entry_id) AS entry_ids,rand() AS random FROM auxiliary WHERE aux_p IN (".implode(',',$props_ext).") GROUP BY aux_p,aux_name HAVING cnt>={$min} AND matched=0" ;
		$props_ext = implode(",",$props_ext) ;
		$sql = "CREATE TEMPORARY TABLE aux_candidates_tmp
			SELECT SQL_NO_CACHE aux_name,aux_p,count(*) AS cnt,sum(entry_is_matched) AS matched,group_concat(entry_id) AS entry_ids FROM (
			SELECT entry.id AS entry_id,entry.ext_id AS aux_name,wd_prop AS aux_p,IF(q IS NULL or user=0,0,1) AS entry_is_matched FROM entry,catalog WHERE catalog.id=entry.catalog AND catalog.active=1 AND wd_prop IN ({$props_ext}) AND wd_qual IS NULL AND entry.ext_id!=''
			UNION ALL
			SELECT entry_id,aux_name,aux_p,entry_is_matched FROM auxiliary WHERE aux_p IN ({$props_ext}) AND aux_name!=''
		) t GROUP BY aux_p,aux_name HAVING cnt>={$min} AND matched=0";
		$this->mnm->getSQL ( $sql ) ;


		$this->mnm->getSQL ( "TRUNCATE aux_candidates" ) ;
		$this->mnm->getSQL ( "INSERT INTO aux_candidates SELECT * FROM aux_candidates_tmp" ) ;
		$this->mnm->getSQL ( "DROP TABLE aux_candidates_tmp" ) ;

	}

	public function createNewPeople ( $min_cnt=10 , $min_commands=7 ) {
		return ; # DEACTIVATED; created some weird items, no idea why (eg Q114587317)
		require_once ( "/data/project/mix-n-match/manual_lists/large_catalogs/shared.php" ) ;
		$this->lc = new \largeCatalog (2,$this->mnm) ; # VIAF
		$this->lc->prop2field[2]['P213'] = 'ISNI' ; // Activating for this specific purpose
		$this->lc->prop2field[2]['P1006'] = 'NTA' ; // Activating for this specific purpose

		$this->mnm->tfc->getQS('mixnmatch:CreateNewPeople','',true) ;

		$touched_catalogs = [] ;
		$sql = "SELECT * FROM common_names_human WHERE cnt>={$min_cnt}" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while ( $cnh = $result->fetch_object() ) {
			$entry_ids = explode ( ',' , $cnh->entry_ids ) ;
			try {
				$q = $this->createItemFromMultipleEntries ( $entry_ids , $cnh->name , $min_commands , $min_cnt ) ;
				print "Created https://www.wikidata.org/wiki/{$q} for {$cnh->name}\n" ;
				// Remove common name from list
				$sql = "DELETE FROM common_names_human WHERE id={$cnh->id}" ;
				$this->mnm->getSQL ( $sql ) ;
			} catch (\Exception $e) {
				print_r($e);
			}
		}

		foreach ( $touched_catalogs AS $catalog => $dummy ) {
			$this->mnm->queue_job($catalog,'microsync');
		}
	}

	public function updateIssues() {
		# Close issues from inactive catalogs
		$sql = "UPDATE `issues` set `status`='INACTIVE_CATALOG' where `status`='OPEN' AND EXISTS (SELECT * FROM entry,catalog WHERE entry_id=entry.id AND entry.catalog=catalog.id AND active!=1)" ;
		$this->mnm->getSQL ( $sql ) ;

		# Close date mismatch issues where the mnm data is Jan 01
		$sql = "UPDATE `issues` SET `status`='JAN01' WHERE `status`='OPEN' AND `type`='MISMATCH_DATES' AND json LIKE '%mnm_time%-01-01%'" ;
		$this->mnm->getSQL ( $sql ) ;

		# Remove issues matched to Q0 or Q-1
		$sql = "DELETE FROM `issues` where `status`='OPEN' AND EXISTS (SELECT * FROM entry WHERE entry_id=entry.id AND entry.q<=0 AND entry.user>0)" ;
		$this->mnm->getSQL ( $sql ) ;

		# Auto-close duplicates that are redirects
		$sql = "SELECT * FROM `issues` WHERE `status`='OPEN' AND `type` IN ('WD_DUPLICATE','MISMATCH')" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$items = json_decode ( $o->json ) ;
			$items = (array) $items ;
			$items = array_values($items);
			if ( count($items) != 2 ) continue ; # Too complicated...
			$q1 = $items[0] ;
			$q2 = $items[1] ;
			$sql = "SELECT * FROM `page`,`redirect` WHERE `page_namespace`=0 AND `page_title` IN ('{$q1}','{$q2}') AND page_id=rd_from AND `rd_namespace`=0 AND `rd_title` IN ('{$q1}','{$q2}') AND `page_title`!='rd_title'" ;
			$this->mnm->openWikidataDB() ;
			$result2 = $this->mnm->tfc->getSQL ( $this->mnm->dbwd , $sql ) ;
			if ($o2 = $result2->fetch_object()) {
				$sql = "UPDATE `issues` SET `status`='RESOLVED_ON_WIKIDATA' WHERE id={$o->id}" ;
				$this->mnm->getSQL ( $sql ) ;
			}
		}
	}

	protected function updatePropertyCacheAdd2SQL ( $get_prop , &$sql ) {
		$query = "SELECT ?p ?v ?vLabel { ?p rdf:type wikibase:Property ; wdt:P{$get_prop} ?v SERVICE wikibase:label { bd:serviceParam wikibase:language 'en' } }" ;
		foreach ( $this->mnm->tfc->getSPARQL_TSV ( $query ) as $o ) {
			$prop = preg_replace('|\D|','',$this->mnm->tfc->parseItemFromURL($o['p']))*1 ;
			$item = preg_replace('|\D|','',$this->mnm->tfc->parseItemFromURL($o['v']))*1 ;
			$sql[] = "({$get_prop},{$prop},{$item},'".$this->mnm->escape($o['vLabel'])."')" ;
		}
	}

	public function updatePropertyCache() {
		$sql = [] ;
		$this->updatePropertyCacheAdd2SQL(17,$sql) ;
		$this->updatePropertyCacheAdd2SQL(31,$sql) ;
		if ( count($sql)<20000 ) {
			throw new \Exception(__METHOD__.": Emergency exit in case something went wrong with the queries");
		}

		$this->mnm->getSQL("TRUNCATE `property_cache`") ;
		$sql = "INSERT INTO `property_cache` (`prop_group`,`property`,`item`,`label`) VALUES " . implode(',',$sql) ;
		$this->mnm->getSQL($sql) ;
	}

	public function crossmatchViaAux() {
		$total_matched = 0 ;
		$used_catalogs = [] ;

		$rely_on_entry_is_matched = false ;

		if ( $rely_on_entry_is_matched ) {
			$sql = 'UPDATE auxiliary SET entry_is_matched=1 WHERE entry_is_matched=0 AND EXISTS (SELECT * FROM entry WHERE entry.id=entry_id AND q IS NOT NULL AND q>0 AND user IS NOT NULL and user>0)' ;
			$this->mnm->getSQL ( $sql ) ;
		}

		$sql = "SELECT aux_p,aux_name,group_concat(entry_id) AS entry_ids,count(entry_id) AS cnt,sum(entry_is_matched) AS entry_is_matched FROM auxiliary WHERE aux_p IN (214,227) GROUP BY aux_p,aux_name HAVING cnt>1 AND cnt>entry_is_matched" ;
		if ( $rely_on_entry_is_matched ) $sql .= " AND entry_is_matched>0" ;

		$result = $this->mnm->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			$sql = "SELECT entry.* FROM entry,catalog WHERE entry.id IN ({$o->entry_ids}) AND catalog.id=entry.catalog AND catalog.active=1" ;
			$result2 = $this->mnm->getSQL ( $sql ) ;
			$catalogs = [] ;
			$manual_qs = [] ;
			$entries_without_q = [] ;
			while ( $o2 = $result2->fetch_object() ) {
				if ( isset($o2->user) and $o2->user != null and $o2->user > 0 ) $manual_qs[$o2->q] = $o2->q ;
				else {
					$entries_without_q[] = $o2->id ;
					if ( !isset($used_catalogs[$o2->catalog]) ) $catalogs[$o2->catalog] = 1 ;
				}
			}
			if ( count($manual_qs) == 0 ) continue ; # No q values to set
			if ( count($manual_qs) > 1 ) continue ; # Too many q values to set; TODO log potential issue
			if ( count($entries_without_q) == 0 ) continue ; # No entries to set

			$q = '' ;
			foreach ( $manual_qs AS $key => $dummy ) $q = $key ;
			foreach ( $entries_without_q AS $entry_id ) {
				#print "{$this->mnm->root_url}/#/entry/{$entry_id}\n" ;
				$this->mnm->setMatchForEntryID ( $entry_id , $q , 4 , true , false ) ;
				$total_matched++ ;
			}
			foreach ( $catalogs AS $catalog ) $used_catalogs[$catalog] = 1 ;
		}

		foreach ( $used_catalogs AS $catalog => $dummy ) {
			$this->mnm->queue_job($catalog,'microsync',0,'',0,0,'LOW_PRIORITY');
		}

		return $total_matched ;
	}

	public function matchPeopleByKnownNameDates() {
		$this->mnm->getSQL ( "truncate human_dates_tmp" ) ;

		# Fill tmp table
		$sql = "INSERT IGNORE INTO human_dates_tmp (years,name,has_auto_matched) SELECT concat(year_born,'-',year_died) AS years,ext_name,1 FROM vw_dates WHERE (q is null or user=0) AND year_born!='' AND year_died!=''" ;
		$this->mnm->getSQL ( $sql ) ;
		$sql = "INSERT INTO human_dates_tmp (years,name,has_fully_matched) SELECT concat(year_born,'-',year_died) AS years,ext_name,1 FROM vw_dates WHERE q is NOT null AND user>0 AND year_born!='' AND year_died!='' ON DUPLICATE KEY UPDATE has_fully_matched=1" ;
		$this->mnm->getSQL ( $sql ) ;

		# Retrieve results
		$sql = "SELECT * FROM human_dates_tmp WHERE has_auto_matched=1 and has_fully_matched=1" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			if ( !preg_match ( '/^(\d+)-(\d+)$/' , $o->years , $m ) ) continue ;
			$did_set = $this->matchOnKnownDates ( $m[1] , $m[2] , $o->name ) ;
		}



		$this->mnm->getSQL ( "truncate human_dates_tmp" ) ;
	}

	protected function matchOnKnownDates ( $born , $died , $name , $min_q_required = 1) {
		$q = '' ;
		$qs = [] ;
		$entries2set = [] ;

		$sql = "SELECT * FROM vw_dates WHERE ext_name='" . $this->mnm->escape($name) . "' AND `year_born`='{$born}' AND `year_died`='{$died}'" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			if ( isset($o->q) AND isset($o->user) AND $o->user>0 AND $o->q>0 ) {
				$q = "{$o->q}" ;
				if ( isset($qs[$q]) ) $qs[$q]++ ;
				else $qs[$q] = 1 ;
			} else if ( (isset($o->user) and $o->user=0) or (!isset($o->q)) ) {
				$entries2set[] = $o ;
			} else { # Something odd, abort, abort!
				return ;
			}
		}

		if ( count($qs) != 1 ) return ; // None, or multiple, q values, abort!
		if ( count($entries2set) == 0 ) return ; // Nothing to do
		if ( $qs[$q] < $min_q_required ) return ; // Not enough other evidence

		$ret = 0 ;
		foreach ( $entries2set AS $o ) {
			if ( $this->mnm->setMatchForEntryID ( $o->entry_id , $q , 4 , true , false ) ) $ret++ ;
		}
		return $ret ;
	}

	public function updateCommonNames() {
		$this->mnm->getSQL ( "truncate common_names_human" ) ;
		$sql = "INSERT IGNORE INTO `common_names_human` (`name`,`cnt`,`entry_ids`) SELECT `ext_name`,count(DISTINCT `catalog`) AS `cnt`,group_concat(`id`) as `entry_ids` FROM `entry` WHERE `type`='Q5' AND `q` IS NULL AND `ext_name` LIKE '____% ____% ____%' AND `ext_desc`!='' AND `catalog` IN (SELECT `id` FROM `catalog` WHERE `active`=1) GROUP BY `ext_name` HAVING `cnt`>=5" ;
		$this->mnm->getSQL ( $sql ) ;

		$this->mnm->getSQL ( "truncate common_names_dates" ) ;
		$sql = "INSERT IGNORE INTO common_names_dates (`name`,cnt,entry_ids) select concat(born,' - ',died),count(DISTINCT catalog) AS cnt,group_concat(distinct entry_id) from person_dates,entry WHERE is_matched=0 AND length(born)=10 and length(died)=10 AND entry_id=entry.id GROUP BY born,died HAVING cnt>1" ;

		$this->commonNameDateFixer() ;
	}

	protected function commonNameDateFixer($table='common_names_dates') {
		$useless_catalogs = [5285] ;
		$useless_pairs = [ [143,768] , [3875,2020] , [2060,4143] ] ;

		$to_delete = '' ;
		$sql = "SELECT * FROM `{$table}`" ;
		$result_main = $this->mnm->getSQL ( $sql ) ;
		while($o_main = $result_main->fetch_object()) {
			$sql = "SELECT DISTINCT `catalog` FROM `entry` WHERE `id` IN ({$o_main->entry_ids})" ;
			$result = $this->mnm->getSQL ( $sql ) ;
			$catalogs = [] ;
			while($o = $result->fetch_object()) $catalogs[$o->catalog] = $o->catalog ;
			foreach ( $useless_catalogs AS $c ) unset($catalogs[$c]) ;
			foreach ( $useless_pairs AS $pair ) {
				if ( isset($catalogs[$pair[0]]) and isset($catalogs[$pair[1]]) ) unset($catalogs[$pair[1]]) ;
			}
			if ( count($catalogs) == $o_main->cnt ) continue ;
			$new_cnt = count($catalogs) ;
			if ( $new_cnt <= 1 ) {
				if ( $to_delete!='' ) $to_delete .= ',' ;
				$to_delete .= $o_main->id ;
			} else {
				$sql = "UPDATE `{$table}` SET `cnt`={$new_cnt} WHERE `id`={$o_main->id}" ;
				$this->mnm->getSQL ( $sql ) ;
			}
		}

		if ( $to_delete != '' ) {
			$sql = "DELETE FROM `{$table}` WHERE `id` IN ({$to_delete})" ;
			$this->mnm->getSQL ( $sql ) ;
		}

	}


	public function importRelationsIntoAux() {
		$sql = 'INSERT IGNORE INTO auxiliary (entry_id,aux_p,aux_name) SELECT mnm_relation.entry_id,property,concat("Q",entry.q) FROM entry,mnm_relation WHERE target_entry_id=entry.id AND user>0 AND q>0 AND NOT EXISTS (SELECT * FROM auxiliary WHERE auxiliary.entry_id=mnm_relation.entry_id AND aux_p=property) AND property IN (50,170)' ;
		$this->mnm->getSQL ( $sql ) ;
	}

	public function deleteMultimatchesForFullyMatchedEntries() {
		$sql = "DELETE FROM multi_match WHERE EXISTS (SELECT * FROM entry WHERE entry_id=entry.id AND q IS NOT NULL AND user IS NOT NULL and user>0)" ;
		$this->mnm->getSQL ( $sql ) ;
	}

	public function updatePaintersList() {
		$filename = "/data/project/mix-n-match/manual_lists/paintings_without_creator.tab" ;
		$sql = "SELECT concat('Q',wbit_item_id) AS q,wbx_text
			FROM wbt_text_in_lang,wbt_text,wbt_term_in_lang,wbt_item_terms
			WHERE wbxl_language='en'
			AND wbxl_text_id=wbx_id
			AND wbtl_text_in_lang_id=wbxl_id
			AND wbtl_type_id=2
			AND wbit_term_in_lang_id=wbtl_id
			AND wbx_text LIKE 'painting by %'
			AND NOT EXISTS (SELECT * FROM page,pagelinks,linktarget WHERE pl_target_id=lt_id AND pl_from=page_id AND page_namespace=0 AND page_title=concat('Q',wbit_item_id) AND lt_namespace=120 AND lt_title='P170')" ;
		$tmp_path = tempnam(sys_get_temp_dir(), "MnM_Manintenance_updatePaintersList");
		$tmp_file = fopen($tmp_path, "w");
		$this->mnm->openWikidataDB() ;
		$result2 = $this->mnm->tfc->getSQL ( $this->mnm->dbwd , $sql ) ;
		while ($o = $result2->fetch_object()) {
			fwrite($tmp_file,"{$o->q}\t{$o->wbx_text}\n");
		}
		fclose($tmp_file);
		if ( file_exists($filename) ) unlink($filename) ;
		rename($tmp_path,$filename);
	}

	protected function retirePropertiesWithCatalog () {
		$sql = "UPDATE `props_todo` SET `status`='HAS_CATALOG',`note`='Auto-matched to catalog' WHERE `status`='NO_CATALOG' AND `property_num` IN (SELECT `wd_prop` FROM `catalog` WHERE `wd_prop` IS NOT NULL and `wd_qual` IS NULL and `active`=1)" ;
		$this->mnm->getSQL ( $sql ) ;
	}

	protected function importNewPropertiesFromSPARQL ( $sparql , $default_type = '' ) {
		$props = [] ;
		$sql = "SELECT `property_num` FROM `props_todo`" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $props[] = $o->property_num ;


		$j = $this->mnm->tfc->getSPARQL($sparql);
		foreach ( $j->results->bindings AS $b ) {
			$p = preg_replace ( '|^.+/P|' , '' , $b->p->value ) * 1 ;
			$label = $this->mnm->escape($b->pLabel->value) ;
			if ( in_array($p, $props) ) continue ;
			$sql = "INSERT IGNORE INTO `props_todo` (`property_num`,`property_name`,`default_type`) VALUES ({$p},'{$label}','{$default_type}')" ;
			$this->mnm->getSQL ( $sql ) ;
		}
	}

	public function updatePropsTodo() {
		$this->retirePropertiesWithCatalog();

		# People
		$this->importNewPropertiesFromSPARQL('SELECT ?p ?pLabel WHERE { ?p rdf:type wikibase:Property; wdt:P31 wd:Q19595382. MINUS { ?p wdt:P2264 _:b2. } SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". } } order by ?pLabel','Q5');

		# Taxa
		$this->importNewPropertiesFromSPARQL('SELECT ?p ?pLabel WHERE { ?p rdf:type wikibase:Property; wdt:P31 wd:Q42396390 . MINUS { ?p wdt:P2264 _:b2. } SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". } } order by ?pLabel','Q5');

		$this->retirePropertiesWithCatalog();
	}

	public function fixHTMLentitiesForNamesInCatalog ( $catalog ) {
		# Find all suitable catalogs:
		# SELECT * FROM catalog WHERE active=1 AND exists (SELECT * FROM entry WHERE entry.catalog=catalog.id AND ext_name like "%&%;%")
		$catalog *= 1 ;
		if ( $catalog <= 0 ) return ;
		$sql = "SELECT * FROM `entry` WHERE `catalog`={$catalog} AND `ext_name` LIKE '%&%;%'" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$name = trim ( html_entity_decode ( $o->ext_name ) ) ;
			if ( $name == $o->ext_name ) continue ;
			$sql = "UPDATE `entry` SET `ext_name`='" . $this->mnm->escape($name) . "' WHERE `id`={$o->id}" ;
			$this->mnm->getSQL ( $sql ) ;
		}
	}

	public function removeMultiMatchForManualMatches () {
		$sql = "DELETE FROM multi_match WHERE EXISTS (SELECT * FROM entry WHERE entry_id=entry.id AND q IS NOT NULL AND user IS NOT NULL and user>0)" ;
		$this->mnm->getSQL ( $sql ) ;
	}

	public function fixupWdMatches () {
		# matches in deactivated catalogs must go
		$sql = "DELETE FROM wd_matches WHERE catalog IN (SELECT id FROM catalog WHERE active!=1)" ;
		$this->mnm->getSQL ( $sql ) ;

		# In case any matches were added with catalog=0
		$sql = "UPDATE wd_matches SET catalog=(SELECT entry.catalog FROM entry WHERE entry.id=entry_id) WHERE wd_matches.catalog=0" ;
		$this->mnm->getSQL ( $sql ) ;

		# matches in catalogs without property/with qualifier
		$sql = "UPDATE wd_matches SET status='N/A' WHERE status!='N/A' AND catalog IN (SELECT id FROM catalog WHERE wd_prop IS NULL or wd_qual IS NOT NULL)" ;
		$this->mnm->getSQL ( $sql ) ;

		# TODO remove matches for deleted entries
	}


	public function createItemFromMultipleEntries ( $entry_ids , $entry_name = '' , $min_commands = 7 , $min_cnt = 10 ) {

		$entries = [] ;
		foreach ( $entry_ids AS $entry_id ) {
			try {
				$entry = new Entry ( $entry_id , $this->mnm ) ;
				$e = $entry->core_data() ;
			} catch (\Exception $e) {
				continue ;
			}

			if ( $entry_name == '' ) $entry_name = $e->ext_name ;
			if ( isset($e->q) AND isset($e->user) AND $e->q !== null AND $e->q !== FALSE AND $e->user>0 ) {
				throw new \Exception(__METHOD__." {$this->mnm->root_url}/#/entry/{$entry->id()} ({$entry_name}) already has a match: Q{$e->q}" ) ;
			}
			$entries[] = $e ;
		}
		
		$merged_commands = [] ;
		foreach ( $entries AS $entry ) {
			if ( isset($this->lc) ) $commands = $this->mnm->getCreateItemForEntryCommands ( $entry , $this->lc ) ;
			else $commands = $this->mnm->getCreateItemForEntryCommands ( $entry ) ;
			if ( !is_array($commands) ) {
				throw new \Exception(__METHOD__." Failed to assemble commands for an entry of {$entry_name}" ) ;
			}
			foreach ( $commands AS $command ) {
				if ( $command == 'CREATE' ) continue ;
				if ( !preg_match ( '|^LAST\t(\S+)\t(.+)$|' , $command , $m ) ) continue ;
				$cmd1 = $m[1] ;
				$cmd2 = $m[2] ;
				if ( preg_match ( '|^(.+?)\t(.+)$|' , $cmd2 , $m ) ) {
					$cmd1 .= "\t" . trim($m[1]) ;
					$cmd2 = $m[2] ;
				}
				if ( !isset($merged_commands[$cmd1]) ) $merged_commands[$cmd1] = [] ;
				if ( preg_match ( '|^[LDA].*$|' , $cmd1) ) $merged_commands[$cmd1] = [ $cmd1 => $cmd2 ] ; # Only one
				else $merged_commands[$cmd1][$cmd2] = $cmd2 ;
			}
		}

		$mc2 = [ "CREATE" ] ;
		foreach ( $merged_commands AS $command => $parts ) {
			$mc2[] = trim ( "LAST\t{$command}\t" . implode ( "\t" , $parts ) ) ;
		}
		$merged_commands = $mc2 ;

		if ( count($merged_commands) < $min_commands ) { # Skip small ones
			throw new \Exception(__METHOD__." < {$min_commands} commands from {$entry_name}" ) ;
		}

		// Create and run commands
		$q = $this->mnm->tfc->runCommandsQS ( $merged_commands ) ;
		if ( isset($q) and $q!==FALSE and $q!=null and $q!='' ) {
			foreach ( $entries AS $entry ) {
				$this->mnm->setMatchForEntryID ( $entry->id , $q , 4 , true ) ;
				$touched_catalogs[$entry->catalog] = 1 ;
			}
		} else {
			throw new \Exception(__METHOD__." Creation of {$entry_name} failed" ) ;
		}
		return $q ;
	}

	public function getUnmatchedOrAutomatchedEntryBatches($catalog_id=0,$batch_size=1000,$max_random_results=10000) {
		$sql = "SELECT DISTINCT `q` FROM `entry` WHERE `user`=0 AND `q`>0 AND `q` IS NOT NULL" ;
		return $this->getEntryBatchesForSQL($sql,$catalog_id,$batch_size,$max_random_results);
	}

	public function getFullyMatchedEntryBatches($catalog_id=0,$batch_size=1000,$max_random_results=10000) {
		$sql = "SELECT DISTINCT `q` FROM `entry` WHERE `user`>0 AND `q`>0 AND `q` IS NOT NULL" ;
		return $this->getEntryBatchesForSQL($sql,$catalog_id,$batch_size,$max_random_results);
	}

	protected function getEntryBatchesForSQL($sql,$catalog_id=0,$batch_size=1000,$max_random_results=10000) {
		if ( ($catalog_id??0)!=0 ) $sql .= " AND `catalog`={$catalog_id}" ;
		else {
			$r = rand() / getrandmax() ;
			$sql .= " AND `random`>={$r} ORDER BY `random` LIMIT {$max_random_results}" ;
		}

		$qs = [] ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$qs[] = 'Q'.$o->q ;
			if ( count($qs) < $batch_size ) continue ;
			yield $qs ;
			$qs = [] ;
		}
		if ( count($qs)>0 ) yield $qs ;
		else yield from [] ;
	}

	public function unlinkDisambiguationItems($catalog_id=0) {
		$this->mnm->openWikidataDB() ;
		foreach ( $this->getUnmatchedOrAutomatchedEntryBatches($catalog_id) as $qs ) {
			$sql = "SELECT DISTINCT page_title FROM page,pagelinks,linktarget WHERE pl_target_id=lt_id AND pl_from=page_id AND page_namespace=0 AND lt_namespace=0" ;
			$sql .= " AND lt_title IN ('".implode("','",$this->disambiguation_indicator_items)."')" ;
			$sql .= " AND page_title IN ('" . implode("','",$qs) . "')" ;
			$qs = [] ;
			$result = $this->mnm->tfc->getSQL ( $this->mnm->dbwd , $sql ) ;
			while($o = $result->fetch_object()) $qs[] = preg_replace ( '/\D/' , '' , $o->page_title ) ;

			if ( count($qs)==0 ) continue ;
			$sql = "UPDATE entry SET q=null,user=null,timestamp=null WHERE user=0 AND q IN (".implode(',',$qs).")" ;
			$this->mnm->getSQL ( $sql ) ;
		}
	}

	public function unlinkDeletedItems($qs) {
		$this->mnm->openWikidataDB() ;
		$sql = "SELECT page_title FROM page WHERE page_namespace=0 AND page_title IN ('".implode("','",$qs)."')" ;
		$result = $this->mnm->tfc->getSQL ( $this->mnm->dbwd , $sql ) ;
		$exists = [] ;
		while($o = $result->fetch_object()) $exists[$o->page_title] = true ;
		$non_existing = [] ;
		foreach ( $qs AS $q ) {
			if ( !isset($exists[$q]) ) $non_existing[] = preg_replace ( '/\D/' , '' , "{$q}" ) ;
		}
		unset($exists);
		if ( count($non_existing)==0 ) return ;

		$sql = "UPDATE `entry` SET `q`=NULL,`user`=NULL,`timestamp`=NULL WHERE `q` IN (".implode(',',$non_existing).")" ;
		$this->mnm->getSQL($sql);
	}

	public function fixRedirectedItems($qs) {
		$redirected_items = [] ; # page_id => Qxxx
		$sql = "SELECT page_id,page_title FROM page WHERE page_namespace=0 AND page_is_redirect=1 AND page_title IN ('".implode("','",$qs)."')" ;
		$this->mnm->openWikidataDB() ;
		$result = $this->mnm->tfc->getSQL ( $this->mnm->dbwd , $sql ) ;
		while($o = $result->fetch_object()) $redirected_items[$o->page_id] = $o->page_title ;
		if ( count($redirected_items) == 0 ) return;

		$q2q = $this->mnm->getRedirectTargets($redirected_items);
		if ( count($q2q) == 0 ) return ;

		unset($redirected_items);
		foreach ( $q2q AS $q_from => $q_to ) {
			if ( !preg_match('|^Q(\d+)$|',$q_to,$m) ) continue ; # Paranoia
			$q_to = $m[1] ;
			$q_from = preg_replace ( '/\D/' , '' , "{$q_from}" ) ;
			$sql = "UPDATE `entry` SET `q`={$q_to} WHERE `q`={$q_from}" ;
			$this->mnm->getSQL($sql);
		}
	}

	public function purgeAutomatchesFromCatalog($catalog_id) {
		$catalog_id =  ($catalog_id??0) * 1 ;
		if ( $catalog_id <= 0 ) throw new \Exception(__METHOD__.": Bad catalog ID ({$catalog_id})" ) ; 

		$sql = "UPDATE entry SET q=NULL,user=NULL,`timestamp`=NULL WHERE catalog={$catalog_id} AND user=0" ;
		$this->mnm->getSQL ( $sql ) ;

		$sql = "DELETE FROM multi_match WHERE catalog={$catalog_id}" ;
		$this->mnm->getSQL ( $sql ) ;
	}

	public function getActiveCatalogIDsFromString ( $s ) {
		$catalogs = [] ;
		if ( !isset($s) ) return $catalogs ;
		if ( trim(strtolower($s)) == 'all' ) {
			$sql = "SELECT `id` FROM `catalog` WHERE `active`=1" ;
			$result = $this->mnm->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) $catalogs[] = $o->id ;
		} else {
			# TODO support comma-separated ID lists?
			$catalog = new Catalog ( $s , $this->mnm ) ;
			if ( $catalog->isActive() ) $catalogs[] = $catalog->id() ;
		}
		return $catalogs ;
	}



	// unused but should
	public function fixJobsWithNextTsButNoRepeat () {
		$sql = 'UPDATE `jobs` SET `next_ts`="" WHERE `repeat_after_sec` IS NULL AND `next_ts`!=""' ;
		$this->mnm->getSQL ( $sql ) ;
	}
}

?>