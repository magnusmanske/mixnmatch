<?php

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

# ________________________________________________________________________________________________________________________
# Helper
# A base class for other helper classes

class Helper {
	public $use_curl = false ;
	public $mnm ;
	protected $catalog ;
	protected $code_fragment ;
	protected $rewrite_functions = [] ;
	protected $current_results = [] ;
	protected $dangerous_php = ['`','exec','system','passthru','shell_exec','mail','file_get_contents','file_put_contents','fpassthru','mkdir','umask','tmpfile','fopen','readfile','fwrite','unlink','popen','rename','rmdir','chgrp','chmod','chown','copy','delete','mnm','tfc'] ;
	
	/**
	 * Constructor
	 *
	*/
	public function __construct ( /*int*/ $catalog , $mnm = '' ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
		if ( !isset($catalog) or $catalog*1 == 0 ) die ( "Helper constructor: bad catalog" ) ;
		$this->catalog = $catalog*1 ;
	}

	/**
	 * Returns the current catalog
	 *
	 * @return int catalog ID, or undefined
	*/
	public function get_catalog() {
		return $this->catalog ;
	}

	/**
	 * Returns the current code fragment
	 *
	 * @return object code fragment, or undefined
	*/
	public function get_code_fragment() {
		return $this->code_fragment ;
	}

	public function get_cf_function() {
		if ( isset($this->cf_function) ) return $this->cf_function ;
	}

	/**
	 * Enacts all given commands
	 *
	 * @param array $commands of Command
	*/
	public function enact ( array $commands ) {
		foreach ( $commands AS $command ) $command->enact($this->mnm) ;
	}

	/**
	 * Sets the code fragment to use in processEntry() and clear_old_dates()
	 *
	 * @param object $cf code fragment object
	 * @throws Exception if the code fragment is bad or missing
	*/
	public function setCodeFragment ( $cf ) {
		if ( !isset($cf->success) or !$cf->success ) throw new \Exception(__METHOD__.': Trying to set a code fragment with bad/missing success flag') ;
		if ( !isset($cf->is_active) or !$cf->is_active ) throw new \Exception(__METHOD__.": Trying to set a code fragment with bad/missing is_active flag") ;
		$this->sanitizePHP ( $cf ) ;
		$this->code_fragment = $cf ;
	}

	/**
	 * Checks the PHP code for dangerous commands
	 *
	 * @param object $cf code fragment object
	 * @throws Exception if dangerous PHP code was found
	*/
	private function sanitizePHP ( &$cf ) {
		foreach ( $this->dangerous_php AS $bw ) {
			if ( preg_match ( '|\b'.preg_quote($bw).'\b|i' , $cf->php ) ) {
				throw new \Exception(__METHOD__.': Dangerous PHP code' ) ;
			}
		}

		$direct_bad_words = ['$this','`'] ;
		foreach ( $direct_bad_words AS $bw ) {
			if ( FALSE !== stristr($cf->php,$bw) ) {
				throw new \Exception(__METHOD__.': Dangerous PHP code' ) ;
			}
		}
	}

	/**
	 * Checks if there is a relevant code fragment in the DB
	 *
	 * @throws Exception if no catalog is set
	 * @throws Exception if no function is set
	*/
	public function hasCodeFragment() {
		if ( !isset($this->catalog) ) throw new \Exception(__METHOD__.': no catalog' ) ;
		if ( !isset($this->cf_function) ) throw new \Exception(__METHOD__.': no cf_function' ) ;
		return $this->mnm->hasCodeFragment ( $this->cf_function , $this->catalog , false ) ;
	}

	/**
	 * Loads the relevant code fragment from the DB and tries to set it
	 *
	 * @throws Exception if no catalog is set
	 * @throws Exception if no function is set
	*/
	public function loadCodeFragment() {
		if ( !isset($this->catalog) ) throw new \Exception(__METHOD__.': no catalog' ) ;
		if ( !isset($this->cf_function) ) throw new \Exception(__METHOD__.': no cf_function' ) ;
		$cf = $this->mnm->loadCodeFragment ( $this->cf_function , $this->catalog , false ) ;
		$this->setCodeFragment ( $cf ) ;
	}

	/**
	 * Updates the `last_update` timestamp of the current code fragment
	 *
	 * @throws Exception if no catalog is set
	*/
	public function touchCodeFragment() {
		if ( !isset($this->catalog) ) throw new \Exception(__METHOD__.': no catalog' ) ;
		if ( !isset($this->cf_function) ) throw new \Exception(__METHOD__.': no cf_function' ) ;
		$this->mnm->touchCodeFragment ( $this->cf_function , $this->catalog ) ;
	}

	/**
	 * Tries to turn a (reasonable) date string into proper '0000-00-00' format
	 *
	 * @return string ISO date
	*/
	protected function parse_date ( $d ) {
		if ( preg_match ( '/^\s*\d{1,4}\s*$/' , $d ) ) return str_pad(trim($d),4,'0',STR_PAD_LEFT) ;
		if ( preg_match ( '/^\s*(\d{1,2})\.\s*(\d{1,2})\.\s*(\d{3,4})\s*$/' , $d , $m ) ) {
			$d = str_pad($m[3],4,'0',STR_PAD_LEFT) . '-' . str_pad($m[2],2,'0',STR_PAD_LEFT) . '-' . str_pad($m[1],2,'0',STR_PAD_LEFT) ;
			return $d ;
		} else if ( preg_match ( '/^\s*(\d{1,2})\.\s*(\d{3,4})\s*$/' , $d , $m ) ) {
			$d = str_pad($m[2],4,'0',STR_PAD_LEFT) . '-' . str_pad($m[1],2,'0',STR_PAD_LEFT) ;
			return $d ;
		}
		$d = date_parse ( $d ) ;
		while ( strlen($d['year']) < 4 ) $d['year'] = '0' . $d['year'] ;
		while ( strlen($d['month']) < 2 ) $d['month'] = '0' . $d['month'] ;
		while ( strlen($d['day']) < 2 ) $d['day'] = '0' . $d['day'] ;
		$ret = $d['year'] ;
		if ( $d['month'] != '00' ) {
			$ret .= '-' . $d['month'] ;
			if ( $d['day'] != '00' ) $ret .= '-' . $d['day'] ;
		}
		return $ret ;
	}

	/**
	 * Rewrites "global" functions to member functions
	 *
	 * @param $php string PHP code (from code fragment)
	 & @return string PHP code, with bespoke local method replacements for specific short-hand functions
	*/
	protected function rewriteGlobalMethods ( $php ) {
		foreach ( $this->rewrite_functions AS $fn_orig => $fn_new ) {
			$php = preg_replace ( '|\b'.$fn_orig.'\s*\(|' , '$this->'.$fn_new.'(' , $php ) ;
		}
		return $php ;
	}	

	/**
	 * Convenience function to add a command to $this->current_results
	*/
	public function setAux ( $entry_id , $property , $value ) {
		array_push ( $this->current_results , Command::addAux($entry_id,$property,$value) ) ;
	}

	/**
	 * Convenience function to add a command to $this->current_results
	*/
	public function setLocation ( $entry_id , $lat , $lon ) {
		array_push ( $this->current_results , Command::setLocation($entry_id,$lat,$lon) ) ;
	}

	/**
	 * Convenience function to add a command to $this->current_results
	*/
	private function setEntryName ( $entry_id , $value ) {
		array_push ( $this->current_results , Command::setEntryName($entry_id,$value) ) ;
	}

	/**
	 * Convenience function to add a command to $this->current_results
	*/
	private function setEntryDescription ( $entry_id , $value ) {
		array_push ( $this->current_results , Command::setEntryName($entry_id,$value) ) ;
	}

	/**
	 * Convenience function to add a command to $this->current_results
	*/
	private function setAlias ( $entry_id , $label , $language = '' ) {
		array_push ( $this->current_results , Command::addAlias($entry_id,$label,$language) ) ;
	}

	/**
	 * Convenience function to add a command to $this->current_results
	*/
	public function setMatch ( $entry_id , $q ) {
		array_push ( $this->current_results , Command::setMatch($entry_id,$q) ) ;
	}

	/**
	 * Convenience function to add a command to $this->current_results
	*/
	private function setPersonDates ( $entry_id , $born , $died ) {
		array_push ( $this->current_results , Command::setPersonDates($entry_id,$born,$died) ) ;
	}

	protected function curl_exec_utf8($ch) { // From https://stackoverflow.com/questions/2510868/php-convert-curl-exec-output-to-utf8
		$data = curl_exec($ch);
		if (!is_string($data)) return $data;

		unset($charset);
		$content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);

		/* 1: HTTP Content-Type: header */
		preg_match( '@([\w/+]+)(;\s*charset=(\S+))?@i', $content_type, $matches );
		if ( isset( $matches[3] ) )
			$charset = $matches[3];

		/* 2: <meta> element in the page */
		if (!isset($charset)) {
			preg_match( '@<meta\s+http-equiv="Content-Type"\s+content="([\w/]+)(;\s*charset=([^\s"]+))?@i', $data, $matches );
			if ( isset( $matches[3] ) )
				$charset = $matches[3];
		}

		/* 3: <xml> element in the page */
		if (!isset($charset)) {
			preg_match( '@<\?xml.+encoding="([^\s"]+)@si', $data, $matches );
			if ( isset( $matches[1] ) )
				$charset = $matches[1];
		}

		/* 4: PHP's heuristic detection */
		if (!isset($charset)) {
			$encoding = mb_detect_encoding($data);
			if ($encoding)
				$charset = $encoding;
		}

		/* 5: Default for HTML */
		if (!isset($charset)) {
			if (strstr($content_type, "text/html") === 0)
				$charset = "ISO 8859-1";
		}

		/* Convert it if it is anything but UTF-8 */
		/* You can change "UTF-8"  to "UTF-8//IGNORE" to 
		   ignore conversion errors and still output something reasonable */
		if (isset($charset) && strtoupper($charset) != "UTF-8")
			$new_data = iconv($charset, 'UTF-8//IGNORE', $data);
		if(isset($new_data) and $new_data!==false) $data = $new_data;

		return $data;
	}
	
	public function get_contents_from_url ( $url , $force_curl = false ) {
		# TODO make curl automatic fallback
		if ( $this->use_curl or $force_curl ) {
			#$agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
			$agent = 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0';

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_VERBOSE, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_USERAGENT, $agent);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_URL,$url);
			#$s=curl_exec($ch);
			$s = $this->curl_exec_utf8($ch);
			#if ( !isset($s) or $s=='' or $s==null ) $s = $this->curl_exec($ch);
			if ( !isset($s) or $s=='' or $s==null ) $s = @file_get_contents ( $url ) ;
			return $s ;
		} else { # CURL
			return @file_get_contents ( $url ) ;
		}
	}

}

?>