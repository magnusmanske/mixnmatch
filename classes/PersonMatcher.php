<?php

namespace MixNMatch;

require_once ( '/data/project/mix-n-match/public_html/php/wikidata.php' ) ; # TODO required?
require_once dirname(__DIR__) . '/vendor/autoload.php';

class PersonMatcher {
	public $mnm ;
	private $verbose ;
	private $allow_auto_date_match ;
	private $use_search_for_names ;
	private $dbwd ;
	private $first_name_variants ;
	private $transliterationTable = ['á'=>'a','Á'=>'A','à'=>'a','À'=>'A','ă'=>'a','Ă'=>'A','â'=>'a','Â'=>'A','å'=>'a','Å'=>'A','ã'=>'a','Ã'=>'A','ą'=>'a','Ą'=>'A','ā'=>'a','Ā'=>'A','ä'=>'ae','Ä'=>'AE','æ'=>'ae','Æ'=>'AE','ḃ'=>'b','Ḃ'=>'B','ć'=>'c','Ć'=>'C','ĉ'=>'c','Ĉ'=>'C','č'=>'c','Č'=>'C','ċ'=>'c','Ċ'=>'C','ç'=>'c','Ç'=>'C','ď'=>'d','Ď'=>'D','ḋ'=>'d','Ḋ'=>'D','đ'=>'d','Đ'=>'D','ð'=>'dh','Ð'=>'Dh','é'=>'e','É'=>'E','è'=>'e','È'=>'E','ĕ'=>'e','Ĕ'=>'E','ê'=>'e','Ê'=>'E','ě'=>'e','Ě'=>'E','ë'=>'e','Ë'=>'E','ė'=>'e','Ė'=>'E','ę'=>'e','Ę'=>'E','ē'=>'e','Ē'=>'E','ḟ'=>'f','Ḟ'=>'F','ƒ'=>'f','Ƒ'=>'F','ğ'=>'g','Ğ'=>'G','ĝ'=>'g','Ĝ'=>'G','ġ'=>'g','Ġ'=>'G','ģ'=>'g','Ģ'=>'G','ĥ'=>'h','Ĥ'=>'H','ħ'=>'h','Ħ'=>'H','í'=>'i','Í'=>'I','ì'=>'i','Ì'=>'I','î'=>'i','Î'=>'I','ï'=>'i','Ï'=>'I','ĩ'=>'i','Ĩ'=>'I','į'=>'i','Į'=>'I','ī'=>'i','Ī'=>'I','ĵ'=>'j','Ĵ'=>'J','ķ'=>'k','Ķ'=>'K','ĺ'=>'l','Ĺ'=>'L','ľ'=>'l','Ľ'=>'L','ļ'=>'l','Ļ'=>'L','ł'=>'l','Ł'=>'L','ṁ'=>'m','Ṁ'=>'M','ń'=>'n','Ń'=>'N','ň'=>'n','Ň'=>'N','ñ'=>'n','Ñ'=>'N','ņ'=>'n','Ņ'=>'N','ó'=>'o','Ó'=>'O','ò'=>'o','Ò'=>'O','ô'=>'o','Ô'=>'O','ő'=>'o','Ő'=>'O','õ'=>'o','Õ'=>'O','ø'=>'oe','Ø'=>'OE','ō'=>'o','Ō'=>'O','ơ'=>'o','Ơ'=>'O','ö'=>'oe','Ö'=>'OE','ṗ'=>'p','Ṗ'=>'P','ŕ'=>'r','Ŕ'=>'R','ř'=>'r','Ř'=>'R','ŗ'=>'r','Ŗ'=>'R','ś'=>'s','Ś'=>'S','ŝ'=>'s','Ŝ'=>'S','š'=>'s','Š'=>'S','ṡ'=>'s','Ṡ'=>'S','ş'=>'s','Ş'=>'S','ș'=>'s','Ș'=>'S','ß'=>'SS','ť'=>'t','Ť'=>'T','ṫ'=>'t','Ṫ'=>'T','ţ'=>'t','Ţ'=>'T','ț'=>'t','Ț'=>'T','ŧ'=>'t','Ŧ'=>'T','ú'=>'u','Ú'=>'U','ù'=>'u','Ù'=>'U','ŭ'=>'u','Ŭ'=>'U','û'=>'u','Û'=>'U','ů'=>'u','Ů'=>'U','ű'=>'u','Ű'=>'U','ũ'=>'u','Ũ'=>'U','ų'=>'u','Ų'=>'U','ū'=>'u','Ū'=>'U','ư'=>'u','Ư'=>'U','ü'=>'ue','Ü'=>'UE','ẃ'=>'w','Ẃ'=>'W','ẁ'=>'w','Ẁ'=>'W','ŵ'=>'w','Ŵ'=>'W','ẅ'=>'w','Ẅ'=>'W','ý'=>'y','Ý'=>'Y','ỳ'=>'y','Ỳ'=>'Y','ŷ'=>'y','Ŷ'=>'Y','ÿ'=>'y','Ÿ'=>'Y','ź'=>'z','Ź'=>'Z','ž'=>'z','Ž'=>'Z','ż'=>'z','Ż'=>'Z','þ'=>'th','Þ'=>'Th','µ'=>'u','а'=>'a','А'=>'a','б'=>'b','Б'=>'b','в'=>'v','В'=>'v','г'=>'g','Г'=>'g','д'=>'d','Д'=>'d','е'=>'e','Е'=>'E','ё'=>'e','Ё'=>'E','ж'=>'zh','Ж'=>'zh','з'=>'z','З'=>'z','и'=>'i','И'=>'i','й'=>'j','Й'=>'j','к'=>'k','К'=>'k','л'=>'l','Л'=>'l','м'=>'m','М'=>'m','н'=>'n','Н'=>'n','о'=>'o','О'=>'o','п'=>'p','П'=>'p','р'=>'r','Р'=>'r','с'=>'s','С'=>'s','т'=>'t','Т'=>'t','у'=>'u','У'=>'u','ф'=>'f','Ф'=>'f','х'=>'h','Х'=>'h','ц'=>'c','Ц'=>'c','ч'=>'ch','Ч'=>'ch','ш'=>'sh','Ш'=>'sh','щ'=>'sch','Щ'=>'sch','ъ'=>'','Ъ'=>'','ы'=>'y','Ы'=>'y','ь'=>'','Ь'=>'','э'=>'e','Э'=>'e','ю'=>'ju','Ю'=>'ju','я'=>'ja','Я'=>'ja'];

	# $allow_auto_date_match [bool] : Match over single date pair match (as automatch); might rescue some, but many false positives
	# $use_search_for_names [bool] : Use search function for names; slower, but more resiliant
	function __construct ( $allow_auto_date_match , $use_search_for_names , $verbose , $mnm = '' ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
		$this->verbose = $this->verbose ;
		$this->allow_auto_date_match = $allow_auto_date_match ;
		$this->use_search_for_names = $use_search_for_names ;

		if ( $this->use_search_for_names ) return ;
		$this->loadFirstNameVariants() ;
	}

	public function candidateEntriesGenerator($catalog_id) {
		$catalog_id = ($catalog_id??0)*1 ;
		$sql = "" ;
		if ( $catalog_id != 0 ) {
			$sql = "SELECT * FROM `vw_dates` WHERE `catalog` IN ({$catalog_id}) AND (q IS NULL or user=0) AND born!='' AND died!=''" ;
		} else {
			$sql = "SELECT max(id) AS m FROM entry" ;
			$result = $this->mnm->getSQL ( $sql ) ;
			$o = $result->fetch_object() ;
			$r = (int) ( $o->m * ( rand() / getrandmax() ) );

			$sql = "SELECT entry_id,born,died,ext_name FROM person_dates,entry WHERE entry_id=entry.id AND entry_id>={$r} AND (q IS NULL or q=-1 or user=0) AND born!='' AND died!='' ORDER BY entry_id LIMIT 50000" ;
		}
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) yield $o ;
		yield from [] ;
	}

	public function process_entry ( $o ) {
		if ( !$this->isSaneDate($o->born) ) return ;
		if ( !$this->isSaneDate($o->died) ) return ;

		$born = $this->extractYearFromDate ( $o->born ) ;
		$died = $this->extractYearFromDate ( $o->died ) ;

		if ( $this->verbose ) print "Checking {$o->ext_name} ({$born}-{$died})\n" ;
		$candidate_items_by_name = $this->searchForPeople ( $o ) ;
		if ( $this->verbose and count($candidate_items_by_name)>0) print "Has candidate items\n" ;
		$items = $this->getItemSubsetByYears ( $candidate_items_by_name , $born , $died ) ;
		if ( $this->verbose and count($items)>0) print "Has items\n" ;

		if ( count($items) == 0 ) return ; // No match
		if ( count($items) > 1 ) $this->mnm->addIssue ( $o->entry_id , 'WD_DUPLICATE' , $items ) ;

		$q = $items[0] ;
		if ( !$this->hasHumanLink($q) ) return ;

		if ( $this->verbose ) print "{$o->ext_name} {$this->mnm->root_url}/#/entry/{$o->entry_id} => https://www.wikidata.org/wiki/{$q}\n" ;
		try {
			$status = $this->mnm->setMatchForEntryID ( $o->entry_id , $q , 3 , true , false ) ;
		} catch (Exception $e) {
			$status = false ;
		}
		if ( $this->verbose and !$status ) print "ERROR: {$this->mnm->last_error}\n" ;
	}

	function get_dbwd() {
		if ( !isset($this->dbwd) ) {
			$this->dbwd = $this->mnm->openWikidataDB() ;
		}
		return $this->dbwd ;
	}

	public function searchForPeople ( $o ) {
		if ( $this->use_search_for_names ) {
			return $this->getPeopleBySearch ( $o ) ;
		} else {
			return $this->getCandidateItemsByLabel ( $this->getPossibleNameVariants ( $o->ext_name ) ) ;
		}
	}

	public function hasHumanLink($q) {
		if ( $this->use_search_for_names ) return true ; // P31=Q5 was part of the original query, no need to check again
		$has_human_link = false ;
		$sql = "SELECT * FROM page,pagelinks,linktarget WHERE pl_target_id=lt_id AND page_namespace=0 AND page_title='{$q}' AND pl_from=page_id AND lt_title='Q5' AND lt_namespace=0 LIMIT 1" ;
		$result2 = $this->mnm->tfc->getSQL ( $this->get_dbwd() , $sql ) ;
		while ($x = $result2->fetch_object()) $has_human_link = true ;
		return $has_human_link ;
	}

	public function extractYearFromDate ( $d ) {
		if ( !preg_match ( '/^(\d{3,4})\b/' , $d , $m ) ) return 0 ;
		return $m[1] * 1 ;
	}

	public function isSaneDate ( $d ) {
		if ( $d == '' ) return false ;
		$year = $this->extractYearFromDate ( $d ) ;
		if ( $year == 0 ) return false ;
		if ( $year >= 2025 ) return false ;
		return true ;
	}

	public function getItemSubsetByYears ( $candidate_items , $born , $died ) {
		$ret = [] ;
		if ( count($candidate_items) == 0 ) return $ret ;

		$chunks = array_chunk ( $candidate_items , 100 ) ;

		foreach ( $chunks AS $chunk ) {
			$sparql = "SELECT DISTINCT ?q { VALUES ?q { wd:" . 
				implode(' wd:',$chunk) . 
				" } . ?q wdt:P569 ?born ; wdt:P570 ?died. FILTER ( year(?born)={$born}).FILTER ( year(?died)={$died} ) }" ;
			#FILTER (?born >= '{$born}-01-01T00:00:00Z'^^xsd:dateTime && ?born <= '{$born}-12-31T00:00:00Z'^^xsd:dateTime )
			#FILTER (?died >= '{$died}-01-01T00:00:00Z'^^xsd:dateTime && ?died <= '{$died}-12-31T00:00:00Z'^^xsd:dateTime )
			$ok = true ;
			try {
				$res = $this->mnm->tfc->getSPARQLitems ( $sparql ) ;
				foreach ( $res AS $q ) $ret[$q] = $q ;
			} catch (Exception $e) {
				$ok = false ;
			}
		}

		$ret = array_values($ret);
		return $ret ;
	}


	private function getPeopleBySearch ( $entry ) {
		$ret = [] ;
		$query = $this->mnm->getSimplifiedName ( $this->mnm->sanitizePersonName ( $entry->ext_name ) ) ;
		$results = $this->mnm->getSearchResults ( $query , 'P31' , 'Q5' ) ;
		foreach ( $results AS $r ) $ret[] = $r->title ;
		return $ret ;
	}

	private function getCandidateItemsByLabel ( $names ) {
		$ret = [] ;
		if ( count($names) == 0 ) return $ret ;
		$sql = [] ;
		$likes = [] ;
		$dbwd = $this->get_dbwd() ;
		foreach ( $names AS $name ) {
			$escaped_name = $dbwd->real_escape_string ( $name ) ;
			$sql[] = $escaped_name ;
			if ( preg_match ( '/%/' , $escaped_name ) ) $likes[] = $escaped_name ;
		}
		$sql = "SELECT DISTINCT concat('Q',wbit_item_id) FROM wbt_text,wbt_item_terms,wbt_type,wbt_term_in_lang,wbt_text_in_lang WHERE wbit_term_in_lang_id = wbtl_id AND wbtl_type_id = wby_id AND wbtl_text_in_lang_id = wbxl_id AND wbxl_text_id = wbx_id AND wby_name IN ('label','alias') AND (wbx_text IN ('" . implode("','",$sql) . "')" ;
		if ( count($likes) > 0 ) {
			foreach ( $likes AS $like ) $sql .= " OR (wbx_text LIKE '$like')" ;
		}
		$sql .= ") AND EXISTS (SELECT * FROM page,pagelinks,linktarget WHERE pl_target_id=lt_id AND page_namespace=0 AND page_title=concat('Q',wbit_item_id) AND pl_from=page_id AND lt_title='Q5' AND lt_namespace=0)" ;
		$result = $this->mnm->tfc->getSQL ( $dbwd , $sql ) ;
		while($o = $result->fetch_object()){
			$ret[] = $o->term_full_entity_id ;
		}
		return $ret ;
	}

	private function loadFirstNameVariants () {
		$this->first_name_variants = [] ;
		$sparql = 'SELECT ?nameLabel (GROUP_CONCAT(DISTINCT ?translit; separator="|") as ?trans) { ?name wdt:P31/wdt:P279* wd:Q202444. ?name wdt:P2440 ?translit . SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". } } GROUP BY ?nameLabel' ;
		$j = $this->mnm->tfc->getSPARQL ( $sparql ) ;
		foreach ( $j->results->bindings AS $b ) {
			$row = explode ( '|' , $b->trans->value ) ;
			$row[] = $b->nameLabel->value ;
			$a = [] ;
			foreach ( $row AS $r ) {
				$a[] = $r ;
				$nvs = $this->getPossibleNameVariants ( $r ) ;
				foreach ( $nvs AS $nv ) $a[] = $nv ;
			}
			$a = array_unique ( $a ) ;
			$this->first_name_variants[] = $a ;
		}
	}

	private function getPossibleNameVariants ( $name ) {
		$name = preg_replace ( '/\b(Mmle|pseud\.|diverses)\b/' , '' , $name ) ;
		$name = preg_replace ( '/ Bt$/' , ' Baronet' , $name ) ;

		$names = array ( $name ) ;
		if ( preg_match ( '/^(\S+) (\S)\. (\S+)$/' , $name , $m ) ) $names[] = $m[1] . ' ' . $m[2] . '% ' . $m[3] ;

		$names[] = getSimplifiedName ( $name ) ;

		$name = preg_replace ( '/\s[A-Z]\.{0,1}\s/' , ' ' , $name ) ; # Single letter
		$name = preg_replace ( '/^[A-Z]\.{0,1}\s/' , ' ' , $name ) ; # Single letter
		$name = trim ( preg_replace ( '/\s+/' , ' ' , $name ) ) ;
		
		$names[] = $name ;
		
		$n = preg_replace ( '/^(jhr|jkvr)\.\s+/' , '' , $name ) ;
		$n = preg_replace ( '/\s*\[(sr|jr)\.*\]$/' , '' , $name ) ;

		if ( preg_match ( '/^Sir /' , $name ) ) {
			$n = preg_replace ( '/^Sir /' , '' , $name ) ;
			$n = preg_replace ( '/\s*\(.+\)/' , '' , $n ) ;
			$names[] = $n ;
		} else if ( preg_match ( '/^(\w+)\s(\w+)\s(\w+)$/' , $name , $m ) ) {
			$names[] = $m[1] . ' ' . $m[3] ;
			$names[] = $m[2] . ' ' . $m[3] ;
		}
		if ( preg_match ( '/^(Baron|Baronesse{0,1}|Graf|Gräfin) (.+)$/' , $name , $m ) ) {
			$names[] = $m[1] ;
		}

		if ( preg_match ( '/^(.+) de (.+)$/' , $name , $m ) ) $names[] = $m[1].' '.$m[2] ;
		if ( preg_match ( '/^(.+) de la (.+)$/' , $name , $m ) ) $names[] = $m[1].' '.$m[2] ;
		if ( preg_match ( '/^(.+) of (.+)$/' , $name , $m ) ) $names[] = $m[1].' '.$m[2] ;
		if ( preg_match ( '/^(.+) von (.+)$/' , $name , $m ) ) $names[] = $m[1].' '.$m[2] ;
		if ( preg_match ( '/^(.+)[ ,]+[sj]r\.{0,1}$/i' , $name , $m ) ) $names[] = $m[1] ;
		if ( preg_match ( '/^(.+)[ ,]+I+\.{0,1}$/i' , $name , $m ) ) $names[] = $m[1] ;

		$n2 = preg_replace ( '/^([A-Z]\.\s*|[A-Z][a-z]\.\s*)+$/' , '' , $name ) ;
		if ( $n2 != $name ) $names[] = $n2 ;
		
		foreach ( $names AS $a => $b ) $names[$a] = str_replace ( '"' , '' , $b ) ; // Quote paranoia

		foreach ( $names AS $a => $b ) { // Dash
			if ( !preg_match ( '/^(.+)-(.+)$/' , $b , $m ) ) continue ;
			$names[] = $m[1] . ' ' . $m[2] ;
		}

		foreach ( $names AS $a => $b ) { // Middle initial
			if ( !preg_match ( '/^(.+) [A-Z]\. (.+)$/' , $b , $m ) ) continue ;
			$names[] = $m[1] . ' ' . $m[2] ;
		}

		foreach ( $names AS $a => $b ) {
			foreach ( $this->first_name_variants AS $row ) {
				foreach ( $row AS $v1 ) {
					foreach ( $row AS $v2 ) {
						if ( $v1 == $v2 ) continue ;
						$p1 = '/^(.*)\s*\b'.$v1.'\b\s*(.*)$/' ;
						$p2 = '/^(.*)\s*\b'.$v2.'\b\s*(.*)$/' ;
						if ( preg_match ( $p1 , $b , $m ) ) $names[] = trim ( $m[1] . ' ' . $v2 . ' ' . $m[2] ) ;
						if ( preg_match ( $p2 , $b , $m ) ) $names[] = trim ( $m[1] . ' ' . $v1 . ' ' . $m[2] ) ;
					}
				}
			}
		}

		foreach ( $names AS $a => $b ) { // English "normalization"
			$name = $b ;
			foreach ( $this->transliterationTable AS $tk => $tv ) $name = str_replace ( $tk , $tv , $name ) ;
			if ( $b == $name ) continue ;
			$name = ucwords ( $name ) ;
			$names[] = $name ;
		}

		foreach ( $names AS $a => $b ) {
			$n = ucwords ( strtolower ( $b ) ) ;
			if ( $n != $b ) $names[] = $n ;
		}

		return array_unique ( $names ) ;
	}


} ;

?>