<?PHP

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class User extends MnMbase {
	protected $data ;
	protected $data_loaded = false ;
	protected $mnm ;
	protected static $user_cache = [] ;
	protected $min_days_since_user_registration = 14;

	/**
	 * Constructor
	 *
	*/
	public function __construct ( /*int*/ $id = 0 , $mnm = '' ) {
		$this->id = intval($id) ;
		$this->data = (object) [] ;
		if ( $mnm != '' ) $this->setMnM ( $mnm ) ;
	}

	public function setFromName ( $user_name ) {
		$uid = $this->getOrCreateUserID ( $user_name ) ;
		$this->loadData ( true ) ;
	}

	/*
	public function isUsernameBlocked ( $user_name ) {
		$uid = $this->getOrCreateUserID ( $user_name ) ;
		if ( $uid == -1 ) return false ; // Paranoia
		$user_key = self::sanitizeUserName ( $user_name ) ;
		if ( !isset(self::$user_cache[$user_key]) ) return false ; // Paranoia
		if ( time() - self::$user_cache[$user_key]['last_block_check'] > 60*15 ) { // Cache block check for 15 min
			self::$user_cache[$user_key]['is_blocked'] = self::checkUserBlocked ( $user_name ) ;
		}
		return self::$user_cache[$user_key]['is_blocked'] ;
	}
	*/

	public function isBlocked () {
		$this->checkValidID() ;
		$this->loadData() ;
		$user_key = self::sanitizeUserName ( $this->data->name ) ;
		if ( !isset(self::$user_cache[$user_key]) ) return false ; // Paranoia
		if ( time() - self::$user_cache[$user_key]['last_block_check'] > 60*15 ) { // Cache block check for 15 min
			self::$user_cache[$user_key]['is_blocked'] = self::checkUserBlocked ( $this->data->name ) ;
		}
		return self::$user_cache[$user_key]['is_blocked'] ;
	}

	public function getOrCreateUserID ( $user_name_original ) {
		$this->checkMnM() ;
		$user_key = self::sanitizeUserName ( $user_name_original ) ;
		if ( isset(self::$user_cache[$user_key]) ) return self::$user_cache[$user_key]['id'] ;

		$user_name = $this->escape($user_name_original) ;
		$last_block_check = time() ;
		$sql = "INSERT IGNORE `user` (`name`,`last_block_check`) VALUES ('{$user_name}','{$last_block_check}')" ;
		$this->getSQL ( $sql ) ;
		$user_id = -1 ;
		$sql = "SELECT * FROM `user` WHERE `name`='{$user_name}'" ;
		$result = $this->getSQL ( $sql ) ;
		if ($o = $result->fetch_object()) {
			$user_id = $o->id ;
			self::$user_cache[$user_key] = [
				"id" => $o->id ,
				"last_block_check" => $o->last_block_check ,
				"is_blocked" => self::checkUserBlocked ( $user_name_original )
			] ;
		}
		return $user_id ;
	}

	public static function checkUserBlocked ( $user_name ) {
		$url = "https://www.wikidata.org/w/api.php?action=query&list=users&ususers=".urlencode($user_name)."&usprop=blockinfo|registration&format=json" ;
		$j = json_decode ( file_get_contents ( $url ) ) ;

		# Check if user is blocked
		if ( isset ( $j->query->users[0]->blockid ) ) return true ;

		# Check if user is too new (sockpuppets)
		$ts = strtotime($j->query->users[0]->registration);
		$diff = time()-$ts;
		$diff_days = round($diff / (60 * 60 * 24));
		if ( $diff_days < $this->min_days_since_user_registration ) return true;

		# All good
		return false ;
	}

	public static function isAutoMatchUser ( $user_id ) {
		if ( $user_id == 0 ) return true ;
		if ( $user_id == 3 ) return true ;
		if ( $user_id == 4 ) return true ;
		return false ;
	}

	protected static function sanitizeUserName ( $user_name ) {
		return str_replace ( ' ' , '_' , trim($user_name) ) ;
	}

	protected function loadData ( $force = false ) {
		if ( $this->data_loaded and !$force ) return ;
		$this->checkValidID() ;
		$sql = "SELECT * FROM `user` WHERE `id`={$this->id}" ;
		$result = $this->getSQL ( $sql ) ;
		if($o = $result->fetch_object()){
			$this->data = $o ;
			$this->data_loaded = true ;
		} else {
			throw new Exception(__METHOD__.": Error retrieving user {$this->id} from database" ) ; 
		}
	}

}
