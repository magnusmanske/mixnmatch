<?PHP

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class UpdateCatalog {
	public $mnm ;
	public $default_aux = [] ;
	protected $catalog_id ;
	protected $update_info ;
	protected $update_existing_description ;
	protected $update_all_descriptions ;
	protected $add_new_entries ;
	protected $catalog_is_empty ;
	protected $total_existing_entries ;
	protected $entries_are_cached ;
	protected $default_type = '' ;
	protected $url_pattern = '' ;
	protected $entry_cache ;
	protected $has_born_died = false ;
	protected $testing = false ;
	protected $test_output = true ;
	protected $max_cache_size = 50000 ;
	protected $import_file_path = '/data/project/mix-n-match/import_files' ;

	function __construct( int $catalog , $mnm = '' , $update_info = '' ) {
		$this->mnm = $mnm=='' ? new MixNMatch : $mnm ;
		$this->catalog_id = $catalog * 1 ;
		if ( $update_info != '' ) {
			$this->update_info = $update_info ;
			$this->check_update_info() ;
		} else {
			$this->load_update_info() ;
		}
	}

	public function setTesting ( $testing = true , $test_output = true ) {
		$this->testing = $testing ;
		$this->test_output = $test_output ;
	}

	public function catalog_id() : int {
		return $this->catalog_id ;
	}

	# Manually set URL pattern
	public function set_url_pattern ( string $url_pattern ) {
		$this->url_pattern = $url_pattern ;
	}

	public function run_updates() {
		if ( $this->testing ) return ;
		$this->mnm->queue_job($this->catalog_id(),'microsync');
		$this->mnm->queue_job($this->catalog_id(),'automatch_by_search');
		if ( $this->has_born_died ) $this->mnm->queue_job($this->catalog_id(),'match_person_dates');
	}

	protected function read_from_tsv($file) {
		return $this->read_from_sv ( $file , "\t" ) ;
	}

	protected function read_from_csv($file) {
		return $this->read_from_sv ( $file , ',' ) ;
	}

	protected function read_from_sv($file,$delimiter) {
		if(!ini_set('default_socket_timeout', 15)) throw new \Exception(__METHOD__.": unable to change socket timeout" ) ; 

		if (($handle = fopen($file, "r")) !== FALSE) {
			try {
			    while (($data = fgetcsv($handle, 10000, $delimiter)) !== FALSE) {
			        yield $data;
			    }
			} finally {
			    fclose($handle);
			}
		}
		else throw new \Exception(__METHOD__.": Problem opening {$file}");
	}

	protected function get_source_type () {
		if ( isset($this->update_info->data_format) ) return strtoupper($this->update_info->data_format);
		if ( isset($this->update_info->file_uuid) ) {
			$uuid = $this->mnm->escape($this->update_info->file_uuid) ;
			$sql = "SELECT * FROM `import_file` WHERE `uuid`='{$uuid}'" ;
			$result = $this->mnm->getSQL ( $sql ) ;
			if ($o = $result->fetch_object()) return strtoupper($o->type) ;
		}
		return 'UNKNOWN' ;
	}

	protected function get_source_location () {
		if ( isset($this->update_info->source_url) ) {
			return $this->update_info->source_url ;
		}
		if ( isset($this->update_info->file_uuid) ) {
			return $this->import_file_path($this->update_info->file_uuid);
		}
		throw new \Exception(__METHOD__.": Unknown source location: ".json_encode($this->update_info) ) ; 
	}

	public function import_file_path ( $uuid ) {
		return "{$this->import_file_path}/{$uuid}" ;
	}

	# Returns an array of a row, one cell per column
	protected function read_from_datasource() {
		$source_type = $this->get_source_type() ;
		$source_location = $this->get_source_location() ;
		switch ( $source_type ) {
			case 'TSV':
				foreach ($this->read_from_tsv($source_location) as $n => $line) yield $line ;
				break ;
			case 'CSV':
				foreach ($this->read_from_csv($source_location) as $n => $line) yield $line ;
				break ;
			default:
				throw new \Exception(__METHOD__.": Unknown source type '{$source_type}'" ) ; 
		}
	}

	public function get_header_row () {
		foreach ( $this->read_from_datasource() as $row_id => $cols ) {
			return $cols ;
		}
		throw new \Exception(__METHOD__.": No header row found" ) ; 
	}

	private function parse_single_row ( $cols , $colmap , $min_cols , $patterns , $just_add , $line_counter , $row_id ) {
		foreach ( $cols AS $k => $v ) $cols[$k] = trim($v) ;
		if ( count($cols) < $min_cols ) throw new \Exception(__METHOD__.": Less than {$min_cols} in row {$row_id}" ) ;
		if ( !isset($cols[$colmap->id]) ) throw new \Exception(__METHOD__.": ID not defined in column header" ) ;
		if ( !isset($cols[$colmap->name]) ) throw new \Exception(__METHOD__.": Name not defined in column header" ) ;

		# Parse parts
		$column_properties = $this->parse_column_properties($colmap,$cols) ;
		foreach ( $this->default_aux AS $prop_value ) $column_properties->aux[] = $prop_value ;
		$entry = (object) [
			'catalog' => $this->catalog_id ,
			'id' => $cols[$colmap->id] ,
			'name' => $cols[$colmap->name] ,
			'desc' => isset($colmap->desc) ? (isset($cols[$colmap->desc])?$cols[$colmap->desc]:'') : '' ,
			'url' => isset($colmap->url) ? (isset($cols[$colmap->url])?$cols[$colmap->url]:'') : ''  ,
			'type' => isset($colmap->type) ? (isset($cols[$colmap->type])?$cols[$colmap->type]:$this->default_type) : $this->default_type ,
			'born' => isset($colmap->born)?(isset($cols[$colmap->born])?$cols[$colmap->born]:''):'' ,
			'died' => isset($colmap->died)?(isset($cols[$colmap->died])?$cols[$colmap->died]:''):'' ,
			'aux' => $column_properties->aux
		] ;
		// if ( $entry->born==$entry->died ) $entry->died = ''; # Paranoia
		if ( !isset($entry->id) or $entry->id=='' ) throw new \Exception(__METHOD__.": No ID given in row {$line_counter->all}" ) ;
		if ( !isset($entry->name) or $entry->name=='' ) throw new \Exception(__METHOD__.": No name given in row {$line_counter->all}" ) ;
		if ( !preg_match('|^Q\d+$|',$entry->type) ) $entry->type = '' ; # Paranoia
		if ( !isset($entry->url) or $entry->url == '' ) $entry->url = str_replace ( '$1' , $entry->id , $this->url_pattern ) ;
		if ( isset($colmap->q) and isset($cols[$colmap->q]) and $cols[$colmap->q]!='' ) $entry->q = $cols[$colmap->q] ;
		if ( isset($patterns->born) ) $entry->born = $this->parse_date($patterns->born,$cols);
		if ( isset($patterns->died) ) $entry->died = $this->parse_date($patterns->died,$cols);
		foreach ( $patterns AS $prop => $pattern ) $this->parse_prop ( $prop , $pattern , $cols , $entry->aux ) ;

		if ( $entry->id=='' ) {
			print "No ext_id for " . json_encode($cols) . "\n" ;
			return;
		}

		if ( $entry->born.$entry->died != '' ) $this->has_born_died = true ;

		$entry_id = '' ;
		if ( !$just_add ) {
			$existing = $this->get_entry_with_id ( $entry->id ) ;
			if ( !isset($existing) ) $just_add = true ;
			else $entry_id = $existing->id ;
		}
		if ( $just_add ) { # New entry
			if ( !$this->add_new_entries ) return ; # No adding new entries
			#print "Adding {$entry->id}\n" ;
			$line_counter->added++ ;
			if ( $this->testing ) $this->log ( "Adding: " . json_encode($entry) ) ;
			else {
				try {
					$entry_id = $this->mnm->addNewEntry ( $entry ) ;
				} catch(Exception $e) {
					echo __METHOD__.': Caught exception: ',  $e->getMessage(), "\n";
					return ;
				}
			}
		} else { # Entry exists
			$has_updated = false ;
			# Description
			if ( $this->update_existing_description and $entry->desc != '' and $existing->ext_desc != $entry->desc ) {
				if ( ( $this->update_all_descriptions and $existing->ext_desc != $entry->desc ) or $existing->ext_desc == '' ) {
					if ( $this->testing ) $this->log ( "Setting description for {$existing->id} to '{$entry->desc}'" ) ;
					else $this->mnm->setDescriptionForEntryID ( $existing->id , $entry->desc ) ;
					$has_updated = true ;
				}
			}

			# Person dates
			if ( $entry->born.$entry->died != '' ) {
				if ( $this->testing ) $this->log ( "Dates: {$entry->id} => {$entry->born}/{$entry->died}" ) ;
				else $this->mnm->setPersonDates ( $existing->id , $entry->born , $entry->died ) ;
				$has_updated = true ;
			}

			# Aux data
			foreach ( $entry->aux AS $a ) {
				if ( $this->testing ) $this->log ( "Aux: {$existing->id} => {$a[0]}/{$a[1]}" ) ;
				else $this->mnm->setAux ( $existing->id , $a[0] , $a[1] ) ;
				$has_updated = true ;
			}

			if ( $has_updated ) $line_counter->updated++ ;
		}

		foreach ( $column_properties->aliases AS $language => $text ) {
			if ( $this->testing ) $this->log ( "Alias: {$entry_id} => {$language}/{$text}" ) ;
			else $this->mnm->setAlias ( $entry_id , $text , $language ) ;
		}

		foreach ( $column_properties->descriptions AS $language => $text ) {
			if ( $this->testing ) $this->log ( "Description: {$entry_id} => {$language}/{$text}" ) ;
			else $this->mnm->setLanguageDescription ( $entry_id , $text , $language ) ;
		}

		if ( isset($column_properties->location->lat) ) {
			if ( $this->testing ) $this->log ( "Location: {$entry_id} => ".json_encode($column_properties->location) ) ;
			else $this->mnm->setLocation ( $entry_id , $column_properties->location->lat , $column_properties->location->lon ) ;
		}

		if ( isset($colmap->autoq) and isset($cols[$colmap->autoq]) and $cols[$colmap->autoq]!='' ) {
			if ( $this->testing ) $this->log ( "Automatch: {$entry_id} => {$cols[$colmap->autoq]}" ) ;
			else $this->mnm->setMatchForEntryID ( $entry_id , $cols[$colmap->autoq] , 0 , true , false ) ;
		}
	}

	# Valid columns: id(mandatory), name(mandatory), desc, url, type, q(reliable match), autoq(guess), Pxxx, Lxx, Dxx, Axx
	# Returns object with line counts
	public function update_catalog () {
		# Init
		$columns = $this->update_info->columns??[] ;
		$num_header_rows = $this->update_info->num_header_rows??0 ;
		$min_cols = $this->update_info->min_cols??-1 ;
		$patterns = $this->update_info->patterns??[] ;

		if ( is_array($patterns) ) $patterns = (object) $patterns ;
		if ( $min_cols == -1 ) $min_cols = count($columns) ;
		$colmap = (object) [] ;
		foreach ( $columns as $num => $col ) {
			$col = trim($col) ;
			if ( $col != '' ) $colmap->$col = $num ;
		}
		foreach ( ['id','name'] as $k ) {
			if ( !isset($colmap->$k) ) throw new \Exception(__METHOD__.": No columns for '{$k}'" ) ; 
		}
		$just_add = $this->catalog_is_empty || ($this->update_info->just_add??false) ;
		$line_counter = (object) ['all'=>0 , 'added'=>0 , 'updated'=>0 ] ;
		$skip_first = $num_header_rows ;
		if ( isset($this->update_info->skip_first_rows) ) $skip_first += $this->update_info->skip_first_rows*1 ;
		foreach ( $this->read_from_datasource() as $row_id => $cols ) {
			if ( count($cols) == 0 or trim(implode('',$cols)) == '' ) continue ; # Blank row, ignore
			$line_counter->all++ ;
			if ( $line_counter->all <= $skip_first ) continue ;
			#if ( $line_counter->all == $skip_first+1 ) print "Starting in anger\n" ;
			if ( isset($this->update_info->read_max_rows) && $line_counter->all >= $this->update_info->read_max_rows ) break ;

			if ( $this->update_info->ignore_errors??false ) {
				try {
					$this->parse_single_row ( $cols , $colmap , $min_cols , $patterns , $just_add , $line_counter , $row_id ) ;
				} catch (\Exception $e) {
					echo __METHOD__.': Caught exception: ',  $e->getMessage(), "\n";
				}
			} else {
				$this->parse_single_row ( $cols , $colmap , $min_cols , $patterns , $just_add , $line_counter , $row_id ) ;
			}
		}
		return $line_counter ;
	}

	public function store_update_info ( $user_id , $note = '' ) {
		$user_id *= 1 ;
		$note = $this->mnm->escape($note);
		if ( $user_id <= 0 ) throw new \Exception(__METHOD__.": Can't update storage info for {$this->catalog_id}: user ID is '{$user_id}'" ) ;
		$sql = "UPDATE `update_info` SET `is_current`=0 WHERE `catalog`={$this->catalog_id} AND `is_current`=1" ;
		$this->mnm->getSQL ( $sql ) ;
		$sql = "INSERT INTO `update_info` (`catalog`,`json`,`note`,`user_id`,`is_current`) VALUES ({$this->catalog_id},'" .
			json_encode($this->update_info) .
			"','{$note}',{$user_id},1)" ;
		$this->mnm->getSQL ( $sql ) ;
	}

	protected function log ( $message ) {
		if ( $this->test_output ) print "{$message}\n" ;
	}

	protected function load_update_info () {
		$sql = "SELECT * FROM `update_info` WHERE `catalog`={$this->catalog_id} AND `is_current`=1 LIMIT 1" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		if ($o = $result->fetch_object()) $this->update_info = json_decode($o->json) ;
		$this->check_update_info();
	}

	protected function check_update_info () {
		if ( !isset($this->update_info) ) throw new \Exception(__METHOD__.": No update info for catalog {$this->catalog_id}" ) ; 
		if ( $this->update_info->url_pattern??''!='' ) $this->set_url_pattern($this->update_info->url_pattern);
		$this->update_existing_description = $this->update_info->update_existing_description??false ;
		$this->update_all_descriptions = $this->update_info->update_all_descriptions??false ;
		$this->add_new_entries = $this->update_info->add_new_entries??true ;
		$this->default_type = $this->update_info->default_type??'' ;
		$this->default_aux = $this->update_info->default_aux??[] ;
		$this->catalog_stats();
	}

	protected function parse_column_properties($colmap,$cols) {
		$ret = (object)['aux'=>[],'aliases'=>[],'descriptions'=>[],'location'=>(object)[]] ;
		foreach ( $colmap AS $k => $colnum ) {
			if ( !isset($cols[$colnum]) ) $v = '' ;
			else $v = $cols[$colnum] ;
			if ( preg_match('|^D([a-z]+)$|',$k,$m) ) {
				if ( $v != '' ) $ret->descriptions[$m[1]] = $v ;
			} else if ( preg_match('|^L([a-z]+)$|',$k,$m) ) {
				if ( $v != '' ) $ret->aliases[$m[1]] = $v ;
			} else if ( preg_match('|^P(\d+)$|',$k,$m) ) { # TODO load property from WD, act on type rather than 625
				$p_numeric = $m[1] ;
				if ( preg_match('|^\s*POINT\s*\(\s*(\S+?)[, ](\S+?)\s*\)\s*$|',$v,$n) ) $v = "{$n[2]}/{$n[1]}" ;
				if ( $p_numeric==625 and preg_match('|^(\S+)/(\S+)$|',$v,$n) ) $ret->location = (object)['lat'=>$n[1],'lon'=>$n[2]] ;
				else if ( $v != '' ) $ret->aux[] = [ $k , $v ] ;
			}
		}
		return $ret ;
	}

	protected function parse_prop($prop,$p,$cols,&$aux) {
		if ( !preg_match('|^P\d+$|',$prop) ) return ;
		if ( !isset($p->col) ) return ;
		if ( !isset($p->pattern) ) return ;
		if ( !isset($cols[$p->col]) ) return ;
		if ( !preg_match($p->pattern,$cols[$p->col],$m) ) return ;
		$aux[] = [$prop,$m[1]] ;
	}

	protected function parse_date($p,$cols) {
		if ( !isset($p->col) ) return '' ; #'No col' ;
		if ( !isset($cols[$p->col]) ) return '' ; #"No cols[{$p->col}]" ;
		if ( !isset($p->pattern) ) return '' ; #'No pattern' ;
		if ( !preg_match ( $p->pattern , $cols[$p->col] , $m ) ) return '' ; #"No match for '{$p->pattern}'" ;
		return $m[1] ;

	}

	protected function catalog_stats() {
		$this->total_existing_entries = 0 ;
		$this->catalog_is_empty = false ;
		$sql = "SELECT count(*) AS cnt FROM entry WHERE catalog={$this->catalog_id}" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$this->total_existing_entries = $o->cnt ;
		}
		$this->catalog_is_empty = $this->total_existing_entries==0 ;
		$this->entries_are_cached = $this->total_existing_entries < $this->max_cache_size ;

		# Load URL pattern from property, if none is given
		$sql = "SELECT * FROM catalog WHERE id={$this->catalog_id}" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $cat = $o ;
		if ( isset($cat) and isset($cat->wd_prop) and !isset($cat->wd_qual) and $this->url_pattern=='' ) {
			$p = 'P'.$cat->wd_prop ;
			$wil = new \WikidataItemList ;
			$wil->loadItems ( [ $p ] ) ;
			$i = $wil->getItem ( $p ) ;
			if ( isset($i) ) $this->set_url_pattern ( $i->getFirstString ( 'P1630' ) ) ;
		}

		# Cache entries if possible
		$this->entry_cache = [] ;
		if ( $this->entries_are_cached ) {
			$sql = "SELECT * FROM entry WHERE catalog={$this->catalog_id}" ;
			$result = $this->mnm->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) $this->entry_cache[$o->ext_id] = $o ;
		}
	}

	protected function get_entry_with_id ( $id ) {
		if ( $this->entries_are_cached ) {
			if ( isset($this->entry_cache[$id]) ) return $this->entry_cache[$id];
		} else {
			$entry = new Entry ( 0 , $this->mnm ) ;
			try {
				$entry->setFromExternalID ( $this->catalog_id , $id ) ;
				return $entry->core_data() ;
			} catch (Exception $e) {
				# Ignore
			} 
		}
	}
}

?>