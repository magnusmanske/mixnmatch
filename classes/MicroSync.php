<?PHP

namespace MixNMatch ;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class MicroSync {
	public $mnm ;
	public $qs = [] ;
	protected $ext_cache = [] ;
	protected $logs = [] ;
	protected $last_catalog ;

	function __construct ( $mnm = '' ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
	}

	public function init_catalogs () {
		$sql_ignore_catalogs = " AND id NOT IN (506)" ; # Hard ignore, because data problem in source
		$this->catalogs = [] ;
		$sql = "SELECT * FROM catalog WHERE `active`=1" ;
		$sql .= $sql_ignore_catalogs ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $this->catalogs[$o->id] = $o ;

		# Check all catalogs with property
		$todo = [] ;
		$sql = "SELECT * FROM catalog WHERE `active`=1 AND wd_prop is not null and wd_qual is null" ;
		$sql .= $sql_ignore_catalogs ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $todo[$o->id] = $o->wd_prop ;
		return $todo ;
	}

	public function get_random_catalog () {
		return $this->catalogs[array_rand($this->catalogs)]->id ;
	}

	public function finalize ($specific_catalog) {
		# Run QS
		if ( $this->last_catalog != 150 ) {
			$msg = 'mixnmatch:microsync' ;
			if ( $specific_catalog ) $msg .= ' from catalog '.$this->last_catalog ;
			$this->mnm->tfc->getQS($msg,'',true) ;
		}
		$this->updateWikidataReports() ;
	}

	protected function getFullyMatchedEntryIdBatches ( $catalog_id , $batch_size = 5000 ) {
		$catalog_id *= 1 ;
		$entry_id2q = [] ;
		$sql = "SELECT `id`,`q` FROM `entry` WHERE `catalog`={$catalog_id} AND q IS NOT NULL AND user>0" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$entry_id2q["{$o->id}"] = "Q{$o->q}" ;
			if ( count($entry_id2q) < $batch_size ) continue ;
			yield $entry_id2q ;
			$entry_id2q = [] ;
		}
		if ( count($entry_id2q)>0 ) yield $entry_id2q ;
		yield from [] ; # TO make sure it returns something
	}

	public function fixRedirects ( $catalog_id ) {
		# Get items in $unique_qs that were redirected
		$dbwd = $this->mnm->tfc->openDB ( 'wikidata' , 'wikidata' , true ) ;
		foreach ( $this->getFullyMatchedEntryIdBatches($catalog_id) AS $unique_qs ) {
			$sql = "SELECT page_title,rd_title FROM `page`,`redirect` WHERE `page_id`=`rd_from` AND `rd_namespace`=0 AND `page_is_redirect`=1 AND `page_namespace`=0 AND `page_title` IN ('".implode("','",$unique_qs)."')" ;
			$result = $this->mnm->tfc->getSQL ( $dbwd , $sql ) ;
			while($o = $result->fetch_object()) {
				$old_q = preg_replace('|\D|','',$o->page_title) * 1 ;
				$new_q = preg_replace('|\D|','',$o->rd_title) * 1 ;
				if ( $old_q <= 0 or $new_q <= 0 ) continue ; # Paranoia
				$sql = "UPDATE entry SET q={$new_q} WHERE `catalog`={$catalog_id} AND `q`={$old_q}" ;
				$this->mnm->getSQL ( $sql ) ;
			}
		}
	}

	public function fixDeletedItems ( $catalog_id ) {
		# Get items in $unique_qs that were deleted
		$dbwd = $this->mnm->tfc->openDB ( 'wikidata' , 'wikidata' , true ) ;
		$deleted_items = [] ;
		foreach ( $this->getFullyMatchedEntryIdBatches($catalog_id) AS $unique_qs ) {
			$found_qs = [] ;
			$sql = "SELECT page_title FROM `page` WHERE `page_namespace`=0 AND `page_title` IN ('".implode("','",$unique_qs)."')" ;
			$result = $this->mnm->tfc->getSQL ( $dbwd , $sql ) ;
			while($o = $result->fetch_object()) $found_qs[$o->page_title] = true ;
			foreach ( $unique_qs AS $ext_id => $q ) {
				if ( $q=='Q0' or isset($found_qs[$q]) ) continue ;
				# print "https://www.wikidata.org/wiki/{$q} was deleted\n" ;
				$deleted_items[$q] = preg_replace('|\D|','',$q)*1 ;
			}
		}
		foreach ( $deleted_items AS $q ) $this->mnm->removeMatchesToItem ( $q , 4 , true , true ) ;
	}

	protected function getItemAndExternalIdForPropertyFromSPARQL ( $prop , $batch_size = 5000 ) {
		$ret = [] ;
		$case_insensitive = in_array ( $prop , Auxiliary::$props_also_use_lowercase ) ;
		$sparql = "SELECT ?item ?value { ?item wdt:P{$prop} ?value }" ; #  ORDER BY ?item
		foreach ( $this->mnm->tfc->getSPARQL_TSV ( $sparql ) as $row ) {
			$q = preg_replace('|^.+/Q|','',"{$row['item']}")*1 ;
			$value = $row['value'] ;
			if ( $case_insensitive ) $value = strtolower($value) ;
			$ret[] = [$q,$value] ; # q(numeric), ext_id
			if ( count($ret)<$batch_size ) continue ;
			yield $ret ;
			$ret = [] ;
		}
		if ( count($ret)>0 ) yield $ret ;
		else yield from [] ;
	}

	protected function getEntriesForExtIds ( $catalog , $prop , $ext_ids ) {
		$ret = [] ;
		$ext_ids_escaped = array_unique(array_map(function($id){return $this->mnm->escape($id);},$ext_ids));
		$ext_ids_chunked = array_chunk($ext_ids_escaped,1000);
		$property_is_case_insensitive = in_array ( $prop , Auxiliary::$props_also_use_lowercase ) ;
		foreach ( $ext_ids_chunked AS $ext_ids_escaped ) {
			$sql = "SELECT * FROM `entry` WHERE `catalog`={$catalog} AND `ext_id` IN ('".implode("','",$ext_ids_escaped)."')" ;
			$result = $this->mnm->getSQL ( $sql ) ;
			while($o = $result->fetch_object()){
				if ( $property_is_case_insensitive ) $o->ext_id = strtolower($o->ext_id) ;
				$ret[$o->ext_id] = $o ;
			}
		}
		return $ret ;
	}

	public function checkCatalog ( $catalog , $prop ) {
		$catalog *= 1 ;
		if ( $catalog <= 0 ) return ; # Paranoia
		$this->last_catalog = $catalog ;
		$prop = preg_replace('|\D|','',"{$prop}")*1 ;
		if ( $prop <= 0 ) return ; # Paranoia
		$this->fixRedirects($catalog);
		$this->fixDeletedItems($catalog) ;

		$issues = [ 'extid_not_in_mnm'=>[] , 'item_differs'=>[] , 'multiple_q_in_mnm'=>[] , 'multiple_extid_in_wd'=>[] ] ;

		# Multiple items with the same external ID in WD
		# TODO: lcase?
		$sparql = "SELECT ?extid (count(?q) AS ?cnt) (GROUP_CONCAT(?q; SEPARATOR = '|') AS ?items) { ?q wdt:P{$prop} ?extid } GROUP BY ?extid HAVING (?cnt>1)" ;
		foreach ( $this->mnm->tfc->getSPARQL_TSV ( $sparql ) as $row ) {
			$items = [] ;
			foreach ( explode('|',$row['items']) AS $item ) $items[] = preg_replace('|\D|','',"{$item}")*1 ;
			$issues['multiple_extid_in_wd'][] = [ 'ext_id'=>$row['extid'] , 'items'=>$items ] ;
		}

		# Multiple items with same Wikidata item in MnM
		$sql = "SELECT q,group_concat(id) AS ids,group_concat(ext_id) AS ext_ids FROM entry WHERE catalog={$catalog} AND q IS NOT NULL and q>0 AND user>0 GROUP BY q HAVING count(id)>1" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			$ids = explode(',',$o->ids) ;
			$ext_ids = explode(',',$o->ext_ids) ;
			$entries = [] ;
			foreach ( $ids AS $num => $id ) $entries[] = [ 'entry_id'=>$id , 'ext_id'=>$ext_ids[$num] ] ;
			$issues['multiple_q_in_mnm'][] = [ 'q_mnm'=>$o->q , 'entries' => $entries ] ;
		}

		# Different items between WD and MnM, external IDs not in MnM
		foreach ( $this->getItemAndExternalIdForPropertyFromSPARQL($prop) AS $batch ) {
			$ext_ids = array_unique(array_map(function($tuple){return $tuple[1];},$batch));
			if ( count($ext_ids)==0 ) continue ; # Paranoia
			$entries = $this->getEntriesForExtIds($catalog,$prop,$ext_ids);
			foreach ( $batch as $tuple ) {
				if ( isset($entries[$tuple[1]]) ) {
					$entry = $entries[$tuple[1]] ;
					if ( $entry->user == 0 ) { # Found a match but not in MnM yet
						$this->mnm->setMatchForEntryID ( $entry->id , $tuple[0] , 4 , true , false ) ;
					} else if ( $tuple[0] != $entry->q ) { # Fully matched but to different item
						if ( $entry->q <= 0 and $tuple[0]>0 ) { # Entry has N/A or Not In Wikidata, overwrite
							$this->mnm->setMatchForEntryID ( $entry->id , $tuple[0] , 4 , false , false ) ;
						} else {
							$issues['item_differs'][] = [ 'ext_id'=>$tuple[1] , 'q_wd'=>$tuple[0] , 'q_mnm'=>$entry->q , 'entry_id'=>$entry->id , 'ext_url'=>$entry->ext_url ] ;
						}
					}
				} else {
					$issues['extid_not_in_mnm'][] = [ 'ext_id'=>$tuple[1] , 'q_wd'=>$tuple[0] ] ;
				}
			}
		}

		$wikitext = $this->wikitextFromIssues ( $catalog , $issues ) ;
		$timestamp = date ( 'Ymd' ) ;
		$this->updateCatalog ( $wikitext , $catalog , $timestamp ) ;
	}

	protected function getFormatterURLForProp ( $prop ) {
		$pprop = "P{$prop}" ;
		$url = "https://www.wikidata.org/w/api.php?action=wbgetentities&ids={$pprop}&format=json" ;
		$j = json_decode(file_get_contents($url)) ;
		$claims = $j->entities->{$pprop}->claims ;
		if ( !isset($claims->P1630) ) return '' ;
		return $claims->P1630[0]->mainsnak->datavalue->value ;
	}

	protected function wikitextFromIssues ( $catalog_id , $issues ) {
		$prop = $this->catalogs[$catalog_id]->wd_prop ;
		$formatter_url = $this->getFormatterURLForProp($prop);
		$max_rows = 400 ;
		$rows = [] ;
		$rows[] = "A report for the [{$this->mnm->root_url}/ Mix'n'match] tool. '''This page will be replaced regularly!'''" ;
		$rows[] = "''Please note:''" ;
		$rows[] .= "* If you fix something from this list on Wikidata, please fix it on Mix'n'match as well, if applicable. Otherwise, the error might be re-introduced from there." ;
		$rows[] .= "* 'External ID' refers to the IDs in the original (external) catalog; the same as the statement value for the associated  property.\n" ;
		$rows[] .= "==[{$this->mnm->root_url}/#/catalog/$catalog_id " . $this->catalogs[$catalog_id]->name . "]==" ;
		$rows[] .= "{$this->catalogs[$catalog_id]->desc}\n" ;

		if ( count($issues['extid_not_in_mnm'])>0 ) {
			$rows[] = "== Unknown external ID ==" ;
			if ( count($issues['extid_not_in_mnm'])>$max_rows ) {
				$rows[] = "* ".count($issues['extid_not_in_mnm'])." external IDs in Wikidata but not in Mix'n'Match. Too many to show individually." ;
			} else {
				$this->annotateExternalIDs($catalog_id,$issues['extid_not_in_mnm']);
				$this->sortByExtID($issues['extid_not_in_mnm']);
				$rows[] = "{| class='wikitable'\n! External ID !! External label !! Item" ;
				foreach ( $issues['extid_not_in_mnm'] AS $i ) {
					$extid = $this->formatExtID($i,$formatter_url) ;
					$ext_name = $i['ext_name']??'' ;
					$rows[] = "|-\n| {$extid} || {$ext_name} || {{Q|{$i['q_wd']}}}" ;
				}
				$rows[] = "|}\n" ;
			}
		}

		if ( count($issues['item_differs'])>0 ) {
			$rows[] = "== Different items for the same external ID ==" ;
			if ( count($issues['item_differs'])>$max_rows ) {
				$rows[] = "* ".count($issues['item_differs'])." enties have different items on Mix'n'match and Wikidata. Too many to show individually." ;
			} else {
				$this->annotateExternalIDs($catalog_id,$issues['item_differs']);
				$this->sortByExtID($issues['item_differs']);
				$rows[] = "{| class='wikitable'\n! External ID !! External label !! Item in Wikidata !! Item in Mix'n'Match !! Mix'n'match entry" ;
				foreach ( $issues['item_differs'] AS $i ) {
					$extid = $this->formatExtID($i,$formatter_url) ;
					$ext_name = $i['ext_name']??'' ;
					$mnm_url = $this->mnm->getEntryURL($i['entry_id']) ;
					$rows[] = "|-\n| {$extid} || {$ext_name} || {{Q|{$i['q_wd']}}} || {{Q|{$i['q_mnm']}}} || [{$mnm_url} {$i['entry_id']}]" ;
				}
				$rows[] = "|}\n" ;
			}
		}

		if ( count($issues['multiple_q_in_mnm'])>0 ) {
			$rows[] = "== Same item for multiple external IDs in Mix'n'match ==" ;
			if ( count($issues['multiple_q_in_mnm'])>$max_rows ) {
				$rows[] = "* ".count($issues['multiple_q_in_mnm'])." items have more than one match in Mix'n'Match. Too many to show individually." ;
			} else {
				$rows[] = "{| class='wikitable'\n! Item in Mix'n'Match !! Mix'n'match entry !! External ID !! External label" ;
				foreach ( $issues['multiple_q_in_mnm'] AS $i ) {
					$this->annotateExternalIDs($catalog_id,$i['entries']);
					$this->sortByExtID($i['entries']);
					foreach ( $i['entries'] AS $num => $entry ) {
						if ( $num == 0 ) $r = "|-\n|rowspan=".count($i['entries'])."|{{Q|{$i['q_mnm']}}}|| " ;
						else $r = "|-\n|| " ;
						$extid = $this->formatExtID($entry,$formatter_url) ;
						$ext_name = $entry['ext_name']??'' ;
						$mnm_url = $this->mnm->getEntryURL($entry['entry_id']) ;
						$rows[] = "{$r}[{$mnm_url} {$entry['entry_id']}] || {$extid} || {$ext_name}" ;
					}
				}
				$rows[] = "|}\n" ;
			}
		}

		if ( count($issues['multiple_extid_in_wd'])>0 ) {
			$rows[] = "== Multiple items for the same external ID in Wikidata ==" ;
			if ( count($issues['multiple_extid_in_wd'])>$max_rows ) {
				$rows[] = "* ".count($issues['multiple_extid_in_wd'])." external IDs have at least two items on Wikidata. Too many to show individually." ;
			} else {
				$this->sortByExtID($issues['multiple_extid_in_wd']);
				$rows[] = "{| class='wikitable'\n! External ID !! Items in Mix'n'Match" ;
				foreach ( $issues['multiple_extid_in_wd'] AS $i ) {
					$extid = $this->formatExtID($i,$formatter_url) ;
					$rows[] = "|-\n| {$extid} || {{Q|".implode('}}<br/>{{Q|',$i['items'])."}}" ;
				}
				$rows[] = "|}\n" ;
			}
		}

		return implode("\n",$rows);
	}

	protected function sortByExtID(&$issues) {
		usort($issues, function ($a, $b) {
			return strcmp($a['ext_id'],$b['ext_id']);
		});
	}

	protected function annotateExternalIDs($catalog_id,&$issues) {
		$ext_ids = [] ;
		foreach ( $issues AS $i ) {
			if ( !isset($i['ext_id']) ) continue ;
			$ext_ids[$i['ext_id']] = $this->mnm->escape($i['ext_id']) ;
			$i['ext_name'] = '' ;
		}
		if ( count($ext_ids)==0 ) return ;
		$sql = "SELECT `ext_id`,`ext_name` FROM `entry` WHERE `catalog`={$catalog_id} AND `ext_id` IN ('".implode("','",$ext_ids)."')" ;
			$ext_ids = [] ;
		$result = $this->mnm->getSQL ( $sql ) ;
		$ext_id2name = [] ;
		while($o = $result->fetch_object()) $ext_id2name[$o->ext_id] = $o->ext_name ;
		foreach ( $issues AS $k => $i ) {
			if ( !isset($ext_id2name[$i['ext_id']])) continue ;
			$issues[$k]['ext_name'] = $ext_id2name[$i['ext_id']] ;
		}
	}

	protected function formatExtID ( $issue , $formatter_url='' ) {
		$ret = $issue['ext_id'] ;
		if ( !preg_match('|^[a-zA-Z0-9._ -]+$|',$ret) ) $ret = "<nowiki>{$ret}</nowiki>" ;
		#if ( isset($issue['ext_url']) ) $ret = "[{$issue['ext_url']} {$ret}]" ;
		#else 
		if ( $formatter_url!='' ) $ret = "[".str_replace('$1',$ret,$formatter_url)." {$ret}]" ;
		return $ret ;
	}

	# Update single catalog report page on Wikidata
	protected function updateCatalog ( $new_wikitext , $catalog , $timestamp ) {
		$ini = parse_ini_file ( '/data/project/mix-n-match/bot.ini' ) ;
		$wiki_user = $ini['user'] ;
		$wiki_pass = $ini['pass'] ;

		$page_title = "User:Magnus Manske/Mix'n'match report" ;
		if ( $catalog > 0 ) $page_title .= '/' . $catalog ;

		$revison_comment = "Update {$timestamp}" ;
		$api_url = 'https://www.wikidata.org/w/api.php' ;

		$this->mnm->loginToWiki ( $api_url , $wiki_user , $wiki_pass ) ;
		$this->mnm->setWikipageText ( $api_url , $page_title , $new_wikitext , $revison_comment ) ;
	}

	# Update all reports and the main report page
	protected function updateWikidataReports () {
		$wiki = "A report for the [{$this->mnm->root_url}/ Mix'n'match] tool. '''This page will be replaced regularly!'''" ;
		$wiki .= "\n''red links indicate there never was an issue with that catalog''\n" ;
		$wiki .= "\n{| class='wikitable'" ;
		$wiki .= "\n!Catalog!!Report!!Wikidata Property" ;
		foreach ( $this->catalogs AS $catalog => $v ) {
			if ( !isset($v->wd_prop) or isset($v->wd_qual) ) continue ;
			$wiki .= "\n|-" ;
			$wiki .= "\n|[{$this->mnm->root_url}/#/catalog/catalog=$catalog #$catalog]" ;
			$wiki .= "\n|[[/$catalog|{$v->name}]]" ;
			if ( isset($v->wd_prop) and !isset($v->wd_qual) ) $wiki .= "\n|".'{{'."P|{$v->wd_prop}".'}}' ;
			else $wiki .= "||" ;
		}
		$wiki .= "\n|}\n" ;
		$timestamp = date ( 'Ymd' ) ;
		$this->updateCatalog ( $wiki , 0 , $timestamp ) ;
	}


}

?>