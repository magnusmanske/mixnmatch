<?PHP

namespace MixNMatch ;

require_once dirname(__DIR__) . '/vendor/autoload.php';

// This compares Mix'n'match catalogs in the Wikidata properties, against the properties stored in the catalogs.
// It will send me an email if it finds something funny.

class SyncPropCatalog {
	public $mnm ;
	protected $ignore_wdprops = [1415,345,804,236,590,5257]; # 1415 temp deact for Andrew Gray (?); 345=misc IMDB series; 5257=BirdLife, different IDs
	protected $catalogs = [] ;

	function __construct ( $mnm = '' ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
	}

	protected function pad ( $s ) {
		if ( preg_match ( '/^P/' , $s ) ) {
			while ( strlen($s) < 5 ) $s = " $s" ;
		} else {
			while ( strlen($s) < 3 ) $s = " $s" ;
		}
		return $s ;
	}

	protected function getCatalogList ( $list ) {
		$ret = '' ;
		foreach ( $list AS $c ) { #implode ( ', ' , $wdprop2cat[$p['q']] ) ;
			if ( $ret != '' ) $ret .= ', ' ;
			$ret .= "[https://mix-n-match.toolforge.org/#/catalog/{$c} {$c}]" ;
			if ( !isset($this->catalogs[$c]) ) continue ;
			if ( !isset($this->catalogs[$c]->name) ) continue ;
			$ret .= " <small>(''{$this->catalogs[$c]->name}'')</small>" ;
		}
		return $ret ;
	}

	protected function updateIssues ( $wiki ) {
		$ini = parse_ini_file ( '/data/project/mix-n-match/bot.ini' ) ;
		$wiki_user = $ini['user'] ;
		$wiki_pass = $ini['pass'] ;

		$page_title = "User:Magnus Manske/Mix'n'match report/issues" ;
		$timestamp = date ( 'Ymd' ) ;
		$revison_comment = "Update {$timestamp}" ;
		$api_url = 'https://www.wikidata.org/w/api.php' ;

		$this->mnm->loginToWiki ( $api_url , $wiki_user , $wiki_pass ) ;
		$this->mnm->setWikipageText ( $api_url , $page_title , $wiki , $revison_comment ) ;
	}


	public function run() {
		$wdprop2cat = [] ;
		$sql = "SELECT * FROM catalog WHERE active=1" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$this->catalogs[$o->id] = $o ;
			if ( isset($o->wd_prop) and !isset($o->wd_qual) ) $wdprop2cat[$o->wd_prop][] = $o->id ;
		}

		$props = [] ;
		$sparql = 'SELECT ?q ?qLabel ?mnm { ?q rdf:type wikibase:Property . OPTIONAL { ?q wdt:P2264 ?mnm } SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". } } ORDER BY ?q' ;
		$sparql .= ' #!!'.rand() ; // Try to force fresh cache, not sure if that works
		$j = $this->mnm->tfc->getSPARQL ( $sparql ) ;
		foreach ( $j->results->bindings AS $v ) {
			$x = [] ;
			$x['q'] = preg_replace ( '/^.+\/[PQ]/' , '' , $v->q->value ) ;
			if ( isset($props[$x['q']]) ) {
				$props[$x['q']]['catalog'][] = $v->mnm->value ;
			} else {
				$x['label'] = $v->qLabel->value ;
				if ( isset($v->mnm) and $v->mnm !== null ) $x['catalog'] = [$v->mnm->value] ;
				$props[$x['q']] = $x ;
			}
		}

		$out = [] ;

		// Properties that have mnm id but mnm doesn't have property
		foreach ( $props AS $p ) {
			if ( !isset($p['catalog']) ) continue ;
			$cats = $p['catalog'] ;
			foreach ( $cats AS $c ) {
				if ( !isset($this->catalogs[$c]) ) {
					$s = $this->pad("{{P|".$p['q']."}}") . " has catalog ".$this->getCatalogList([$c]).", but that is inactive/does not exist" ;
					if ( isset($wdprop2cat[$p['q']]) ) {
						$s .= "; cantidate catalogs: " . $this->getCatalogList($wdprop2cat[$p['q']]) ;
					}
					$out['Catalog inactive/does not exist (&rArr; remove from/alter in property)'][] = $s ;
					continue ;
				}
				$cat = $this->catalogs[$c] ;
				if ( !isset($cat->wd_prop) or $cat->wd_prop === null or $cat->wd_prop == '' or $cat->wd_prop == 0 ) {
					if ( !in_array($p['q'],$this->ignore_wdprops) ) $out['Catalog has property, but not the reverse (&rArr; add property to catalog)'][] = "{{P|".$p['q']."}} has catalog ".$this->getCatalogList([$c]).", but that catalog does not have the property" ;
					continue ;
				}
				if ( $cat->wd_prop != $p['q'] ) {
					$out['Catalog has different property'][] = "{{P|".$p['q']."}} has catalog ".$this->getCatalogList([$c]).", but that catalog has property {{P".$cat->wd_prop."}}" ;
					continue ;
				}
			}
		}

		// Catalogs with property, but wd does not have the statement
		foreach ( $this->catalogs AS $c ) {
			if ( !isset($c->wd_prop) ) continue ;
			if ( isset($c->wd_qual) ) continue ;
			if ( in_array ( $c->wd_prop , $this->ignore_wdprops ) ) continue ;
			if ( !isset($props[$c->wd_prop]) ) {
				$out['No such property (&rArr; remove property from catalog)'][] = "Catalog ".$this->getCatalogList([$c->id])." has property "."{{P|".$c->wd_prop."}}".", but no such property exists" ;
				continue ;
			}
			$p = $props[$c->wd_prop] ;
			if ( !isset($p['catalog']) ) {
				$out['Property does not link to catalog (&rArr; add {{P|2264}} to property)'][] = "Catalog [https://mix-n-match.toolforge.org/#/catalog/{$c->id} {$c->id}] has property "."{{P|".$c->wd_prop."}}".", but that property does not link back to the catalog (''{$c->name}'')" ;
				continue ;
			}
			if ( !in_array ( $c->id , $p['catalog'] ) ) {
				$out['Property has different catalog'][] = "Catalog ".$this->getCatalogList([$c->id])." has property "."{{P|".$c->wd_prop."}}".", but that property has catalogs " . $this->getCatalogList ( $p['catalog'] ) ;
				continue ;
			}
			// OK
		}

		foreach ( $wdprop2cat AS $p => $x ) {
			if ( count($x) == 1 ) continue ;
			if ( in_array ( $p , $this->ignore_wdprops ) ) continue ;
			if ( isset($props[$p]) and isset($props[$p]['catalog']) AND count($props[$p]['catalog']) > 1 ) continue ; // Not perfect, but...
			$out['Property is used multiple times'][] = "Property {{P|$p}} is used " . count($x) . " times, in catalogs " . $this->getCatalogList ( $x ) ;
		}

		if ( count($out) == 0 ) return ;

		$wiki = ":''This status is updated daily''\n" ;
		ksort ( $out ) ;
		foreach ( $out AS $header => $list ) {
			$wiki .= "== {$header} ==\n" ;
			foreach ( $list AS $line ) $wiki .= "# {$line}\n" ;
			$wiki .= "\n" ;
		}
		$this->updateIssues ( trim($wiki) ) ;
	}

}

?>