<?PHP

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

# ATTEMPT AT ABSTRACING A MnM ENTRY - NOT USED YET

class Entry extends MnMbase {
	protected $catalog_id ; # ID (int)
	protected $external_id ;
	protected $name ;
	protected $desc ;
	protected $url ;
	protected $q ; # Matched Wikidata item (int)
	protected $user ; # ID (int)
	protected $timestamp ;
	protected $random ;
	protected $type ;

	protected $multi_q = [] ; # Multi-match; array of (int)
	protected $aux = [] ; # array of arrays like ['P123','the value']
	protected $location ; # {lat,lon}
	protected $born ; # YYYY[-MM[-DD]]
	protected $died ; # YYYY[-MM[-DD]]
	protected $aliases = [] ;
	protected $descriptions = [] ;
	protected $mnm_relation = [] ;

	protected $has_data = false ;
	protected $vars_int = ['id','catalog_id','user'] ;
	protected $vars_str = ['external_id','name','desc','url','timestamp','born','died','type'] ;
	protected $vars_arr = ['multi_q','aux','aliases','descriptions'] ;
	protected $vars_misc = ['q','location','random'] ;

	/**
	 * Constructor
	 *
	*/
	public function __construct ( $entry = 0 , $mnm = '' ) {
		if ( $mnm != '' ) $this->setMnM ( $mnm ) ;
		if ( !isset($entry) or $entry == 0 or $entry == null ) $this->id = 0 ;
		else if ( is_object($entry) or is_array($entry) ) $this->setFromEntryObject ( $entry ) ;
		else $this->setFromEntryID ( $entry ) ;
	}

	public function reset ( $reset_id = true ) {
		if ( isset($this->id) and $reset_id ) unset ( $this->id ) ;
		if ( $this->has_data ) {
			foreach ( array_merge($this->vars_int,$this->vars_str,$this->vars_misc) AS $k ) {
				if ( isset($this->$k) ) unset ( $this->$k ) ;
			}
			foreach ( $this->vars_arr AS $k ) $this->$k = [] ;
			$this->has_data = false ;
		}
	}

	public function setFromEntryObject ( $entry ) {
		$this->reset();
		if ( is_array($entry) ) $entry = (object) $entry ;
	
		# Integers
		foreach ( $this->vars_int AS $k ) {
			if ( isset($entry->$k) ) $this->$k = intval ( "{$entry->$k}" ) ;
		}

		# String
		foreach ( $this->vars_str AS $k ) {
			if ( isset($entry->$k) ) $this->$k = trim ( $entry->$k ) ;
		}

		# Bespoke
		if ( isset($entry->location) ) {
			if ( is_array($entry->location) ) $this->location = (object) ['lat'=>$entry->location['lat'],'lon'=>$entry->location['lon']] ;
			else if ( is_object($entry->location) ) $this->location = $entry->location ;
			else throw new \Exception("Entry class: Entry {$this->id} : Bad location: ".json_encode($entry->location) ) ;
		}
		if ( isset($entry->aux) ) {
			foreach ( $entry->aux AS $v ) {
				$this->aux[] = [ 'P'.$this->sanitizeQ($v[0]) , trim("{$v[1]}") ] ;
			}
		}
		if ( isset($entry->q) ) {
			if ( is_array($entry->q) ) {
				foreach ( $entry->q AS $q ) $this->multi_q[] = $this->sanitizeQ ( $q ) ;
			} else {
				$this->q = $this->sanitizeQ ( $entry->q ) ;
			}
		}

		$this->has_data = true ;
	}

	public function setFromEntryID ( $entry_id ) {
		$this->reset();
		$this->id = intval ( $entry_id ) ;
		if ( isset($this->mnm) ) {
			$this->loadEntryFromDatabase() ;
		}
	}

	public function setFromExternalID ( $catalog_id , $external_id ) {
		$catalog_id = intval ( $catalog_id ) ;
		$this->checkMnM();
		$this->id = 0 ;
		$sql = "SELECT /* ".__METHOD__." */ * FROM `entry` WHERE `catalog`={$catalog_id} AND `ext_id`='" . $this->escape ( $external_id ) . "'" ;
		$result = $this->getSQL ( $sql ) ;
		if($o = $result->fetch_object()) {
			$this->importFromDatabaseObject ( $o ) ;
		} else {
			throw new \Exception(__METHOD__." Entry with external ID '{$external_id}' : Could not load core data" ) ;
		}
	}

	public function loadEntryFromDatabase () {
		$this->checkMnM();
		$this->checkValidID() ;
		$sql = "SELECT /* ".__METHOD__." */ * FROM entry WHERE id={$this->id} LIMIT 1" ;
		$result = $this->getSQL ( $sql ) ;
		if($o = $result->fetch_object()) {
			$this->importFromDatabaseObject ( $o ) ;
		} else {
			throw new \Exception(__METHOD__." Entry {$this->id} : Could not load core data" ) ;
		}
	}

	public function loadMetadataFromDatabase () {
		$this->checkMnM();
		$this->checkValidID() ;
		$this->loadMetadataTableIntoArray ( 'aliases' ) ;
		$this->loadMetadataTableIntoArray ( 'descriptions' ) ;
		$this->loadMetadataTableIntoArray ( 'mnm_relation' ) ;

		# Location
		$result = $this->getSQL ( "SELECT /* ".__METHOD__." */ * FROM `location` WHERE `entry_id`={$this->id} LIMIT 1" ) ;
		if ( $o = $result->fetch_object() ) $this->location = (object) ['lat'=>$o->lat,'lon'=>$o->lon] ;

		# Dates
		$result = $this->getSQL ( "SELECT /* ".__METHOD__." */ * FROM `person_dates` WHERE `entry_id`={$this->id} LIMIT 1" ) ;
		if ( $o = $result->fetch_object() ) {
			if ( $o->born != '' ) $this->born = $o->born ;
			if ( $o->died != '' ) $this->died = $o->died ;
		}

		# Multi match
		$result = $this->getSQL ( "SELECT /* ".__METHOD__." */ * FROM `multi_match` WHERE `entry_id`={$this->id} LIMIT 1" ) ;
		if ( $o = $result->fetch_object() ) $this->multi_q = explode(',',$o->candidates) ;

		# Aux
		$result = $this->getSQL ( "SELECT /* ".__METHOD__." */ * FROM `auxiliary` WHERE `entry_id`={$this->id}" ) ;
		while ( $o = $result->fetch_object() ) $this->aux[] = [ "P{$o->aux_p}" , $o->aux_name ] ;
	}

	public function id() {return $this->id ; }
	public function catalog_id() {return $this->catalog_id ; }
	public function name() {return $this->name ; }
	public function external_id	() {return $this->external_id ; }
	public function desc() {return $this->desc ; }
	public function url() {return $this->url ; }
	public function q() {return $this->q ; }
	public function user() {return $this->user ; }
	public function timestamp() {return $this->timestamp ; }
	public function random() {return $this->random ; }
	public function type() {return $this->type ; }
	public function aux() {return $this->aux ; }
	public function location() {return $this->location ; }
	public function born() {return $this->born ; }
	public function died() {return $this->died ; }
	public function multi_match() {return $this->multi_q ; }
	public function aliases() {return $this->aliases ; }
	public function descriptions() {return $this->descriptions ; }
	public function mnm_relation() {return $this->mnm_relation ; }

	public function core_data() {
		return (object) [
			'id'=>$this->id(),
			'catalog'=>$this->catalog_id(),
			'ext_id'=>$this->external_id(),
			'ext_url'=>$this->url(),
			'ext_name'=>$this->name(),
			'ext_desc'=>$this->desc(),
			'q'=>$this->q(),
			'user'=>$this->user(),
			'timestamp'=>$this->timestamp(),
			'random'=>$this->random(),
			'type'=>$this->type(),
		] ;
	}

	protected function sanitizeQ ( $q ) {
		if ( !isset($q) or $q === null ) throw new \Exception(__METHOD__." unset/null q" ) ;
		if ( preg_match ( '/^[PQ](\d+)$/' , "$q" , $m ) ) $q = $m[1] ;
		$q = intval ( $q ) ;
		return $q ;
	}

	protected function loadMetadataTableIntoArray ( $table_name ) {
		$this->checkMnM() ;
		$this->checkValidID() ;
		$sql = "SELECT /* ".__METHOD__." */ * FROM `{$table_name}` WHERE `entry_id`={$this->id}" ;
		$result = $this->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			unset($o->entry_id) ;
			$this->$table_name[] = $o ;
		}
	}

	protected function getCatalog () {
		$this->checkMnM() ;
		$this->checkValidID() ;
		if ( !isset($this->catalog_id) ) throw new \Exception(__METHOD__." Entry {$this->id} : No catalog ID set" ) ;
		$catalog = $this->mnm->loadCatalog ( $this->catalog_id , true ) ;
		if ( !isset($catalog) ) throw new \Exception(__METHOD__." Entry {$this->id} : No catalog {$this->catalog} in database" ) ;
		return $catalog ;
	}

	protected function importFromDatabaseObject ( $o ) {
		$this->reset();
		$this->id = $o->id ;
		$this->external_id = trim($o->ext_id) ;
		$this->catalog_id = intval ( "{$o->catalog}" ) ;
		$this->name = trim($o->ext_name) ;
		$this->desc = trim($o->ext_desc) ;
		$this->url = trim($o->ext_url) ;
		foreach ( ['q','user'] AS $k ) {
			if ( isset($o->$k) and $o->$k!=null and $o->$k!='' ) $this->$k = intval ( $o->$k ) ;
		}
		foreach ( ['timestamp','type'] AS $k ) {
			if ( isset($o->$k) and $o->$k!=null and $o->$k!='' ) $this->$k = trim ( $o->$k ) ;
		}
		foreach ( ['random'] AS $k ) {
			if ( isset($o->$k) and $o->$k!=null and $o->$k!='' ) $this->$k = floatval ( $o->$k ) ;
		}
		$this->has_data = true ;
	}

}

?>