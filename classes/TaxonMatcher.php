<?php

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class TaxonMatcher {
	public $mnm ;
	protected $catalogs ;
	protected $use_desc = [ 827 ] ;
	protected $ranks = [
		'variety' => 'Q767728' ,
		'subspecies' => 'Q68947' ,
		'species' => 'Q7432' ,
		'superfamily' => 'Q2136103' ,
		'subfamily' => 'Q2455704' ,
		'class' => 'Q37517' ,
		'suborder' => 'Q5867959' ,
		'genus' => 'Q34740' ,
		'family' => 'Q35409' ,
		'order' => 'Q36602' ,
		'Q16521' => '' 
	] ;

	function __construct( $catalogs = 'all_entries' , $mnm = '' ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
		$this->parseCatalogs($catalogs) ;
	}

	public function run() {
		if ( count($this->catalogs) == 0 ) return ;

		$sql = "SELECT * FROM entry WHERE catalog IN (" . implode(',',$this->catalogs) . ") AND (q IS NULL OR user=0)" ;
		$sql .= " AND `type` IN ('" . implode("','",array_values($this->ranks)) . "Q16521')" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			if ( in_array($o->catalog,$this->use_desc) ) $taxon = $o->ext_desc ;
			else $taxon = $o->ext_name ;

			$type = $o->type ;
			if ( !isset($this->ranks[$type]) ) {
				foreach ( $this->ranks AS $k => $v ) {
					if ( $v == $type ) $type = $k ;
				}
			}

			if ( $o->catalog == 169 ) {
				if ( preg_match ( '/\[([a-z ]+)[,\]]/i' , $o->ext_desc , $m ) ) { $taxon = $m[1] ; # First in []
				} else {
					$type = '' ;
					foreach ( $this->ranks AS $k => $v ) {
						if ( preg_match ( '/ '.$k.'$/i' , $o->ext_desc ) ) $type = $k ;
						if ( preg_match ( '/^'.$k.' of /i' , $o->ext_desc ) ) $type = $k ;
					}
					if ( $type == '' ) continue;
				}
			}

			$taxon = preg_replace ( '/ ssp\. /' , ' subsp. ' , $taxon ) ;
			$rank = $this->ranks[$type] ;

			$query = "haswbstatement:P31=Q16521 haswbstatement:\"P225={$taxon}|P1420={$taxon}\"" ;
			if ( isset($rank) AND $rank != '' ) $query .= " haswbstatement:P105={$rank}" ;
			$items = $this->mnm->getCachedWikidataSearch($query,'','',false) ;

			if ( count($items) != 1 ) continue ;
			$q = $items[0] ;
			$this->mnm->setMatchForEntryID ( $o->id , $q , 4 , true , false ) ;
		}

		# Mark taxon catalogs as done
		$sql = "UPDATE catalog SET taxon_run=1 WHERE id IN (".implode(',',$this->catalogs).") AND taxon_run=0" ;
		$this->mnm->getSQL ( $sql ) ;
	}

	public function generateTaxonCandidatesTable() {
		$sql = "TRUNCATE common_names_taxon" ;
		$this->mnm->getSQL ( $sql ) ;
		$sql = "INSERT INTO common_names_taxon (name,total,cnt) VALUES SELECT ext_name,count(id) AS total,count(case when 1 is null or user=0 then 1 end) as unmatched FROM entry WHERE `type`='Q16521' AND catalog NOT IN (830) GROUP BY ext_name HAVING unmatched>0 and total>=3";
		$this->mnm->getSQL ( $sql ) ;
	}

	protected function parseCatalogs($catalogs) {
		if ( $catalogs=='all_entries' or $catalogs=='random' ) {
			$sql = "SELECT DISTINCT `catalog` FROM `entry` WHERE `type`='Q16521'" ;
			if ( $catalogs=='random' ) $sql .= " ORDER BY rand() LIMIT 1" ;
			$result = $this->mnm->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) $this->catalogs[] = $o->catalog ;
		} else if ( $catalogs=='new' ) {
			$sql = "SELECT `id` FROM `catalog` WHERE `taxon_run`=0 AND `active`=1" ;
			$result = $this->mnm->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) $this->catalogs[] = $o->id ;
		} else {
			$catalogs = preg_replace('|[^0-9,]|','',$catalogs);
			$this->catalogs = explode ( ',' , $catalogs ) ;
		}
	}

}

?>