<?PHP

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class StatementText {
	public $mnm ;
	public $prop_info ;
	#protected $properties_move_to_aux = [17,21,102,171,225,407] ;
	protected $common_mistakes = [
		[[17,291],'United Kingdom','Q145'],
		[[17,291],'USA','Q30'],
		[[17,291,131],'Highland','Q208279'],
		[[17,291,131],'Cumbria','Q23066'],
		[[17,291,131],'Argyll and Bute','Q202174'],
		[[19,20],'Berlin','Q64'],
		[[19,20],'Hamburg','Q1055'],
		[[19,20],['München','Munich'],'Q1726'],
		[[19,20],['Wien','Vienna','Wien, Österreich'],'Q1741'],
		[[19,20],['Paris','Paris, Frankreich','Paris, France'],'Q90'],
		[[19,20],['London','London, England','London, England, Großbritannien'],'Q84'],
		[21,['Female','F'],'Q6581072'],
		[21,['Male','M'],'Q6581097'],
		[102,['Democrat','Democratic'],'Q29552'],
		[102,'Republican','Q29468'],
		[407,['English','eng'],'Q1860'],
		[407,['Spanish','spa'],'Q1321'],
		[407,['French','fre'],'Q150'],
		[407,['German','ger'],'Q188'],
	] ;


	function __construct($mnm=null) {
		if ( isset($mnm) ) $this->mnm = $mnm ;
		else $this->mnm = new MixNMatch ;
	}

	public function run_everything() {
		$this->fix_common_mistakes();
		$this->move_aux_to_statement_text();
		$this->update_matched_state();
		$this->resolve_parent_taxon();
		$this->move_from_statement_text_to_auxiliary();
	}

	public function fix_common_mistakes() {
		foreach ($this->common_mistakes as $m) {
			$properties = $m[0] ;
			if ( !is_array($properties) ) $properties = [ $properties ] ;
			$texts = $m[1] ;
			if ( !is_array($texts) ) $texts = [ $texts ] ;
			foreach ( $texts AS $k => $text ) $texts[$k] = $this->mnm->escape($text) ;
			$q = preg_replace('|\D|','',$m[2]) ;
			$sql = "UPDATE `statement_text` SET `q`='{$q}' WHERE `property` IN (".implode(',',$properties).") AND `text` IN ('".implode("','",$texts)."')" ;
			$this->mnm->getSQL($sql);
		}
	}

	public function move_aux_to_statement_text() {
		$this->load_prop_info() ;
		$item_props = array_keys($this->prop_info['wikibase-item']) ;
		$from_where = ' FROM `auxiliary` WHERE `aux_p` IN ('.implode(',',$item_props).') AND `aux_name` NOT RLIKE "^Q\\\\d+$"';
		$sql = "INSERT IGNORE INTO `statement_text` (`entry_id`,`property`,`text`,`in_wikidata`,`entry_is_matched`) SELECT `entry_id`,`aux_p`,`aux_name`,`in_wikidata`,`entry_is_matched` {$from_where}" ;

		$this->mnm->dbm->begin_transaction();
		$this->mnm->getSQL($sql);
		$sql = "DELETE {$from_where}" ;
		$this->mnm->getSQL($sql);
		$this->mnm->dbm->commit();
	}

	public function update_matched_state() {
		$sql = 'UPDATE statement_text SET entry_is_matched=1 WHERE entry_is_matched=0 AND EXISTS (SELECT * FROM entry WHERE entry_id=entry.id AND q is not null and user>0)' ;
		$this->mnm->getSQL($sql);
	}

	public function resolve_parent_taxon() {
		$sql = "SELECT DISTINCT `text` FROM `statement_text` WHERE `property` IN (171,225) AND `q` IS NULL" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$taxon_q = $this->find_taxon_q($o->text) ;
			if ( !isset($taxon_q) ) continue ;
			$taxon_name_safe = $this->mnm->escape($o->text);
			$sql = "UPDATE `statement_text` SET `q`={$taxon_q} WHERE `text`='{$taxon_name_safe}' AND `q` IS NULL" ;
			$this->mnm->getSQL ( $sql ) ;
		}
	}

	public function move_from_statement_text_to_auxiliary() {
		$from_where = "FROM `statement_text` WHERE `q` IS NOT NULL" ;
		#$from_where .= " AND `property` IN (".implode(',',$this->properties_move_to_aux).")" ;
		$sql = "INSERT IGNORE INTO `auxiliary` (entry_id,aux_p,aux_name,in_wikidata,entry_is_matched) SELECT entry_id,property,concat('Q',q),in_wikidata,entry_is_matched {$from_where}" ;

		$this->mnm->dbm->begin_transaction();
		$this->mnm->getSQL ( $sql ) ;
		$sql = "DELETE {$from_where}" ;
		$this->mnm->getSQL ( $sql ) ;
		$this->mnm->dbm->commit();
	}



	protected function load_prop_info() {
		if ( isset($this->prop_info) ) return ; # Already loaded
		$this->prop_info = [] ;
		$sql = "SELECT * FROM `wb_property_info`" ;
		$dbwd = $this->mnm->openWikidataDB() ;
		$result = $this->mnm->tfc->getSQL ( $dbwd , $sql ) ;
		while($o = $result->fetch_object()) {
			$this->prop_info[$o->pi_type][$o->pi_property_id] = json_decode($o->pi_info);
		}
		#print_r(array_keys($this->prop_info));
	}

	protected function find_taxon_q ( $taxon_name ) {
		# Try to find in Mix'n'match
		$candidate_qs = [] ;
		$taxon_name_safe = $this->mnm->escape($taxon_name);
		$sql = "SELECT DISTINCT `q` FROM `entry` WHERE `ext_name`='{$taxon_name_safe}' AND `q` IS NOT NULL AND `user`>0 AND `type`='Q16521'" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $candidate_qs[] = $o->q ;
		if ( count($candidate_qs)==1 ) return $candidate_qs[0] ;
		if ( count($candidate_qs)>1 ) return ; # Multiple taxa with same name, not touching this

		# Try to find on Wikidata
		$results = $this->mnm->getSearchResults('','P225',$taxon_name) ;
		if ( count($results) == 1 ) return preg_replace('/\\D/','',$results[0]->title);
	}

}

?>