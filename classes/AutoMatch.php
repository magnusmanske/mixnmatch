<?PHP

namespace MixNMatch ;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class AutoMatch {
	public $mnm ;
	public $strict_type = true ;
	public $catalog_id = 0 ;
	public $verbose = false ;
	protected $batch_size = 1000 ;
	protected $name_batch_size = 1000 ;
	protected $use_aliases_for_matching = true ;
	protected $simple_notype = false ;
	protected $catalog ;
	protected $meta_items = ['Q4167410','Q11266439','Q4167836','Q13406463','Q22808320'] ;
	protected $skip_plural_search=[162,659,660,661,519,662,663,664,665,666,667,668,669,474,633,670,671,672] ;
	protected $single_language_only = false ;
	protected $single_search_language = 'en' ;

	function __construct ( $mnm = '' ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
	}

	protected function sanitize_query ( $query , $type ) {
		if ( $type == 'Q5' ) {
			$query = trim ( preg_replace ( '|\s*\b[A-Z]\.\s*|' , ' ' , $query ) ) ; # Initials
		}
		return $query ;
	}

	protected function get_search ( $query , $type = '' ) {
		if ( $type != '' and $this->strict_type ) $query .= " haswbstatement:P31=" . $type ;
		if ( $type != 'Q13442814' ) $query .= " -haswbstatement:P31=Q13442814" ; # No "scholarly article"
		$query = $this->sanitize_query ( $query , $type ) ;
		if ( $this->verbose) print "{$query}\n" ;
		$url = "http://www.wikidata.org/w/api.php?action=query&list=search&format=json&srsearch=" . urlencode ( $query ) ;
		return json_decode ( file_get_contents ( $url ) ) ;
	}

	protected function search_aliases ( $aliases , &$j , $type ) {
		if ( !isset($aliases) ) return ;
		$aliases = explode ( '|' , $aliases ) ;
		foreach ( $aliases AS $a ) {
			$j2 = $this->get_search ( $a , $type ) ;
			foreach ( $j2->query->search AS $x ) $j->query->search[] = $x ;
		}
	}

	# Filters [ "Qxxx" => something ] by removing disambiguation etc items
	protected function filter_disambiguation_items ( &$candidate_items ) {
		$qlist = [] ;
		foreach ( $candidate_items AS $q => $dummy ) $qlist[] = 'Q' . $this->mnm->sanitizeQ($q) ;
		$sql = "SELECT DISTINCT page_title FROM page,pagelinks,linktarget WHERE pl_target_id=lt_id AND page_namespace=0 AND page_title IN ('" . implode("','",$qlist) . "') AND pl_from=page_id AND lt_title IN ('".implode("','",$this->meta_items)."')" ;
		$result = $this->mnm->tfc->getSQL ( $this->mnm->dbwd , $sql ) ;
		while($o = $result->fetch_object()) unset ( $candidate_items[$o->page_title] ) ;
	}

	protected function process_entries_batch(&$entries,$batch_size) {
		if ( count($entries) < $batch_size ) return ;

		$string2entry_id = [] ;
		foreach ( $entries AS $e ) {
			$s = ucfirst ( trim ( preg_replace ( '/_/' , ' ' , $e->ext_name ) ) ) ;
			$string2entry_id[$s][] = $e->id ;
			if ( preg_match ( '/(.+) \(/' , $s , $m ) ) $string2entry_id[trim($m[1])][] = $e->id ;
		}

		# Prepare possible titles
		$titles = [] ;
		foreach ( $string2entry_id AS $k => $v ) $titles[] = $this->mnm->escape($k) ;

		# Run query
		$candidates = [] ;
		$site = $this->mnm->escape($this->catalog->data()->search_wp.'wiki') ;
		$sql = "SELECT * FROM wb_items_per_site WHERE ips_site_id='{$site}' AND ips_site_page IN ('".implode("','",$titles)."')" ;
		$result = $this->mnm->tfc->getSQL ( $this->mnm->dbwd , $sql ) ;
		while($o = $result->fetch_object()){
			if ( !isset($string2entry_id[$o->ips_site_page]) ) continue ;
			$entry_ids = $string2entry_id[$o->ips_site_page] ;
			if ( count($entry_ids) != 1 ) continue ;
			$entry_id = $entry_ids[0] ;
			$q = 'Q' . $o->ips_item_id ;
			$candidates[$q] = $entry_id ;
		}
		$this->filter_disambiguation_items ( $candidates ) ;

		foreach ( $candidates AS $q => $entry_id ) {
			$this->log_match ( $entry_id , $q ) ;
			$this->mnm->setMatchForEntryID ( $entry_id , $q , 0 , true , false ) ;
		}

		# Cleanup
		$entries = [] ;
	}

	protected function automatch_simple_check_catalog () {
		# Check if catalog is valid
		$sql = "SELECT * FROM catalog WHERE id={$this->catalog_id}" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			if ( $this->single_language_only and $o->search_wp != '' ) $this->single_search_language = $o->search_wp ;
			if ( $o->active != 1 ) throw new \Exception(__METHOD__.": Catalog {$this->catalog_id} is inactive" ) ;
			return ;
		}
		throw new \Exception(__METHOD__.": Catalog {$this->catalog_id} does not exist" ) ;
	}

	# Fix broken multimatch ("0"-item).
	protected function fix_broken_multimatch () {
		return ; # Deactivated; slow, unnecessary, I hope
		$ts = $this->mnm->getCurrentTimestamp();
		$sql = "UPDATE entry SET q=(SELECT candidates FROM multi_match WHERE entry_id=entry.id)*1,user=0,timestamp='{$ts}' WHERE q is NULL AND id IN (SELECT entry_id FROM multi_match WHERE candidate_count=1)";
		$this->mnm->getSQL ( $sql ) ;
		$sql = "DELETE FROM multi_match WHERE candidate_count<2" ;
		$this->mnm->getSQL ( $sql ) ;
	}

	protected function automatch_simple_init ( &$types , &$name2ids ) {
		$sql = "SELECT id,ext_name AS name,`type`," ;
		$sql .= "(SELECT group_concat(DISTINCT label SEPARATOR '|') FROM aliases WHERE entry_id=entry.id) AS aliases" ;
		$sql .= " FROM entry WHERE catalog=$this->catalog_id and q IS NULL" ;
		$sql .= $this->get_sql_log_paranoia() ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			$n = trim($o->name) ;
			$n = preg_replace ( '/^([Ss]ir|[Ll]ady|[Dd]ame) /' , '' , $n ) ;
			$n = preg_replace ( '/, geb\..*$/' , '' , $n ) ;
			if ( $this->catalog_id == 1151 and preg_match ( '/^(\S+) (\S+)$/' , $name , $m ) ) $name = $m[2] . ' ' . $m[1] ; # HARDCODED japanese names, "last first" => "first last"
			$name2ids[$n][] = $o->id ;
			
			if ( preg_match ( '/-/' , $n ) ) $name2ids[str_replace('-',' ',$n)][] = $o->id ;
			if ( preg_match ( '/\(/' , $n ) ) $name2ids[preg_replace('/\s+\(.*?\)/','',$n)][] = $o->id ; // ()

			if ( preg_match ( '/\s*[A-Z]\.{0,1}\s+/' , $n ) ) {
				$name2ids[trim(preg_replace('/\s*[A-Z]\.{0,1}\s+/',' ',$n))][] = $o->id ; // Single letter
			}

			$ascii_name = $this->mnm->asciify($n);
			if ( $ascii_name != $n ) $name2ids[$ascii_name][] = $o->id ;
			
			
			if ( $o->type != 'Q5' and preg_match ( '/^(.+)s$/' , $n , $m ) and !in_array($this->catalog_id,$this->skip_plural_search) ) { // Plural => singular, for non-people
				$n2 = $m[1] ;
				if ( preg_match ( '/^(.+)ies$/' , $n , $m ) ) $n2 = $m[1].'y' ;
				$name2ids[$n2][] = $o->id ;
				if ( preg_match ( '/-/' , $n2 ) ) $name2ids[str_replace('-',' ',$n2)][] = $o->id ;
			}

			if ( $this->use_aliases_for_matching and isset($o->aliases) ) {
				$aliases = explode ( '|' , $o->aliases ) ;
				foreach ( $aliases AS $alias ) {
					$alias = trim($alias) ;
					if ( $alias == '' ) continue ;
					$name2ids[$alias][] = $o->id ;
					$ascii_name = $this->mnm->asciify($alias);
					if ( $ascii_name != $n ) $name2ids[$ascii_name][] = $o->id ;
				}
			}

			if ( $o->type != '' ) $types[$o->type]++ ;
		}
	}

	protected function automatch_simple_limiter ( &$types ) {
		# Check for limiter; rare
		$catalog = new Catalog ( $this->catalog_id , $this->mnm ) ;
		$o = $catalog->data();
		if ( trim($o->limiter) == '' ) return ;
		$j = json_decode ( $o->limiter ) ;
		foreach ( $j AS $type ) $types[$type]++ ;
	}

	protected function automatch_simple_types ( &$types ) {
		if ( $this->simple_notype ) return [] ;
		$types = array_keys($types) ;
		if ( count($types) == 1 and array_pop(array_values()) == 'Q5' ) {
			return [ 'Q5' ] ;
		} else if ( count($types) > 0 ) { # Deactivated; doesn't do anything
			#$sparql = "SELECT ?q { VALUES ?types { wd:" . implode ( ' wd:' , $types ) . " } ?q wdt:P279* ?types }" ;
			#$items = $this->mnm->tfc->getSPARQLitems ( $sparql ) ;
		}
		return [] ;
	}

	protected function automatch_simple_filter_by_type ( $qlist , $linkto , $name2q , $name2ids , &$multimatch , &$candidates ) {
		$goodq = [] ;
		$sql = "SELECT DISTINCT page_title FROM page,pagelinks,linktarget WHERE pl_target_id=lt_id AND page_namespace=0 AND page_title IN ('Q" . implode("','Q",$qlist) . "') AND pl_from=page_id AND lt_title IN ( '".implode("','",$linkto)."' )" ;
		$result = $this->mnm->tfc->getSQL ( $this->mnm->dbwd , $sql ) ;
		while($o = $result->fetch_object()){
			$q = preg_replace ( '/^Q/' , '' , $o->page_title ) ;
			$goodq[$q] = $q ;
		}
		
		foreach ( $name2q AS $name => $qs ) {
			if ( !isset($name2ids[$name]) ) continue ;
			$maybe = [] ;
			foreach ( $qs AS $q ) {
				if ( isset($goodq[$q]) ) $maybe[$q] = $q ;
			}
			if ( count($maybe) != 1 ) {
				if ( count($maybe) > 1 ) {
					foreach ( $name2ids[$name] AS $id ) {
						foreach($maybe AS $m) $multimatch[$id][$m] = $m ;
					}
				}
				continue ;
			}
			foreach ( $maybe AS $q ) {
				foreach ( $name2ids[$name] AS $id ) $candidates[''.$id] = $q ;
			}
		}
	}

	protected function automatch_simple_no_filter ( $name2q , $name2ids , &$multimatch , &$candidates ) {
		foreach ( $name2q AS $name => $qs ) {
			if ( !isset($name2ids[$name]) ) continue ;
			if ( count($qs) == 1 ) {
				foreach ( $qs AS $q ) {
					foreach ( $name2ids[$name] AS $id ) $candidates["{$id}"] = $q ;
				}
			} else {
				foreach ( $name2ids[$name] AS $id ) {
					foreach($qs AS $m) $multimatch[$id][$m] = $m ;
				}
			}
		}
	}

	protected function automatch_simple_search_names ( $names , &$name2q , &$qlist ) {
		$sql = "SELECT wbx_text AS name,wbit_item_id AS q FROM wbt_text,wbt_item_terms,wbt_term_in_lang,wbt_text_in_lang WHERE wbit_term_in_lang_id=wbtl_id AND wbtl_text_in_lang_id=wbxl_id AND wbxl_text_id=wbx_id" ;
		if ( $this->single_language_only ) $sql .= " AND wbxl_language='" . $this->mnm->escape ( $this->single_search_language ) . "' " ;
		$sql .= " AND wbx_text IN ('" . implode("','",$names) . "') GROUP BY name,q" ;
		$result = $this->mnm->tfc->getSQL ( $this->mnm->dbwd , $sql ) ;
		while($o = $result->fetch_object()){
			$q = $o->q;
			$name2q[$o->name][$q] = $q ;
			$qlist[$q] = $q ;
		}
	}

	protected function set_catalog ( $catalog_id ) {
		$this->catalog_id = $catalog_id * 1 ;
		if ( $this->catalog_id <= 0 ) throw new \Exception(__METHOD__.': No/bad catalog' ) ; 
		try {
			$catalog = new Catalog ( $this->catalog_id , $this->mnm ) ;
			$using = $catalog->usesAutomatchers() ;
		} catch (exception $e) {
			$using = 0 ;
		}
		if ( !$using ) throw new \Exception(__METHOD__.": No automatchers for catalog {$this->catalog_id}" ) ;
		$this->catalog = $this->mnm->loadCatalog($catalog_id,true);
	}

	# Assumes `entry.id` is the key in the rest of the query
	protected function get_sql_log_paranoia () {
		return " AND NOT EXISTS (SELECT * FROM `log` WHERE log.entry_id=entry.id AND log.action='remove_q')" ;
	}

	protected function log_match ( $entry_id , $q ) {
		if ( !$this->verbose ) return ;
		$entry_id *= 1 ;
		$this->mnm->sanitizeQ ( $q ) ;
		print "https://mix-n-match.toolforge.org/#/entry/{$entry_id} => https://www.wikidata.org/wiki/Q$q\n" ;
	}

	public function automatch_from_other_catalogs ( $catalog_id ) {
		$this->set_catalog ( $catalog_id ) ;
		$sql = "SELECT ext_name,`type`,q FROM entry 
			WHERE ext_name IN (SELECT DISTINCT ext_name FROM entry WHERE catalog={$this->catalog_id} AND q IS NULL)
			AND q IS NOT NULL AND q > 0 AND user IS NOT NULL AND user>0
			AND catalog IN (SELECT id from catalog WHERE active=1)
			GROUP BY ext_name,type
			HAVING count(DISTINCT q)=1";
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$sql = "SELECT id FROM entry WHERE catalog={$this->catalog_id} AND ext_name='" . $this->mnm->escape($o->ext_name) . "' AND `type`='" . $this->mnm->escape($o->type) . "' AND q IS NULL" ;
			$entry_ids = [] ;
			$result2 = $this->mnm->getSQL ( $sql ) ;
			while($o2 = $result2->fetch_object()) $entry_ids[] = $o2->id;
			if ( count($entry_ids) != 1 ) continue ; # Paranoia
			$this->log_match ( $entry_ids[0] , $o->q ) ;
			$this->mnm->setMatchForEntryID ( $entry_ids[0] , $o->q , 0 , true , false ) ;
		}
	}

	public function automatch_by_sitelink ( $catalog_id ) {
		$this->set_catalog ( $catalog_id ) ;
		$entries = [] ;
		$sql = "SELECT * FROM entry WHERE catalog={$catalog_id} AND q IS NULL" ;
		$sql .= $this->get_sql_log_paranoia() ;
		$result = $this->mnm->getSQL ( $sql ) ;
		$this->mnm->openWikidataDB() ;
		while($o = $result->fetch_object()){
			$entries[$o->id] = $o ;
			$this->process_entries_batch($entries,$this->batch_size) ;
		}
		$this->process_entries_batch($entries,0) ;
	}

	public function automatch_by_search ( $catalog_id ) {
		$this->mnm->openWikidataDB();
		$this->set_catalog ( $catalog_id ) ;
		$sql = "SELECT *,(SELECT group_concat(DISTINCT label SEPARATOR '|') FROM aliases WHERE entry_id=entry.id) AS aliases" ;
		$sql .= " FROM entry WHERE catalog={$this->catalog_id} AND (q IS NULL OR q=-1)" ;
		$sql .= $this->get_sql_log_paranoia() ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			$j = $this->get_search ( $o->ext_name , $o->type ) ;
			$this->search_aliases($o->aliases,$j,$o->type);
			if ( count($j->query->search) == 0 ) continue ; # Nothing found

			$candidate_items = [] ;
			foreach ( $j->query->search AS $t ) $candidate_items[$t->title] = $t->title ;
			$this->filter_disambiguation_items ( $candidate_items ) ;
			$candidate_items = array_values ( $candidate_items ) ;

			# Single match
			if ( count($candidate_items) == 1 ) {
				$q = $candidate_items[0] ;
				$this->mnm->setMatchForEntryID ( $o->id , $q , 0 , true , true ) ;
				continue ;
			}

			# Multi-match
			$this->mnm->setMatchForEntryID ( $o->id , $candidate_items[0] , 0 , true , true ) ; # First one has highest quality in search
			$this->mnm->setMultiMatch ( $o->id , $candidate_items ) ;
		}

		# Unnecessary, but just in case...
		$catalog = new Catalog ( $this->catalog_id , $this->mnm ) ;
		$catalog->updateStatistics() ;
	}

	public function automatch_simple ( $catalog_id ) {
		$this->set_catalog ( $catalog_id ) ;
		$this->automatch_simple_check_catalog() ;

		$types = [] ;
		$name2ids = [] ;
		$this->automatch_simple_init ( $types , $name2ids ) ;
		$this->automatch_simple_limiter ( $types ) ;
		$linkto = $this->automatch_simple_types ( $types ) ;

		$names = [] ;
		foreach ( $name2ids AS $k => $v ) $names[] = $this->mnm->escape ( $k ) ;
		if ( count($names) == 0 ) return ;

		$this->mnm->openWikidataDB() ;
		$multimatch = [] ;
		$names = array_chunk ( $names , $this->name_batch_size ) ;
		foreach ( $names AS $names2 ) {
			$candidates = [] ;
			$name2q = [] ;
			$qlist = [] ;
			try {
				$this->automatch_simple_search_names ( $names2 , $name2q , $qlist ) ;
			} catch (Exception $ex) {
				# Ignore
			}
			if ( count($qlist) == 0 ) continue ; // Nothing to do

			// Filter disambiguation candidates
			$this->filter_disambiguation_items ( $qlist ) ;
			if ( count($qlist) == 0 ) continue ; // Nothing to do
			foreach ( $name2q AS $name => $q ) {
				if ( !isset($qlist[$q]) ) {
					unset ( $name2q[$name][$q] ) ;
					if ( count($name2q[$name]) == 0 ) unset ( $name2q[$name] ) ;
				}
			}
			
			try {
				if ( count($linkto) > 0 ) $this->automatch_simple_filter_by_type ( $qlist , $linkto , $name2q , $name2ids , $multimatch , $candidates ) ;
				else $this->automatch_simple_no_filter ( $name2q , $name2ids , $multimatch , $candidates ) ;
			} catch (Exception $ex) {
				# Ignore
			}

			foreach ( $candidates AS $entry => $q ) {
				$this->log_match ( $entry , $q ) ;
				$this->mnm->setMatchForEntryID ( $entry , $q , 0 , true , false ) ;
			}
		}

		foreach ( $multimatch AS $entry => $list ) $this->mnm->setMultiMatch ( $entry , $list ) ;
		$this->fix_broken_multimatch() ;
		$catalog = new Catalog ( $this->catalog_id , $this->mnm ) ;
		$catalog->updateStatistics() ;
	}

}

?>