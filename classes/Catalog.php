<?PHP

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class Catalog extends MnMbase {
	protected $data ;
	protected $data_loaded = false ;
	protected $mnm ;
	public static $default_metadata = [
		'use_description_for_new' => 1 ,
		'use_automatchers' => 1 ,
		'limiter' => '' ,
	] ;

	/**
	 * Constructor
	 *
	*/
	public function __construct ( /*int*/ $id = 0 , $mnm = '' ) {
		$this->id = $id ;
		$this->data = (object) [] ;
		if ( $mnm != '' ) $this->setMnM ( $mnm ) ;
	}


	public function loadCatalogs ( $catalog_ids = [] , $active_only = false ) {
		# Rows from `catalog`
		$conditions = [] ;
		if ( count($catalog_ids) > 0 ) $conditions[] = "`id` IN (" . implode(',',$catalog_ids) . ")" ;
		if ( $active_only ) $conditions[] = "`active`=1" ;
		$sql = "SELECT /* ".__METHOD__." */ * FROM `catalog`" ;
		if ( count($conditions) > 0 ) $sql .= " WHERE (" . implode(') AND (',$conditions) . ")" ;
		$catalogs = [] ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $catalogs[$o->id] = (array) $o ;

		# Key-value data
		$sql = "SELECT /* ".__METHOD__." */ * FROM `kv_catalog`" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			$catalog_id = $o->catalog_id ;
			if ( !isset($catalogs[$catalog_id]) ) continue ;
			$k = $o->kv_key ;
			$v = $o->kv_value ;
			if ( isset($catalogs[$catalog_id][$k]) ) {
				if ( !is_array($catalogs[$catalog_id][$k]) ) $catalogs[$catalog_id][$k] = [ $catalogs[$catalog_id][$k] ] ;
				$catalogs[$catalog_id][$k][] = $v ;
			} else $catalogs[$catalog_id][$k] = $v ;
		}

		# Default data
		foreach ( $catalogs AS $catalog_id => $catalog_data ) {
			foreach ( Catalog::$default_metadata AS $k => $v ) {
				if ( !isset($catalogs[$catalog_id][$k]) ) $catalogs[$catalog_id][$k] = $v ;
			}
		}

		return $catalogs ;
	}

	public function updateStatistics() {
		$this->checkValidID() ;
		$sql = "INSERT IGNORE INTO overview (catalog) VALUES ({$this->id})" ;
		$this->getSQL ( $sql ) ;

		$sql = "SELECT /* ".__METHOD__." */ IFNULL(count(*),0) AS total,IFNULL(sum(q is null),0) AS q_null,IFNULL(sum(q=0),0) AS q_zero,IFNULL(sum(q=-1),0) AS q_minus_one,IFNULL(sum(user=0),0) AS u_zero,IFNULL(sum(q>0 and user>0),0) AS manual FROM entry where catalog={$this->id}" ;
		$result = $this->getSQL ( $sql ) ;
		$o = $result->fetch_object() ;

		$sql = "UPDATE overview SET " ;
		$sql .= "total = {$o->total}," ;
		$sql .= "noq = {$o->q_null}," ;
		$sql .= "autoq = {$o->u_zero}," ;
		$sql .= "na = {$o->q_zero}," ;
		$sql .= "manual = {$o->manual}," ;
		$sql .= "nowd = {$o->q_minus_one}," ;
		$sql .= "multi_match = (SELECT count(*) FROM multi_match WHERE multi_match.catalog={$this->id})," ;
		$sql .= "types = (SELECT group_concat(DISTINCT `type` separator '|') FROM entry where catalog={$this->id})" ;
		$sql .= " WHERE catalog={$this->id}" ;
		$this->getSQL ( $sql ) ;
	}

	public function activate ( /*int*/ $mode = 1 ) {
		$this->setDataValue ( 'active' , intval($mode) ) ;
	}

	public function deactivate () {
		$this->activate ( 0 ) ;
	}

	public function isActive () {
		try {
			$this->loadData ( true ) ;
			return ( $this->data->active == 1 ) ;
		} catch (exception $e) {
			$this->last_error = $e->getMessage() ;
		}
		return false ;
	}

	public function useAutomatchers ( $state ) {
		$this->setDataValue ( 'use_automatchers' , intval($state) ) ;
	}

	public function usesAutomatchers () {
		$this->loadData ( true ) ;
		return $this->data->use_automatchers ;
	}

	public function createNew ( $meta , $user_id = 0 ) {
		if ( is_array($meta) ) $meta = (object) $meta ;
		if ( !is_object($meta) ) throw new \Exception(__METHOD__.': "$meta" needs to be an object (or array)' ) ; 
		if ( !isset($meta->name) ) throw new \Exception(__METHOD__.": 'name' property required" ) ; 
		$prop = preg_replace ( '/\D/' , '' , $meta->property ) ;
		$sql = "INSERT INTO catalog (`name`,`url`,`desc`,`type`," ;
		if ( $prop != '' ) $sql .= "`wd_prop`," ;
		$sql .= "`search_wp`,`owner`,`note`) VALUES (" ;
		$sql .= '"' . $this->mnm->escape($meta->name) . '",' ;
		$sql .= '"' . $this->mnm->escape($meta->url??'') . '",' ;
		$sql .= '"' . $this->mnm->escape($meta->desc??'') . '",' ;
		$sql .= '"' . $this->mnm->escape($meta->type??'') . '",' ;
		if ( $prop != '' ) $sql .= '"' . $prop . '",' ;
		$sql .= '"' . $this->mnm->escape($meta->lang??'en') . '",' ;
		$sql .= $user_id . ",'" . $this->mnm->escape($meta->note??'') . "')" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		$this->id = $this->mnm->dbm->insert_id ;
		$this->loadData ( true ) ;
		return $this->id ;
	}

	public function outputOverview ( &$out ) {
		$this->checkValidID() ;
		if ( !isset($out['data']) ) $out['data'] = [] ;
		$sql = "SELECT /* ".__METHOD__." */ * FROM `overview` WHERE overview.catalog={$this->id}" ; // overview is "manually" updated, but fast; vw_overview is automatic, but slow
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			foreach ( $o AS $k => $v ) $out['data'][$o->catalog][$k] = $v ;
		}

		$sql = "SELECT /* ".__METHOD__." */ * FROM `catalog` WHERE id={$this->id}" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			foreach ( $o AS $k => $v ) $out['data'][$o->id][$k] = $v ;
		}
	}

	public function data ( $force = false ) {
		$this->loadData ( $force ) ;
		return $this->data ;
	}

	public function setDataValue ( $field_name , $value ) {
		$this->checkValidID() ;
		$field_name_safe = $this->escape($field_name) ;
		if ( isset(self::$default_metadata[$field_name]) ) {
			# Remove old
			$sql = "DELETE FROM `kv_catalog` WHERE `catalog_id`={$this->id} AND `kv_key`='{$field_name_safe}'" ;
			$this->getSQL ( $sql ) ;

			# Add (all) new
			if ( !is_array($value) and !is_object($value) ) $value = [ $value ] ;
			foreach ( $value AS $v ) {
				if ( !isset($v) or $v == '' ) continue ;
				$value_safe = $this->escape($v) ;
				$sql = "INSERT IGNORE INTO `kv_catalog` (`catalog_id`,`kv_key`,`kv_value`) VALUES ({$this->id},'{$field_name_safe}','{$value_safe}')" ;
				$this->getSQL ( $sql ) ;
			}
		} else {
			$value_safe = $this->escape($value) ;
			$sql = "UPDATE `catalog` SET `{$field_name_safe}`={$value_safe} WHERE `id`={$this->id}" ;
			$this->getSQL ( $sql ) ;
		}
		$this->data->$field_name = $value ;
	}


	/**
	 * Returns all external IDs as array keys, with the MnM IDs as values
	 *
	*/
	public function get_all_external_ids() {
		$this->checkValidID() ;
		$ret = [] ;
		$sql = "SELECT /* ".__METHOD__." */ `id`,`ext_id` FROM `entry` WHERE `catalog`={$this->id}" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $ret[$o->ext_id] = $o->id ;
		return $ret ;
	}

	public function syncFromSPARQL ( $property ) {
		$this->checkValidID() ;
		$this->checkMnM() ;
		$this->mnm->sanitizeQ ( $property ) ;
		if ( $property*1 <= 0 ) return ;

		# Get manually matched ext_ids
		$ext_ids = [] ;
		$sql = "SELECT /* ".__METHOD__." */ `ext_id` FROM `entry` WHERE `catalog`={$this->id} AND `q` IS NOT NULL AND `q`>0 AND `user`>0" ;
		$result = $this->getSQL($sql) ;
		while ( $o = $result->fetch_object() ) $ext_ids[] = $o->ext_id ;

		# Get and check SPARQL results
		$sparql = "SELECT /* ".__METHOD__." */ ?q ?v { ?q wdt:P{$property} ?v }" ;
		$j = $this->mnm->tfc->getSPARQL ( $sparql ) ;
		if ( !isset($j) or $j === FALSE or !isset($j->results) ) return ;

		foreach ( $j->results->bindings AS $b ) {
			$v = trim($b->v->value) ;
			if ( in_array($v,$ext_ids) ) continue ; # Already set
			$q = $this->mnm->tfc->parseItemFromURL ( $b->q->value ) ;
			$this->mnm->sanitizeQ ( $q ) ;
			$this->mnm->setMatchForCatalogExtID ( $this->id , $v , $q , 3 , true , false ) ;
		}
	}

	protected function loadData ( $force = false ) {
		if ( $this->data_loaded and !$force ) return ;
		$this->checkValidID() ;

		# Row from catalog table
		$sql = "SELECT /*NO_RO*/ /* ".__METHOD__." */ * FROM `catalog` WHERE `id`={$this->id}" ;
		$result = $this->getSQL ( $sql ) ;
		if($o = $result->fetch_object()){
			$this->data = $o ;
		} else {
			throw new \Exception(__METHOD__.": Error retrieving catalog {$this->id} from database" ) ; 
		}

		# Misc metadata
		$sql = "SELECT /*NO_RO*/ /* ".__METHOD__." */ * FROM `kv_catalog` WHERE `catalog_id`={$this->id}" ;
		$result = $this->getSQL ( $sql ) ;
		while ($o = $result->fetch_object()) {
			$k = $o->kv_key ;
			if ( isset($this->data->$k) ) {
				if ( !is_array($this->data->$k) ) $this->data->$k = [ $this->data->$k ] ;
				$this->data->$k[] = $o->kv_value ;
			} else $this->data->$k = $o->kv_value ;
		}

		# Default metadata
		foreach ( self::$default_metadata AS $k => $v ) {
			if ( !isset($this->data->$k) ) $this->data->$k = $v ;
		}

		$this->data_loaded = true ;
	}

}
