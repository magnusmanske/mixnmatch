<?PHP

namespace MixNMatch ;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class LargeCatalogs {
	public $mnm ;
	protected $catalogs = [] ;
	protected $dblc ;

	function __construct ( $mnm = '' ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
	}

	public function getRequest ( $key , $default = '' ) {
		return $this->mnm->tfc->getRequest ( $key , $default ) ;
	}

	public function getCatalogs () {
		if ( count($this->catalogs) > 0 ) return $this->catalogs ;
		$sql = "SELECT * FROM `catalog`" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$this->catalogs[$o->id] = $o ;
		}
		return $this->catalogs ;
	}

	public function getSQL ( $sql ) {
		$this->connect2dblc() ;
		return $this->mnm->tfc->getSQL ( $this->dblc , $sql , 2 ) ;
	}

	protected function connect2dblc () {
		if ( !isset($this->dblc) ) $this->dblc = $this->mnm->tfc->openDBtool ( 'mixnmatch_large_catalogs_p' ) ;
	}

}