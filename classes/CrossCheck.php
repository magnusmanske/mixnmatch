<?php

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once ( '/data/project/mix-n-match/manual_lists/large_catalogs/shared.php' ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

class CrossCheck {
	protected $testing = false ;
	protected $verbose = false ;
	public $min_count_create = 3 ;
	public $mnm ;

	function __construct ( $mnm = '' , $testing = false ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
		$this->testing = $testing ;
		$this->verbose = $this->testing ;
		$this->qs = $this->mnm->tfc->getQS('mix-n-match');
		$this->lc = new \largeCatalog ( 2 ) ;
	}

	function search_for_person_name ( $name ) {
		$query = $this->mnm->sanitizePersonName ( $name ) ;
		$query .= ' haswbstatement:P31=Q5' ;
		$url = "https://www.wikidata.org/w/api.php?action=query&list=search&srnamespace=0&srlimit=500&format=json&srsearch=" . urlencode($query) ;
		try {
			$j = json_decode ( @file_get_contents ( $url ) ) ;
		} catch (\Exception $e) {
			print $e->getMessage()."\n" ;
			return [] ;
		}
		if ( !isset($j) or !isset($j->query) or !isset($j->query->search) ) return [] ;
		$results = $j->query->search ;
		if ( !isset($results) ) return [] ;
		return array_map(function($value) { return $value->title;} , $results);
	}

	function query_for_items_with_dates ( $year_born , $year_died ) {
		$year_born2 = $year_born*1 + 1 ;
		$year_died2 = $year_died*1 + 1 ;
		$sparql = "SELECT ?q WHERE {
			?q wdt:P569 ?dob. hint:Prior hint:rangeSafe true.
			FILTER(\"{$year_born}-00-00\"^^xsd:dateTime <= ?dob && ?dob < \"{$year_born2}-00-00\"^^xsd:dateTime)
			?q wdt:P570 ?dod. hint:Prior hint:rangeSafe true.
			FILTER(\"{$year_died}-00-00\"^^xsd:dateTime <= ?dod && ?dod < \"{$year_died2}-00-00\"^^xsd:dateTime)
			}" ;
		$ret = [] ;
		try {
			$ret = $this->mnm->tfc->getSPARQLitems($sparql,"q") ;
		} catch (\Exception $e) {
			print $e->getMessage()."\n" ;
		}
		return $ret ;
	}

	function match_all_entries_to_existing_item($entries,$q,$full_label) {
		$q = $this->mnm->sanitizeQ($q);
		#if ( preg_match)
		if ( !isset($q) or $q=='Q' or $q=='Q0' or $q=='0' or $q=='' or $q==null or !is_string($q) ) {
			if ( $this->verbose ) print "Invalid item to match to: {$q}\n" ;
			return ;
		}
		if ( $this->verbose ) print "Matching entries for {$full_label} to https://www.wikidata.org/wiki/{$q}\n" ;
		foreach ( $entries AS $entry ) {
			if ( $this->verbose ) print "- Matching entry https://mix-n-match.toolforge.org/#/entry/{$entry->id}\n" ;
			if ( !$this->testing ) $this->mnm->setMatchForEntryID($entry->id,$q,3,true,false);
		}
	}

	function label_born_died_exists($label,$year_born,$year_died,$entries,$full_label) {
		$search_items = $this->search_for_person_name($label)??[];
		$query_items = $this->query_for_items_with_dates($year_born,$year_died)??[];
		$intersection = array_intersect($search_items, $query_items) ;
		if ( !is_array($intersection) ) return true ; # Weird flex but OK...
		$intersection = array_values($intersection);
		if (count($intersection) > 0) {
			if ( count($intersection) == 1 ) $this->match_all_entries_to_existing_item($entries,$intersection[0],$full_label);
			else if ( $this->verbose ) print "Item(s) for {$full_label} exist: ".implode(',',$intersection)."\n" ;
			return true ;
		}
		return false ;
	}

	function create_new_item_and_match_entries($all_commands,$entries,$full_label) {
		if ( $this->testing ) $q = null ;
		else $q = $this->mnm->tfc->runCommandsQS ( $all_commands ) ;
		if ( isset($q) ) {
			$this->mnm->fix_references_in_item($q);
			if ( $this->verbose ) print "Created https://www.wikidata.org/wiki/{$q} for {$full_label}\n" ;
			foreach ( $entries AS $num => $entry ) {
				$this->mnm->setMatchForEntryID ( $entry->id , $q , 3 , true , false ) ;
				if ( $this->verbose ) print "- https://mix-n-match.toolforge.org/#/entry/{$entry->id}\n" ;
			}
		} else if ( $this->testing ) {
			if ( $this->verbose ) print "TESTING -- SKIPPING CREATION FOR {$full_label}\n" ;
		} else {
			if ( $this->verbose ) print "FAILED to CREATE NEW ITEM FOR {$full_label}\n" ;
		}
	}


	function process_entry_group($entry_ids,$year_born,$year_died) {
		$matched = [] ;
		$auto = [] ;
		$unmatched = [] ;
		$label = "" ;
		$sql = "SELECT * FROM entry WHERE id IN (".join(',',$entry_ids).")" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			$label = $o->ext_name ;
			if ( $o->q == null ) $unmatched[] = $o ;
			else if ( $o->user == 0 ) $auto[] = $o ;
			else if ( $o->q >= 0 ) $matched[] = $o ;
		}
		$auto_or_unmatched = array_merge($auto,$unmatched) ;
		$full_label = "{$label} ({$year_born}-{$year_died})" ;

		# At least one already matched entry
		if ( count($matched) > 0 ) {
			# Paranoia
			$qs = array_unique(array_map(function($entry) { return $entry->q; } , $matched));
			if ( count($qs) == 1 ) $this->match_all_entries_to_existing_item($auto_or_unmatched,$qs[0],$full_label);
			else if ( $this->verbose ) {
				if ( $this->verbose ) {
					print "More than two entries for {$full_label}, not touching this\n" ;
					print_r($matched);
				}
			}
			return ;
		}

		# No already matched entries
		if ( count($auto_or_unmatched) < $this->min_count_create ) return ;

		# Make sure no such entry on Wikidata
		if ( $this->label_born_died_exists($label,$year_born,$year_died,$auto_or_unmatched,$full_label) ) return ;

		# Generate QS commands
		try {
			$entry_ids = [] ;
			foreach ( $auto_or_unmatched AS $entry ) $entry_ids[] = $entry->id ;
			$all_commands = $this->mnm->getCreateItemCommandsForEntries($entry_ids,$this->lc,$this->verbose);
		} catch (\Exception $e) {
			print $e->getMessage()."\n" ;
		}
		if (!isset($all_commands) ) return ;

		if ( $this->testing ) print_r($all_commands);

		# Generate and run QS
		$this->create_new_item_and_match_entries($all_commands,$auto_or_unmatched,$full_label);
	}

	function run_catalog ( $catalog_id ) {
		$sql = "SELECT * FROM catalog WHERE id={$catalog_id}" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		if ( $o = $result->fetch_object() ) {
			if ( $o->active != 1 ) die ( "Catalog {$catalog_id} is not active\n" ) ;
		} else die ( "Catalog {$catalog_id} does not exist\n" ) ;

		$sql = "SELECT group_concat(DISTINCT e1.id) as entry_ids,pd1.year_born,pd1.year_died,count(DISTINCT e1.catalog) AS cnt 
			FROM entry e1,entry e2,person_dates pd1,person_dates pd2
			WHERE e1.id=pd1.entry_id AND e2.id=pd2.entry_id
			AND e2.catalog={$catalog_id} AND (e2.q is null or e2.user=0) AND pd2.year_born!='' AND pd2.year_died!=''
			AND pd1.year_born=pd2.year_born
			AND pd1.year_died=pd2.year_died
			AND e1.ext_name=e2.ext_name

			GROUP BY e1.ext_name,pd1.year_born,pd1.year_died
			HAVING cnt>=2
			ORDER BY cnt DESC" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			$entry_ids = explode ( "," , $o->entry_ids ) ;
			$this->process_entry_group($entry_ids,$o->year_born,$o->year_died);
		}

	}

	public function run_all_catalogs() {
		$sql = "SELECT * FROM catalog WHERE active=1 AND has_person_date='yes' ORDER BY rand()" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			$catalog_id = $o->id ;
			if ( $this->verbose ) print "Using catalog #{$catalog_id}: {$o->name}\n" ;
			$this->run_catalog ( $catalog_id ) ;
		}
	}

	public function run_random_catalog() {
		# Random catalog with person dates
		$sql = "SELECT * FROM catalog WHERE active=1 AND has_person_date='yes' ORDER BY rand() LIMIT 1" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		$o = $result->fetch_object();
		if ( $this->verbose ) print "Using catalog #{$o->id}: {$o->name}\n" ;
		$this->run_catalog ( $o->id ) ;
	}

}


?>