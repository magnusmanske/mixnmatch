<?php

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class Auxiliary {
	public $catalog_blacklist = [
		506
	] ;
	public $catalog_property_blacklist = [ # Catalog ID, property ID
		[2099,"P428"]
	] ;
	public $property_blacklist = [ 
		'P233' ,
		'P235' ,  # See https://www.wikidata.org/wiki/Topic:Ue8t23abchlw716q
		'P846' ,
		'P2528' ,
		'P4511'
	] ;
	public $catalogs_not_to_sync_to_wikidata = [
		655
	] ;
	public static $props_also_use_lowercase = [2002] ;

	public $mnm ;
	private $testing = false ;
	public $properties_that_have_external_ids ;
	private $properties_using_items ;

	function __construct ( $testing = false , $mnm = '' ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
		$this->testing = $testing ;
	}

	public function isCatalogPropertyCombinationSuspect ( $catalog_id , $property ) {
		$this->mnm->sanitizeQ ( $catalog_id ) ;
		$this->mnm->sanitizeQ ( $property ) ;
		$property = "P{$property}" ;
		foreach ( $this->catalog_property_blacklist AS $cp ) {
			if ( $catalog_id == $cp[0] and $property == $cp[1] ) return true ;
		}
		return false ;
	}

	public function internalAuxiliaryMatcher ( $catalog_id=0 ) {
		$property_blacklist = [17,18,19,27,50,106,131,170,2528,233] ;
		$log_file = '/data/project/mix-n-match/internal_auxiliary_matcher.log' ;

		$sql = "SELECT /* ".__METHOD__." */ e1.id,e1.catalog,e2.q FROM entry e1, entry e2,auxiliary,catalog
		WHERE entry_id=e1.id
		AND e1.catalog IN (SELECT id FROM catalog WHERE active=1)
		AND (e1.q IS NULL OR e1.user=0)
		AND catalog.id=e2.catalog AND catalog.active=1
		AND wd_prop>0 AND wd_prop=aux_p AND wd_qual IS NULL
		AND e2.ext_id=aux_name
		AND e2.q>0 AND e2.q IS NOT NULL AND e2.user>0" ;
		$sql .= " AND `aux_p` NOT IN (" . implode(',',$property_blacklist) . ")" ;
		if ( isset($catalog_id) ) $sql .= " AND catalog.id={$catalog_id}";
		$sql .= " LIMIT 5000";

		$catalogs = [] ;
		$id2q = [] ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$id2q["{$o->id}"] = $o->q ;
			$catalogs["{$o->catalog}"] = true ;
		}

		$ts = $this->mnm->getCurrentTimestamp();

		$log_line = "{$ts}\n" ;
		file_put_contents ( $log_file , $log_line , FILE_APPEND ) ;

		foreach ( $id2q AS $entry_id => $q ) {
			$this->mnm->setMatchForEntryID ( $entry_id , $q , 4 , true , false ) ;
		}

		foreach ( $catalogs AS $cid => $dummy ) {
			$this->mnm->queue_job($cid,'microsync');
		}
	}

	protected function auiliaryEntriesGenerator ( int $catalog , $use_random , int $batch_size = 5000 ) {
		if ( $use_random or !isset($catalog) or $catalog<=0 ) { # Random catalog
			$sql = "SELECT /* ".__METHOD__." */ * FROM catalog WHERE active=1 ORDER BY rand() LIMIT 1" ;
			$result = $this->mnm->getSQL($sql);
			if($o = $result->fetch_object()) $catalog = $o->id ;
		}

		#$r = rand()/getrandmax()  ;
		$sql = "SELECT /* ".__METHOD__." */ entry_id,aux_p,aux_name,catalog,auxiliary.id AS aux_id FROM entry,auxiliary WHERE entry.id=entry_id AND (q is null or user=0)";
		#if ( $use_random ) $sql .= " AND random>=$r" ;
		#if ( $catalog != 0 ) 
		$sql .= " AND catalog={$catalog}" ;
		if ( count($this->catalog_blacklist) > 0 ) $sql .= " AND catalog NOT IN (" . implode(',',$this->catalog_blacklist) . ")" ;
		#if ( $use_random ) $sql .= " ORDER BY random" ;
		$results = [] ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$results[] = $o ;
			if ( count($results)<$batch_size ) continue ;
			yield $results ;
			$results = [] ;
		}
		if ( count($results)>0 ) yield $results ;
		else yield from [] ;
	}

	public function matchEntriesViaAuxiliary ( $catalog , $use_random = false ) {
		$catalogs_updated = [] ;
		$ts = $this->mnm->getCurrentTimestamp() ;

		foreach ( $this->auiliaryEntriesGenerator($catalog,$use_random) AS $results ) {
			$batchsize = 30 ;
			$grouped = [] ;
			foreach ( $results AS $o ) {
				$p = $o->aux_p ;
				if ( $this->isCatalogPropertyCombinationSuspect ( $catalog , $p ) ) continue ;

				if ( !isset($grouped[$o->catalog]) ) $grouped[$o->catalog] = [] ;
				if ( !isset($grouped[$o->catalog][$p]) ) $grouped[$o->catalog][$p] = [ [] ] ;
				$lg = count($grouped[$o->catalog][$p])-1 ;
				if ( count($grouped[$o->catalog][$p][$lg]) >= $batchsize ) $grouped[$o->catalog][$p][++$lg] = [] ;
				$grouped[$o->catalog][$p][$lg][] = $o ;
			}


			foreach ( $grouped AS $catalog => $prop_groups ) {
				foreach ( $prop_groups AS $prop => $groups ) {
					$also_use_lowercase = in_array($prop,self::$props_also_use_lowercase) ;
					foreach ( $groups AS $group ) {
						$ids = [] ;
						$aux2entry = [] ;
						foreach ( $group AS $o ) {
							if ( preg_match ( '/"/' , $o->aux_name ) ) continue ;
							$an = trim($o->aux_name) ;
							$ids[] = '"' . $an . '"' ;
							$aux2entry[$an] = $o->entry_id ;
							if ( $also_use_lowercase and strtolower($an)!=$an ) {
								$an = strtolower($an);
								$ids[] = '"' . $an . '"' ;
								$aux2entry[$an] = $o->entry_id ;
							}
						}
						$sparql = "SELECT ?q ?id WHERE { VALUES ?id { " . implode(' ',$ids) . " } ?q wdt:P$prop ?id }" ;
						$j = $this->mnm->tfc->getSPARQL ( $sparql ) ;
						foreach ( $j->results->bindings AS $b ) {
							$id = $b->id->value ;
							$q = $this->mnm->tfc->parseItemFromURL ( $b->q->value ) ;

							if ( !isset($aux2entry[$id]) ) {
								$this->log ( "Failed lookup aux2entry for {$prop}:'{$id}'\n" ) ;
								continue ;
							}
							$entry_id = $aux2entry[$id] ;

							# Check if this is the only item with that property/value
							$search_result_items = $this->mnm->getCachedWikidataSearch ( '' , $prop , $id ) ;
							if ( count($search_result_items) > 1 ) {
								$this->log ( "WD items with {$prop}:'{$id}': " . json_encode($search_result_items) , 2 ) ;
								//$this->mnm->addIssue ( $o->id , 'WD_DUPLICATE' , $search_result_items ) ;
								continue ;
							}

							# Set new match
							$this->log ( $this->mnm->getEntryURL($entry_id) . " => https://www.wikidata.org/wiki/{$q} because {$prop}:'{$id}'" , 2 ) ;
							$this->mnm->setMatchForEntryID ( $entry_id , $q , 4 , true , false ) ;
							$catalogs_updated[$catalog] = 1 ;
						}
					}
				}

			}
		}
		foreach ( $catalogs_updated AS $catalog_id => $dummy ) {
			$catalog = new Catalog ( $catalog_id , $this->mnm ) ;
			$catalog->updateStatistics() ;
			$this->mnm->queue_job($catalog_id,'aux2wd');
		}
	}

	# Generator
	protected function getCandidateEntriesAndQs ( int $catalog , int $chunk_size = 5000 ) {
		$id2q = [] ;
		$sql = '' ;
		if ( $catalog > 0 ) {
			$sql = "SELECT /* ".__METHOD__." */ id,q FROM entry WHERE catalog=$catalog AND user>0 AND q IS NOT NULL AND q>0" ;
			$this->mnm->loadCatalog($catalog,true);
		} else {
			$r = rand()/getrandmax()  ;
			$sql = "SELECT /* ".__METHOD__." */ id,q FROM entry WHERE random>=$r AND user>0 AND q IS NOT NULL AND q>0 AND catalog NOT IN (".
				implode(",",$this->catalogs_not_to_sync_to_wikidata).
				") ORDER BY random LIMIT 20000" ;
		}
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$id2q[$o->id] = $o->q ;
			if ( count($id2q)<$chunk_size ) continue ;
			yield $id2q ;
			$id2q = [] ;
		}
		if ( count($id2q)>0 ) yield $id2q ;
		else yield from [] ;
	}

	# $o is entry object
	protected  function getSourceForEntry ( $o ) {
		$source = "" ;
		if ( !isset($this->mnm->catalogs[$o->catalog]) ) return $source ;
		$catalog = $this->mnm->catalogs[$o->catalog] ;

		$stated_in = "" ;
		if ( isset($catalog->source_item) ) {
			$stated_in = "\tS248\t{$catalog ->source_item}" ;
		}
		if ( isset($catalog->wd_prop) ) {
			$prop = $catalog->wd_prop ;
			# Try to get "applicable 'stated in' value from property
			if ( $stated_in == "" ) {
				$this->mnm->wil->loadItems ( ["P{$prop}"] ) ;
				$i = $this->mnm->wil->getItem("P{$prop}") ;
				if ( isset($i) ) {
					$claims = $i->getClaims("P9073");
					if ( count($claims) > 0 ) {
						$source_q = $i->getTarget($claims[0]) ;
						$stated_in = "\tS248\t{$source_q}" ;
					}
				}
			}
			$source = "{$stated_in}\tS{$prop}\t\"{$o->ext_id}\"" ;
		} else if ( $o->ext_url != '' ) { # Fallback: Link to external entry URL
			$source = "{$stated_in}\tS854\t\"{$o->ext_url}\"" ;
		} else { # Final fallback: link to Mix'n'match entry
			$source = "{$stated_in}\tS854\t\"https://mix-n-match.toolforge.org/#/entry/{$o->id}\"" ;
		}
		return $source ;
	}

	public function addAuxiliaryToWikidata ( int $catalog ) {
		if ( in_array($catalog, $this->catalogs_not_to_sync_to_wikidata) ) return ;
		$this->initializePropertyLists() ;

		$blacklisted_prop_ids = [] ;
		foreach ( $this->property_blacklist AS $prop ) {
			$this->mnm->sanitizeQ ( $prop ) ;
			$blacklisted_prop_ids[] = $prop ;
		}

		$qs = $this->mnm->tfc->getQS('mixnmatch:aux2wd','/data/project/mix-n-match/bot.ini',true) ;

		foreach ( $this->getCandidateEntriesAndQs ( $catalog ) AS $id2q ) {
			$this->log ( "ID2Q : ".count($id2q) , 2 ) ;

			$aux = [] ;
			$sources = [] ;
			$sql = "SELECT /* ".__METHOD__." */ * FROM vw_aux WHERE id IN (" . implode(',',array_keys($id2q)) . ") AND in_wikidata=0" ;
			$sql .= " AND aux_p NOT IN (" . implode ( ',' , $blacklisted_prop_ids ) . ")" ;
			$result = $this->mnm->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) {
				$q = 'Q'.$id2q[$o->id] ;
				if ( $q == 'Q0' ) continue ;

				if ( $this->isCatalogPropertyCombinationSuspect ( $o->catalog , $o->aux_p ) ) continue ;
				$aux[$q][] = [ 'p' => 'P'.$o->aux_p , 'v' => $o->aux_name , 'aux_id' => $o->aux_id , 'entry_id' => $o->id ] ;

				$source = $this->getSourceForEntry ( $o ) ;
				if ( $source != "" ) $sources[$q] = preg_replace ( '|^\tS|' , "\t!S" , $source ) ;
			}
			unset ( $id2q ) ;

			$this->log ( "AUX : ".count($aux) , 2 ) ;
			if ( count($aux) == 0 ) continue ; // Nothing to do

			$aux_chunked = array_chunk ( $aux , 50 , true ) ;

			foreach ( $aux_chunked AS $chunk_num => $aux ) {
				$this->log ( "Chunk #{$chunk_num}" , 2 ) ;
				$wil = new \WikidataItemList ;
				$wil->loadItems ( array_keys ( $aux ) ) ;

				$commands = [] ;
				foreach ( $aux AS $q => $values ) {
					$i = $wil->getItem ( $q ) ;
					if ( !isset($i) ) {
						$this->mnm->addIssue ( $values['entry_id'] , 'ITEM_DELETED' , [$q] ) ;
						continue ;
					}

					$source = '' ;
					if ( isset($sources[$q]) ) {
						$source = $sources[$q] ;
						# Deactivated as per https://www.wikidata.org/wiki/Topic:V2tl70hbgsvundhx
						#$source .= "\tS813\t" . $this->mnm->date2expression(date('Y-m-d')) ;
					}

					foreach ( $values AS $v ) {
						$p = $v['p'] ;
						if ( $i->hasClaims($p) ) continue ;

						if ( !isset($v['entry_id']) ) $entry_link = '' ;
						else $entry_link = "\t/*via https://mix-n-match.toolforge.org/#/entry/{$v['entry_id']} ;*/" ;


						$value = trim($v['v']) ;
						$alt = array ( $value ) ;
						if ( $p == 'P856' ) {
							$value = preg_replace ( '/\/$/' , '' , $value ) ;
							$alt[] = preg_replace('/^.+?:/','http:',$value) ;
							$alt[] = preg_replace('/^.+?:/','https:',$value) ;
							$alt[] = preg_replace('/^.+?:/','http:',"$value/") ;
							$alt[] = preg_replace('/^.+?:/','https:',"$value/") ;
						}

						$exists = false ;
						foreach ( $alt AS $a ) {
							#if ( in_array ( strtolower($a) , $existing ) ) $exists = true ;
						}

						if ( !$exists and in_array ( $p , $this->properties_that_have_external_ids) ) {
							$search_results = $this->mnm->getSearchResults ( '' , $p , $value ) ;
							if ( isset($search_results) and count($search_results) > 0 ) {
								$sql = '' ;
								if ( count($search_results) == 1 and $search_results[0]->title == $q ) {
									// Already have that
									$sql = "UPDATE auxiliary SET in_wikidata=1 WHERE id={$v['aux_id']} AND in_wikidata=0" ;
								} else {
									$items = [] ;
									foreach ( $search_results AS $sr ) {
										if ( $q != $sr->title ) $items[] = $sr->title ;
									}
									if ( count($items) == 1 ) {
										$this->mnm->addIssue($v['entry_id'],'MISMATCH',[$q,$items[0]]);
									} else if ( count($items) > 0 ) {
										$this->mnm->addIssue($v['entry_id'],'WD_DUPLICATE',$items);
									}
								}
								if ( $sql != '' ) $this->mnm->getSQL($sql) ;
								$exists = true ;
							}
						}

						if ( $exists ) continue ;

						if ( $this->mnm->hasPropertyEverBeenRemovedFromItem ( $q , $p ) ) {
							$this->log ( "hasPropertyEverBeenRemovedFromItem: {$q}/{$p}" , 2 ) ;
							continue ;
						}

						if ( in_array ( $p , $this->properties_using_items ) ) {
							$commands[] = "{$q}\t{$p}\t{$value}{$source}{$entry_link}" ;
						} else if ( in_array ( $p , $this->property_blacklist ) ) {
							// Don't use
						} else if ( $p == 'P571' ) {
							$commands[] = "{$q}\t{$p}\t{$value}{$source}{$entry_link}" ;
							$this->log ( json_encode($commands) , 2 ) ; // TODO what is this?
							return;
						} else if ( in_array ( $p , ['P625'] ) ) {
							if ( preg_match ( '/^\@[0-9\.\-]+\/[0-9\.\-]+$/' , $value ) ) $commands[] = "{$q}\t{$p}\t{$value}{$source}{$entry_link}" ;
							else $commands[] = "{$q}\t{$p}\t@" . str_replace ( ',' , '/' , $value ) . $source . $entry_link ;
						} else if ( $p == 'P973' ) {
							if ( isset($sources[$q]) and $sources[$q] == "{$p}\t\"{$value}\"" ) {
								$commands[] = "{$q}\t{$p}\t\"{$value}\"{$entry_link}" ; # No source, is its own source
							} else {
								$commands[] = "{$q}\t{$p}\t\"{$value}\"{$source}{$entry_link}" ;
							}
						} else $commands[] = "{$q}\t{$p}\t\"{$value}\"{$source}{$entry_link}" ;
					}
					$this->mnm->fix_references_in_item($q);
				}
				if ( count($commands) > 0 ) {
					$this->mnm->tfc->runCommandsQS ( $commands , $qs ) ;
				}
			}
		}
	}

	public function initializePropertyLists () {
		# Get properties with item type
		if ( !isset($this->properties_using_items) ) {
			$sparql = 'SELECT ?p WHERE { ?p rdf:type wikibase:Property; wikibase:propertyType wikibase:WikibaseItem }' ;
			$this->properties_using_items = $this->mnm->tfc->getSPARQLitems($sparql,'p');
		}

		# Get properties with ExternalId type
		if ( !isset($this->properties_that_have_external_ids) ) {
			$sparql = 'SELECT ?p WHERE { ?p rdf:type wikibase:Property; wikibase:propertyType wikibase:ExternalId }' ;
			$this->properties_that_have_external_ids = $this->mnm->tfc->getSPARQLitems($sparql,'p');
		}
	}

	private function log ( $message , $level = 1 ) {
		if ( !$this->testing and $level > 1 ) return ;
		print "{$message}\n" ;
	}
}

?>