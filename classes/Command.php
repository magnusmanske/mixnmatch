<?php

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

# ________________________________________________________________________________________________________________________
# Command
# To log commands for processing, testing, debugging, interface

class Command implements \JsonSerializable {
	const PERSON_DATES = 1;
	const AUXILIARY = 2;
	const ENTRY_PART = 3;
	const ALIAS = 4;
	const LOCATION = 5;
	const LOCATION_TEXT = 6;
	const ENTRY = 7;
	const MATCH = 8 ;

	const ADD = 1;
	const SET = 2;
	const REMOVE = 3;

	private $target ;
	private $action ;
	private $entry_id ;
	private $data ;

	/**
	 * Constructor
	 *
	 * @param $target int a target constant
	 * @param $action int a target constant
	 * @param $entry_id int an ID in the entry table
	 * @param $data generic commands-specific data, usually an object
	*/
	public function __construct ( int $target , int $action , int $entry_id , $data ) {
		$this->target = $target ;
		$this->action = $action ;
		$this->data = $data ;
		$this->entry_id = $entry_id ;
	}

	public static function addAux ( int $entry_id , $property , string $value ) : Command {
		return new Command ( self::AUXILIARY , self::ADD , $entry_id , (object) ['property'=>$property,'value'=>$value] ) ;
	}

	public static function setDescription ( int $entry_id , string $value ) : Command {
		return new Command ( self::ENTRY_PART , self::SET , $entry_id , (object) ['key'=>'ext_desc','value'=>$value] ) ;
	}

	public static function setEntryName ( int $entry_id , string $value ) : Command {
		return new Command ( self::ENTRY_PART , self::SET , $entry_id , (object) ['key'=>'ext_name','value'=>$value] ) ;
	}

	public static function setEntryType ( int $entry_id , string $value ) : Command {
		return new Command ( self::ENTRY_PART , self::SET , $entry_id , (object) ['key'=>'type','value'=>$value] ) ;
	}

	public static function setPersonDates ( int $entry_id , string $born , string $died , bool $is_matched = false ) : Command {
		return new Command ( self::PERSON_DATES , self::SET , $entry_id , (object) ['born'=>$born,'died'=>$died,'is_matched'=>$is_matched] ) ;
	}

	public static function setLocation ( int $entry_id , float $lat , float $lon ) : Command {
		return new Command ( self::LOCATION , self::SET , $entry_id , (object) ['lat'=>$lat,'lon'=>$lon] ) ;
	}

	public static function addAlias ( int $entry_id , string $value , string $language = '' ) : Command {
		return new Command ( self::ALIAS , self::ADD , $entry_id , (object) ['value'=>$value,'language'=>$language] ) ;
	}

	public static function setLocationText ( int $entry_id , $property , string $value ) : Command {
		return new Command ( self::LOCATION_TEXT , self::SET , $entry_id , (object) ['property'=>$property,'value'=>$value] ) ;
	}

	public static function setMatch ( int $entry_id , string $q ) : Command {
		return new Command ( self::MATCH , self::SET , $entry_id , (object) ['q'=>$q] ) ;
	}

	public static function addEntry ( object $o ) : Command {
		return new Command ( self::ENTRY , self::ADD , 0 , $o ) ;
	}

	public function getTarget() {
		return $this->target ;
	}

	public function getAction() {
		return $this->action ;
	}

	public function getData() {
		return $this->data ;
	}

	public function getEntryID() {
		return $this->entry_id ;
	}

	public function enact ( &$mnm ) {
		if ( $this->action == self::ADD and $this->target == self::ENTRY )
			return $mnm->addNewEntry ( $this->data ) ;

		$entry_id = $this->entry_id * 1 ;
		if ( $entry_id <= 0 ) throw new \Exception("Bad entry ID '{$this->entry_id}' for command ".json_encode($this)) ;

		if ( $this->action == self::ADD and $this->target == self::AUXILIARY )
			return $mnm->setAux ( $entry_id , $this->data->property , $this->data->value ) ;
		if ( $this->action == self::SET and $this->target == self::LOCATION )
			return $mnm->setLocation ( $entry_id , $this->data->lat , $this->data->lon ) ;
		if ( $this->action == self::ADD and $this->target == self::ALIAS )
			return $mnm->setAlias ( $entry_id , $this->data->value , $this->data->language ) ;
		if ( $this->action == self::SET and $this->target == self::PERSON_DATES )
			return $mnm->setPersonDates ( $entry_id , $this->data->born , $this->data->died ) ;
		if ( $this->action == self::SET and $this->target == self::MATCH )
			return $mnm->setMatchForEntryID ( $entry_id , $this->data->q , 4 , true , false ) ;

		if ( $this->action == self::SET and $this->target == self::ENTRY_PART ) {
			$key = $this->data->key ;
			if ( !in_array($key,['ext_name','ext_desc','type']) ) throw new \Exception("Bad key in command ".json_encode($this)) ;
			if ( $key == 'ext_desc' ) {
				try {
					$mnm->setDescriptionForEntryID ( $entry_id , $this->data->value ) ;
				} catch (Exception $e) {
					#echo 'Caught exception: ',  $e->getMessage(), "\n";
				}
			} else {
				$value = $mnm->escape ( $this->data->value ) ;
				$mnm->getSQL ( "UPDATE `entry` SET `{$key}`='$value' WHERE id={$entry_id}" ) ;
			}
			return ;
		}

		if ( $this->action == self::SET and $this->target == self::LOCATION_TEXT ) {
			$property = $mnm->sanitizeQ($this->data->property) ;
			$value = $mnm->escape($this->data->value) ;
			$mnm->getSQL("INSERT IGNORE INTO `statement_text` (entry_id,property,`text`) VALUES ({$entry_id},{$property},'{$value}')");
			return ;
		}

		// Unknown target/command combination
		throw new \Exception ( "Can not enact command: " . json_encode($this) ) ;
	}

	public function jsonSerialize() {
		return get_object_vars($this);
	}

}

?>