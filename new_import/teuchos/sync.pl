#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use URI::Escape;
use Data::Dumper ;
use JSON ;

my $catalog_id = 87 ;

my $first = 1 ;
open OUT, "> out.json" or die $! ;
print OUT '{"catalog":' . $catalog_id . ',"entries":' ;
print OUT "[\n" ;

batch_parse () ;

print OUT "\n]}" ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}

sub batch_parse {
	my $index_page_list = generate_pagelist () ;
	foreach my $url ( @{$index_page_list} ) {
		my $page = get $url ;
		parse_page ( $page , $url ) ;
	}
}

sub continuous_parse {
	my ( $url_pattern , $start , $step ) = @_ ;
	my $pos = $start ;
	while ( 1 ) {
		my $url = $url_pattern ;
		$url =~ s/\$1/$pos/g ;
		my $page = get $url ;
		my $found = parse_page ( $page ) ;
		last if $found == 0 ;
		$pos += $step ;
	}
}

##########

sub generate_pagelist {
	return [ 'http://beta.teuchos.uni-hamburg.de/TeuchosWebUI/prosopography/tx-frame-search-biographies.action?index=0&number=10000' ] ;
}

sub parse_page {
	my ( $page , $url ) = @_ ;
	$page =~ s/\s+/ /g ;
	while ( $page =~ m/<div class="teuchos-search-results-data">\s*<p>\s*<a href="([^"]+)">\s*(.+?)<\/a>\s*<\/p>\s*<\/div>/g ) {
		
		my $out = get_blank() ;
		$out->{url} = "http://beta.teuchos.uni-hamburg.de$1" ;
		$out->{name} = $2 ;
		
		next unless $out->{url} =~ m|%2Fteuchosx%3AP-(.+?)&amp;searchSurname| ;
		$out->{id} = uri_unescape ( $1 ) ;
		
		while ( $out->{name} =~ m|\((.+?)\)|g ) {
			$out->{desc} .= '; ' unless $out->{desc} eq '' ;
			$out->{desc} .= $1 ;
		}
		$out->{name} =~ s|\((.+?)\)||g ;
		$out->{name} =~ s|\s+| |g ;
		$out->{name} =~ s| $|| ;
		$out->{name} =~ s| ,|,|g ;
		
		$out->{name} =~ s|^(.+?), (.+)$|$2 $1| ;
		
		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
	}
}

