#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;


open OUT, "> out.json" or die $! ;

my $last_id = '' ;
my $first = 1 ;
print OUT '{"catalog":70,"entries":' ;
print OUT "[\n" ;
foreach my $num ( 0 .. 20000 ) {
	my $url = "http://catalogue.nli.ie/Browse/Author?from=&page=$num" ;
	my $page = get $url ;
	parse_page ( $page ) ;
}
print OUT "\n]}" ;

0 ;

sub parse_page {
	my ( $page ) = @_ ;
	$page =~ s/\s+/ /g ;
	while ( $page =~ m|<a href="/Search/Results\?type=ids&amp;lookfor=([a-z]+\d+)\+">(.+?)</a>|g ) {

		my $out = get_blank() ;
		$out->{id} = $1 ;
		$out->{url} = "http://catalogue.nli.ie/Search/Results?type=ids&lookfor=$1+" ;
		$out->{name} = $2 ;
		
		$out->{name} =~ s|&quot;|"|g ;
		
		if ( $out->{name} =~ m|^([^,]+), ([^,]+), (.+)$| ) {
			$out->{desc} = $3 ;
			$out->{name} = "$2 $1" ;
		} else {
			$out->{name} =~ s|^([^,]+), ([^,]+)$|$2 $1| ;
		}

		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
	}
}

sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}