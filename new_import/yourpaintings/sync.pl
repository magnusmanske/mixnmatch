#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

# ATTENTION: This does not generate descriptions, as these are only in the individual pages!


open OUT, "> out.json" or die $! ;

my $first = 1 ;
print OUT '{"catalog":2,"entries":' ;
print OUT "[\n" ;
foreach my $letter ( 'A' .. 'Z' ) {
	my $last_page = '' ;
	foreach my $num ( 1 .. 20 ) {
		my $url = "http://www.bbc.co.uk/arts/yourpaintings/artists?letter=$letter&type=list&num=200&page=$num" ;
		my $page = get $url ;
		last if $page eq $last_page ;
		$last_page = $page ;
		parse_page ( $page ) ;
	}
}
print OUT "\n]}" ;

0 ;

sub parse_page {
	my ( $page ) = @_ ;
	my @rows = split "\n" , $page ;
	foreach my $r ( @rows ) {
		next unless $r =~ m/class="artist"/ ;
		next unless $r =~ m|href="(http://www.bbc.co.uk/arts/yourpaintings/artists/)(.+?)"| ;
		my ( $url , $id ) = ( $1.$2 , $2 ) ;
		next unless $r =~ m|<strong>(.+?)</strong>, (.+?)</a>| ;
		my $name = "$2 $1" ;
		my $desc = '' ;
		if ( $id =~ m|-b-(\d{4})\b| ) {
			$desc = "born $1" ;
		}
		if ( $id =~ m|-d-(\d{4})\b| ) {
			$desc .= "; " if $desc ne '' ;
			$desc .= "died $1" ;
		}
		
		my $out = {
			'id' => $id ,
			'url' => $url ,
			'name' => $name ,
			'desc' => $desc ,
			'aux' => []
		} ;
		
		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
		
#		print "$id\t$url\t$name\t\n" ;
	}
}
