#!/usr/bin/python
import csv
import json
import re

regex = re.compile(r"^([\S]+), (.+)$")

#id,catalog,ext_id,ext_url,ext_name,ext_desc,q,user,timestamp,random,type

j = { 'catalog':67 , 'entries':[] }
with open('data_from_nono.csv') as csvfile:
	reader = csv.reader ( csvfile , delimiter=',', quotechar='"')
	for row in reader:
		if row[0] == 'id' :
			continue
		id = row[0]
		name = row[4].replace("'","\\'")
		
		matchObj = re.match( r'^([\S]+), (.+)$', name)
		if matchObj:
			name = matchObj.group(2) + ' ' + matchObj.group(1)[0] + matchObj.group(1)[1:].lower()
		
		desc = row[5].replace("'","\\'")
		print "UPDATE entry SET ext_name='"+name+"',ext_desc='"+desc+"' WHERE catalog=67 AND id="+id+";"

