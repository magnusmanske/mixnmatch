#!/usr/bin/perl

# wget -o authors.gz http://openlibrary.org/data/ol_dump_authors_latest.txt.gz

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my $catalog_id = 98 ;

my $first = 1 ;
open OUT, "> out.json" or die $! ;
print OUT '{"catalog":' . $catalog_id . ',"entries":' ;
print OUT "[\n" ;

open PIPE , 'zcat ol_dump_authors_latest.txt.gz |' ;
while ( <PIPE> ) {
	chomp ;
	my @d = split "\t" , $_ ;
	next unless $d[0] eq '/type/author' ;
	
	my $out = get_blank() ;
	next unless $d[1] =~ m|^/authors/(.+)$| ;
	$out->{id} = $1 ;
	$out->{url} = "https://openlibrary.org/works/$1" ;
	
	my $j = decode_json ( $d[4] ) ;
	$out->{name} = $j->{name} ;
	
	next unless defined $j->{birth_date} ; # SUBSET
	next unless defined $j->{death_date} ; # SUBSET
	
	my @desc ;
	push @desc , "born " . $j->{birth_date} if defined $j->{birth_date} ;
	push @desc , "died " . $j->{death_date} if defined $j->{death_date} ;
	push @desc , "location: " . $j->{location} if defined $j->{location} ;
	if ( defined $j->{links} ) {
		foreach my $l ( @{$j->{links}} ) {
			if ( $l->{title} eq 'biography' ) { push @desc , "Bio: " . $l->{url} }
			if ( $l->{title} eq 'VIAF' ) { push @desc , "VIAF: " . $l->{url} }
		}
	}
	
	$out->{desc} = join "; " , @desc ;


	if ( $first ) { $first = 0 ; }
	else { print OUT ",\n" ; }
	print OUT encode_json ( $out ) ;
}
close PIPE ;

print OUT "\n]}" ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}
