#!/usr/bin/php
<?php

exec('./try_all.sh') ; # To refesh; takes a long time

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;

$catalog = 2 ; # Art UK artist ID

$mnm = new MixNMatch ;

$has_ids = [] ;
$sql = "SELECT ext_id FROM entry WHERE catalog={$catalog}" ;
$result = $mnm->getSQL($sql) ;
while ( $o = $result->fetch_object() ) $has_ids[$o->ext_id] = 1 ;

$dir = '/data/project/mix-n-match/manual_lists/art_uk/artists' ;

$files = scandir($dir) ;
foreach ( $files AS $file ) {
	if ( !preg_match ( '|\.html$|' , $file ) ) continue ;

	$html = file_get_contents("{$dir}/{$file}") ;
	$html = preg_replace ( '|\s+|' , ' ' , $html ) ;
	if ( !preg_match_all ( '|<li [^>]*\bid="(.+?)".*?>.*?<span class="title">(.+?)<.*?</li>|' , $html , $lis , PREG_SET_ORDER ) ) continue ;
	foreach ( $lis AS $li ) {
		$id = trim($li[1]) ;
		if ( isset($has_ids[$id]) ) continue ;
		$name = trim($li[2]) ;
		#print "{$id} : {$name}\n" ;
		$desc = '' ;
		$type = 'Q5';
		if ( preg_match ( '|^(.+?)\s*(\(.*)$|' , $name , $m ) ) {
			$name = trim($m[1]) ;
			$desc = trim($m[2]) ;
		}

		$desc = trim ( preg_replace ( '|[\(\())]|' , '' , $desc ) ) ;

		if ( preg_match ( '/\b(ltd\.|school|british|group|academy|art)\b/i' , $name ) ) {
			$type = 'Q43229' ;
		}

		#print "{$id} : {$name} | {$desc} | {$type}\n" ;

		$o = (object) [
			'catalog' => $catalog,
			'name' => $name,
			'id' => $id,
			'url' => 'https://artuk.org/discover/artists/'.$id ,
			'type' => $type,
			'desc' => $desc
		] ;
		#print_r($o);

		$has_ids[$id] = 1 ;
		$mnm->addNewEntry($o) ;
	}
}

// Deactivated due to bad results
//$mnm->queue_job($catalog,'automatch') ;
$mnm->queue_job($catalog,'automatch_search') ;
$mnm->queue_job($catalog,'automatch_from_other_catalogs') ;
$job_id_update = $mnm->queue_job($catalog,'update_person_dates') ;
$mnm->queue_job($catalog,'match_person_dates',$job_id_update) ;

?>