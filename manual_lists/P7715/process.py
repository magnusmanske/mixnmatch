#!/usr/bin/python3
import sys
import datetime
import csv
import json

if len(sys.argv) < 2:
	sys.exit('Missing argument: CSV file')

# Read CSV file as dictionary
dictReader = csv.DictReader(open(sys.argv[1]), fieldnames = None, delimiter = '\t', quotechar = '"')
# , strict = True 

out = {"catalog":3700,"entries":[]}
for row in dictReader:
	if row['taxonomicStatus'] != 'Accepted':
		continue
	o = {
		"id":row['taxonID'],
		"name":row['scientificName'],
		"desc":row['taxonRank']+" | "+row['scientificNameAuthorship']+" | "+row['family']+" | "+row['genus']+" | "+row['verbatimTaxonRank']+" | "+row['nomenclaturalStatus']+" | "+row['taxonomicStatus'],
		"type":"Q16521",
		"url":"http://www.worldfloraonline.org/taxon/"+row['taxonID'],
	}
	out["entries"].append(o)

print ( json.dumps ( out , sort_keys=True, indent=4 ) )

# TODO
# FAMILY	Q35409
# ORDER	Q36602
# GENUS	Q34740
# SPECIES	Q7432
# SUBSPECIES	Q68947
