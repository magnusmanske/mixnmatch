<?PHP

require_once dirname(__DIR__) . '../../vendor/autoload.php';
require_once ( '/data/project/listeria/Chris-G-botclasses/botclasses.php' );
require_once ( "/data/project/quickstatements/public_html/quickstatements.php" ) ;

class largeCatalog {

	public $db ;
	public $dbwd ;
	public $wil ;
	public $mnm ;
	
	// Mapping of Wikidata properties to database fields
	public $prop2field = [
		1 => [
			'P268' => 'ext_id' ,
			'P214' => 'viaf'
		] ,
		2 => [
//			'P213' => 'ISNI' , // Deactivated as per discussion with Jura
			'P214' => 'ext_id' ,
			'P227' => 'DNB' ,
			'P244' => 'LC' ,
			'P268' => 'BNF' ,
			'P269' => 'SUDOC' ,
			'P245' => 'JPG' , // ULAN
			'P349' => 'NDL' ,
			'P906' => 'SELIBR' ,
			'P691' => 'NKC' ,
			'P951' => 'NSZL' ,
			'P1015' => 'BIBSYS' ,
			'P1017' => 'BAV' ,
			'P1207' => 'NUKAT' ,
			'P1273' => 'BNC' ,
			'P1309' => 'EGAXA' ,
			'P1368' => 'LNB' ,
			'P1006' => 'NTA' , # Was deactivated, but don't remember why
			'P2163' => 'FAST'
		] ,
		3 => [
			'P227' => 'ext_id' ,
			'P214' => 'viaf'
		] ,
		4 => [
			'P496' => 'ext_id'
		] ,
		5 => [
			'P1566' => 'ext_id'
		] ,
#		6 => [ '1006' => 'ext_id' ]
		8 => [
			'P648' => 'ext_id',
			'P214' => 'viaf',
			'P946' => 'bnf' ,
			'P946' => 'isin' ,
		] ,
	] ;

	protected $catalog ;
	protected $catalogs = [] ;
	protected $dbname = 'mixnmatch_large_catalogs_p' ;
	protected $initialized = false ;
	protected $bad_gnd = [] ;

//_____________________________________________________________________________________________________________________________

	public function __construct ( $cat , $mnm='' ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch\MixNMatch ;
		$this->catalog = $cat * 1 ;
		$this->wil = new WikidataItemList ;
		$this->loadCatalogs() ;
		$this->initialized = true ;
	}
	
	public function getCatalogID () {
		return $this->catalog ;
	}
	
	public function getSubjectItem () {
		$ret = $this->getCat()->subject ;
		if ( isset($ret) ) return "Q$ret" ;
	}

	public function getSQL ( $sql ) {
		if ( !isset($this->db) ) $this->openDB() ;
		return getSQL ( $this->db , $sql ) ;
	}
	
	public function getCat () {
		return $this->catalogs[$this->catalog] ;
	}
	
	public function getCatalogs () {
		return $this->catalogs ;
	}
	
	public function getFullDatabaseName() {
		return 's51434__'.$this->dbname ;
	}
	
	public function getMainProp () {
		return 'P' . $this->getCat()->property ;
	}
	
	public function updatePersonFromWikidata ( $prop , $field , $indexed = false , $table = '' , $pagesize = 0 , $offset = 0 ) {
		return $this->updateFromWikidata ( $prop , $field , 'Q5' , $indexed , $table , $pagesize , $offset ) ;
	}

	public function updateCatalogStats () {
		$table = $this->getCat()->table ;
		$id = $this->catalog ;
		$this->getSQL ( "UPDATE catalog SET entries=(select count(ext_id) from `{$table}`) WHERE id={$id}" ) ;
	}

	public function updateFromWikidataViaLDF ( $prop , $field = 'ext_id' ) { # $prop is Pxxx
		$table = $this->getCat()->table ;
		$page = 1 ;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 3);
		while ( 1 ) {
			$url = "https://query.wikidata.org/bigdata/ldf?predicate=http%3A%2F%2Fwww.wikidata.org%2Fprop%2Fdirect%2F{$prop}&page={$page}" ;
			curl_setopt($ch, CURLOPT_URL, $url); 
			$rows = explode ( "\n" , curl_exec($ch) ) ;
			$cnt = 0 ;
			foreach ( $rows AS $row ) {
				if ( !preg_match ( '/wd:Q(\d+)\s+wdt:P\d+\s+"(.+?)"/' , $row , $m ) ) continue ;
				$cnt++ ;
				$q = $m[1] * 1 ;
				$v = $m[2] * 1 ;
				$sql = "UPDATE `{$table}` SET q={$q} WHERE `{$field}`={$v} AND q IS NULL" ;
				$this->getSQL ( $sql ) ;
			}
			if ( $cnt == 0 ) break ;
			$page++ ;
		}
		curl_close($ch) ;
	}

	public function updateFromWikidata ( $prop , $field , $instance_of , $indexed = false , $table = '' , $pagesize = 0 , $offset = 0 ) {
		if ( $table == '' ) $table = $this->getCat()->table ;
		$mp = $this->getMainProp() ;
		
//print "Getting $field from DB\n" ;
		if ( !$indexed ) {
			$master = [] ;
			$sql = "SELECT id,`$field`,ext_id FROM `$table` WHERE `$field`!='' AND q IS NULL" ;
			$result = $this->mnm->tfc->getSQL ( $this->db , $sql ) ;
			while($o = $result->fetch_object()) $master[$o->$field] = [ $o->id , $o->ext_id ] ;
		}
//print "Got " . count($master) . " entries\n" ;

//print "Getting $prop from Wikidata\n" ;
		$sparql = "SELECT ?q ?extid ?main { " ;
		if ( $instance_of != '' ) $sparql .= "?q wdt:P31 wd:$instance_of . " ;
		$sparql .= "?q wdt:$prop ?extid . OPTIONAL { ?q wdt:$mp ?main } }" ;
		if ( $pagesize != 0 ) {
			$sparql .= " LIMIT $pagesize OFFSET $offset" ;
		}
#print "$sparql\n" ;
		$j = $this->mnm->tfc->getSPARQL ( $sparql ) ;
		if ( !isset($j) or $j == null ) {
			if ( $pagesize == 0 and $offset == 0 ) return $this->updateFromWikidata ( $prop , $field , $instance_of , $indexed , $table , 100000 , 0 ) ;
			die ( "SPARQL failed: $sparql\n" ) ;
		}
#print "Got " . count($j->results->bindings) . " items ($pagesize/$offset)\n" ;

		foreach ( $j->results->bindings AS $x ) {
			
			$v = [] ;
			if ( $prop == 'P1368' ) $x->extid->value = 'LNC10-' . $x->extid->value ; // VIAF and WD have different formats, fix hack
			if ( $indexed ) {
				$sql = "SELECT id,`$field`" ;
				if ( $table == '' ) $sql .= ",ext_id" ;
				$sql .= " FROM `$table` WHERE `$field`='" . $this->db->real_escape_string($x->extid->value) . "' AND q IS NULL" ;
				$result = $this->mnm->tfc->getSQL ( $this->db , $sql ) ;
				while($o = $result->fetch_object()) {
					if ( isset($o->ext_id) ) {
						$v = [ $o->id , $o->ext_id ] ;
					} else $v = [ $o->id ] ;
				}
			} else {
				if ( !isset($master[$x->extid->value]) ) continue ;
				$v = $master[$x->extid->value] ;
			}
			if ( count($v) == 0 ) continue ;
			if ( !preg_match ( '/Q(\d+)$/' , $x->q->value , $m ) ) continue ;
			$q = $m[1] ;
			
			if ( isset($v[1]) and isset($x->main) and isset($x->main->value) and $v[1] != $x->main->value ) {
				$this->addReport ( $v[0] , $q , $prop , "{{Q|$q}} says $mp is '{$x->main->value}' but mismatches with $prop on source catalog" , "EXT_MISMATCH" ) ;
				continue ;
			}
			$sql = "UPDATE `$table` SET q=$q WHERE id={$v[0]} AND q IS NULL" ;
			$this->mnm->tfc->getSQL ( $this->db , $sql ) ;
		}
		
		if ( $pagesize > 0 and $pagesize == count($j->results->bindings) ) {
			$this->updateFromWikidata ( $prop , $field , $instance_of , $indexed , $table , $pagesize , $offset+$pagesize ) ;
		}
	}

	
	// Gets the URL pattern from a Wikidata property (default: main property of current catalog)
	// and returns the URL for a given ID
	public function getExternalURL ( $extid , $prop = '' ) {
		if ( !$this->initialized ) return ;
		if ( $prop == '' ) $prop = 'P' . $this->getCat()->property ;
		$this->wil->loadItem ( $prop ) ;
		$i = $this->wil->getItem ( $prop ) ;
		if ( !isset($i) ) return ;
		$urls = $i->getStrings ( 'P1630' ) ;
		if ( count($urls) == 0 ) return ;
		$url = str_replace ( '$1' , urlencode($extid) , $urls[0] ) ;
		return $url ;
	}
	
	// Finds people items on Wikidata that have $p_ret property but no $p_nope property, using SPARQL
	// Returns array with [ (numerical) q => $p_ret ]
	public function getSPARQLoneButNotTheOtherProp ( $p_ret , $p_nope ) {
		$q2ret = [] ;
		$sparql = "SELECT ?q ?ret { ?q wdt:P31 wd:Q5 ; wdt:$p_ret ?ret MINUS { ?q wdt:$p_nope [] } }" ;
		$j = $this->mnm->tfc->getSPARQL ( $sparql ) ;
		foreach ( $j->results->bindings AS $x ) {
			if ( !preg_match ( '/Q(\d+)$/' , $x->q->value , $m ) ) continue ;
			$q = $m[1] ;
			$ret = $x->ret->value ;
			$q2ret[$q] = $ret ;
		}
		return $q2ret ;
	}

	public function hasPropertyEverBeenRemovedFromItem ( $q , $prop ) {
		$this->wil->sanitizeQ ( $q ) ;
		$this->wil->sanitizeQ ( $prop ) ;
		$sql = "SELECT count(*) AS cnt from page,revision_compat WHERE page_title='$q' AND page_namespace=0 AND page_id=rev_page AND (" ;
		$sql .= "rev_comment LIKE '%wbremoveclaims-remove%Property:{$prop}%' OR " ;
		$sql .= "(rev_comment LIKE '%wbcreateclaim-create%Property:{$prop}%' AND rev_user_text='Reinheitsgebot')" ; # Bot added this prop before, byt probably was reverted
		$sql .= ")" ;
		$this->openWikidataDB() ;
		$result = getSQL ( $this->dbwd , $sql ) ;
		while($o = $result->fetch_object()) {
			if ( $o->cnt >= 1 ) return true ;
		}
		return false ;
	}
	
	public function getTimestamp() {
		return date ( 'YmdHis' ) ;
	}
	
	public function getCurrentDate () {
		return preg_replace ( '/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/' , '+$1-$2-$3T00:00:00Z/11' , $this->getTimestamp() ) ;
	}

	public function writeReportToWikidata ( $subpage , $wikitext ) {
		$ini = parse_ini_file ( '/data/project/mix-n-match/bot.ini' ) ;
		$wiki_user = $ini['user'] ;
		$wiki_pass = $ini['pass'] ;
		$w = new wikipedia ;
		$w->quiet = true ;
		$w->url = "https://www.wikidata.org/w/api.php";
		$w->setUserAgent( 'User-Agent: '.$wiki_user.' (http://www.wikidata.org/wiki/User:' . str_replace(' ','_',$wiki_user) . ')'  );
		$w->login( $wiki_user , $wiki_pass );
		$page = "User:Magnus Manske/$subpage" ;
		$title = str_replace(' ','_',$page) ;

		$p = $w->getpage ( $title ) ;
		if ( trim($p) == trim($wikitext) ) { print "Skipping $page\n" ; return ; }

		$w->edit( $title, $wikitext, "Update ".$this->getTimestamp() );
	}
	
	public function wiki2html ( $wiki , $q ) {
		if ( trim($wiki) == '' ) return '' ;
		$q = 'Q' . preg_replace ( '/\D/' , '' , "$q" ) ;
		$url = "https://www.wikidata.org/w/api.php?action=flow-parsoid-utils&from=wikitext&to=html&content=" . urlencode($wiki) . "&title={$q}&format=json" ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		$fpu = 'flow-parsoid-utils' ;
		$html = $j->$fpu->content ;
		$html = preg_replace ( '/ href="\.\//' , ' target="_blank" href="https://www.wikidata.org/wiki/' , $html ) ;
		return $html ;
	}
	
	public function addReport ( $id_in_catalog , $q , $prop , $message , $type ) {
		$html = $this->wiki2html ( $message , $q ) ;
		$sql = "INSERT IGNORE INTO report (q,prop,catalog_id,id_in_catalog,message,`html`,`timestamp`,`type`,random) VALUES (" ;
		$sql .= preg_replace ( '/\D/' , '' , $q ) . "," ;
		$sql .= preg_replace ( '/\D/' , '' , $prop ) . "," ;
		$sql .= $this->catalog . "," ;
		$sql .= ($id_in_catalog*1) . "," ;
		$sql .= "'" . $this->db->real_escape_string ( $message ) . "'," ;
		$sql .= "'" . $this->db->real_escape_string ( $html ) . "'," ;
		$sql .= "'" . $this->getTimestamp() . "'," ;
		$sql .= "'" . $this->db->real_escape_string ( $type ) . "'," ;
		$sql .= "rand())" ;
		$this->mnm->tfc->getSQL ( $this->db , $sql ) ;
	}
	
	public function openWikidataDB () {
		if ( !isset($this->dbwd) ) $this->dbwd = $this->mnm->tfc->openDB ( 'wikidata' , 'wikidata' ) ; // or !$this->dbwd->ping() 
		return $this->dbwd ;
	}
	
	public function getQS () {
		$toolname = 'mix-n-match' ; // Or fill this in manually
		$qs = new QuickStatements() ;
		$qs->use_oauth = false ;
		$qs->bot_config_file = "/data/project/$toolname/bot.ini" ;
		$qs->toolname = "Mix'n'match:Large catalogs (" . $this->getCat()->name . ")" ;
		$qs->sleep = 1 ; // Sleep sec between edits
		return $qs ;
	}
	
	public function runQS ( $commands ) {
		$qs = $this->getQS() ;
		$tmp = $qs->importData ( $commands , 'v1' ) ;
		$qs->runCommandArray ( $tmp['data']['commands'] ) ;
	}
	
	// Checks for "undifferentiated" GNDs
	// Tries a DB-maintained list first, otherwise checks at the source, writes bad ones back to DB
	// Returns '' if undifferentiated, $id otherwise
	public function checkGND ( $id ) {
		if ( count($this->bad_gnd) == 0 ) {
			$sql = "SELECT gnd FROM bad_gnd" ;
			$result = $this->mnm->tfc->getSQL ( $this->db , $sql ) ;
			while($o = $result->fetch_object()) $this->bad_gnd[] = $o->gnd ;
//			print "Read " . count($this->bad_gnd) . " bad GNDs" ;
		}

		if ( in_array ( $id , $this->bad_gnd ) ) return '' ;

		// Query GND
		$url = "https://d-nb.info/gnd/$id/about/lds" ;
		$html = @file_get_contents ( $url ) ;
		if ( $html != null and !preg_match ( '/UndifferentiatedPerson/' , $html ) ) return $id ; // Seems legit
		$this->bad_gnd[] = $id ;
		$sql = "INSERT IGNORE INTO bad_gnd (gnd) VALUES ('$id')" ;
		$this->mnm->tfc->getSQL ( $this->db , $sql ) ;
		return '' ;
	}

	public function getItemsFromSearch ( $s , $lang = '' ) {
		$ret = [] ;
		if ( $s == '' ) return $ret ;
		if ( $lang == '' ) $lang = $this->getCat()->language ;
		$url = "https://www.wikidata.org/w/api.php?action=wbsearchentities&search=" . urlencode($s) . "&language=$lang&format=json&type=item&limit=50" ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		foreach ( $j->search AS $x ) {
			if ( !isset($x->label) ) continue ;
			if ( strtolower($x->label) != strtolower($s) ) continue ;
			$ret[] = $x->id ;
		}
		return $ret ;
	}


	public function openDB () {
		if ( !isset($this->db) ) $this->db = $this->mnm->tfc->openDBtool ( $this->dbname ) ;
		return $this->db ;
	}


//_____________________________________________________________________________________________________________________________

	protected function loadCatalogs () {
		$this->openDB() ;
		$sql = "SELECT * FROM catalog" ;
		$result = $this->mnm->tfc->getSQL ( $this->db , $sql ) ;
		while($o = $result->fetch_object()) $this->catalogs[$o->id] = $o ;
	}

} ;


?>