CREATE TABLE `open_library_authors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ext_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` mediumtext DEFAULT NULL,
  `personal_name` mediumtext DEFAULT NULL,
  `aliases` mediumtext DEFAULT NULL,
  `links` mediumtext DEFAULT NULL,
  `photos` mediumtext DEFAULT NULL,
  `birth_date` mediumtext DEFAULT NULL,
  `death_date` mediumtext DEFAULT NULL,
  `viaf` varchar(32) CHARACTER SET ascii COLLATE ascii_general_ci DEFAULT NULL,
  `bnf` varchar(32) CHARACTER SET ascii COLLATE ascii_general_ci DEFAULT NULL,
  `isni` varchar(32) CHARACTER SET ascii COLLATE ascii_general_ci DEFAULT NULL,
  `q` int(11) DEFAULT NULL,
  `wd_sync_version` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ext_id` (`ext_id`),
  KEY `q` (`q`,`wd_sync_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;