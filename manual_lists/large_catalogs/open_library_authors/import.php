#!/usr/bin/php
<?PHP

# Est. runtime: 16 min

require_once ( '../shared.php' ) ;

$table_keys = [ 'ext_id' , 'name' , 'personal_name' , 'aliases' , 'links' , 'photos' , 'birth_date', 'death_date' , 'viaf' , 'bnf' , 'isni' , 'q' ] ;

$lc = new largeCatalog ( 8 ) ;
$catalog_id = $lc->getCatalogID() ;

$table = 'open_library_authors';
$data_dir = "/data/project/mix-n-match/manual_lists/large_catalogs/{$table}" ;
$data_file = "{$data_dir}/ol_dump_authors_latest.txt.gz" ; # see download_current_dataset.sh

$lc->getSQL ( "DROP TABLE IF EXISTS `{$table}`" ) ;
$lc->getSQL ( file_get_contents ( './table.sql') ) ;

$batch_size = 1000 ;
$count = 0 ;
$sql = '' ;
$fh = gzopen ( $data_file , 'r' ) ;
while ( !gzeof($fh) ) {
	$parts = explode ( "\t" , gzgets ( $fh ) ) ;
	$j = json_decode($parts[4]);

	if ( isset($j->links) ) {
		if ( !isset($j->remote_ids) ) $j->remote_ids = (object) [];
		foreach ( $j->links AS $l ) {
			if ( preg_match('|^VIAF ID: (.+)$|',$l->title,$m) ) $j->remote_ids->viaf = $m[1];
			if ( preg_match('|^BnF ID: (.+)$|',$l->title,$m) ) $j->remote_ids->bnf = $m[1];
			if ( preg_match('|^ISNI ID: (.+)$|',$l->title,$m) ) $j->remote_ids->isni = str_replace(' ', '', $m[1]);
		}
	}

	$row = [
		preg_replace('|^/authors/|','',$parts[1]), // ext_id
		$j->name, // name
		$j->personal_name, // personal_name
		json_encode($j->aliases), // aliases
		json_encode($j->links), // links
		json_encode($j->photos), // photos
		$j->birth_date, // birth_date
		$j->death_date, // death_date
		$j->remote_ids->viaf, // viaf
		$j->remote_ids->bnf, // bnf
		$j->remote_ids->isni, // isni
	];


	foreach ( $row AS $k => $v ) {
		if ( !isset($v) ) {
			$row[$k] = 'null';
		} else if ( $v!='null' ) {
			if ( strlen($v)>32 ) { # Avoid too-long fields
				if ( $table_keys[$k]=='viaf' or $table_keys[$k]=='bnf' or $table_keys[$k]=='isni' ) {
					$v = '';
				}
			}
			$row[$k] = '"'.$lc->db->real_escape_string("{$v}").'"' ;
		}
	}
	if ( isset($j->remote_ids->wikidata)) {
		$q = preg_replace('|\D|','',$j->remote_ids->wikidata);
		if ($q=='') array_push($row, 'null');
		else array_push($row, $q);
	} else {
		array_push($row, 'null');
	}

	if ( $sql != '' ) $sql .= ',' ;
	else $sql = "INSERT INTO {$table} (`" . implode ( '`,`' , $table_keys ) . "`) VALUES " ;
	$sql .= "(" . implode ( "," , $row ) . ")" ;
	$count++ ;
	if ( $count >= $batch_size or gzeof($fh) ) {
		try {
		$lc->getSQL ( $sql ) ;
		} catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
			print "{$sql}\n\n\n";
		}
		$sql = '' ;
		$count = 0 ;
	}
}
gzclose($fh) ;

exec ( './update_from_wikidata.php' ) ;

?>