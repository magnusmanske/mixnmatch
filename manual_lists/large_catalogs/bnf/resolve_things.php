#!/usr/bin/php
<?PHP

require_once ( '../shared.php' ) ;

$lc = new largeCatalog ( 1 ) ;

$lang = $lc->getCat()->language ;

// Place
$sql = "SELECT * FROM thing WHERE type_q='486972' AND q IS NULL AND catalog_id=" . $lc->getCatalogID() ;
#$sql .= " AND id=8" ; # TESTING
if(!$result = $lc->db->query($sql)) die('There was an error running the query [' . $lc->db->error . ']'."\n$sql\n");
while($o = $result->fetch_object()) {
	if ( !preg_match ( '/^(.+) \((.+)\)$/' , $o->name , $m ) ) {
		if ( !preg_match ( '/^(.+), (.+?)$/' , $o->name , $m ) ) {
			$m = [ '' , $o->name , '' ] ;
		}
	}
	$name = trim($m[1]) ;
	$parent = trim($m[2]) ;
	$i_n = $lc->getItemsFromSearch ( $name ) ;
	if ( count($i_n) == 0 ) continue ;
	$i_p = $lc->getItemsFromSearch ( $parent ) ;
#	if ( count($i_p) == 0 ) continue ;
	$sparql = "SELECT ?q { VALUES ?q { wd:" . implode ( ' wd:' , $i_n ) . " } ." ;
	if ( count($i_p) > 0 ) $sparql .= "VALUES ?parent { wd:" . implode ( ' wd:' , $i_p ) . " } ." ;
	$sparql .= "?q wdt:P31/wdt:P279* wd:Q{$o->type_q} ." ;
	if ( count($i_p) > 0 ) $sparql .= "?q wdt:P131* ?parent ." ;
	$sparql .= "SERVICE wikibase:label { bd:serviceParam wikibase:language '[AUTO_LANGUAGE],$lang'. } }" ;
	$results = getSPARQLitems ( $sparql ) ;
	if ( count($results) != 1 ) continue ;
	$q = $results[0] ;
	$sql = "UPDATE thing SET q={$results[0]} WHERE id={$o->id} AND q IS NULL" ;
	if(!$result2 = $lc->db->query($sql)) die('There was an error running the query [' . $lc->db->error . ']'."\n$sql\n");
	print "$name ($parent) => $q\n" ;
//	print "$q\n" ;	exit(0);
}

?>