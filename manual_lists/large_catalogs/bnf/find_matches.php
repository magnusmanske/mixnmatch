#!/usr/bin/php
<?PHP

require_once ( '../shared.php' ) ;

$qs_file = getcwd() . '/results.find_matches2.qs' ;
$fh = fopen ( $qs_file , 'w' ) ;
$lc = new largeCatalog ( 1 ) ;
$p_main = $lc->getMainProp() ;
$p_viaf = 'P214' ;
$table = $lc->getCat()->table ;
$report = [ ":''This page is periodically updated/replaced by a bot''" , "----" ] ;

// People with BNF but not VIAF on Wikidata
if ( 1 ) {
	$q2ext = $lc->getSPARQLoneButNotTheOtherProp ( $p_main , $p_viaf ) ;
	if ( count($q2ext) > 0 ) {
		$sql = "SELECT * FROM $table WHERE ext_id IN ('" . implode("','",array_values($q2ext)) . "') AND q IS NOT NULL AND viaf!=''" ;
		if(!$result = $lc->db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
		while($o = $result->fetch_object()){
			if ( !isset($q2ext[$o->q]) ) {
				$rep = "# BNF says {{Q|{$o->q}}} should have BNF [" . $lc->getExternalURL($o->ext_id) . " {$o->ext_id}], but currently " ;
				foreach ( $q2ext AS $q2 => $ext2 ) {
					if ( $o->ext_id != $ext2 ) continue ;
					$rep .= "{{Q|$q2}} has it" ;
					break ;
				}
				$report[] = $rep ;
				continue ;
			}
			if ( $q2ext[$o->q] != $o->ext_id ) {
				$report[] = "#MISMATCH BETWEEN {$o->id}:{$o->ext_id}={$o->q} AND {$q2ext[$o->q]}" ;
				$lc->addReport ( $o->id , $o->q , $p_main , "Mismatch between {$o->id}:{$o->ext_id}={$o->q} AND {$q2ext[$o->q]}" , 'MISMATCH' ) ;
				continue ;
			}
			if ( $lc->hasPropertyEverBeenRemovedFromItem ( $o->q , $p_viaf ) ) {
				$report[] = "# VIAF [" . $lc->getExternalURL($o->viaf,$p_viaf) . " {$o->viaf}] was not added to {{Q|{$o->q}}}, as a VIAF has been removed before from that item" ;
				$lc->addReport ( $o->id , $o->q , $p_viaf , "VIAF [" . $lc->getExternalURL($o->viaf,$p_viaf) . " {$o->viaf}] was not added to {{Q|{$o->q}}}, as a VIAF has been removed before from that item" , 'REMOVED_BEFORE' ) ;
				continue ;
			}
			fwrite ( $fh , "Q{$o->q}\t$p_viaf\t\"{$o->viaf}\"\n" ) ;
		}
	}
}

// People with VIAF but not BNF on Wikidata
if ( 1 ) {
	$q2viaf = $lc->getSPARQLoneButNotTheOtherProp ( $p_viaf , $p_main ) ;
	if ( count($q2viaf) > 0 ) {
		$sql = "SELECT * FROM $table WHERE viaf IN ('" . implode("','",array_values($q2viaf)) . "') AND q IS NOT NULL" ;
		if(!$result = $lc->db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
		while($o = $result->fetch_object()){
			if ( !isset($q2viaf[$o->q]) ) {
				$rep = "# BNF says {{Q|{$o->q}}} should have VIAF [" . $lc->getExternalURL($o->viaf,$p_viaf) . " {$o->viaf}], but currently " ;
				foreach ( $q2viaf AS $q2 => $viaf2 ) {
					if ( $o->viaf != $viaf2 ) continue ;
					$rep .= "{{Q|$q2}} has it" ;
					break ;
				}
				$report[] = $rep ;
				continue ;
			}
			if ( $q2viaf[$o->q] != $o->viaf ) {
				$report[] = "#MISMATCH BETWEEN {$o->id}:{$o->ext_id}={$o->q} AND {$q2viaf[$o->q]}" ;
				$lc->addReport ( $o->id , $o->q , $p_viaf , "Mismatch between {$o->id}:{$o->ext_id}={$o->q} AND {$q2viaf[$o->q]}" , 'MISMATCH' ) ;
				continue ;
			}
			if ( $lc->hasPropertyEverBeenRemovedFromItem ( $o->q , $p_main ) ) {
				$report[] = "# BNF [" . $lc->getExternalURL($o->ext_id) . " {$o->ext_id}] was not added to {{Q|{$o->q}}}, as a BNF has been removed before from that item" ;
				$lc->addReport ( $o->id , $o->q , $p_main , "BNF [" . $lc->getExternalURL($o->ext_id) . " {$o->ext_id}] was not added to {{Q|{$o->q}}}, as a BNF has been removed before from that item" , 'REMOVED_BEFORE' ) ;
				continue ;
			}
			fwrite ( $fh , "Q{$o->q}\t$p_main\t\"{$o->ext_id}\"\n" ) ;
		}
	}
}

// All people with name/day of birth/day of death matches that DO NOT HAVE BNF on Wikidata
// Very slow and stressful for SPARQL
if ( 0 ) {
	$sql = "SELECT * FROM $table WHERE q is null AND length(born)=10 AND length(died)=10" ;
	if(!$result = $lc->db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
	while($o = $result->fetch_object()){
		if ( !preg_match ( '/^(\d{4})-(\d{2})-(\d{2})$/' , $o->born , $born ) ) continue ;
		if ( !preg_match ( '/^(\d{4})-(\d{2})-(\d{2})$/' , $o->died , $died ) ) continue ;
	
		// Name variants
		$names = [ $o->name ] ;
		if ( preg_match ( '/^(\S+)\s+.+?\s+(\S+)$/' , $o->name , $m ) ) $names[] = "{$m[1]} {$m[2]}" ; // Just first and last name
		if ( preg_match ( '/^(.+) de (.+)$/' , $o->name , $m ) ) $names[] = $m[1].' '.$m[2] ;
		if ( preg_match ( '/^(.+) de la (.+)$/' , $o->name , $m ) ) $names[] = $m[1].' '.$m[2] ;
		if ( preg_match ( '/^(.+) of (.+)$/' , $o->name , $m ) ) $names[] = $m[1].' '.$m[2] ;
		if ( preg_match ( '/^(.+) von (.+)$/' , $o->name , $m ) ) $names[] = $m[1].' '.$m[2] ;
		foreach ( $names AS $a => $b ) $names[$a] = str_replace ( '"' , '' , $b ) ; // Quote paranoia
		foreach ( $names AS $a => $b ) { // Dash
			if ( preg_match ( '/^(.+)-(.+)$/' , $b , $m ) ) $names[] = $m[1] . ' ' . $m[2] ;
		}

		$namelist = '"' . strtolower ( implode ( '","' , $names ) ) . '"' ;
		$sparql = "SELECT DISTINCT ?q WHERE {" ;
		$sparql .= " ?q wdt:P569 ?time0 . FILTER ( ?time0 = '{$born[1]}-{$born[2]}-{$born[3]}T00:00:00Z'^^xsd:dateTime )" ;
		$sparql .= " ?q wdt:P570 ?time1 . FILTER ( ?time1 = '{$died[1]}-{$died[2]}-{$died[3]}T00:00:00Z'^^xsd:dateTime )" ;
		$sparql .= ' OPTIONAL { ?q rdfs:label ?label } OPTIONAL { ?q rdfs:alias ?alias } ' ;
		$sparql .= "FILTER ( LCASE(str(?label)) IN ($namelist) || LCASE(str(?alias)) IN ($namelist) )" ;
		$sparql .= '}' ;
	//print "\n$sparql\n\n" ;

		$items = getSPARQLitems ( $sparql ) ;
		if ( count($items) != 1 ) continue ;

		$q = 'Q'.$items[0] ;
		$lc->wil->loadItem ( $q ) ;
		$i = $lc->wil->getItem ( $q ) ;
		if ( !isset($i) ) continue ; // Item does not load
		if ( !$i->hasClaims($p_main) and !$lc->hasPropertyEverBeenRemovedFromItem ( $q , $p_main ) ) fwrite ( $fh , "$q\t$p_main\t\"{$o->ext_id}\"\n" ) ; // BNF
		if ( !$i->hasClaims($p_viaf) and $o->viaf!='' and !$lc->hasPropertyEverBeenRemovedFromItem ( $q , $p_viaf ) ) fwrite ( $fh , "$q\t$p_viaf\t\"{$o->viaf}\"\n" ) ; // VIAF
	}
}

fclose ( $fh ) ;

$lc->writeReportToWikidata ( 'BNF report' , implode("\n",$report) ) ;

?>