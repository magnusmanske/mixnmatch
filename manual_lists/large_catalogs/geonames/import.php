#!/usr/bin/php
<?PHP

require_once ( '../shared.php' ) ;

$table_keys = [ 'ext_id' , 'name' , 'asciiname' , 'alternatenames' , 'latitude' , 'longitude' , 'feature_class' , 'feature_code' , 'country_code' , 'cc2' , 'admin1_code' , 'admin2_code' , 'admin3_code' , 'admin4_code' , 'population' , 'elevation' , 'dem' , 'timezone' , 'modification_date' ] ;

$lc = new largeCatalog ( 5 ) ;
$catalog_id = $lc->getCatalogID() ;

$data_dir = '/data/scratch/large_catalogs/geonames' ;
$data_file = "$data_dir/allCountries.txt" ;

$lc->getSQL ( "DROP TABLE IF EXISTS `geonames`" ) ;
$lc->getSQL ( file_get_contents ( './table.sql') ) ;

$batch_size = 10000 ;
$count = 0 ;
$sql = '' ;
$fh = fopen ( $data_file , 'r' ) ;
while ( !feof($fh) ) {
	$row = explode ( "\t" , fgets ( $fh ) ) ;
	foreach ( $row AS $k => $v ) $row[$k] = $lc->db->real_escape_string ( $v ) ;
	if ( $sql != '' ) $sql .= ',' ;
	else $sql = "INSERT INTO geonames (`" . implode ( '`,`' , $table_keys ) . "`) VALUES " ;
	$sql .= "('" . implode ( "','" , $row ) . "')" ;
	$count++ ;
	if ( $count >= $batch_size or feof($fh) ) {
		$lc->getSQL ( $sql ) ;
		$sql = '' ;
		$count = 0 ;
	}
}
fclose($fh) ;

exec ( './update_from_wikidata.php' ) ;

?>