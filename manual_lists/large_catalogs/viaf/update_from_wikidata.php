#!/usr/bin/php
<?PHP

require_once ( '../shared.php' ) ;

$lc = new largeCatalog ( 2 ) ;

// VIAF has links to Wikidata, so ... self-update!
if ( 0 ) {
	$sql = "UPDATE viaf SET q=substring(WKP,2)*1 WHERE WKP != '' AND q is null" ;
	if(!$result = $lc->db->query($sql)) die('There was an error running the query [' . $lc->db->error . ']'."\n$sql\n");
}

foreach ( $lc->prop2field[$lc->getCatalogID()] AS $prop => $field ) {
if ( $prop != 'P1368' ) continue ; # HACK FIXME
	$lc->updatePersonFromWikidata ( $prop , $field ) ;
}


?>