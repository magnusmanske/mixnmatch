#!/usr/bin/php
<?PHP

require_once ( '../shared.php' ) ;

$wd_sync_version = 2 ; // This script's version, integer
$lc = new largeCatalog ( 2 ) ;

function markAsChecked ( $id ) {
	global $lc , $wd_sync_version , $table ;
	$sql = "UPDATE $table SET wd_sync_version=$wd_sync_version WHERE id={$id}" ;
	if(!$result = $lc->db->query($sql)) die('There was an error running the query [' . $lc->db->error . ']'."\n$sql\n");
}

# rows were created on fresh table, so IDs are roughly 1..max
$table = $lc->getCat()->table ;
$sql = "SELECT max(id) AS m FROM `$table`" ;
if(!$result = $lc->db->query($sql)) die('There was an error running the query [' . $lc->db->error . ']'."\n$sql\n");
$o = $result->fetch_object() ;
$max = $o->m ;

$main_prop = $lc->getMainProp() ;
$main_prop_source = preg_replace ( '/^P/' , 'S' , $main_prop ) ;

$source = "\tS813\t" . $lc->getCurrentDate() ;
if ( $lc->getCat()->subject*1 > 0 ) $source .= "\tS248\t" . $lc->getSubjectItem() ;

$cnt = 0 ; // Set to >0 for max items processed, <=0 for "forever"
while ( 1 ) {
	$r = rand ( 1 , $max ) ;
	$sql = "SELECT * FROM `$table` WHERE id>=$r AND q IS NOT NULL AND wd_sync_version<$wd_sync_version ORDER BY id LIMIT 1" ; // Random
//	$sql = "SELECT * FROM `$table` WHERE q IS NOT NULL AND wd_sync_version<$wd_sync_version ORDER BY id LIMIT 1" ; // Systematic
//	$sql = "SELECT * FROM `$table` WHERE q IS NOT NULL AND wd_sync_version<$wd_sync_version AND JPG!='' ORDER BY id LIMIT 1" ; // Specific property TEST FIXME
	$lc->openDB() ;
	if(!$result = $lc->db->query($sql)) die('There was an error running the query [' . $lc->db->error . ']'."\n$sql\n");
	$o = $result->fetch_object() ;
	if ( $o == null ) continue ; // ID does not seem to exist, odd, try again
//	if ( $o->q == null ) continue ; // Only where we know the Wikidata item SUPERSEDED BY SQL
//	if ( $o->wd_sync_version >= $wd_sync_version ) continue ; // Already did this one SUPERSEDED BY SQL
	
	$q = "Q{$o->q}" ;
	$check = [] ;
	foreach ( $lc->prop2field[$lc->getCatalogID()] AS $prop => $field ) {
		if ( $o->$field == '' ) continue ;
		if ( $prop == 'P213' ) $check[$prop] = preg_replace ( '/^(.{4})(.{4})(.{4})(.{4})$/' , '$1 $2 $3 $4' , $o->$field ) ;
		else if ( $prop == 'P2163' ) $check[$prop] = preg_replace ( '/\D/' , '' , $o->$field ) ;
		else if ( $prop == 'P1309' ) $check[$prop] = preg_replace ( '/^[a-z]+/' , '' , $o->$field ) ;
		else if ( $prop == 'P1368' ) $check[$prop] = preg_replace ( '/^LNC10-/' , '' , $o->$field ) ;
		else if ( $prop == 'P227' ) { // Check for undifferentiated GNDs
			$f = $lc->checkGND ( $o->$field ) ;
			if ( $f == '' ) continue ;
			$check[$prop] = preg_replace ( '/\s/' , '' , $o->$field ) ;
		} else $check[$prop] = preg_replace ( '/\s/' , '' , $o->$field ) ;
		
		// Check value against regexp
		$lc->wil->loadItem ( $prop ) ;
		$pi = $lc->wil->getItem ( $prop ) ;
		if ( isset($pi) and $pi->hasClaims('P1792') ) {
			$has_match = false ;
			foreach ( $pi->getString('P1792') AS $rx ) {
				if ( preg_match ( '/^'.$rx.'$/' , $check[$prop] ) ) $has_match = true ;
			}
			if ( !$has_match ) {
				$lc->addReport ( $o->id , $q , $prop , "Regular expression failure for '{$check[$prop]}' on {{P|" . preg_replace('/\D/','',$prop) . "}}" , 'REGEX' ) ;
				unset ( $check[$prop] ) ;
				continue ;
			}
		}
	}
	if ( count($check) == 0 ) { // Paranoia
		markAsChecked ( $o->id ) ;
		continue ;
	}

	if ( count($check) == 1 ) { // Skipping this for now - has to be matched through something...
		markAsChecked ( $o->id ) ;
		continue ;
	}

	$lc->wil->loadItem ( $q ) ;
	$i = $lc->wil->getItem ( $q ) ;
	if ( !isset($i) ) { // Paranoia
		markAsChecked ( $o->id ) ;
		continue ;
	}
	
	if ( !$i->hasTarget ( 'P31' , 'Q5' ) ) { // Humans only for now
		markAsChecked ( $o->id ) ;
		continue ;
	}

	if ( count ( $i->getStrings($main_prop) ) > 1 ) { // No items with multiple VIAF
		markAsChecked ( $o->id ) ;
		continue ;
	}
	
	$has_mismatches = false ;
	$commands = [] ;
	foreach ( $check AS $prop => $value ) {
		$values_in_wd = $i->getStrings ( $prop ) ;
		if ( count($values_in_wd) == 0 ) {
			if ( $lc->hasPropertyEverBeenRemovedFromItem ( $q , $prop ) ) continue ; // Someone removed this property from this item one before

			// Check if this is set anywhere else
			$sparql = "SELECT ?q { ?q wdt:$prop \"$value\" }" ;
			$hasit = getSPARQLitems ( $sparql ) ;

			if ( count($hasit) > 0 ) { // That's already on Wikidata!
				if ( count($hasit) > 1 or 'Q'.$hasit[0] != $q ) { // ... in ANOTHER item!
					$lc->addReport ( $o->id , $q , $prop , "VIAF says {{Q|{$o->q}}} has {{P|" . preg_replace('/\D/','',$prop) . "}} is '$s', but {{Q|{$hasit[0]}}} already has it" , 'OTHER_I' ) ;
					$has_mismatches = true ;
				}
				continue ;
			}

			// Generate command
			$command = "$q\t$prop\t\"$value\"" ;
			if ( $prop != $main_prop ) $command .= "\t$main_prop_source\t\"{$o->ext_id}\"" ;
			$command .= $source ;
			$commands[] = $command ;
		} else {
			foreach ( $values_in_wd AS $s ) {
				if ( trim($s) == trim($value) ) continue ;
				$lc->addReport ( $o->id , $q , $prop , "{{Q|{$o->q}}} says {{P|" . preg_replace('/\D/','',$prop) . "}} is '$s', but {$lc->getCat()->name} says '$value'" , 'MISMATCH' ) ;
				$has_mismatches = true ;
			}
		}
	}
	
	if ( count($commands) == 0 ) { // Nothing to do
		markAsChecked ( $o->id ) ;
		continue ;
	}

	if ( $has_mismatches ) { // TODO check in DB, maybe all resolved?
		markAsChecked ( $o->id ) ;
		continue ;
	}

	$lc->runQS ( implode("\n",$commands) ) ;
	
	markAsChecked ( $o->id ) ;

	sleep ( 5 ) ;
	
	$cnt-- ;
	if ( $cnt == 0 ) exit();
}

?>