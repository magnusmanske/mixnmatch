#!/bin/bash

csv_file=/shared/external-data/OpenUPRN/osopenuprn_202006.csv
chunksize=100000


mkfifo osopenuprn.csv

for (( i = 2; i <= 40000000; i=$i+$chunksize )) 
  do 
     mysqlimport --defaults-file=~/replica.my.cnf --local -h tools.db.svc.eqiad.wmflabs --fields-terminated-by=, s51434__mixnmatch_large_catalogs_p osopenuprn.csv &
     tail -n +$i $csv_file | head -n $chunksize > osopenuprn.csv
     sleep 5
 done

\rm osopenuprn.csv
